/**
 * @file main.cpp
 * @author David Head
 * @brief Calling code for Bead-Spring simulator.
 */


/**
 * @mainpage Bead-Spring simulation package.
 * 
 * This software simulates collections of beads connected by (extensional and angular) springs,
 * as a standard if simple model for filaments. Crosslinks can be made and broken between beads.
 * To see options, call with `-h`.
 */


//
// Includes.
//

// Standard includes
#include <iostream>
#include <string>
#include <map>
#include <omp.h>
#include <getopt.h>							// For parsing the command line arguments.
#include <time.h>							// For seeding the pRNG (if not specified by either ...
											// ... the command line arg or the parameters file).
// Local includes.
#include "sequentialFilenames.h"

// Main simulation class
#include "BeadSpring.h"


//
// Enumerated types.
//

/// Return error codes.
enum class errorCode
{
	noError,
	exceptionThrown,
	couldNotParseArguments,
	noStateToExtend,
	badNewParameter,
};


//
// Main.
//

// Main: Parses command line arguments
int main( int argc, char* argv[] )
{

	// Global try-except loop
	try
	{
		// Variables declared in command line arguments
		bool quiet                  = false;
		bool zeroTime               = false;
		long pRNG_seed				= (unsigned long) time(NULL);
		unsigned int numThreads     = std::thread::hardware_concurrency();
		double extendTime           = 0.0;
		std::string output_prefix	= "";
		std::string params_fname	= "parameters.xml";
		std::string log_fname		= "log.out";
		std::string state_fname     = "";
		
		// For changing/adding parameters to the parameters or state file.
		std::map<std::string,std::string> newParams;

		//
		// Parse command-line arguments using getopt_long
		//
		int c;
		while( true )
		{
			static struct option long_options[] =
			{
				{ "parameters", required_argument,	0, 'p' },
				{ "log"       , required_argument,	0, 'l' },
				{ "output"    , required_argument,	0, 'o' },
				{ "help"      , no_argument      ,  0, 'h' },
				{ "seed"      ,	required_argument,	0, 'r' },
				{ "threads"   , required_argument,  0, 't' },
				{ "state"     , required_argument,  0, 's' },
				{ "time"      , required_argument,  0, 'T' },
				{ "param"     , required_argument,  0, 'P' },
				{ "quiet"     , no_argument      ,  0, 'q' },
				{ "zero_time" , no_argument      ,  0, 'z' }
			};
			
			int option_index = 0;			// Used by getopt_long (persistent argument index).
			
			c = getopt_long( argc, argv, "p:l:o:hb:r:t:s:T:P:qz", long_options, &option_index );
				// The string gives the short arguments; colon signifies a required argument.

			if( c==-1 ) break;		// Reached end of list
			
			// Check each 
			switch( c )
			{
				case 'p': params_fname  = optarg;       break;
				case 'l': log_fname     = optarg;       break;
				case 'o': output_prefix = optarg;       break;
				case 'r': pRNG_seed     = atol(optarg); break;
				case 't': numThreads    = atoi(optarg); break;
				case 's': state_fname   = optarg;       break;
				case 'T': extendTime    = atof(optarg); break;
				case 'q': quiet         = true;         break;
				case 'z': zeroTime      = true;         break;
				
				case 'P':
				{
					std::string opt = std::string(optarg);

					if( opt.find("=")==std::string::npos )
					{
						std::cerr << "Error: Format for each new/modified parameter is: -P <label>=<value>" << std::endl;
						return - (int)errorCode::badNewParameter;
					}

					newParams[opt.substr(0,opt.find("="))] = opt.substr(opt.rfind("=")+1);
					break;
				}

				case 'h':
				{
					std::cout << "Bead-Spring simulator: Call with arguments:" << std::endl << std::endl
							  << " -p/--parameters <params_filename>     Pathname for parameters file (relative to executable)" << std::endl
							  << " -o/--output <dirname='.'>             Output directory (prefix to all outputs)" << std::endl
							  << " -l/--log <logfile>                    Name of log file" << std::endl
							  << " -r/--seed <long>                      Sets the pRNG seed; defaults to epoch time" << std::endl
							  << " -t/--threads <int>                    Number of threads (defaults to hardware concurrency)" << std::endl
							  << " -s/--state <filename>                 Start from state file (on output); normally used with -T" << std::endl
							  << " -T/--time <value>                     Extends by time T from specified state or one with highest index" << std::endl
							  << " -P/--param <name>=<value>             Assigns value to parameter(s), overwriting if necessary" << std::endl
							  << " -q/--quiet                            Sets quiet mode (i.e. no output to stdout)" << std::endl
							  << " -z/--zero_time                        Set time to zero on restart and recalculate output frequency" << std::endl
							  << std::endl;

					return 0;
				}
				
				case '?':
				default : 
				{
					std::cerr << "Could not parse arguments; bailing." << std::endl;
					return - (int)errorCode::couldNotParseArguments;
				}
			}
		}

		// Set the global number of threads for the next parallel region.
		omp_set_num_threads( numThreads>0 ? numThreads : std::thread::hardware_concurrency() );
		
		// If extending, either use the supplied state file or try to extract the most recent one output.
		if( extendTime && state_fname.empty() )
		{
			int lastIndex = sequentialFilenames::largestFileIndex(4,output_prefix,"state",".xml");

			if( lastIndex >= 0 )
			{
				state_fname = sequentialFilenames::indexedFilename(lastIndex,4,"state",".xml");
			}
			else
			{
				std::cerr << "If extending, must either specify a state file or have index save in output directory." << std::endl;
				return - (int)errorCode::noStateToExtend;
			}
		}

		// Initialise the object.
		BeadSpring sim( output_prefix, params_fname, log_fname, pRNG_seed, state_fname, extendTime, newParams, quiet, zeroTime );
		sim.startRun();
	}
	catch( std::exception &error )
	{
		std::cerr << "BeadSpring FAILED due to uncaught exception:\n" << error.what() << std::endl;
		return - (int)errorCode::exceptionThrown;
	}	

	return - (int)errorCode::noError;
}
