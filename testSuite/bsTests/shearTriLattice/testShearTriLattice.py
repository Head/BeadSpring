#
# Tests that a 2D triangular lattice (with no dilution) behaves as expected when subjected to an applied shear strain.
#


# Standard imports
import math
import scipy.optimize
import matplotlib.pyplot as plt
import numpy as np


# Base class
from bsTests import bsTestBase

class testShearTriLattice( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__paramsFile = fname

		# Call parent method with required flags.
		super(testShearTriLattice,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):
	
		#
		# Sanity checks. The "trilattice" IC also requires dim=2, so no need to check that.
		#
		if self._table.getParameter("initialConfig") != "trilattice":
			raise NotImplementedError( "Initial condition must be 'trilattice'" )

		if self._table.getParameter("M") != 1:
			raise NotImplementedError( "Sheared triangular lattice test assumes a monomeric system (i.e. M=1)" )

		if self._table.getParameter("kT") != 0:
			raise NotImplementedError( "Sheared triangular lattice test assumes zero temperature (i.e. kT=0)" )

		#
		# Parameters.
		#
		N = self._table.getParameter("N")
		
		#
		# Tests for all shear types.
		#
		degree, hist = [ self._blockData.getDataSets("nodeDegree")["curves"][-1][axis] for axis in ("x","y") ]

		if self._plot:
			if self._verbose:
				print( "Showing histogram of node degrees; all {} nodes should have z=6".format(N) )

			plt.bar( degree, hist )
			plt.xlabel( "Node degree z" )
			plt.ylabel( "Frequency" )
			plt.show()
				
		for i, z in enumerate(hist):
			if ( i!=6 and z>0.0 ) or ( i==6 and z!=N ):
				raise bsTestBase.FailedTestError( "Incorrect coordination number / node degree" )
				
		if self._verbose:
			print( "Passed; all nodes have degree 6." )
		
		
		#
		# Tests for step shear only.
		# 
		if self._table.getParameter("boxType") == "LEStepShear":
		
			#
			# Step shear parameters.
			#
			gamma = self._table.getParameter("shearStrain")
			a     = self._table.getParameter("l0"         )
			k     = self._table.getParameter("k"          )
			
			#
			# Step shear test 1: Mean link extension; non-zero (positive) to second order in strain.
			#
			t, dl = [ self._table.getDataSets("meanLinkExt")["curves"][0][label] for label in ("x","y") ]
			
			# Non-linear prediction.
			root3over2 = 0.8660254037844386
			dl_NE = a * np.linalg.norm( np.array([0.5+gamma*root3over2,root3over2]) ) - a
			dl_NW = a * np.linalg.norm( np.array([0.5-gamma*root3over2,root3over2]) ) - a
			pred  = ( dl_NE + dl_NW ) / 3.0				# Horizontal links do not extend.

			# Display if requested.
			if self._plot:
				if self._verbose:
					print( "Showing mean link extension versus the (non-linear) prediction." )

				plt.plot( t, dl, "ro-", label="measured" )
				plt.plot( t, [pred]*len(t), label="prediction", ls="--", lw=2, color="k" )
				plt.xlabel( "time")
				plt.ylabel( "Mean extension" )
				plt.legend()
				plt.show()
			
			# Test the final values. Should be smooth since kT=0, so just take the final (i.e. most converged) value.
			relError = abs( dl[-1] - pred ) / pred
			if self._verbose:
				print( "Relative error for final measured extension was {}.".format(relError) )

			if relError > testShearTriLattice.meanExtRelTol():
				raise bsTestBase.FailedTestError( "Final error in mean extension exceeded tolerance (could be convergence issue?)." )
			else:
				if self._verbose:
					print( "Passed test (within prescribed tolerance of {}).".format(testShearTriLattice.meanExtRelTol()) )
		
			#
			# Step shear test 2: Similar for the shear modulus.
			#
			t, sigma = [ self._table.getDataSets("sigma_xy")["curves"][0][label] for label in ("x","y") ]
			
			# Linear prediction.
			pred = gamma * 3**0.5 * k / 4.0
			
			# Display if requested.
			if self._plot:
				if self._verbose:
					print( "Showing shear stress alongside the (linear) predicted stress." )
				
				plt.plot( t, sigma, "ro-", label="measured" )
				plt.plot( t, [pred]*len(t), label="prediction", ls="--", lw=2, color="k" )
				plt.xlabel( "time")
				plt.ylabel( "Shear stress" )
				plt.legend()
				plt.show()
			
			# Test the final value. Again, should be smooth since athermal, so just take the final value.
			relError = abs( sigma[-1] - pred ) / pred
			if self._verbose:
				print( "Relative error for the final measured shear stress was {}.".format(relError) )
			
			if relError > testShearTriLattice.shearRelTol():
				raise bsTestBase.FailedTestError( "Final error in shear stress exceeded tolerance (could be convergence issue?)." )
			else:
				if self._verbose:
					print( "Passed test (within prescribed tolerance of {}).".format(testShearTriLattice.shearRelTol()) )
				
		#
		# Tests for oscillatory shear only.
		# 
		if self._table.getParameter("boxType") == "LEOscillatory":
		
			#
			# Oscillatory shear parameters.
			#
			gamma0 = self._table.getParameter("gamma0")
			omega  = self._table.getParameter("omega" )
			k      = self._table.getParameter("k"     )

			#
			# Oscillatory shear test 2: Gp = (network contribution to) G^{\prime}.
			#
			t, Gp = [ self._table.getDataSets("Gp")["curves"][0][label] for label in ("x","y") ]

			# Prediction.
			pred = k * 3**0.5 / 4.0

			# Display if requested.
			if self._plot:
				if self._verbose:
					print( "Showing G^{\\prime} calculated during runtime, versus the affine prediction." )

				plt.plot( t, Gp, "ro-", label="measured" )
				plt.plot( t, [pred]*len(t), label="prediction", ls="--", lw=2, color="k" )
				plt.xlabel( "time")
				plt.ylabel( r"$G^{\prime}$" )
				plt.legend()
				plt.show()
			
			# Test the final values. Should be smooth since kT=0, so just take the final (i.e. most converged) value.
			relError = abs( Gp[-1] - pred ) / pred
			if self._verbose:
				print( "Relative error for final measured G^{{\\prime}} was {}.".format(relError) )

			if relError > testShearTriLattice.shearRelTol():
				raise bsTestBase.FailedTestError( "Final error in G^{{\\prime}} exceeded tolerance (could be convergence issue)." )
			else:
				if self._verbose:
					print( "Passed test (within prescribed tolerance of {}).".format(testShearTriLattice.shearRelTol()) )

	# Arguments for the executable.
	def _parameterFile( self ): return self.__paramsFile
	def _numThreads   ( self ): return 1

	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]

	# Tolerances.
	@classmethod
	def meanExtRelTol( cls ):
		return 0.02

	@classmethod
	def shearRelTol( cls ):
		return 0.001

	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(2):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing sheared triangular lattice test starting from '{}'".format(fname) )
			testShearTriLattice(executable,verbosity,plot,fname)
			print( " - passed" )
