#include "dataLinksBase.h"


// Need definition of the main class now.
#include "linksBase.h"


#pragma mark -
#pragma mark Utility methods
#pragma mark -

void dataLinksBase::_normalise( double *tgt ) const
{
	double mag = sqrt( _dot(tgt,tgt) );
	for( auto k=0; k<_dim; k++ ) tgt[k] /= mag;
}


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

dataLinksBase::dataLinksBase(
	const linksBase *links,
	std::shared_ptr<dataBeadSpring> data,
	short dim,
	const parameterParser *params
	)
	: _dim(dim), _links(links), _box(links->_box), _dataBeadSpring(data)
{
	// Ensure quantities evaluated continuously, and not in calculateTableRow(), exist in the first row.
	if( !_dataBeadSpring->continuationRun() )
	{
		if( _links->_freezeLinksAfter>-1.0 || _links->_canAttach ) _dataBeadSpring->addScalar( __dataLabel_numAttach, 0 );
		if( _links->_freezeLinksAfter>-1.0 || _links->_canDetach ) _dataBeadSpring->addScalar( __dataLabel_numDetach, 0 );
	}

	// The percolation dimension calculation can be slow, so it is an option whether or not to calculate and output.
	// Defaults to 'false'. Also store the last value calculated as it can be used as a termination condition.
	_outputPercDim = params->flagParamOrDefault(parameterLabels::dataLinks_outputPercDim,false);
	_lastPercDim = 0;	
}


#pragma mark -
#pragma mark Scalar quantities
#pragma mark -

// Always output attach/detach rates if there is time-dependency, to ensure consistency of table output after a restart.
void dataLinksBase::numAttach( int num ) const
{
	if( _links->_freezeLinksAfter>-1.0 || _links->_canAttach ) _dataBeadSpring->addScalar(__dataLabel_numAttach,num);
}

void dataLinksBase::numDetach( int num ) const
{
	if( _links->_freezeLinksAfter>-1.0 || _links->_canDetach ) _dataBeadSpring->addScalar(__dataLabel_numDetach,num);
}
	
void dataLinksBase::calculateTableRow()
{
	// Current number of links.
	_dataBeadSpring->addScalar( __dataLabel_numLinks, _links->numLinks() );
	
	// Mean coordination number.
	unsigned int N = _links->_beads->numBeads();			// Should never be zero, but check just in case.
	_dataBeadSpring->addScalar( __dataLabel_meanCoord, ( N ? 2.0*double(_links->numLinks())/N : 0.0 ) );
	
	// Total spring energy. Could parallelise this if needed.
	double E_links = 0.0;
	for( auto &linkSet : _links->_perThread_links )
		for( auto &ij : linkSet )
			E_links += _links->_linkEnergy( ij );
	_dataBeadSpring->addScalar( __dataLabel_linkEnergy, E_links );
	
	// Mean spring extension.
	double sum_dl = 0.0;
	for( auto &linkSet : _links->_perThread_links )
		for( auto &ij : linkSet )
			sum_dl += _links->_linkExtension( ij );
	_dataBeadSpring->addScalar( __dataLabel_meanExtension, (_links->numLinks()?sum_dl/_links->numLinks():0.0) );

	// Cluster properties.
	_clusterConstruct();						// Also used by block data.

	auto maxClust = 0ul;
	for( auto cluster : _clusterSets ) maxClust = std::max(maxClust,cluster.second.size());
	_dataBeadSpring->addScalar( __dataLabel_largestCluster, maxClust );

	// Percolation dimension. Only if requested as can be slow to calculate.
	if( _outputPercDim )
	{
		_calcPercolationDimension();
		_dataBeadSpring->addScalar( __dataLabel_percolationDimension, _lastPercDim );
	}
}


#pragma mark -
#pragma mark Block quantities
#pragma mark -

void dataLinksBase::calculateBlock()
{
	_blockNodeDegree();
	_blockClusterSizeDist();
	_blockLinksPerBead();
}

// i.e. the distribution of coordination numbers.
void dataLinksBase::_blockNodeDegree() const
{
	// Shorthand.
	auto nPer = _links->_linksPerBead;

	// Largest possible value - given, or a (safe?) over-estimate.
	int maxDegree = ( _links->_maxPerBead==-1 ? __dataMaxDegree : _links->_maxPerBead );
	
	for( auto n=0; n<=maxDegree; n++ )
		_dataBeadSpring->addToFixedHist( __dataLabel_nodeDegreeDist, n, std::count(nPer.begin(),nPer.end(),n), 0, maxDegree+1, maxDegree+1 );
}

// Assumes _clusterConstruct() has been called recently.
void dataLinksBase::_blockClusterSizeDist()
{
	// Shorthands.
	auto numBeads = _links->_beads->numBeads();

	// The maximum number of bins, after logging by 2.
	auto maxBins = 1u;
	while( 1<<maxBins <= numBeads ) maxBins++;

	// Clear the current distribution (kept persistent to avoid reallocations). Range [0,N]; 0 impossible but kept for clarity.
	_clusterSizeDist.resize( numBeads+1 );
	std::fill( _clusterSizeDist.begin(), _clusterSizeDist.end(), 0 );

	// Get the size distribution.
	for( auto cluster : _clusterSets ) _clusterSizeDist[(int)cluster.second.size()]++;

	// Add to the histogram. Loop throgh all logged bins.
	for( auto log2_s=0u; log2_s<=maxBins; log2_s++ )
	{
		auto numClusts = 0u;

		for( auto s=(1<<log2_s); s<(1<<(log2_s+1)); s++ )
			if( s < (int)_clusterSizeDist.size() )
				numClusts += _clusterSizeDist[s];

		_dataBeadSpring->addToFixedHist( __dataLabel_clusterSizeDist, log2_s, numClusts, 0, maxBins, maxBins+1 );
	}
}

// Number of links per bead as measured along the filament, starting from bead 0 being th ehead.
void dataLinksBase::_blockLinksPerBead() const
{
	// Maximum possible number of beads on a filament. -1 means it cannot be defined, e.g. if a beads class is being
	// used that permits filament growth. In this case, would need some alternative means to determine a suitable
	// maximum value now - optional data output with a parameter, arbitrarily large value, or just not output?
	auto maxBeads = _links->_beads->longestPossibleFilament();
	if( maxBeads==-1 )
		errors::throwException("Cannot output 'linksPerBead' bock data as the maximum possible filament length is not defined",__FILE__,__LINE__);

	// Shorthands.
	auto nFils  = _links->_beads->numFilaments();
	auto nBeads = _links->_beads->numBeads();

	// Use a local array for the counting.
	std::vector<long> nLinks(maxBeads);
	std::fill( nLinks.begin(), nLinks.end(), 0 );

	// Count numbers of beads with a link end on that bead index, counting from the head bead for the filament.
	for( auto n=0; n<nBeads; n++ )
		nLinks[ n - _links->_beads->headBead(n) ] += _links->_linksPerBead[n];

	// Copy from the local std::vector<> to the persistent histogram.
	for( auto b=0; b<maxBeads; b++ )
		_dataBeadSpring->addToFixedHist( __dataLabel_linksPerBeadHist, b, 1.0*nLinks[b]/nFils, 0, maxBeads, maxBeads );
}


#pragma mark -
#pragma mark Cluster list construction
#pragma mark -

// Calculate the current cluster distribution using union-by-rank.
void dataLinksBase::_clusterConstruct()
{
	auto numBeads = _links->_beads->numBeads();

	// Size rank and parent arrays to current number of beads.
	_clusterBeadRank  .resize( numBeads );
	_clusterParentBead.resize( numBeads );

	// Initialise all ranks and parents. All beads on the same filament are automatically in the same cluster.
	std::fill( _clusterBeadRank.begin(), _clusterBeadRank.end(), 0 );
	for( auto i=0u; i<_clusterParentBead.size(); i++ ) _clusterParentBead[i] = _links->_beads->headBead(i);

	//
	// Determine roots of each cluster's tree using union by rank.
	//
	for( auto &linkSet : _links->_perThread_links )
		for( auto &ij : linkSet )
		{
			// Get current roots of each head.
			auto root1 = _clusterGetRoot( ij.first  );
			auto root2 = _clusterGetRoot( ij.second );

			// If the same (i.e. already in the same cluster), skip.
			if( root1==root2 ) continue;

			// If on different clusters and have different rank, add smaller to the larger.
			if( _clusterBeadRank[root1] > _clusterBeadRank[root2] )
			{
				_clusterParentBead[root2] = _clusterParentBead[root1];
				continue;
			}
			if( _clusterBeadRank[root2] > _clusterBeadRank[root1] )
			{
				_clusterParentBead[root1] = _clusterParentBead[root2];
				continue;
			}

			// If the same ranks, add one to the other and increase the rank.
			_clusterParentBead[root1] = root2;
			_clusterBeadRank  [root2]++;
		}

	//
	// Get cluster distributions as a map keyed by root (i.e. an arbitrary bead index in that cluster).
	//
	_clusterSets.clear();
	for( auto i=0u; i<_clusterParentBead.size(); i++ )
		_clusterSets[_clusterGetRoot(i)].emplace( i );

	// Check the cluster lists for consistency.
	//debugClusterLists();
}

// Returns root of a bead while also applying path compression.
int dataLinksBase::_clusterGetRoot( int bead )
{
	if( _clusterParentBead[bead] != bead )
		_clusterParentBead[bead] = _clusterGetRoot( _clusterParentBead[bead] );

	return _clusterParentBead[bead];
}


#pragma mark -
#pragma mark Percolation dimension
#pragma mark -

// Uses the algoerithm of Livraghi et al., J. Chem. Theory Comput. 17, 6449 (2021).
// Stores the answer in the persistent variable _lastPercDim.
void dataLinksBase::_calcPercolationDimension()
{
	// If no periodicity, will need a different algorithm (probably a simpler one). Since if we have made it
	// this far, the user has requested percDim to be calculated, best output something now.
	if( !_box->hasPeriodicBoundaries() )
		errors::throwException( "Percolation for non-periodic boundaries not yet implemented", __FILE__, __LINE__ );

	// Generate the set of node indices.
	std::set<int> nodes;
	for( auto n=0; n<_links->_beads->numBeads(); n++ ) nodes.insert( n );

	// Uses the percPerBC<>:: class, which is templated to dimension. Needs nodes, edges=links, and periodic shifts,
	if( _dim==2 )
	{
		// Map of linked node pairs (key) and the periodic shift in units of the box dimensions (value).
		std::map< std::array<int,2>, std::array<int,2> > edges;
		for( auto &linkSet : _links->_perThread_links )
			for( auto &ij : linkSet )
			{
				std::array<int,2> perShift;
				for( auto k=0; k<_dim; k++ ) perShift[k] = _links->_beads->nmlsdPerShiftInDirn(k,ij.first,ij.second);

				edges.insert( { {int(ij.first),int(ij.second)}, perShift } );
			}

		// Use the percPerBC<> class to calculate the percolation dimension; store and also return.
		percPerBC<2> percObj;
		_lastPercDim = percObj.percolationDimension( nodes, edges );
	}

	// 3D is basically the same as 2D, with minor (and obvious) changes.
	if( _dim==3 )
	{
		std::map< std::array<int,2>, std::array<int,3> > edges;
		for( auto &linkSet : _links->_perThread_links )
			for( auto &ij : linkSet )
			{
				std::array<int,3> perShift;
				for( auto k=0; k<_dim; k++ ) perShift[k] = _links->_beads->nmlsdPerShiftInDirn(k,ij.first,ij.second);

				edges.insert( { {int(ij.first),int(ij.second)}, perShift } );
			}

		percPerBC<3> percObj;
		_lastPercDim = percObj.percolationDimension( nodes, edges );
	}
}

#pragma mark -
#pragma mark Debugging
#pragma mark -

void dataLinksBase::debugClusterLists() const
{
	// Check 1. Each cluster set's key (=index of parent node) should be in that set.
	for( auto cluster : _clusterSets )
		if( cluster.second.find(cluster.first) == cluster.second.end() )
			errors::throwException( "Inconsistent cluster lists: Parent node (=key) not in set", __FILE__, __LINE__ );

	// Check 2. Each node index appears exactly once.
	for( auto i=0u; i<_clusterBeadRank.size(); i++ )
	{
		auto count = 0u;
		for( auto cluster : _clusterSets )
			if( cluster.second.find(i) != cluster.second.end() ) count++;
		
		if( count!=1 )
			errors::throwException( "Inconsistent cluster lists: Bead index appears more than once", __FILE__, __LINE__ );
	}

	// Check 3. Total number of beads equals the known number.
	auto total = 0u;
	for( auto cluster : _clusterSets ) total += cluster.second.size();

	if( total != _links->_linksPerBead.size() )
		errors::throwException( "Inconsistent cluster lists: Total number of beands in lists does not match known total", __FILE__, __LINE__ );
}
