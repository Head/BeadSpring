/**
 * @file boxBase.h
 * @author David Head
 * @brief Abstract base class for the box geometry.
 */


#ifndef BEAD_SPRING_BOX_BASE_H
#define BEAD_SPRING_BOX_BASE_H


//
// Includes.
//

// Standard includes.
#include <memory>

// Local includes.
#include "flatXMLParser.h"
#include "threadedCellSort.h"
#include "threadedNeighbourList.h"

// Project includes.
#include "pRNG.h"
#include "errors.h"
#include "dataBeadSpring.h"
#include "logging.h"
#include "parameterParser.h"


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string box_NNRangeFactor   = "NNRangeFactor";
	const std::string box_NNMaxNeighbours = "NNMaxNeighbours";
	const std::string box_NNSortFrequency = "NNSortFrequency";
}

/**
 * @brief Abstract base class for the box geometry.
 * 
 * Abstract base class for the box geometry, for which children must override the following methods:
 *
 * `virtual void wrap( double *x                  ) const noexcept = 0;`			// Wraps single particle coords.
 * 
 * `virtual void wrap( double *x, const double *y ) const noexcept = 0;`			// Wraps x to y.
 *
 * `virtual double volume() const noexcept = 0;`
 *
 * If no Verlet / Nearest Neighbour parameters are defined, will resort to just using a multi-threaded cell sorter.
 * 
 * Friend class `dataBoxBase` for data gathering.
 */
class boxBase
{

private:

	constexpr static auto __saveBoxTag = "Box";

	constexpr static auto __saveBeadsTag  = "Beads" ;
	constexpr static auto __saveLinksTag  = "Links" ;
	constexpr static auto __saveSolverTag = "Solver";

protected:

	/// Dimensionality.
	short _dim;
	
	/// Pointer to all input parameters.
	const parameterParser *_params;
	
	/// Data object.
	std::shared_ptr<dataBeadSpring> _data;

	/// Use either Verlet lists (with the risk of missing contacts), or just a straight cell sorter.
	bool _useNeighbourLists;

	// Cell sort handlers. 2D and 3D versions. Only used instead of NN lists.
	std::unique_ptr< threadedCellSort<2> > _cellSort2D;
	std::unique_ptr< threadedCellSort<3> > _cellSort3D;

	// NN/Verlet list handlers. 2D and 3D versions.
	std::unique_ptr< threadedNeighbourList<2> > _neighbourList2D;
	std::unique_ptr< threadedNeighbourList<3> > _neighbourList3D;
	
	// General parameters for the NN/Verlet lists.
	double _NNRangeFactor  ;						///< NN/Verlet-list max. range relative to max. interaction range.
	int    _NNSortFrequency;						///< NN/Verlet-list frequecy of re-sorting.
	int    _NNMaxNeighbours;						///< NN/Verlet-list max. number of neighbours.
	
	/// The maximum interaction range, prior to multiplication by the range factor.
	double _bareMaxRange;
	
	// I/O.
	virtual void _saveBoxState   ( flatXMLParser *xml ) const {}		///< Save box state.
	virtual void _recoverBoxState( flatXMLParser *xml )       {}		///< Recover box state from saved file.

public:
	
	//
	// Constructors / destructor.
	//
	boxBase() {}
	boxBase( short d, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );	///< Main constructor. 
	virtual ~boxBase() {}
	
	//
	// Initisalisation.
	//

	/// Set up the cell sorter and the NN/Verlet list. Usually overridden, in which case should call this parent method.
	virtual void initialiseCells( double maxRange ) { _bareMaxRange = maxRange; }
			
	// Random position and orientation. Position must be overridden, orientation may be.
	virtual void randomPosition   ( double *x ) const noexcept = 0;		///< Defined by child class.
	virtual void randomOrientation( double *t ) const noexcept;			///< Uniform on unit sphere.
	
	/// Possible initial conditions. Throw exception if not possible for the specific child class.
	virtual void geometryTriLattice( int N, double &a, int &numX, int &numY ) const
		{ errors::throwException("Triangular Lattice not supported by this box",__FILE__,__LINE__); }
	
	//
	// Accessors.
	//

	// Volume of the box (or area if 2D). Must be overriden. Also specify if volume is fixed.
	virtual double volume     () const noexcept = 0;					///< Volume (area in 2D) of box.
	virtual bool   fixedVolume() const noexcept { return true; }		///< Is the volume fixed?

	// Shortest and longest linear dimensions.
	virtual double shortestDimension() const noexcept = 0;				///< Shortest box dimension; defined by child class.
	virtual double longestDimension () const noexcept = 0;				///< Longest box dimension; defined by child class.
	
	/// The maximum interaction range, _not_ including the additional safety margin, defined when the cells were initialised.
	inline double bareMaxInteractionRange() const { return _bareMaxRange; }

	//
	// Modifiers.
	//
	virtual void update( double time, double dt ) {}						///< e.g. if box geometry/periodicity depends on time.
	virtual void updateInteractionList( int N, const double *X );
	virtual unsigned long numPairsInList( unsigned int threadNum ) const;
	virtual const unsigned long* perThreadPairList( unsigned int threadNum ) const;

	//
	// Wrapping. Defaults to none.
	//
	virtual bool hasPeriodicBoundaries() const noexcept { return false; }				///< If box has any periodicity.
	virtual void wrap( double *x ) const noexcept {};									///< Centred on origin.
	virtual void offsetToPrimaryCell( const double *x, double *offset ) const noexcept	///< Offset to move x to primary cell.
		{ for( auto k=0; k<_dim; k++ ) offset[k] = 0.0; }
	virtual int nmlsdPerShiftInDirn( int axis, double relPosn ) const { return 0; }		///< Normalised periodic shift along given axis.

	//
	// Data.
	//
	virtual void dataCalculateTable() const {}
	virtual void dataCalculateBlock() const {}
	
	//
	// I/O.
	//
	void saveState   ( flatXMLParser *xml ) const;
	void recoverState( flatXMLParser *xml );
};


#endif
