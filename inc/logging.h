/**
 * @file logging.h
 * @author David Head
 * @brief Logging objects and routines, global but in their own namespace.
 */


#ifndef BEAD_SPRING_LOGGING_H
#define BEAD_SPRING_LOGGING_H


// Uncomment (or include in compiler options -D...) to output detailed tracking to the log file,
// printing a statement for key stages to help locate bugs. WARNING: Can lead to very large log files.
//#define DETAILED_TRACKING


// Includes.
#include <iostream>
#include <fstream>
#include <string>


/// Logging objects and routines, global but in their own namespace.
 namespace logging
{
	/// The log output filestream object.
	extern std::ofstream logFile;
	
	/// Open the logfile, with an open to overwrite rather than append. Returns 'true' if opened successfully, 'false' otherwise.
	bool openLogFile( std::string filename, bool overwrite = false );

	/// Send a message only if DETAILED_TRACKING is defined. Templated version with value available.
	void detailedTracking( std::string message, std::string file, int line );	

	template<typename T> void detailedTracking( std::string message, T val, std::string file, int line )
	{
#ifdef DETAILED_TRACKING
	logFile << "DETAILED TRACKING: " << message << " " << val
	        << " [in file " << file << ", line " << line << "]" << std::endl;
#endif
	}

	/// Deprecation warning, i.e. warning about code/option/etc that may soon become deprecated/invalid.
	void deprecationWarning( std::string message, std::string file, int line );

}
	
#endif

