/*
 *  _index.h
 *  
 * Stores long-integer indices or "ordinates" of arbitrary dimension, and allows ordering.
 *
 * Only partially implemented - <, > and == are included, but nothing else.
 * Outputs to standard operator in tab-deliminated format.
 * Simply "throws" if the memory could not be allocated
 *
 * To use the ordering within container classes when using _index pointers,
 * use the "_index_ptr_cmp" class also given in this file.
 *
 *  Created by David Head on Fri Sep 10 2004.
 *
 *
 */

#ifndef _UD_INDICES_H
#define _UD_INDICES_H

#include <iostream>

using namespace std;



// Actual class, named "_index" since "index" is used by standard library
template< int dim >
class _index
{

public:
	
	_index( long* );
	_index();
	_index(const _index&);
	virtual ~_index();

	inline long get( int i ) const { return _indices[i]; }
	inline void set( int i, long j ) { _indices[i] = j; }

	template< int d >
	friend bool operator< ( const _index<d>&, const _index<d>& );

	template< int d >
	friend bool operator> ( const _index<d>&, const _index<d>& );

	template< int d >
	friend bool operator== ( const _index<d>&, const _index<d>& );

	template< int d >
	friend ostream& operator<< ( ostream&, const _index<d>& );

private:
	long *_indices;

	inline void print( ostream& ) const;
};


// Constructor initialised with explicit indices
template< int dim >
_index<dim>::
_index ( long *init_indices )
{
	// Here, allocate an array for the (integer) pointer
	_indices = new long[dim];
	
	if( _indices )
	{
		for( int i=0; i<dim; i++ ) _indices[i] = init_indices[i];
	} else throw;
}


// Default constructor, sets all indices to zero
// Constructor
template< int dim >
_index<dim>::
_index ()
{
	// Here, allocate an array for the (integer) pointer
	_indices = new long[dim];
	
	if( _indices )
	{
		for( int i=0; i<dim; i++ ) _indices[i] = 0;
	} else throw;
}

// Copy constructor
template< int dim >
_index<dim>::_index( const _index &in )
{
	_indices = new long[dim];
	if( _indices )
	{
		for( int i=0; i<dim; i++ ) _indices[i] = in._indices[i];
	} else throw;
}


// Destructor - deallocate memory for the _index array
template< int dim >
_index<dim>::
~_index ()
{
	delete [] _indices;
}


// Allows for standard "less than" operator to be used
template< int dim >
bool operator< (const _index<dim> &lhs, const _index<dim> &rhs )
{
	for( int i=0; i<dim; i++ )
	{
		if( lhs._indices[i] < rhs._indices[i] ) return true;
		if( lhs._indices[i] > rhs._indices[i] ) return false;
		// Continues to higher i's only if they are equal
	}
	// Both _indexes are the same
	
	return false;
}


// Allows for standard "greater than" operator to be used
template< int dim >
bool
operator> (const _index<dim> &lhs, const _index<dim> &rhs )
{
	for( int i=0; i<dim; i++ )
	{
		if( lhs._indices[i] > rhs._indices[i] ) return true;
		if( lhs._indices[i] < rhs._indices[i] ) return false;
		// Continues to higher i's only if they are equal
	}
	// Both _indexes are the same
	
	return false;
}


// Allows for standard equality operator to be used
template< int dim >
bool
operator== (const _index<dim> &lhs, const _index<dim> &rhs )
{
	for( int i=0; i<dim; i++ )
		if( lhs._indices[i] != rhs._indices[i] ) return false;

	// Both _indexes are the same
	return true;
}



// Output operator - calls the private "print()" member of the bin class
template< int dim >
inline ostream&
operator<< ( ostream &os, const _index<dim> &a )
{
	a.print( os );
	return os;
}



// The actual routine that ouputs the indices, tab-separated
template< int dim >
inline void _index<dim>::
print( ostream &os ) const
{
	for( int i=0; i<dim; i++ ) {
		os << _indices[i];
		if( i < dim-1 ) os << "\t";		// No final tab
	}
}





// This mini-class allows ordering of _index pointers within STL container
// classes, i.e. map< index<n>*, (?), _index_ptr_cmp<n> >
template< int dim>
class _index_ptr_cmp
{
public:

	inline bool operator() (  _index<dim> *lhs, _index<dim> *rhs ) const
	{
		if( *lhs < *rhs ) return true; else return false;
	}
};


#endif




