#include "boxLEOscillatory.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

boxLEOscillatory::boxLEOscillatory( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data )
	: boxLeesEdwards(dim,params,data)
{
	using namespace parameterLabels;

	// Get the required parameters.
	if( !params->hasScalarParam(boxLEOscillatory_amplitude) )
		errors::throwException( "Oscillatory Lees-Edwards box type requires amplitude to be defined", __FILE__, __LINE__ );
	_gamma0 = params->scalarParam(boxLEOscillatory_amplitude);
	
	if( !params->hasScalarParam(boxLEOscillatory_frequency) )
		errors::throwException( "Oscillatory Lees-Edwards box type requires a frequency to be defined", __FILE__, __LINE__ );
	_omega = params->scalarParam(boxLEOscillatory_frequency);

	// Optional parameters.
	_startTime = params->scalarParamOrDefault(boxLEOscillatory_startTime,0.0);

	// Sanity check for values. Allow zeroes so parameters file can be easily modified to switch oscillations off.
	if( _gamma0 < 0.0 )
		errors::throwException( "Amplitude of oscillatory shear must be non-negative", __FILE__, __LINE__ );

	if( _omega < 0.0 )
		errors::throwException( "Frequency of oscillatory shear must be non-negative", __FILE__, __LINE__ );

	if( _startTime < 0.0 )
		errors::throwException( "Start time for oscillatory shear must be non-negative", __FILE__, __LINE__ );

	//
	// Initialise the data-gathering (auxiliary) object.
	//
	_dataBoxLEOscillatory = std::unique_ptr<dataBoxLEOscillatory>( new dataBoxLEOscillatory(this,data,_dim) );

	//
	// Echo parsed parameters to log.
	//
	logging::logFile << "Construction of boxLEOscillatory. Will apply shear \\gamma0 sin[\\omega()*(t-t0)] for t>t0, with "
	                 << "\\gamma0=" << _gamma0    << ", "
	                 << "\\omega="  << _omega     << ", and "
	                 << "t0="       << _startTime << "." << std::endl;
}


#pragma mark -
#pragma mark Shear control
#pragma mark -

void boxLEOscillatory::update( double t, double dt )
{
	_setShearStrain( t>_startTime ? _gamma0*sin(_omega*(t-_startTime)) : 0.0 );

	// Any data-related quantities that need to be updated.
	_dataBoxLEOscillatory->update(t,dt);
}


#pragma mark -
#pragma mark I/O
#pragma mark -

void boxLEOscillatory::_saveBoxState( flatXMLParser *xml ) const
{
	_dataBoxLEOscillatory->saveBoxState(xml);
	
	boxLeesEdwards::_saveBoxState(xml);
}

void boxLEOscillatory::_recoverBoxState( flatXMLParser *xml )
{
	_dataBoxLEOscillatory->recoverBoxState(xml);

	boxLeesEdwards::_recoverBoxState(xml);
}




