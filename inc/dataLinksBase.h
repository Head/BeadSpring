/**
 * @file dataLinksBase.h
 * @author David Head
 * @brief Auxiliary class for linksBase that performs the various data gathering routines.
 */


#ifndef BEAD_SPRING_DATA_LINKS_BASE_H
#define BEAD_SPRING_DATA_LINKS_BASE_H


//
// Includes.
//

// Standard includes.
#include <algorithm>
#include <vector>
#include <map>
#include <array>
#include <set>
#include <numeric>

// Project includes.
#include "dataBeadSpring.h"
#include "boxBase.h"
#include "percPerBC.h"
#include "parameterParser.h"


//
// Prototypes.
//
class linksBase;


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string dataLinks_outputPercDim = "outputPercDim";
}


/// Auxiliary class for linksBase that performs the various data gathering routines.
class dataLinksBase
{
private:

	constexpr static auto __dataLabel_numLinks = "numLinks";

	constexpr static auto __dataLabel_numAttach = "rateAttach";
	constexpr static auto __dataLabel_numDetach = "rateDetach";
	
	constexpr static auto __dataLabel_meanCoord = "meanLinkCoord";

	constexpr static auto __dataMaxDegree = 20;
	constexpr static auto __dataLabel_nodeDegreeDist = "nodeDegree";

	constexpr static auto __dataLabel_linksPerBeadHist = "linksPerBead";

	constexpr static auto __dataLabel_linkEnergy = "E_links";
	constexpr static auto __dataLabel_meanExtension = "meanLinkExt";

	constexpr static auto __dataLabel_clusterSizeDist = "clustLog2Dist";
	constexpr static auto __dataLabel_largestCluster = "maxClust";
	constexpr static auto __dataClusterSizeDist_maxBins = 100;

	constexpr static auto __dataLabel_percolationDimension = "percDim";

protected:

	/// The dimension.
	short _dim;

	// The pointer to the links object and the box.
	const linksBase *_links;
	const boxBase   *_box  ;

	// Make the links object a friend, so it can access e.g. the data labels defined here.
	friend linksBase;
	
	/// The data object for the whole project's data object.
	std::shared_ptr<dataBeadSpring> _dataBeadSpring;

	//
	// Utility methods.
	//
	inline double _dot( double *x, double *y ) const noexcept { return ( x[0]*y[0] + x[1]*y[1] + (_dim==3?x[2]*y[2]:0.0) ); }
	inline double _dot( double *x            ) const noexcept { return ( x[0]*x[0] + x[1]*x[1] + (_dim==3?x[2]*x[2]:0.0) ); }

	void _normalise( double *n ) const;
	
	//
	// Block quantities.
	//
	virtual void _blockNodeDegree() const;
	virtual void _blockClusterSizeDist();
	virtual void _blockLinksPerBead() const;

	//
	// Cluster calculation.
	//
	std::vector<int> _clusterBeadRank;						///< Used to construct the clusters.
	std::vector<int> _clusterParentBead;
	std::vector<int> _clusterSizeDist;						///< Avoids a double-loop; persistent to reduce resizes.

	std::map< int, std::set<int> > _clusterSets;			///< Keyed by one bean in the cluster (arbitrary).


	virtual void _clusterConstruct();						///< Uses union by rank.
	virtual int  _clusterGetRoot( int bead );				///< Also applies path compression.

	//
	// Percolation dimension calculation.
	//
	bool _outputPercDim;									///< Whether to calculate the percolation dimension or not.
	void _calcPercolationDimension();						///< Calculates percolation dimension and stores in _lastPercDim.
	int  _lastPercDim;										///< Returns the last evaluation of the percolation dimension.

public:

	dataLinksBase( const linksBase *links, std::shared_ptr<dataBeadSpring> data, short dim, const parameterParser *params );
	virtual ~dataLinksBase() {}
	
	// Getters.
	inline int  lastPercDim  () const noexcept { return _lastPercDim; }
	inline bool outputPercDim() const noexcept { return _outputPercDim; }

	// Regular data gathering.
	virtual void calculateTableRow();
	virtual void calculateBlock   ();
	
	// Methods for specific quantities.
	virtual void numAttach( int num ) const;
	virtual void numDetach( int num ) const;

	// Interface for the underlying data object.
	inline bool calculateStressNow() const noexcept { return _dataBeadSpring->calculateStressNow(); }
	inline void addToStressTensor_fr_r( double f_over_r, const double *r, double weight=1.0 )
		{ _dataBeadSpring->addToStressTensor_fr_r(f_over_r,r,weight); }
	inline void addToStressTensor_f_r ( const double *f, const double *r, double weight=1.0 )
		{ _dataBeadSpring->addToStressTensor_f_r(f,r,weight); }
	
	//
	// Debugging.
	//
	virtual void debugClusterLists() const;
};


#endif
