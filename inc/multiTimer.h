//
// multiTimer class
// 
// Handles timing duties with the expected application of optimising simulations.
//
// Typical usage:
//
// multiTimer timer;					// Will automatically start a time for the entire run
//
// timer.startTimerWithLabel( "Name" );	// Start timer with the required label
// timer.stopTimerWithLabel( "Name" );	// Stops the timer (throws an exception if not defined)
// timer.stopCurrentTimer();			// Stops the most recent timer (doesn't allow nesting)
//
// timer.cumulativeTimes(ostream&); 	// Output results to stream
// cout << timer;					// Sim.
//
// 18/11/2008/DAH: The total time counter didn't work on some systems as the system clock
// seemed to loop; therefore now update the total time for every call to stop...().
//
// 03/09/2009/DAH: Switched to using the clock_gettime() function from some version of <time.h>.
// This allows the global clock to be measured independently from thread or process time, which
// is what is typically required for timing loops (i.e. seeing if they are faster when parallelised).
// If not available but trying to run parallel code with openmp, warn user to cerr.
// NOTE: May need to compile with "-lrt" to use clock_gettime().
//
// 06/06/2017/DAH: Switch to C++11 and the chrono library, which removes the '-lrt' issue mentioned
// above.
//


#ifndef _UD_MULTI_TIMER_H
#define _UD_MULTI_TIMER_H


// Standard includes
#include <string>
#include <chrono>			// C++11 onwards
#include <ostream>
#include <map>
#include <sstream>			// For ostringstream
#include <stdexcept>
#include <iostream>


using namespace std;


class multiTimer
{

private:

protected:

	// Standardised name for the whole-run timer, which is automatically started in the constructor
	static string _entireLabel() { return "Entire run"; };

	// Start points and running totals
	map<string,chrono::time_point<chrono::high_resolution_clock> > _start;			// Time started
	map<string,chrono::duration  <double>  			           	 > _cumulative;		// Cumulative times

	// Last started key; used by the concise stopCurrentTimer() method
	string _current;										

	// Readable time string; used in output
	static string _readableTimeString( chrono::duration<double> durn );

public:

	multiTimer() { startTimerWithLabel( _entireLabel() ); };
	virtual ~multiTimer() {};

	// Start a timer going: If it does not already exist, add to the list (and start it going).
	void startTimerWithLabel( string key ) { _start[key] = chrono::high_resolution_clock::now(); _current = key; }

	// Stop a timer, if it exists; throws an exception if not
	void stopTimerWithLabel(string);
	void stopCurrentTimer() { stopTimerWithLabel( _current ); };		// Concise version; closes last started key

	// Output routines: Can use operator<< or pass a stream to Cumulative_Times (i.e. cout, etc.)
	void cumulativeTimes(ostream&);
	friend ostream& operator<< (ostream&,multiTimer&);	
	inline std::string activeTimer() const noexcept { return _current; }
};


#endif


