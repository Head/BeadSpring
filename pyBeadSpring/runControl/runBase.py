#
# Generic run control class. Cannot be launched directly, should instead derive a child class
# from this that overrides the necessary class methods:
#	-	executable(cls)			:	Returns string of executable name.
#	-	stateFilePrefix(cls)	:	Returns string that identifies state files (usually suffixed with indices).
#	-	description(cls)		:	Returns string describing the specific application.
#
# If the executable allows extensions, then the following should be overriden to return the cmd line specifier:
#	-	extendArg(cls)			:	e.g. "-T" or "--extend".
#


#
# Imports.
#
import argparse
import sys
import time
import os
import pathlib				# Python 3.5+
import shutil
import subprocess


#
# Class for run control in general.
#
class runBase:

	def __init__( self ):
		"""Handles typical run control scripts with options for interactive runs, launching on a cluster, and so on."""

		# Instance variaboe initialisations - technically unnecessary in Python, but can hep debugging.
		self._args    = None	# Return from the argparse object after parsing.
		self._runDirs = []		# List of paths to run directories, which end in 'run<index>' if there is more than one.


	#
	# Main control loop.
	#
	def launchRuns( self ):
		"""Main loop. Parses the command line arguments, sets up directories, and launches the runs."""

		# Set up and parse the command line.
		self.parseCmdLineArgs()

		# Set up run directories.
		self.setUpDirectories( alreadyExists = (self._extendTime!=None) )

		# Launch, either consecutively or concurrently, and interactively or to a queue.
		if self._consec:
			self.launchConsecutively()
		else:
			self.launchConcurrently()

	#
	# Parsing command line arguments.
	#
	def setCmdLineOptions( self, parser ):
		"""Options for the command line parser. The defaults are defined here; can create more by overriding this class method."""

		# Positional arguments.
		parser.add_argument( "root" , help="Path to the base directory, not including 'run?' subdirs." )
		parser.add_argument( "range", help="Range of run indices as a Python expression (probably in quotes), or 'None'." )

		# Optional arguments.
		parser.add_argument( "-s", "--seed"      , help="Set the seed for the first run [None]", default=None )
		parser.add_argument( "-p", "--parameters", help="Parameters file (will be copied to 'parameters.xml' on main directory) [parameters.xml]", default="parameters.xml" )
		parser.add_argument( "-t", "--threads"   , help="Number of threads to use (passed to executable and queue) [1]", default="1" )
		parser.add_argument(       "--wallclock" , help="Wall clock time in hours, max. 48 [48]", default="48" )

		# Only allow extensions if the executable supports them / the extend cmd line specifier is provided.
		if self.__class__.extendArg():
			parser.add_argument( "-e", "--extend", help="Extend run by specified amount, starting from last file in states archive [None]", default=None )

		# Run interactively, or submit to a queue? (In future, may need a 'slurm' option here).
		parser.add_argument( "--queue" , help="Submit to HPC queue such as that at ARC [False]", action="store_true" )
		parser.add_argument( "--consec", help="Run jobs consecutively (rather than concurrently) [False]", action="store_true" )

		# Flags including for debugging.
		parser.add_argument( "--verbose", help="Verbose output for this script [False]", action="store_true" )
		parser.add_argument( "--nosave" , help="Delete (rather than archive) state files [False]", action="store_true" )

	def parseCmdLineArgs( self ):
		"""Sets up the command line parse (instance of argparse) and returns the options and arguments."""

		# Get the command line arguments.
		parser = argparse.ArgumentParser( description = self.__class__.description() )
		self.setCmdLineOptions( parser )
		self._args = parser.parse_args()	

		# Just store the root path; will need to be checked later.
		self._root = self._args.root

		# Similarly, just store the parameters file.
		self._parameters = self._args.parameters

		# Store flags.
		self._verbose = self._args.verbose
		self._queue   = self._args.queue
		self._consec  = self._args.consec
		self._nosave  = self._args.nosave

		#
		# Extract values with checking.
		#

		# Initialise values, sometimes to undefined messages to help track down bugs.
		self._range       = "<range unspecified>"
		self._numThreads  = "<no. threads unspecified>"
		self._wallClock   = "<wall clock unspecified>"
		self._currentSeed = "<seed unspecified>"
		self._extendTime  = None

		# Try to evaluate runs range as Python. Will evaluate "None" as None.
		try:
			self._range = eval(self._args.range)
		except:
			print( "Could not evaluate second arg '{}' as a range of run indices.".format(self._args.range) )
			sys.exit(-1)

		# Extract number of threads.
		try:
			self._numThreads = eval(self._args.threads)
		except:
			print( "Could not parse the threads argument '{}' as an integer.".format(self._args.threads) )
			sys.exit(-1)

		# Extract wall clock time.
		try:
			self._wallClock = int( eval(self._args.wallclock) )
		except:
			print( "Could not parse the wall clock time as an integer." )
			sys.exit(-1)

		# Check the range of the wallClock.
		if self._wallClock>self.__class__.maxWallClockTime() or self._wallClock<1:
			print( "Wall clock time must be an integer in the range [1,48]." )
			sys.exit(-1)

		# The extend time could be anything, e.g. "48d" for the biofilm code, so just need to check it was specified.
		if self.__class__.extendArg():
			self._extendTime = self._args.extend			# Will be 'None' if no extend requested.

		# Set the seed now based on epoch time (unless a specific value was given).
		self._currentSeed = ( int(self._args.seed) if self._args.seed else int(time.time()) )

	#
	# Directory modification or creation.
	#
	def setUpDirectories( self, alreadyExists=False, path=None ):
		"""Determines the set of paths to each of the run subdirectories, and for each, either creates
		a new run from scratch using _createRunDir(), or prepares for an extension run using _extendRunDir(),
		dependong on the 'alreadyExists' kwarg."""

		# If no base path provided, use the default.
		basePath = ( path if path else self._root )

		# Convert run indices to a set of run pathnames; just the root directory for a single run.
		self._runDirs = []
		if self._range==None or self._range==0 or self._range==False:
			self._runDirs.append( basePath )
		else:
			# Create all of the required subdirectories. May not be iterable.
			try:
				for index in self._range:
					self._runDirs.append( os.path.join(basePath,"run{}".format(index)) )
			except TypeError:
				print( "Cannot iterate over '{}' as a set of run indices (evaluated as standard Python).".format(self._range) )
				sys.exit(-1)

		# Set up all of directories, including a check that they exist (if they should) or do not (if they shouldn't).
		for dir in self._runDirs:

			# If does not already exist, need to create a new directory.
			if not alreadyExists:

				# Check directory does not already exist first.
				if os.path.exists(dir):
					print( "Cannot create run subdirectories on '{}'; base path already exists.".format(dir) )
					sys.exit(-1)

				self._createRunDir( dir )

			# If run directory is supposed to already exist, extend.
			if alreadyExists:

				# Check it does exist.
				if not os.path.exists(dir):
					print( "Cannot set up directories on '{}' as they must already exist for this operation.".format(dir) )
					sys.exit(-1)
	
				self._extendRunDir( dir )

	def _createRunDir( self, runPath, populate=True ):
		"""Creates the given directory, with option to also populate with e.g. the executable and parameters files."""

		# Try to create the directory.
		if self._verbose:
			print( "Creating run directory '{}'.".format(runPath) )

		pathlib.Path( runPath ).mkdir( parents=True, exist_ok=True )		# Like mkdir -p in terminal.

		# Populate with the executable, the run script, and the parameters file.
		if populate:
			execFile  = self.__class__.executable()
			paramFile = self._parameters
			runScript = self.runScriptName()

			if self._verbose:
				print( "Copying over '{}' and '{}', and creating run script '{}'.".format(execFile,paramFile,runScript) )

			# Copy over the executable and the parameters file.
			shutil.copy( execFile , runPath )
			shutil.copy( paramFile, runPath )

			# Generate a run script and place on the same directory.
			with open( os.path.join(runPath,runScript), 'w' ) as f:
				f.write( self._generateRunScript(os.path.basename(execFile)) )
						
	def _extendRunDir( self, runPath, populate=True ):
		"""Extends the given directory, which is assumed to exist and have a states file on it.
		Will populate with executable and run script if the corresponding flag is 'true'."""

		if self._verbose:
			print( "Extending run directory '{}'.".format(runPath) )
		
		# Populate with the executable and the run script, but _not_ the parameters file.
		if populate:
			execFile  = self.__class__.executable()
			runScript = self.runScriptName()

			if self._verbose:
				print( "Copying over '{}', and creating run script '{}'.".format(execFile,runScript) )

			# Copy over the executable and the parameters file.
			shutil.copy( execFile , runPath )

			# Generate a run script and place on the same directory.
			with open( os.path.join(runPath,runScript), 'w' ) as f:
				f.write( self._generateRunScript(os.path.basename(execFile)) )

	def _generateRunScript( self, execName, execArgs=None, startup=None ):
		"""Returns a string containing a script that can be called with sh or submitted to a queue.
		Requires the actual executable name that will be called as "./<execName>", so any directories
		must have already been removed (e.g. using os.path.basename). Can add extra arguments to the
		executable using the 'execArgs' argument. First lines can be sent as 'startup'; assumed to be
		a newline-terminated string, possibly multi-line."""

		# Initiliase string and get prefix for state files.
		script = ""
		statePrefix = self.__class__.stateFilePrefix()		# For files "<statePrefix>_<text and extension>"

		# Preliminaries for HPC queues including ARC. Consecutive runs have this informatiom in a master script instead.
		if self._queue and not self._consec:
			script += "#$ -cwd -V\n"
			script += "#$ -l h_rt={}:00:00\n".format(self._wallClock)
			script += "#$ -pe smp {}\n".format(self._numThreads)

		# Insert any preliminary commands.
		if startup:
			script += startup
		
		# If extending, unarchive the existing states file.
		if self._extendTime:
			script += "tar -xzf {0}s.tar.gz {0}_*\n".format(statePrefix)

		# Launching the executable with arguments.
		script += "./{}".format(execName)
		script += " -t {}".format(self._numThreads)
		script += " --seed={}".format(self._currentSeed)

		if self._extendTime:
			script += " {} {}".format(self.__class__.extendArg(),self._extendTime)

		if execArgs:
			script += " {}".format(execArgs)

		script += "\n"

		# Add one to the seed, so all runs are independent.
		self._currentSeed += 1 

		# Tidy up after the run: delete executable, and delete individual state files after archiving them.
		# Saving (archiving) state files is also an option that can be switched off to save disk space.
		# Assumes state files are some prefix followed by "_" and index/extension, and that the archive
		# name is the prefix followed by "<...>s.tar.gz" (n.b. currently only tar zip'ed archives supported).
		script += "rm {}\n".format(execName)

		if self._nosave:
			script += "rm -f {}_*\n".format(statePrefix)
		else:
			script += "for file in {}_*; do\n".format(statePrefix)
			script += '  if [ -f "$file" ]; then\n'
			script += "    tar -cvzf {0}s.tar.gz {0}_*\n".format(statePrefix)
			script += "    rm {}_*\n".format(statePrefix)
			script += "  fi\n"
			script += "  break\n"
			script += "done\n"

		# Return with the string.
		return script

	def runScriptName( self ):
		"The name used for the run/queue script."
		return ( self.queueScript() if (self._queue and not self._consec) else self.runScript() )

	def launcherName( self ):
		"The Unix shell command used to launch or enqeue the run script."
		return ( self.submitCmd() if (self._queue and not self._consec) else self.runCmd() )

	#
	# Launching.
	#
	def launchConsecutively( self ):
		"""If not queueing, launches each of the runs one after the over, controlled from within this Python script.
		If queuing, generate a submit script for the base directory that calls the run scripts on each run directory."""

		if self._queue:

			# Generate a submit script that calls each of the run scripts, and launch.
			script = ""
			script += "#$ -cwd -V\n"
			script += "#$ -l h_rt={}:00:00\n".format(self._wallClock)
			script += "#$ -pe smp {}\n".format(self._numThreads)

			# Get the directory wherever this is being called from.
			cwd = os.getcwd()

			# Within this script, call all of the shell scripts in each of the run directories.
			for runDir in self._runDirs:
				script += "cd {}\n".format(runDir)
				script += "sh {}\n".format(self.runScript())
				script += "cd {}\n".format(cwd)

			# Save the submit script to the current directory.
			scriptFName = os.path.join(self.queueScript())
			with open( scriptFName, 'w' ) as f:
				f.write( script )

			# Message for verbose mode.
			subCmd = self.submitCmd()
			if self._verbose:
				print( "Saved consecutive script to '{}'.".format(scriptFName) )
				print( "Launching using '{} {}'.".format(subCmd,scriptFName) )

			# Launch.
			cProc = subprocess.run( [subCmd,scriptFName], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE )	# Python 3.6 at least.
#			cProc = subprocess.run( [subCmd,scriptFName], shell=True, capture_output=True )		# Python 3.9, but not 3.6.

		else:
		
			# Run interactively using the subprocess module.
			launch = self.launcherName()
			script = self.runScriptName()

			# Loop over all runs.
			for runDir in self._runDirs:

				if self._verbose:
					print( "Starting run script for '{}'.".format(runDir) )

				# Use subprocess.run(), which returns with a 'completedProcess' object.
				cProc = subprocess.run( [launch,script], shell=False, cwd=runDir, stdout=subprocess.PIPE, stderr=subprocess.PIPE )	# Python 3.6 at least.
	#			cProc = subprocess.run( [launch,script], shell=True, cwd=runDir, capture_output=True )		# Python 3.9, but not 3.6.

				if self._verbose:
					fullOut = (cProc.stdout+cProc.stderr).decode("UTF-8").strip()
					print( "Completed with code {} and output:\n{}\n.".format(cProc.returncode,fullOut) )

	def launchConcurrently( self ):
		"""Launches each of the runs concurrently as separate processes of the given number of threads.
		Does not use nohup, so this script will need to be running until the last one has finished (although
		can call this script _using_ nohup...)."""

		# Shorthands.
		launch = self.launcherName()
		script = self.runScriptName()

		# Store the completed process objects generated by each run.
		procs = {}

		# Loop through all runs.
		for runDir in self._runDirs:

			if self._verbose:
				print( "Starting run script for '{}'.".format(runDir) )

			# Use subprocess.Popen() rather than run() so can start processes while others have yet to complete.
			try:
				proc = subprocess.Popen( [launch,script], cwd=runDir, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
#				proc = subprocess.Popen( [launch,script], shell=True, cwd=runDir, capture_output=True )		# Python 3.9, but not 3.6.

				# Store as an associative array keyed by the run directory.
				procs[runDir] = proc

			except Exception as err:
				print( "Failed to start run script on '{}' with error '{}'.".format(runDir,err) )

		# Wait for all running processes to finish.
		for dir in procs:
			procs[dir].wait()

			if self._verbose:
				fullOut = (procs[dir].stdout.read()+procs[dir].stderr.read()).decode("UTF-8").strip()
				print( "Run on dir {} completed with code {} and output:\n{}".format(dir,procs[dir].returncode,fullOut) )

	#
	# Class methods.
	#

	# Methods that should normally be overriden.
	@classmethod
	def executable( cls ):
		"Returns with name of the executable as a string."
		return "<no exeuctable name defined>"
	
	@classmethod
	def stateFilePrefix( cls ):
		"""Prefix for saved state files. Assume files are this prefix followed by an underscore and then the
		rest of the filename without underscores (e.g. index and extension), and the archived states file will
		be this prefix followed by "s.tar.gz" (where the 's' is to make it plural)."""
		return "<no state file prefix defined>"
	
	@classmethod
	def description( cls ):
		"The description that appears when called with the help (-h) option."
		return "<no specific description of run control script>"

	@classmethod
	def extendArg( cls ):
		"""The command line argument specifier for an extension to a run, with hyphens, i.e. '-T' or '--extend',
		depending on what the executable expects. If left as 'None', extensions are not allowed."""
		return None

	# Generic parameters that can usuallu be left alone.
	@classmethod
	def maxWallClockTime( cls ):
		"Maximum wall clock time in hours."
		return 48

	@classmethod
	def runScript( cls ):
		"File name used for run scripts that will not be queued."
		return "run.sh"
	
	@classmethod
	def queueScript( cls ):
		"File name used for scripts that get submitted to a queue."
		return "submit.sh"
	
	@classmethod
	def runCmd( cls ):
		"The command line name for executing a run script."
		return "sh"
	
	@classmethod
	def submitCmd( cls ):
		"The command line executable for submit a script to a queue."
		return "qsub"


