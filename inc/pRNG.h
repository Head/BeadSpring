/**
 * @file pRNG.h
 * @author David Head
 * @brief Namespace for the pseudo-random number generator.
 */


#ifndef BEAD_SPRING_PRNG_H
#define BEAD_SPRING_PRNG_H


//
// Includes.
//
#include <random>
#include <vector>
#include <omp.h>
#include <algorithm>


// Namespace for the pseudo-random number generator. Attempts to be thread-safe and to scale well
// in parallel by using a different engine and different distributions for each OpenMP thread.
namespace pRNG
{
	/// Array of pRNG engines to match the maximum number of threads.
	extern std::vector< std::mt19937_64 > engines;

	/// Per-thread unit normal distributions, i.e. mean 0 and variance 1.
	extern std::vector< std::normal_distribution<double> > perThread_unitNormals;

	/// Per-thread standard uniform distribution on the range [0,1].
	extern std::vector< std::uniform_real_distribution<double> > perThread_standardUniforms;

	/// Returns a unit normal using the generator for the given thread.
	double unitNormal( int thread );

	/// Returns a standard unform on [0,1] using the generator for the given thread.
	double stdUniform( int thread );

	/// Create / initialise the pRNG.
	void initialise( long seed );

	/// Returns a vector of integers in the range [0,n) that have been randomly shuffled.
	std::vector<int> shuffledIndices( int n, int thread );
}

#endif
