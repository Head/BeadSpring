//
// Threader neighbour list, essentially a version of the neighbourList class that uses
// threaderCellSort as the sorter (and a sorter is now required); also, there is no
// separate handling for serial, but can be called with just one thread.
//
// Templated to dimension.
//
// Initialise with box dimensions,
// threadedNeighbourList<2> nl(nRange,refreshFreq,10,10);		// 10x10 box.
// threadedNeighbourList<3> nl(nRange,refreshFreq,10,10,3);		// 10x10x3 box.
//
// Here, 'nRange' is the range for two particles to be considered neighbours when constructing
// the neighbour list; typically 1.5-2.5 \times the maximum interaction range; and refreshFreq
// is the number of iterations before refreshing the neighbour lists (using the underlying cell
// sort). To update using positions stored as (x0,y0[,z0],x1,y1[,z2],...) for N particles:
//
//	nl.updateLists_xyz( N, X, true );
//
// ... where the final bool selects between fast, naive partitioning (based on cells) ['true'];
// and a slower, pair-based method that gives better load balancing ['false']. For homogeneous
// systems the naive version is probably okay.
//
// There is also a serial while-loop version, which may be convenient sometimes. Note that the
// neighbour lists still need to be constructed for each iteration:
//
//	unsigned long i, j;
//  while( nl.whileLoopNext(i,j) ) update( i, j, ... );		// Performs single iteration over all neighbour pairings
//
//
// Checking for missed contacts:
// ============================
//
// There is no 'online' method for checking missed contacts, in an attempt to maximise performance. Instead,
// there is a debug method that can be called that raises an exception if a contact was missed (i.e. a contact
// less than the given [maximum] interaction range was not on one of the neighbour lists). This can be called
// directly:
// 
// nl.checkForMissedContacts_xyz( N, X, checkRange );			// ... for positional as (x0,y0[,z0],x1,y1,...)
//
// Note this is in serial and hence should only be called rarely; also, it is perfectly possible to a contact
// to be missed for one or two iterations, but be re-made just prior to calling the check method; therefore it
// is not 100% certain to guarantee no contacts were ever missed (then again, such 'transient' misses would
// presumably make little difference to the results and could easily be within other errors).
//
// A danger with this strategy is that by using round numbers of iterations, the check may always take place
// just after the cell sort (in which case it cannot fail). Hence also provide this method:
//
// nl.automatedMissedContactCheck( checkFreq, checkRange );
//
// ... which checks every checkFreq refreshes, just after the new cell list is constricted, but before the
// new neighbour lists are constructed. This is the ideal time to check for missed contacts. Even though it
// is measured in refreshes rather than iterations, still recommended to keep checkFreq high as the check
// is slow.
//
// However the checks are called, the total number is stored and can be extracted at any time using
// numMissedContactChecks().
// 


#ifndef _UD_THREADED_NEIGHBOUR_LIST_H
#define _UD_THREADED_NEIGHBOUR_LIST_H


//
// Includes
//
#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <omp.h>

// Local includes
#include "threadedCellSort.h"



#pragma mark -
#pragma mark Class template definition
#pragma mark -

template< unsigned short dim >
class threadedNeighbourList
{
private:

protected:

	// The box dimensions.
	double _L[dim];
	
	// The number of updates between refreshes, and the current count.
	unsigned int _refreshFreq;
	unsigned int _refreshSince;
	
	// For the (serial) while-loop version, needs to store thread and pairing. Reset in sort().
	unsigned int  _whileLoop_pairList;
	unsigned long _whileLoop_pairNum;
	
	// Parameters for the automated missed contact check; the manual one (public) has no persistent variables.
	unsigned int _automatedCheckFreq;
	unsigned int _automatedCheckSince;
	double       _automatedCheckRange;

	// The total number of missed-contact checks, automatic or manual	
	unsigned int _totalNumChecks;

	// The neighbour lists and sizes; essentially trimmed versions of the cell-sorter lists.
	std::vector< std::vector<unsigned long> > _perThreadPairLists;
	
	// The range to be considered a neighbour (not necessarily interacting)
	double _neighbourRange;
	
	// The initialiser, called by all constructors.
	void _initialise( double range, unsigned int freq );
	
	// The cell sorter; same dimension as the neighbour list object.
	std::unique_ptr< threadedCellSort<dim> > _cellSorter;
	
	// Per-thread neighbour list generation.
	void _updateLists_xyz_perThread( unsigned long clNumPairs, const unsigned long *clPairList,
										    std::vector<unsigned long> *pairList );
	
	// Returns the distance-squared between the two particle indices, assuming fully periodic BCs, Uses _lastX_xyz for positions.
	const double *_lastX_xyz;
	double _separationSqrd( unsigned long i, unsigned long j ) const noexcept;
	
public:

	// Constructors and destructor. Call with box dimensions, and optionally the number of threads to use.
	threadedNeighbourList() {}
	threadedNeighbourList( double range, unsigned int freq, double  X, double Y );				// 2D constructor
	threadedNeighbourList( double range, unsigned int freq, double  X, double Y, double Z );	// 3D constructor
	threadedNeighbourList( double range, unsigned int freq, double *X );						// Arb. dimension.
	virtual ~threadedNeighbourList();

	// Setting and accessing parameters
	void setRefreshFreq   ( unsigned int freq );				// No. updates per (cell-sort) refresh
	void setNeighbourRange( double range      );				// Will also re-set the cell sorter

	double       neighbourRange() const noexcept { return _neighbourRange; }
	unsigned int refreshFreq   () const noexcept { return _refreshFreq;    }
	
    double ratioSmallestToLargestPairList() const noexcept;		// A measure of load balancing

	// Methods for actual usage. The "_xyz" methods assume the position vector X is in the form (x0,y0[,z0],x1,y1[,z1],...)
	             void           updateLists_xyz  ( unsigned long N, const double *X, bool naivePartition = true );
	inline       unsigned long  numPairsInList   ( unsigned int t ) const noexcept { return  _perThreadPairLists[t].size()/2; }
	inline       unsigned int   numPairLists     ()                 const noexcept { return omp_get_max_threads(); }
	inline const unsigned long* perThreadPairList( unsigned int t ) const          { return &_perThreadPairLists[t][0]; }
	
	//
	// Reading the pairs in serial, using a while loop. Convenient but slow.
	//
	bool whileLoopNext( unsigned long &i, unsigned long &j ) noexcept;
	
	//
	// Debugging
	//
	void checkForMissedContacts_xyz ( unsigned long N, const double *X, double checkRange );
	void automatedMissedContactCheck( unsigned int checkFreq, double checkRange );
	inline unsigned int numMissedContactChecks() const noexcept { return _totalNumChecks; }

	//
	// Output
	//
	template< unsigned short d >
	friend std::ostream& operator<< (std::ostream&,const threadedNeighbourList<d>&);

	//
	// Exceptions
	//
	class DimensionError;
	class BadParameter;
	class MissedContact;
};


#pragma mark -
#pragma mark Custom exceptions
#pragma mark -

template< unsigned short dim >
class threadedNeighbourList<dim>::DimensionError : public std::runtime_error
{
private:
	std::string makeWhat( std::string during, std::string why, std::string file, int line )
	{
		std::ostringstream out;
		out << "Dimension error during " << during << "; " << why 
			<< " [in file " << file << ", line " << line << "]" << std::endl;
		return out.str();
	}

public:
	DimensionError( std::string during, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(during,why,file,line) ) {}
};

template< unsigned short dim >
class threadedNeighbourList<dim>::BadParameter : public std::runtime_error
{
private:
	std::string makeWhat( std::string param, std::string why, std::string file, int line )
	{
		std::ostringstream out;
		out << "Bad value for parameter '" << param << "'; " << why 
			<< " [in file " << file << ", line " << line << "]" << std::endl;
		return out.str();
	}

public:
	BadParameter( std::string param, std::string why, std::string file, int line ) : std::runtime_error( makeWhat(param,why,file,line) ) {}
};

template< unsigned short dim >
class threadedNeighbourList<dim>::MissedContact : public std::runtime_error
{
private:
	std::string makeWhat( unsigned long i, unsigned long j, std::string file, int line )
	{
		std::ostringstream out;
		out << "Missed a contact between particles with indices " << i << " and " << j << "; "
		    << "consider increasing range/decreasing frequency, but could also be a numerical instability."
			<< " [in file " << file << ", line " << line << "]" << std::endl;
	    
		return out.str();
	}

public:
	MissedContact( unsigned long i, unsigned long j, std::string file, int line ) : std::runtime_error( makeWhat(i,j,file,line) ) {}
};


#pragma mark -
#pragma mark Initialisation and clear-up
#pragma mark -

// 2D constructor with explicit box dimensions (i.e. one argument for each).
template< unsigned short dim >
threadedNeighbourList<dim>::threadedNeighbourList( double range, unsigned int freq, double X, double Y )
{
	// Sanity check for dimension.
	if( dim==3 )
		throw DimensionError("initialisation of cell list","2D constructor for 3D object",__FILE__,__LINE__);

	// Copy over box dimensions and call the generic initialisation.
	_L[0] = X;
	_L[1] = Y;
	_initialise( range, freq );	
}

// 3D constructor with explicit box dimensions (i.e. one argument for each).
template< unsigned short dim >
threadedNeighbourList<dim>::threadedNeighbourList( double range, unsigned int freq, double X, double Y, double Z )
{
	// Sanity check for dimension.
	if( dim==2 )
		throw DimensionError("initialisation of cell list","3D constructor for 2D object",__FILE__,__LINE__);

	// Copy over box dimensions and call the generic initialisation.
	_L[0] = X;
	_L[1] = Y;
	_L[2] = Z;
	_initialise( range, freq );	
}

// Arbitrary dimension constructor.
template< unsigned short dim >
threadedNeighbourList<dim>::threadedNeighbourList( double range, unsigned int freq, double *X )
{
	// Copy over box dimensions and call the generic initialisation.
	for( auto k=0; k<dim; k++ ) _L[k] = X[k];
	_initialise( range, freq );
}

// Initialisation
template< unsigned short dim >
void threadedNeighbourList<dim>::_initialise( double range, unsigned int freq )
{
	// Only 2D and 3D supported.
	if( dim!=2 and dim!=3 )
		throw DimensionError("initialisation of cell list","only 2D and 3D supported",__FILE__,__LINE__);
	
	// Check for invalid box dimensions
	for( auto k=0; k<dim; k++ )
		if( _L[k] <= 0.0 )
			throw BadParameter("box dimensions","at least one zero or negative",__FILE__,__LINE__);
	
	// Check parameter values
	if( range <= 0.0 ) throw BadParameter("neighbour range",  "must be positive",__FILE__,__LINE__);
	if( !freq        ) throw BadParameter("refresh frequency","must be positive",__FILE__,__LINE__);

	// Store parameters
	_refreshFreq    = freq;
	_neighbourRange = range;
	
	// Initialise variables
	_refreshSince        = 0u;
	_whileLoop_pairList  = 0u;
	_whileLoop_pairNum   = 0u;
	_automatedCheckFreq  = 0u;
	_automatedCheckSince = 0u;
	_totalNumChecks      = 0u;
	_lastX_xyz           = nullptr;

	// Initialise the cell sorter.
	_cellSorter.reset( new threadedCellSort<dim>(_neighbourRange,_L) );

	// Initialise the pair lists, only ever store as one list per thread.
	_perThreadPairLists.resize( omp_get_max_threads() );
}

// Sets the range at which two particles are considered to be neighbours and included on the list
template< unsigned short dim >
void threadedNeighbourList<dim>::setNeighbourRange( double range )
{
	// Check the value
	if( range <= 0.0 )
		throw BadParameter("neighbour range","must be positive",__FILE__,__LINE__);

	// If the same value, do nothing
	if( range==_neighbourRange ) return;

	// Store the value
	_neighbourRange = range;
	
	// Re-create the cell sorter
	_cellSorter.reset( new threadedCellSort<dim>(_neighbourRange,_L) );
}

// Set the frequency of refreshing of the neighbour lists, each using a cell sort
template< unsigned short dim >
void threadedNeighbourList<dim>::setRefreshFreq( unsigned int freq )
{
	// Check not zero
	if( !freq ) throw BadParameter("refresh frequency","must be positive",__FILE__,__LINE__);
}


// Destructor
template< unsigned short dim >
threadedNeighbourList<dim>::~threadedNeighbourList()
{
	// _cellSorter deleted automatically (std::unique_ptr)
}


#pragma mark -
#pragma mark Usage
#pragma mark -

// Call this method every time the list is iterated. The first time it is called, and periodically thereafter,
// it will use the cell sorter to get per-thread cell lists. These are then trimmed to give the neighbour lists.
template< unsigned short dim >
void threadedNeighbourList<dim>::updateLists_xyz( unsigned long N, const double *X, bool naivePartition )
{
	// If _refreshSince is zero, reset to the requested frequency.
	if( !_refreshSince-- )
	{
		_refreshSince = _refreshFreq - 1;				// Note have checked _refreshFreq is not zero
		
		// If checking for missed contacts, which includes a new cell sort, don't need to do a second one here
		if( _automatedCheckFreq && !_automatedCheckSince-- )
		{
			_automatedCheckSince = _automatedCheckFreq - 1;
			checkForMissedContacts_xyz(N,X,_automatedCheckRange);
		}
		else
		{
			_cellSorter->sortParticles_xyz( N, X );
			if( naivePartition )
				_cellSorter->generatePairLists_cellPartitioning();			// Fast; poor load balancing for heterogeneous systems. 
			else
				_cellSorter->generatePairLists_pairPartitioning();			// Slow; better load balancing.
		}

		// Store the pointer to the positions for when calculating pairs that are (potentially) in range
		_lastX_xyz = X;
		
		// Add the pairs to the per-thread neighbour lists, only including those in range.
		#pragma omp parallel
		{
			auto t = omp_get_thread_num();

			unsigned long  clNumPairs = _cellSorter->numPairsInList(t);
			const unsigned long* clPairList = _cellSorter->perThreadPairList(t);

			_updateLists_xyz_perThread( clNumPairs, clPairList, &_perThreadPairLists[t] );
		}
	}

	// Reset the persistent thread/pair counts for the (serial) while-loop version, in case needed.
	_whileLoop_pairList = 0u;
	_whileLoop_pairNum  = 0u;
}

// The per-thread routine that filters the cell sorter's per-thread lists. The first two arguments are the
// cell sort per-thread list size and array, and the next two are the same for the neighbour list.
template< unsigned short dim >
void threadedNeighbourList<dim>::_updateLists_xyz_perThread(
	unsigned long clNumPairs,
	const unsigned long *clPairList,
	std::vector<unsigned long> *nList )
{
	// If the current size is zero (typically the first call), set the reserve to something probably a bit too large,
	// to minimise the chance of (slow) re-allocations later on.
	if( nList->empty() ) nList->reserve( 2*clNumPairs );		// Could also use some fraction of this
	
	// The square of the neighbour range; calculate once now, outside the loop.
	double neighbourRangeSqrd = _neighbourRange * _neighbourRange;
	
	// Copy over pairs from the cells, omitting those that are beyond the maximum range
	nList->clear();
	for( auto cp=0u; cp<clNumPairs; cp++ )
	{
		// Are the two particles overlapping? - let the (read-only) class method decide
		if( _separationSqrd(clPairList[2*cp],clPairList[2*cp+1]) < neighbourRangeSqrd )
		{
			nList->push_back( clPairList[2*cp  ] );			// push_back() should be fast if the capacity is not reached
			nList->push_back( clPairList[2*cp+1] );
		}
	}
}

// Returns the separation squared between the positions of the two particle indices, using the
// positions stored in _lastX_xyz.
template< unsigned short dim >
double threadedNeighbourList<dim>::_separationSqrd( unsigned long i, unsigned long j ) const noexcept
{
	double dx, sepnSqrd=0.0;
	
	for( auto k=0; k<dim; k++ )
	{
		// Periodic BCs; use remainder() [from math.h] in case particles drift across multiple periodic copies
		dx = remainder( _lastX_xyz[dim*i+k] - _lastX_xyz[dim*j+k], _L[k] );

		// Add to the total magnitude squared
		sepnSqrd += dx*dx;
	}
	
	return sepnSqrd;
}

// Reading in serial using a while-loop. Results in more concise code but cannot be parallelised.
// Essentially the same as the underlying cell sorter method.
template< unsigned short dim >
bool threadedNeighbourList<dim>::whileLoopNext( unsigned long &i, unsigned long &j ) noexcept
{
	// Keep on incrementing until a valid pair is found, or return false. Note that empty pair lists are possible.
	_whileLoop_pairNum++;
	while( _whileLoop_pairNum >= numPairsInList(_whileLoop_pairList) )
	{
		// Move to next list. If already on the last, return with 'false' denoting no more pairings to consider
		if( ++_whileLoop_pairList == numPairLists() ) return false;
		
		// Reset the pairing within this list to zero; if list size is zero, will loop at least once more.
		_whileLoop_pairNum = 0u;		
	}
	
	i = perThreadPairList(_whileLoop_pairList)[2*_whileLoop_pairNum  ];
	j = perThreadPairList(_whileLoop_pairList)[2*_whileLoop_pairNum+1];

	return true;
}

// Ratio of the smallest to the largest pair list sizes, to be used as a measure of
// theoretical load balancing. Returns zero if all lists zero length.
template< unsigned short dim >
double threadedNeighbourList<dim>::ratioSmallestToLargestPairList() const noexcept
{
	auto shortest=_perThreadPairLists[0].size(), longest=shortest;
	for( auto pl=1; pl<numPairLists(); pl++ )
	{
		if( shortest>_perThreadPairLists[pl].size() ) shortest=_perThreadPairLists[pl].size();
		if( longest <_perThreadPairLists[pl].size() ) longest =_perThreadPairLists[pl].size();
	}
	
	if( longest )
		return static_cast<double>(shortest)/longest;
	else
		return 0.0;
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Check for missed interactions. Slow; performs a new cell sort and compares, in serial, any within the given range
// to the current neighbour lists. If any are missing, raises an exception.
// Option to not construct the cell lists.
template< unsigned short dim >
void threadedNeighbourList<dim>::checkForMissedContacts_xyz( unsigned long N, const double *X, double checkRange )
{
	// Check for sensible parameter values
	if( checkRange<=0.0             ) throw BadParameter("check range","must be positive",__FILE__,__LINE__);
	if( checkRange> _neighbourRange ) throw BadParameter("check range","must be less than the neighbour range",__FILE__,__LINE__);

	// Increment the total check count
	_totalNumChecks++;
	
	// Calculate the square of the (maximum) interaction range now, outside the loop
	double checkRangeSqrd = checkRange * checkRange;
	
	// Re-construct the cell sort; use the naive partitioning (will soon serialise the lists anyway)
	_cellSorter->sortParticles_xyz( N, X );
	_cellSorter->generatePairLists_cellPartitioning();
	_lastX_xyz = X;
	
	// Go through each potential contact in the cell lists, in serial; find those closer together than the given check range
	unsigned long i, j;
	while( _cellSorter->whileLoopNext(i,j) )
		if( _separationSqrd(i,j) < checkRangeSqrd )
		{
			// Check all neighbour lists as they existed prior to calling this method; see if this pairing exists on one of them
			bool hasNeighbourPair = false;
			
			for( auto t=0u; t<numPairLists(); t++ )
			{
				for( auto p=0u; p<numPairsInList(t); p++ )
				{
					if(
						( perThreadPairList(t)[2*p]==i && perThreadPairList(t)[2*p+1]==j )
						||
						( perThreadPairList(t)[2*p]==j && perThreadPairList(t)[2*p+1]==i )
					)
					{
						hasNeighbourPair = true;
						break;
					}
				}
				
				if( hasNeighbourPair ) break;				// Cascade of breaks to finish as soon as possible
			}
			
			// If here and hasNeighbourPair==false, there was no neighbour-pairing for the given interaction.
			if( !hasNeighbourPair ) throw MissedContact(i,j,__FILE__,__LINE__);
		}
}

// Set up an automated check during reconstruction of the neighbour lists
template< unsigned short dim >
void threadedNeighbourList<dim>::automatedMissedContactCheck( unsigned int checkFreq, double checkRange )
{
	// A frequency of 'zero' means no automated checking, i.e. to switch it off
	if( !checkFreq ) { _automatedCheckFreq = 0u; return; }
	
	// A positive value means checking is now on; check parameter values
	if( checkRange<=0.0             ) throw BadParameter("check range","must be positive",__FILE__,__LINE__);
	if( checkRange> _neighbourRange ) throw BadParameter("check range","must be less than the neighbour range",__FILE__,__LINE__);

	// Store the parameters and reset the count
	_automatedCheckFreq  = checkFreq;
	_automatedCheckSince = checkFreq-1;			// NB: Don't check straight away
	_automatedCheckRange = checkRange;
}



#pragma mark -
#pragma mark Output
#pragma mark -

template< unsigned short d >
std::ostream& operator<< ( std::ostream &os, const threadedNeighbourList<d> &nl )
{
	// Generic message
	os << "Multi-threaded neighbour list:" << std::endl;
	
	// Neighbour list-specific parameters
	os << " - neighbour range: " << nl._neighbourRange << std::endl;
	os << " - refresh frequency: every " << nl._refreshFreq << " iteration(s)" << std::endl;

	// Automated missed-contact checking; also warn if the frequency looks high.
	if( nl._automatedCheckFreq )
	{
		os << " - will check for missed contacts (i.e. within a range " << nl._automatedCheckRange
		   << ") every " << nl._automatedCheckFreq << " refresh(es)";
		if( nl._automatedCheckFreq<100 ) os << " [WARNING: High frequencies can drastically reduce performance]";
		os << "." << std::endl;
	}
	else
		os << " - no automated check for missed contacts." << std::endl;
	
	// The cell sorter; just echo its output method (which also mentions no. threads and box dimensions)
	os << " - cell sorter: " << *nl._cellSorter;
		
	return os;
}



#endif
