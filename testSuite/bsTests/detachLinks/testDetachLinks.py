#
# Tests for detachment of links, either at a given rate, or for a maximum length.
#

# Standard imports
import numpy as np
import math
import scipy.optimize
import matplotlib.pyplot as plt

# Base class
from bsTests import bsTestBase


class testDetachLinks( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, state, extend ):

		# Store private variables.
		self.__stateFile  = state
		self.__extendTime = extend

		# Call parent method with required flags.
		super(testDetachLinks,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Sanity check(s).
		if self._table.getParameter("M") > 1:
			raise NotImplementedError( "Detach links test assumes a monomeric system (i.e. M=1)" )

		# Get parameters. 'None' if not defined.
		kD            = self._table.getParameter( "kD" )
		kA            = self._table.getParameter( "kA" )
		maxLinkLength = self._table.getParameter( "maxLinkLength" )

		#
		# Check for the detach rate with no new links, or link breakage due to exceeding a maximum length.
		#
		if kD:
			if kA or maxLinkLength:
				raise NotImplementedError( "Cannot test for detach rate kD with attach rate kA or maxLinkLength also defined" )

			# Get no. links per time. Note that t[0] is not 0.0 as this is a continuation of another run.
			t, numLinks = [ self._table.getDataSets("numLinks")["curves"][0][label] for label in ("x","y") ]

			# Fit to an exponential decay (after taking log) to relative change in number of links, so y(0)=1.
			def expDecay( x, a ):
				return - a * np.array(x)
			
			fit = scipy.optimize.curve_fit( expDecay, np.array(t)-t[0], np.log(np.array(numLinks)/numLinks[0]), p0=[kD] )		# First guess is expected solution.
			a, da = fit[0][0], math.sqrt(fit[1][0][0])

			if self._verbose:
				print( "Exponential fit returned exponent a = {0}({1})".format(a,da) )

			kD_sigma = testDetachLinks.detachRateSigma()				# Shorthand.
			if self._plot:
				plt.plot( t, [y/numLinks[0] for y in numLinks], "ro", label="data" )
				plt.plot( t, np.exp(-a*(np.array(t)-t[0])), "r-", label="exp. fit" )

				plt.fill_between( t, np.exp(-(a+kD_sigma*da)*(np.array(t)-t[0])), np.exp(-(a-kD_sigma*da)*(np.array(t)-t[0])), color="r", alpha=0.25 )

				plt.xlabel( "time, including pre-run" )
				plt.ylabel( "nLinks / nLinks(t0)")

				plt.semilogy()
				plt.legend()

				plt.show()

			# Test. Note there might be a small systematic error as output time points correspond to ends of bins; should really shift to middles of bins.
			if abs(a-kD) > kD_sigma * da:
				if self._verbose:
					print( "Measure detach rate {0} differs from expected value {1} by more than {2} sigma.".format(a,kD,kD_sigma) )
				raise bsTestBase.FailedTestError( "fitted link detach rate deviated significantly from the expected value" )
			else:
				if self._verbose:
					print( "Measure detach rate {0} within {2} sigma of expected value {1}.".format(a,kD,kD_sigma) )

		#
		# Check for maximum link length: Assume so small that all links are instantly broken. Assume no attachment or (constant rate) detachment.
		#
		if maxLinkLength:
			if kA or kD:
				raise NotImplementedError( "Cannot test for maximum link length with attach rate kA or detach rate kD defined" )

			# Get no. links per time and detach rate. Time axis no needed for the checks, just for plotting.
			t, numLinks = [ self._table.getDataSets("numLinks"  )["curves"][0][label] for label in ("x","y") ]
			detachRate  =   self._table.getDataSets("rateDetach")["curves"][0]["y"]

			# Since this is a continuation run with no previous table output, all times correspond to times after all links have been broken.
			for n in numLinks:
				if n>0.0:
					if self._verbose: print( "Non-zero links: Table row was {0} (for times {1}).".format(numLinks,t) )
					raise bsTestBase.FailedTestError( "Found links after instanteneous detachment was switched on." )
			
			if self._verbose:
				print( "No links after instantaneous detachment switched on." )
			
			if self._plot:
				plt.plot( t, numLinks, "bo-" )

				plt.xlabel( "time, including pre-run" )
				plt.ylabel( "number of links" )

				plt.show()
			
			# Also check the detach rate was initially "high", to confirm there was some detachment.
			for i, dRate in enumerate(detachRate):
				if (i==0 and dRate<=0.0) or (i>0 and dRate!=0.0):
					if self._verbose:
						print( "Detach rate not all in first time bin as expected; table row was {}.".format(detachRate) )
						raise bsTestBase.FailedTestError( "Detach rate not high (after instantaneous detachment switched on) followed by zeros as expected." )
			
			if self._verbose:
				print( "Detach rate high (after instantaneous detachment switched on) followed by zeros as expected." )

			if self._plot:
				plt.plot( t, detachRate, "bo-" )

				plt.xlabel( "time, including pre-run" )
				plt.ylabel( "detach rate" )

				plt.show()


	# Arguments for the executable
	def _stateFile ( self ): return self.__stateFile
	def _extendTime( self ): return self.__extendTime
	def _numThreads( self ): return 1

	# Cleanup; can use wildcards
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]

	# Parameters used in checks.
	@classmethod
	def detachRateSigma( cls ):				# Maximum sigma for the detach rate kD. Generous as small run on a single system.
		return 50



	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		# All tests start frm a given state. The extension time depends on the parameter being tested.
		for testNum in range(2):
			fname = "state_test{}.xml".format(testNum)
			print( "Performing detach links test starting from file '{}'".format(fname) )
			testDetachLinks(executable,verbosity,plot,fname,(10.0,1.0)[testNum])		# Last arg is extension time.
			print( " - passed" )
