/**
 * @file dataBoxLEOscillatory.h
 * @author David Head
 * @brief Auxiliary class for boxLEOscillatory that performs the various data gathering operations.
 */


#ifndef BEAD_SPRING_DATA_BOX_LE_OSCILLATORY_H
#define BEAD_SPRING_DATA_BOX_LE_OSCILLATORY_H


//
// Includes.
//

// Standard includes.
#include <cmath>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "dataBeadSpring.h"


//
// Prototypes.
//
class boxLEOscillatory;


/// Auxiliary class for boxLEOscillatory that performs the various data gathering operations.
class dataBoxLEOscillatory
{

private:

	// Data output.
	constexpr static auto __dataLabel_Gp  = "Gp";
	constexpr static auto __dataLabel_Gpp = "Gpp";

	// State file I/O.
	constexpr static auto __saveIntSigmaSinTag = "CurentIntSigmaSin";
	constexpr static auto __saveIntSigmaCosTag = "CurentIntSigmaCos";

	constexpr static auto __saveLastStressValueTag = "CurentLastStressValue";
	constexpr static auto __saveLastStressTimeTag  = "CurentLastStressTime";

	constexpr static auto __saveLastGpTag  = "CurentLastGp";
	constexpr static auto __saveLastGppTag = "CurentLastGpp";

	constexpr static auto __saveValueAttr = "value";

protected:

	short _dim;

	const boxLEOscillatory *_boxLEOsc;

	// For evaluating the complex modulus during runtime.
	double _intSigmaSin = 0.0;
	double _intSigmaCos = 0.0;

	double _lastStressValue =  0.0;
	double _lastStressTime  = -1.0;

	double _lastGp  = 0.0;
	double _lastGpp = 0.0;
	
	
	/// The data object for the whole project.
	std::shared_ptr<dataBeadSpring> _dataBeadSpring;

public:

    dataBoxLEOscillatory( const boxLEOscillatory *boxLEOsc, std::shared_ptr<dataBeadSpring> data, short dim );
    virtual ~dataBoxLEOscillatory() {}
    
    //
    // Runtime calculation of the complex shear modulus (assuming linearity).
    //
    void update( double t, double dt );	

	//
	// Save state I/O.
	//
	virtual void saveBoxState   ( flatXMLParser *xml ) const;
	virtual void recoverBoxState( flatXMLParser *xml );
};



#endif

