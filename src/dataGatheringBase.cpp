//
// Implementation
//

#include "dataGatheringBase.h"


#pragma mark -
#pragma mark Custom exceptions
#pragma mark -

// Attempted to add a label not defined at initialisation
class dataGatheringBase::NotPredefinedLabel : public runtime_error
{
private:
	string __makeWhat( string type, string label, string file, int line ) {
		ostringstream out;
		out << "Attempted to add " << type << " label '" << label << "', which was not in the first output"
			<< " [in file " << file << ", line " << line << "]" << endl;
		return out.str();
	}
public:
	NotPredefinedLabel( string type, string label, string file, int line ) : runtime_error( __makeWhat(type,label,file,line) ) {}
};

// Parameters for block data were bad, in some senze
class dataGatheringBase::BadBlockDataParameters : public runtime_error
{
private:
	string __makeWhat( string label, string reason, string file, int line )
	{
		ostringstream out;
		out << "Bad block data parameter(s) for label '" << label << "'; reason: " << reason
			<< " [in file " << file << ", line " << line << "]" << endl;
		return out.str();
	}
public:
	BadBlockDataParameters( string label, string reason, string file, int line ) : runtime_error( __makeWhat(label,reason,file,line) ) {}
};




#pragma mark -
#pragma mark Defining labels with no data
#pragma mark -

// Cumulative scalars
void dataGatheringBase::_addCumulativeScalarLabel( string label )
{
	// Check does not already exist
	if( __exportedFirstScalars )
		throw dataGatheringBase::NotPredefinedLabel( "cumulative scalar", label, __FILE__, __LINE__ );

	__cumulativeTotals[label] = 0.0;
}

// PDFs - also need the bin width. Does not check block data has not already been output,
// as for scalars (with __exportedFirstScalars) - could test for this as well? (see above)
void dataGatheringBase::_addPDFLabel( string label, double binWidth )
{
	// See if this label has already exists in the map (NB: may be empty)
	map<string,double>::const_iterator c_iter=__PDFBinWidths.find(label);
	if( c_iter==__PDFBinWidths.end() ) __PDFs[label];		// Creates an empty map
	
	// If just created an empty map, or empty following a previous output, (re)set the scale
	if( __PDFs[label].empty() )
	{
		// Check the scale is meaningful
		if( binWidth < DBL_EPSILON )
			throw dataGatheringBase::BadBlockDataParameters(label,"PDF bin width not positive", __FILE__, __LINE__ );

		__PDFBinWidths[label] = binWidth;					// Stores the bin width until the next output
	}
}



#pragma mark -
#pragma mark Updating data
#pragma mark -

// Updating instantanous scalar quantities; can weight if necessary (defaults to 1.0)
void dataGatheringBase::_instantaneousValue( string label, double val, double weight )
{
	// Try to find the label
	map<string,double>::const_iterator c_iter = __scalarsTotals.find(label);
	
	// Once a table row has been output, it must be in the list (else the table will be corrupted)
	if( c_iter == __scalarsTotals.end() )
	{
		if( __exportedFirstScalars )
			throw NotPredefinedLabel( "instantaneous scalar", label, __FILE__, __LINE__ );
		else
		{
			__scalarsTotals [label] = weight * val;
			__scalarsWeights[label] = weight;
			return;
		}
	}

	// Label already exists; add to total and weighting
	__scalarsTotals [label] += weight * val;
	__scalarsWeights[label] += weight;
}

// Updating cumulative scalar quantities. No weighting here as it is an integrated quantity.
void dataGatheringBase::_cumulativeValue( string label, double val )
{
	// Try to find the label; should have been supplied with "addCumulativeScalarLabel()",
	// so no option to create it on-the-fly (as with instantanous values)
	map<string,double>::const_iterator c_iter = __cumulativeTotals.find(label);
	if( c_iter == __cumulativeTotals.end() )
		throw NotPredefinedLabel( "cumulative scalar", label, __FILE__, __LINE__ );

	// Label already exists; add to total
	__cumulativeTotals[label] += val;
}

// Update extremal values; similar to _instantaneousValue()
void dataGatheringBase::_maximalValue( string label, double val )
{
	map<string,double>::const_iterator c_iter = __maximums.find(label);
	
	if( c_iter == __maximums.end() )
	{
		if( __exportedFirstScalars )
			throw NotPredefinedLabel( "maximal scalar", label, __FILE__, __LINE__ );
		else
		{
			__maximums[label] = val;
			return;
		}
	}

	// Label already exists; update
	if( __maximums[label] < val ) __maximums[label] = val;
}

void dataGatheringBase::_minimalValue( string label, double val )
{
	map<string,double>::const_iterator c_iter = __minimums.find(label);
	
	if( c_iter == __minimums.end() )
	{
		if( __exportedFirstScalars )
			throw NotPredefinedLabel( "minimal scalar", label, __FILE__, __LINE__ );
		else
		{
			__minimums[label] = val;
			return;
		}
	}

	// Label already exists; update
	if( __minimums[label] > val ) __minimums[label] = val;
}

// Block data; returns true if point added. Range and no. bins provided for each call, but only used when initialising the
// arrays; currently there is no check that the values haven't changed, but the stored (=initial) parameter values are always used.
bool dataGatheringBase::_addToFixedSizeHist( string label, double abscissa, double value, double weight, double minAbscissa, double maxAbscissa, int numBins )
{
	// Check to see if label exists
	auto c_iter=__fixedSizeHist_abscissae.find(label);
	if( c_iter == __fixedSizeHist_abscissae.end() )
	{
		// Sanity check for the provided parameters
		if( minAbscissa > maxAbscissa - DBL_EPSILON )
			throw dataGatheringBase::BadBlockDataParameters(label,"min. abscissa equal to or greater than the max. abscissa", __FILE__, __LINE__ );
		if( numBins < 1 )
			throw dataGatheringBase::BadBlockDataParameters(label,"zero (or negative) no. of bins; must be at least 1", __FILE__, __LINE__ );

		// Initialise parameters corresponding to this label, and the arrays
		__fixedSizeHist_minAbscissae[label] = minAbscissa;
		__fixedSizeHist_maxAbscissae[label] = maxAbscissa;

		__fixedSizeHist_abscissae[label].resize(numBins,0.0);
		__fixedSizeHist_values   [label].resize(numBins,0.0);
		__fixedSizeHist_weights  [label].resize(numBins,0.0);
	}

	// Determining the point; used stored values, not the arguments, so subsequent calls can ignore the last 3 args. if necessary.
	int num = __fixedSizeHist_abscissae[label].size();
	double
		min = __fixedSizeHist_minAbscissae[label],
		max = __fixedSizeHist_maxAbscissae[label],
		wid = ( max - min ) / num;
	int pt  = (int) floor( (abscissa-min)/wid );			// Always rounds down

	if( pt<0 || pt>=num ) return false;						// Outside range; returns false

	__fixedSizeHist_abscissae[label][pt] += weight*abscissa;
	__fixedSizeHist_values   [label][pt] += weight*value;
	__fixedSizeHist_weights  [label][pt] += weight;

	return true;
}

// Same as above, but acts on whole arrays at a time; no return
void dataGatheringBase::_addToFixedSizeHist( string label, double *absArray, double *valArray, long N, double weight,
												double minAbscissa, double maxAbscissa, int numBins )
{
	// See above for comments
	map< string,vector<double> >::const_iterator c_iter=__fixedSizeHist_abscissae.find(label);
	if( c_iter == __fixedSizeHist_abscissae.end() )
	{
		if( minAbscissa > maxAbscissa - DBL_EPSILON )
			throw dataGatheringBase::BadBlockDataParameters(label,"min. abscissa equal to or greater than the max. abscissa", __FILE__, __LINE__ );
		if( numBins < 1 )
			throw dataGatheringBase::BadBlockDataParameters(label,"zero (or negative) no. of bins; must be at least 1", __FILE__, __LINE__ );

		__fixedSizeHist_minAbscissae[label] = minAbscissa;
		__fixedSizeHist_maxAbscissae[label] = maxAbscissa;

		__fixedSizeHist_abscissae[label].resize(numBins,0.0);
		__fixedSizeHist_values   [label].resize(numBins,0.0);
		__fixedSizeHist_weights  [label].resize(numBins,0.0);
	}

	// Precalculate bin quantities; also pointers to the required STL vectors
	int num = __fixedSizeHist_abscissae[label].size();
	double
		min = __fixedSizeHist_minAbscissae[label],
		max = __fixedSizeHist_maxAbscissae[label],
		wid = ( max - min ) / num;
	vector<double>
		*histAbs = &__fixedSizeHist_abscissae[label],
		*histVal = &__fixedSizeHist_values   [label],
		*histWei = &__fixedSizeHist_weights  [label];

	// Loop over all points
	int pt;
	for( long i=0; i<N; i++ )
	{
		pt  = (int) floor( (absArray[i]-min)/wid );			// Always rounds down

		if( pt<0 || pt>=num ) return;	

		histAbs->at(pt) += weight*absArray[i];
		histVal->at(pt) += weight*valArray[i];
		histWei->at(pt) += weight;
	}
}


// Adds a point to a PDF; stores persistently as a map and outputs full range. Scale set the first
// time this is called since the last output.
void dataGatheringBase::_addToPDF( string label, double value, double binWidth )
{
	// Create new (empty) PDF with the given binWidth, if not yet specified.
	_addPDFLabel( label, binWidth );
	
	// Add to the PDF (which we now know must exist)
	int bin = int( 0.5 + value/__PDFBinWidths[label] );		// Round up
	__PDFs[label][bin]++;
}





#pragma mark -
#pragma mark Export
#pragma mark -

void dataGatheringBase::_nextTableRow_instantaneous( scalarTable *table, bool clearAfterOutput, double overallFactor )
{
	// If no quantities have been output, bail now
	if( __scalarsTotals.empty() ) return;

	// Output instantanous scalars to table; need to divide by total weighting; if zero total weight, output zero
	for( map<string,double>::const_iterator c_iter=__scalarsTotals.begin(); c_iter!=__scalarsTotals.end(); c_iter++ )
	{
		double
			total  = c_iter->second,
			weight = __scalarsWeights[c_iter->first];

		table->updateCurrentRow( c_iter->first, overallFactor*( weight ? total/weight : 0.0 ) );
	}

	// Set instantanous values and weights to zero
	if( clearAfterOutput )
		for( map<string,double>::const_iterator c_iter=__scalarsTotals.begin(); c_iter!=__scalarsTotals.end(); c_iter++ )
			{ __scalarsTotals[c_iter->first] = 0.0; __scalarsWeights[c_iter->first] = 0.0; }
}

void dataGatheringBase::_nextTableRow_cumulative( scalarTable *table, bool clearAfterOutput, double overallFactor )
{
	// If no quantities have been output, bail now
	if( __cumulativeTotals.empty() ) return;

	// Output cumulative scalars to table; need to weight by total time since last output
	for( map<string,double>::const_iterator c_iter=__cumulativeTotals.begin(); c_iter!=__cumulativeTotals.end(); c_iter++ )
		table->updateCurrentRow( c_iter->first, overallFactor*c_iter->second );

	// Set cumulative values to zero
	if( clearAfterOutput )
		for( map<string,double>::const_iterator c_iter=__cumulativeTotals.begin(); c_iter!=__cumulativeTotals.end(); c_iter++ )
			__cumulativeTotals[c_iter->first] = 0.0;
}

void dataGatheringBase::_nextTableRow_extrema( scalarTable *table, bool clearAfterOutput )
{
	for( map<string,double>::const_iterator c_iter=__maximums.begin(); c_iter!=__maximums.end(); c_iter++ )
	{
		table->updateCurrentRow( c_iter->first, c_iter->second );
		if( clearAfterOutput ) __maximums[c_iter->first] = - DBL_MAX;
	}

	for( map<string,double>::const_iterator c_iter=__minimums.begin(); c_iter!=__minimums.end(); c_iter++ )
	{
		table->updateCurrentRow( c_iter->first, c_iter->second );
		if( clearAfterOutput ) __minimums[c_iter->first] = DBL_MAX;
	}
}

// Export scalars to a Table object
void dataGatheringBase::outputNextTableRow( scalarTable *table )
{
	// Call protected methods for each data type (to allow derived classes to override each individually)
	_nextTableRow_instantaneous(table);
	_nextTableRow_cumulative   (table);
	_nextTableRow_extrema      (table);

	// From now on, instantaneous scalar labels must match those already present
	__exportedFirstScalars = true;
}

void dataGatheringBase::outputNextBlockData( flatXMLParser *xml )			// Single time slice
{
	//
	// Fixed-size histograms, which have weighting factors
	//
	for( map< string, vector<double> >::const_iterator c_iter=__fixedSizeHist_abscissae.begin(); c_iter!=__fixedSizeHist_abscissae.end(); c_iter++ )
	{
		int
			numBins = (int)__fixedSizeHist_abscissae[c_iter->first].size();

		double
			minAbscissa = __fixedSizeHist_minAbscissae[c_iter->first],
			maxAbscissa = __fixedSizeHist_maxAbscissae[c_iter->first],
			binWidth    = ( maxAbscissa - minAbscissa ) / numBins;

		histogram<1,1> hist(binWidth);
		hist.setOrigin( minAbscissa );
		
		for( int bin=0; bin<numBins; bin++ )
		{
			double
				abs = __fixedSizeHist_abscissae[c_iter->first][bin],
				val = __fixedSizeHist_values   [c_iter->first][bin],
				wgt = __fixedSizeHist_weights  [c_iter->first][bin];

			if( wgt )
				hist.newPoint( abs/wgt, val/wgt );
			else
				hist.newPoint( minAbscissa + (bin+0.5)*binWidth, 0.0 );			// Nominal abscissa, including both end points exactly
		}

		hist.exportXML(*xml->getOutputStream(),c_iter->first);
		
		// Reset the arrays
		for( int bin=0; bin<numBins; bin++ )
		{
			__fixedSizeHist_abscissae[c_iter->first][bin] = 0.0;
			__fixedSizeHist_values   [c_iter->first][bin] = 0.0;
			__fixedSizeHist_weights  [c_iter->first][bin] = 0.0;
		}		
	}		

	//
	// Probability density functions.
	//
	for( map<string,double>::iterator iter=__PDFBinWidths.begin(); iter!=__PDFBinWidths.end(); iter++ )
	{
		// Readable local-scope shorthands
		string label    = iter->first;
		double binWidth = iter->second;

		// Create histogram 
		histogram<1,1> hist(binWidth);
		hist.setOrigin(0.0);

		// Get the total number of points added, the first bin index and the last bin index
		long total = 0, firstBin = -1, lastBin = -1;
		for( map<int,long>::const_iterator c_iter=__PDFs[label].begin(); c_iter!=__PDFs[label].end(); c_iter++ )
		{
			total += c_iter->second;
			if( firstBin==-1 || c_iter->first < firstBin ) firstBin = c_iter->first;
			if( lastBin ==-1 || c_iter->first > lastBin  ) lastBin  = c_iter->first;
		}
		
		// If some points added in this time interval, create histogram bin-by-bin, normalised so that the integral is unity
		// Output empty bins as zero.
		if( total )
		{
			double norm = 1.0 / ( total*binWidth );
			for( long i=firstBin; i<lastBin+1; i++ )
				if( __PDFs[label].find(i) == __PDFs[label].end() )		// No data for this bin
					hist.newPoint( binWidth*i, 0.0 );
				else
					hist.newPoint( binWidth*i, norm*__PDFs[label][i] );

			// Export as XML. Empty histograms will output nothing; works fine with current Python scripts.
			hist.exportXML( *xml->getOutputStream(), label );
		}
		// Currently, if no points, no output. This should not cause problems as the Python scripts can handle
		// irregular block data arrays (unlike the tables, for which each label must appear at each time).
		
		// Clear the map; keep binWidth for this label (will be reset at the next call to _addToPDF)
		__PDFs[label].clear();
	}
}


#pragma mark -
#pragma mark Label construction
#pragma mark -

// Static methods
string dataGatheringBase::_label( string base, string suffix )
{
	ostringstream out;
	out << base << suffix;
	return out.str();
}
string dataGatheringBase::_label( string base, int num )
{
	ostringstream out;
	out << base << num;
	return out.str();
}
string dataGatheringBase::_label( string base, int num, string suffix )
{
	ostringstream out;
	out << base << num << suffix;
	return out.str();
}
string dataGatheringBase::_label( string base, string mid, int num )
{
	ostringstream out;
	out << base << mid << num;
	return out.str();
}
string dataGatheringBase::_label( string base, string mid, int num, string suffix )
{
	ostringstream out;
	out << base << mid << num << suffix;
	return out.str();
}
string dataGatheringBase::_label( string base, int num1, string mid, int num2 )
{
	ostringstream out;
	out << base << num1 << mid << num2;
	return out.str();
}

string dataGatheringBase::_label( string base, string mid, string suffix, int num )
{
	ostringstream out;
	out << base << mid << suffix << num;
	return out.str();	
}
