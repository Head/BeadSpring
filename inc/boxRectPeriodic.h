/**
 * @file boxRectPeriodic.h
 * @author David Head
 * @brief Rectangular box in 2 or 3 dimensions, with periodic wrapping at the boundaries.
 */


#ifndef BEAD_SPRING_RECTANGULAR_PER_BC_H
#define BEAD_SPRING_RECTANGULAR_PER_BC_H


//
// Includes.
//

// Standard includes.
#include <string>
#include <vector>
#include <stdexcept>
#include <cmath>
#include <omp.h>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "boxBase.h"			// Parent/base class
#include "logging.h"
#include "pRNG.h"
#include "parameterParser.h"


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	// Label for this box type.
	const std::string boxType_rectPer = "rectPeriodic";

	// Box dimensions.
	const std::string boxRectPerBC_X = "X";
	const std::string boxRectPerBC_Y = "Y";
	const std::string boxRectPerBC_Z = "Z";
}


/**
 * @brief Rectangular box in 2 or 3 dimensions, with periodic wrapping at the boundaries.
 */
class boxRectPeriodic : public boxBase
{

private:

	constexpr static auto __root3over2      = 0.8660254037844;
	constexpr static auto __ICTriLatticeTol = 1e-4;

protected:

	std::vector<double> _X;						///< Box dimensions. Expanded to size _dim in constructor.

public:

	// Constructors / destructor.
	boxRectPeriodic() {}
	boxRectPeriodic( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );
	virtual ~boxRectPeriodic() {}

	//
	// Initisalisation.
	//

	/// Set up the cell sorter and the NN/Verlet list.
	virtual void initialiseCells( double maxRange ) override;

	/// Random position somewhere in the box. Use parent method for orientation (default is fine).
	virtual void randomPosition( double *x ) const noexcept override;

	/// For specific initial configurations.
	virtual void geometryTriLattice( int N, double &a, int &numX, int &numY ) const override;

	//
	// Wrapping functions.
	//
	virtual bool hasPeriodicBoundaries() const noexcept override { return true; }			///< Always returns `true`.

	/**
	 * @brief Wraps x[d] in-place to origin-centred cell.
	 */
	virtual void wrap( double *x ) const noexcept override;

	/**
	 * @brief Calculates in-place the offset to get `x` into the primary cell.
	 * 
	 * Calculates (but does not apply) the offset required to get the passed point into the primary cell,
	 * which can then be added repeatedly to *e.g.* all beads on a filament without re-calculation.
	 */
	virtual void offsetToPrimaryCell( const double *x, double *offset ) const noexcept override;

	/**
	 * @brief Normalised periodic shift in the given direction.
	 *
	 * Returns the normalised (*i.e.* in units of edge length) periodic shift for the given
	 * relative coordinate along the same direction, *i.e.* a `relPosn` of $X$ would return 1,
	 * whereas a `relPosn` of $-X$ would return -1 for `axis==0` - similarly for `axis==1` ($Y$)
	 * and (if 3D) `axis==2` ($Z$).
	 */
	virtual int nmlsdPerShiftInDirn( int axis, double relPosn ) const override;

	//
	// Accessors.
	//

	/// Volume of the box (or area if 2D).
	virtual double volume() const noexcept override { return _X[0] * _X[1] * ( _dim==3 ? _X[2] : 1.0 ); }

	/// Shortest box edge length.
	virtual double shortestDimension() const noexcept override { return *std::min_element(_X.begin(),_X.end()); }

	/// Longest box edge length (*i.e.* ignoring diagonals).
	virtual double longestDimension () const noexcept override { return *std::max_element(_X.begin(),_X.end()); }
};



#endif
