//
// Base class for data-gathering objects
//
// Handles repetitive idioms such as time-averaging in as transparent a manner
// as possible. These routines are typically protected; the intention is that this
// class will be overriden by one specific to the problem in question.
//
//
// Auxiliary routines:
// ==================
//
// - Label construction, i.e. of the form <string><integer><string>, are supported by a range
//   of _label() routines with arguments corresponding to the order of the label components.
//
// - Conversion from symmetric (i,j) indices [with i<=j] to a sequential index k is handled by
//   a number of routines with the prefix '_symToSeq_'. Note however these have not been
//   fully checked.
//
//
// Scalars:
// =======
//
// Currently two types of scalar supported; "instantaneous" and "cumulative". The first gives instantanous
// values that are averaged over time between output; the second gives cumulative values between outputs.
//
// Weighting is allowed for instantaneous scalars but not cumulative ones, since these are time-integrated
// quantities [scalaing by a total weight amounts to a time average]; instead, normalisation to time must
// be handled during output by overriding the _nextTableRow_cumulative() method.
//
// For all scalars, the list of labels (map keys) must be defined from the start and kept constant, even
// if this means outputtting zeros when there is no data. This may include t=0, which may be a problem
// for cumulative scalars (which may not have registered any values at t=0 and therefore will not have a
// label). For this reason, the labels should be given during construction by the method _addCumulativeScalarLabel().
//
// 2013/6/1/DAH: New scalar types 'maximal' and 'minimal' added; stores and outputs the extremal value.
//
// Block data:
// ==========
//
// Fixed-size histograms are currently supported, accessed using the method:
//_addToFixedSizeHist( string label, double abscissa, double value, double weight=1.0,
//									  double minAbscissa=0.0, double maxAbscissa=1.0, int numBins=10 );
// - which returns True if the point was added. The final 3 arguments are only needed the first time round
//   and set the range; they will not be checked subsequently so any values can be used after these first.
//
// PDFs are also now supported, where it is possible to use variable bin widths - the first value will be
// used for the remainder of the data measurement period (until the next output).
//
// Data extraction:
// ===============
//
// Performed by two routines: nextTableRow(Table*) and nextBlockData(flatXMLParser*), which take pointers to
// custom objects Table and flatXMLParser.
//




#ifndef _BIOFILM_DATA_GATHERING_BASE_H
#define _BIOFILM_DATA_GATHERING_BASE_H


//
// Includes.
//

// Standard includes
#include <map>
#include <string>
#include <stdexcept>
#include <vector>
#include <sstream>
#include <math.h>
#include <float.h>

// User-defined objects
#include "scalarTable.h"
#include "flatXMLParser.h"
#include "histogram.h"


using namespace std;


class dataGatheringBase
{

private:

	//
	// Scalars [output in table form]
	//
	
	// Storage of scalar quantities; total and weighting (i.e. number) added for the same string labels
	// Labels must be defined in advance, to ensure same order and data output for all times; if not
	// point added during the time interval, it is output with nominal value 0.0.
	map<string,double> __scalarsTotals;
	map<string,double> __scalarsWeights;
	bool __exportedFirstScalars;

	map<string,double> __cumulativeTotals;				// No weights for cumulative scalars [see text at top of .h file]

	map<string,double> __maximums;						// Extremums
	map<string,double> __minimums;

	//
	// Block data [histograms, PDFs; will expand range of options as needed]
	//
	
	// Fixed-size histograms; can be incremented with variable weights
	map< string,vector<double> > __fixedSizeHist_abscissae;			// Can determine no. pts [= no. bins+1] from the sizes of these arrays
	map< string,vector<double> > __fixedSizeHist_values;
	map< string,vector<double> > __fixedSizeHist_weights;
	map< string,       double  > __fixedSizeHist_minAbscissae;
	map< string,       double  > __fixedSizeHist_maxAbscissae;
	
	// 1D probability distribution functions; elastic (i.e. bin size and number can vary between outputs)
	map<string, map<int,long> > __PDFs;
	map<string, double > __PDFBinWidths;

protected:

	//
	// Common idioms for label construction; i.e. appending integers to strings etc.
	// List can easily be extended as and when required.
	//
	static string _label( string base, string suffix );
	static string _label( string base, int num );
	static string _label( string base, int num, string suffix );
	static string _label( string base, string mid, int num );
	static string _label( string base, string mid, int num, string suffix );
	static string _label( string base, int num1, string mid, int num2 );
	static string _label( string base, string mid, string suffix, int num );
	
	//
	// For quantites index by two integers i and j (say), where there is a symmetry i<->j, it is sometimes
	// useful to convert to a sequential index; that is handled by these routines. Note that these return
	// (i,j) in the form i<=j, but will take i>j (and perform the flipping itself).
	// All routines prefixed by _symToSeq; parameterised by range N (0<=i<N and 0<=j<N assumed)
	//
	inline static long _symToSeq_numSeqIndices( long N ) { return N*(N+1)/2; }
	inline static long _symToSeq_seqFromPair( long i, long j, long N )
		{ return ( i<j ? i*(2*N+1-i)/2+j-i : j*(2*N+1-j)/2+i-j ); }
	inline static void _symToSeq_pairFromSeq( long k, long &i, long &j, long N )
		{ i = N; while( i*(2*N+1-i)/2 > k ) {i--;} j = k - i*(2*N+1-i)/2 + i; }


	//
	// Specifying labels (and possibly other parameters) without data.
	//
	void _addCumulativeScalarLabel( string label );
	void _addPDFLabel( string label, double binWdth );
	
	//
	// Updating data inbetween exports
	//

	// Scalars

	// Updating instantanous scalar quantities; can weight if necessary (defaults to 1.0)
	virtual void _instantaneousValue( const string label, double val, double weight = 1.0 );
	virtual void _cumulativeValue   ( const string label, double val );			// No weighting here [see top of .h file]
	virtual void _maximalValue      ( const string label, double val );			// No weighting for extrema
	virtual void _minimalValue      ( const string label, double val );
	
	// Ouptut to table; separate routines for each type
	virtual void _nextTableRow_instantaneous( scalarTable *table, bool clearAfterOutput = true,  double overallFactor = 1.0 );
	virtual void _nextTableRow_cumulative   ( scalarTable *table, bool clearAfterOutput = false, double overallFactor = 1.0 );
		// Allows weighting, i.e. time interval, and whether or not to clear the values
	virtual void _nextTableRow_extrema      ( scalarTable *table, bool clearAfterOutput = true );		// No sense weighting

	// Block data
	
	// Returns true if point added; point-by-point or by array versions allowed
	virtual bool _addToFixedSizeHist( string label, double abscissa, double value, double weight=1.0,
									  double minAbscissa=0.0, double maxAbscissa=1.0, int numBins=10 );
	virtual void _addToFixedSizeHist( string label, double *absArray, double *valArray, long N, double weight=1.0,
									  double minAbscissa=0.0, double maxAbscissa=1.0, int numBins=10 );

	// Probability density functions; if calling for the first time with this label, also specify the scale = bin width.
	virtual void _addToPDF( string label, double value, double scale = 1.0 );
	
	//
	// May be useful for derived classes
	//
	inline bool _haveExportedScalars() const { return __exportedFirstScalars; }

public:

	// Constructors / destructor
	dataGatheringBase() : __exportedFirstScalars(false) {}
	virtual ~dataGatheringBase() {}

	//
	// Export methods; all data should be export via these routines (or overloaded versions of them)
	//
	virtual void outputNextTableRow ( scalarTable   *table );
	virtual void outputNextBlockData( flatXMLParser *xml   );			// Single time slice
	
	//
	// Custom exceptions
	//
	class NotPredefinedLabel;
	class BadBlockDataParameters;
};




#endif



