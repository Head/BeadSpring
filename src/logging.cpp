// Variables and functions for logging.
#include "logging.h"

// The log file output stream.
std::ofstream logging::logFile;

// Open the log file with the given filename.
bool logging::openLogFile( std::string fname, bool overwrite )
{
	logging::logFile.open( fname.c_str(), ( overwrite ? std::ios::out : std::ios::app) );
	return logging::logFile.is_open();
}

// Send a message only if DETAILED_TRACKING is defined. Templated version with a value available.
void logging::detailedTracking( std::string message, std::string file, int line )
{
#ifdef DETAILED_TRACKING
	logging::logFile << "DETAILED TRACKING: " << message
	                 << " [in file " << file << ", line " << line << "]" << std::endl;
#endif
}

// Deprecation warning, i.e. warning about code/option/etc that may soon become deprecated/invalid.
// Could output to std::cerr or logfile; std::cerr would prompt an immediate update but may be lost.
void logging::deprecationWarning( std::string message, std::string file, int line )
{
	logging::logFile << "Deprecation warning: " << message << ""
	                 << " [in file " << file << ", line " << line << "]" << std::endl;
}

