/**
 * @file beadsBase.h
 * @author David Head
 * @brief Concrete base class for the beads and internal (*i.e.* not cross-) links.
 */


#ifndef BEAD_SPRING_BEAD_BASE_H
#define BEAD_SPRING_BEAD_BASE_H


//
// Includes.
//

// Standard includes.
#include <string>
#include <vector>
#include <algorithm>
#include <atomic>
#include <cmath>
#include <omp.h>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "boxBase.h"
#include "logging.h"
#include "pRNG.h"
#include "errors.h"
#include "dataBeadSpring.h"
#include "dataBeadsBase.h"
#include "parameterParser.h"


/**
 * Namespace for parameter labels.
 */
namespace parameterLabels
{
	const std::string beads_typeBasic = "basic";

	const std::string beads_numFils     = "N";
	const std::string beads_beadsPerFil = "M";

	const std::string beads_spacing   = "b"    ;
	const std::string beads_stretchAE = "AE"   ;
	const std::string beads_bendKappa = "kappa";
	
	const std::string beads_epsilon = "epsilon";
	const std::string beads_sigma   = "sigma"  ;
	const std::string beads_cutOff  = "cutOff" ;
}

/**
 * Alias for index pairings.
 */
using indexPair = std::pair<unsigned long,unsigned long>;


/**
 * @brief Concrete base class for the beads and internal (*i.e.* not cross-) links.
 *
 * Handles beads and how they are organised onto filaments (*i.e.* indexing), all bead positions, forces
 * and (potentially) velocities.
 * Also handles the inter-bead forces not mediate by crosslinks, *i.e.* stretching, bending and excluded volume.
 * Calculates inter-bead separations using the `box` module.
 * 
 * Read-only access to the `box` module.
 * 
 * Friend class `dataBeadsBase` for data gathering.
 */
class beadsBase
{

private:

	constexpr static auto __saveBeadsTag         = "Beads";

	constexpr static auto __saveFirstBeadsTag    = "FirstBeads";
	constexpr static auto __saveNumFilamentsAttr = "numFilaments";

	constexpr static auto __saveBeadPositionsTag = "BeadPositions";
	constexpr static auto __saveNumBeadsAttr     = "numBeads";

	constexpr static auto __twoToTheSixth        = 1.1224620483093;
	constexpr static auto __root3over2           = 0.8660254037844;

	constexpr static auto __maxForceMag          = 100.0;

	std::atomic<int> __diagNumForceLimits;
	
	int __wrapToPrimaryCount;
	constexpr static auto __wrapToPrimaryEvery   = 100;

protected:

	short _dim;										///< Dimensionality.
	
	const parameterParser *_params;					///< The parameters object as parsed from the parameters file.

	const boxBase *_box;							///< Pointer to the box object.

	//
	// Bead and filament parameters and properties.
	//
	int _N;											///< Number of filaments; fixed for the base class.
	int _M;											///< Beads per filament; fixed and uniform for the base class.

	double _b;										///< Inter-bead spacing.
	double _ks;										///< Stretching modulus (=AE/b).
	double _kb;										///< Bending modulus (=2 kappa/b^{3}).
	
	double _EVEpsilon;								///< Excluded volume energy.
	double _EVSigma;								///< Excluded volume range parameter.
	double _EVCutOff;								///< Excluded volume cut-off distance.

	std::vector<double> _X;							///< All bead (scalar) positions.
	std::vector<double> _F;							///< All bead (scalar) forces.
	
	//
	// Indexing. No range checking.
	//
	virtual inline int _firstBeadOnFilament( int fil  ) const noexcept { return fil * _M;  }
	virtual inline int _numBeadsOnFilament ( int fil  ) const noexcept { return _M;        }
	virtual inline int _filamentForBead    ( int bead ) const noexcept { return bead / _M; }

	virtual inline int _index( int fil )                    const noexcept { return _dim * _firstBeadOnFilament(fil);            }
	virtual inline int _index( int fil, int bead )          const noexcept { return _dim*( _firstBeadOnFilament(fil)+bead );     }
	virtual inline int _index( int fil, int bead, short d ) const noexcept { return _dim*( _firstBeadOnFilament(fil)+bead ) + d; }
	
	//
	// Separation methods and containers.
	//
	std::vector< unsigned int           > _nearbyBeads_numPairs;		///< No. bead pairings in each perThread vector
	std::vector< std::vector<indexPair> > _nearbyBeads_indices ;		///< Each pair obeys (first)<(second).
	std::vector< std::vector<double>    > _nearbyBeads_rSqrd   ;		///< Indices match.
	std::vector< std::vector<double>    > _nearbyBeads_dx      ;		///< Separation vector.

	virtual void _perThread_getNearbyBeads( unsigned int thread, double maxRSqrd, bool notAdj );

	//
	// Force calculation methods.
	//
	virtual void _addStretchForces( int fil );							///< Specify filament index.
	virtual void _addBendForces   ( int fil );
	virtual void _addExcludedVolumeForces( unsigned int threadNum );	///< Specify thread number.

	//
	// Utility methods.
	//
	inline double _dot( double *x, double *y ) const noexcept { return ( x[0]*y[0] + x[1]*y[1] + (_dim==3?x[2]*y[2]:0.0) ); }
	inline double _dot( double *x            ) const noexcept { return ( x[0]*x[0] + x[1]*x[1] + (_dim==3?x[2]*x[2]:0.0) ); }

	void _tangent  ( int fil, int bead, double *tgt ) const;			///< Tangent vector from bead to bead+1 on fil.
	void _normalise( double *n                      ) const;			///< Normalises `n` in-place.

	//
	// I/O.
	//
	virtual void _saveBeadData   ( flatXMLParser *xml ) const;			///< Saves all bead data to file.
	virtual void _recoverBeadData( flatXMLParser *xml );				///< Recovers bead data from file.
	
	//
	// Data.
	//	
	std::unique_ptr<dataBeadsBase> _dataBeadsBase;						///< Friend class for data gathering.
	friend dataBeadsBase;
	
public:

	//
    // Constructors / destructor.
	//
    beadsBase() {}

	/**
	 * @brief Main constructor. Checks parameters and initialises containers.
	 * 
	 * @param dim                Dimensionality.
	 * @param params             Parameters object.
	 * @param box                Need read access to the `box` module.
	 * @param data               Friend data gathering class.
	 * @param needVelocityVector If solver requires velocities.
	 */
    beadsBase( short dim, const parameterParser *params, const boxBase *box, std::shared_ptr<dataBeadSpring> data, bool needVelocityVector );
 
    virtual ~beadsBase() {}


    //
    // Accessors.
    //
    virtual inline bool isMonomeric ()        const noexcept { return (_M==1); }
    virtual inline int  numDOF      ()        const noexcept { return _dim * numBeads(); }
    virtual inline int  numFilaments()        const noexcept { return _N; }
    virtual inline int  numBeads    ()        const noexcept { return _N * _M; }
	virtual inline int  headBead    ( int n ) const noexcept { return _firstBeadOnFilament(_filamentForBead(n)); }

	virtual double totalBeadVolume() const;						///< Total volume of all beads, ignoring overlaps.

	virtual inline       double* positionVector()       { return _X.data(); }
	virtual inline const double* positionVector() const { return _X.data(); }

	virtual inline       double* forceVector()       { return _F.data(); }
	virtual inline const double* forceVector() const { return _F.data(); }

	virtual double longestInteraction() const { return std::max(_b,(_EVEpsilon?_EVCutOff:0.0)); }

	virtual       unsigned int nearbyBeads_numPairs( unsigned int t ) const noexcept { return _nearbyBeads_numPairs[t];        }
	virtual const indexPair*   nearbyBeads_indices ( unsigned int t ) const noexcept { return _nearbyBeads_indices [t].data(); }
	virtual const double*      nearbyBeads_rSqrd   ( unsigned int t ) const noexcept { return _nearbyBeads_rSqrd   [t].data(); }
	virtual const double*      nearbyBeads_dx      ( unsigned int t ) const noexcept { return _nearbyBeads_dx      [t].data(); }

	/**
	 * @brief Returns the maximum longest filament possible, in terms of the number of beads, or `-1` if not defined.
	 * 
	 * This function aims to return the maximum number of beads in any one filament. If for some
	 * reason this is not possible. *e.g.* for a derived class allowing dynamic filament growth,
	 * then `-1` is returned, denoting this quantitiy is not well defined for the corresponding child class.
	 * 
	 * @return Maximum number of beads possible in a filament, or `-1` if not defined for this beads class.
	 */
	virtual inline int longestPossibleFilament() const noexcept { return _M; }	

    //
    // Separation and force calculations.
    //

	/**
	 * @brief Get the bead indices and separations of all nearby beads.
	 * 
	 * 'Nearby' defined as those within the bare maximum interaction range
	 * as returned by the box ("bare" means without the scaling-up for the NN/Verlet lists).
	 * Will not include beads that are adjacent on the same filament if `notAdjacent==true`.
	 */
    virtual void getNearbyBeads( bool notAdjacent=true );

	/**
	 * @brief Called before calculate forces, in serial.
	 *
	 * Called prior to calculating the forces, from a serial context.
	 * Used to initialise e.g. diagnostic variables.
	 **/
	virtual void forceCalculationStart() noexcept
		{ __diagNumForceLimits = 0; }

	/**
	 * @brief Calculates stretching, bending and excluded volume forces from within an OpenMP parallel region.
	 * 
	 * Clears the force vector and then adds the forces.
	 * Assumed to be called from within a `#pragma imp parallel` region (**not** `for`).
	 * Skips stretching and bending forces if monomeric.
	 * Skips excluded volume calculations if the energy is zero.
	 */
    virtual void threadedCalculateForces();

	/**
	 * @brief Called after the force calculates, in serial.
	 *
	 * Called after calculating the forces, from a serial context.
	 * Used to e.g. save diagnostic variables.
	 **/
	virtual void forceCalculationStop() noexcept
		{ _dataBeadsBase->numForceLimits( __diagNumForceLimits ); }

	/**
	 * @brief Distance squared between two beads, including any periodicity.
	 * 
	 * Call with bead indices. Calculates then discards the separation vector.
	 */
    virtual double distSqrdBetweenBeads( int bead1, int bead2 ) const;
    
	/**
	 * @brief Normalised periodic shift along the given axis.
	 *
	 * Returns with the integer multiple of the box edge length between the two
	 * given bead indices (as 'bead2-bead1') for the given axis dimension.
	 */
	int nmlsdPerShiftInDirn( int axis, unsigned long bead1, unsigned long bead2 ) const;

    /**
	 * @brief General bead update.
	 * 
	 * Updates bead data, for instance, wrapping bead positions to the primary cell
	 * in the case of periodic boundary conditions.
	 * @param t Current time
	 * @param dt Time interval for this time step
	 */
    virtual void update( double t, double dt );
    

	//
	// Initial configurations.
	//

	/**
	 * @brief Isotropic initial conditions.
	 * 
	 * Sets the initial conditions to be isotropic, *i.e.* all fibres orientations are uniformly distributed
	 * over the unit circle or sphere, and the head bead for each fibre is uniformly distributed over space.
	 * All fibres are initially straight with beads uniformly separated.
	 */
	virtual void setICIsotropic ();

	/**
	 * @brief Triangular lattice initial condition.
	 * 
	 * Triangular lattice using parameters (probably) provided by the box. Assumes 2D and a monomeric system.
	 * @param latticeSpacing The lattice spacing parameter a.
	 * @param numBeads_x Number of beads in the x-direction.
	 * @param numBeads_y Number of beads in the y-direction.
	 */
	virtual void setICTriLattice( double latticeSpacing, int numBeads_x, int numBeads_y );
	
	//
	// Data.
	//
	virtual void dataCalculateTable() const {}
	virtual void dataCalculateBlock() const { _dataBeadsBase->calculateBlock(); }

	//
	// I/O.
	//
	void saveState   ( flatXMLParser *xml ) const;
	void recoverState( flatXMLParser *xml );
};





#endif



