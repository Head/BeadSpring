#include "boxLEStepShear.h"

#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

boxLEStepShear::boxLEStepShear( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data )
	: boxLeesEdwards(dim,params,data)
{
	using namespace parameterLabels;

	// Need a shear strain.
	if( !_params->hasScalarParam(boxLEStepShear_strain) )
		errors::throwException( "Shear strain (in x-y plane) not specified", __FILE__, __LINE__ );
	_shearStrain = _params->scalarParam(boxLEStepShear_strain);

	// Optionally a start time. Use negative value to mean 'no start time,' i.e. always on.
	_startShear = _params->scalarParamOrDefault(boxLEStepShear_start,-1.0);
	
	//
	// Clarify what has been parsed to the log file.
	//
	logging::logFile << "Construction of boxLEStepShear with step strain (in x-y plane) " << _shearStrain;
	if( _startShear > -1.0 ) logging::logFile << " starting at t=" << _startShear;
	logging::logFile << "." << std::endl;
}


#pragma mark -
#pragma mark Modifiers
#pragma mark -

void boxLEStepShear::update( double time, double dt )
{
	// Note since t>=0, if _shearStart=-1 (default) the step shear will always be applied.
	_setShearStrain( (time>_startShear) ? _shearStrain : 0.0 );
}
