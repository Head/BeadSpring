#include "BeadSpring.h"

#pragma mark -
#pragma mark Constructors / destructor.
#pragma mark -

BeadSpring::BeadSpring(
	std::string outputPrefix,
	std::string paramsFile,
	std::string logFile,
	long seed,
	std::string stateFile,
	double extendTime,
	std::map<std::string,std::string> newParams,
	bool quietMode,
	bool zeroTime )
	: _quietMode(quietMode), _outputPrefix(outputPrefix)
{
	using namespace parameterLabels;

	//
	// Initialise log files etc.
	//
	
	// Before anything else, ensure the output prefix has a trailing '/', as assumed below.
	if( !_outputPrefix.empty() && _outputPrefix.back()!='/' ) _outputPrefix.push_back( '/' );

	// Try to open the log file (global, in its own namespace). Flag 'true' to overwrite, 'false' to append.
	if( !logging::openLogFile( _outputPrefix+"log.out", stateFile.empty() ) )
		errors::throwException( "Could not open the log file on the specified output directory", __FILE__, __LINE__ );

	// Parameters are either in a separate file, or in the save state file.
	_params = std::unique_ptr<parameterParser>( new parameterParser() );
	_params->parseFile( stateFile.empty() ? paramsFile : stateFile );

	// Modify parameters with any new or altered values provided in the command line.
	for( auto &a : newParams ) _params->overwriteOrAddParam( a.first, a.second );

	// Get the dimension. Will be passed to all modules.
	if( !_params->hasIntegerParam(bsDimension) )
		errors::throwException( "No dimension defined (label '" + bsDimension + "')", __FILE__, __LINE__ );
	_dim = _params->integerParam(bsDimension);

	// Initialise the pseudo-random number generator, with seed [main.cpp sets to time(NULL) if not specified].
	pRNG::initialise( seed );

	// Initialise the saved state indexer.
	_saveFileIndexer = std::unique_ptr<sequentialFilenames>( new sequentialFilenames(4,__savePrefix,".xml") );

	// Initialise the data handler. Final flag is whether or not to append to existing data files.
	_data = std::make_shared<dataBeadSpring>( _dim, outputPrefix, _params.get(), !stateFile.empty() );
	
	//
	// Message to log, before the modules add their messages.
	//
	if( !stateFile.empty() )
		logging::logFile << "\n\nRecovering from state file " << stateFile << ".\n" <<std::endl;

	logging::logFile << "BeadSpring simulation for d=" << _dim << "; (c) David Head, University of Leeds 2019." << std::endl << std::endl;
	logging::logFile << "pRNG initialised with seed " << seed << "." << std::endl;
	logging::logFile << "Multi-threaded algorithms will use " << omp_get_max_threads() << " thread(s)." << std::endl;
	
	//
	// Initialise the modules.
	//
	
	// If loading from a save state, convert to a parser object first.
	flatXMLParser *stateParser = ( stateFile.empty() ? nullptr : new flatXMLParser(stateFile,ios::in) );
	
	// Set the module pointers to null.
	_box    = nullptr;
	_solver = nullptr;
	_beads  = nullptr;
	_links  = nullptr;
	
	// Initialise. The order here matters; see the various _init...() methods to see dependencies.
	_initBox   ( stateParser );
	_initSolver( stateParser );
	_initBeads ( stateParser );
	_initLinks ( stateParser );

	// Let the box initialise it's NN / cell sorting objects.
	_box->initialiseCells( std::max(_beads->longestInteraction(),_links->longestInteraction()) );

	//
	// Run control parameters.
	//

	// Time step.
	double dt;
	if( !_params->hasScalarParam(bsTimeStep) )
		errors::throwException( "No time step (defined by parameter '" + bsTimeStep + "')", __FILE__, __LINE__ );
	dt = _params->scalarParam(bsTimeStep);
	if( dt <= 0.0 )
		errors::throwException( "Time step must be positive", __FILE__, __LINE__ );

	// If recovering from a file, start fron the given time value and extend, otherwise start from t=0.
	if( stateParser )
	{
		stateParser->readStartElementWithTag( __saveMainTag, true );		// 'true' goes back to start of file.
		auto stateTime = atof( stateParser->attribute(__saveTimeAttr).c_str() );
		
		// Restart clock from saved time, or reset to zero if requested.
		double 
			startT = ( zeroTime ? 0.0 : stateTime ),
			endT   = startT + extendTime;

		_clock = std::unique_ptr<timeStepper>( new timeStepper(dt,endT) );
		if( !zeroTime ) _clock->setTime( stateTime );

		logging::logFile << "Will run to t=" << endT << ", starting from t=" << startT << "."  << std::endl;

		// Data gathering frequency matches the saved files, unless resetting time, in which case revert
		// to the default, i.e. total number of outputs specified in the parameters file (see below).
		if( !zeroTime )
		{
			if( stateParser->hasAttribute(__saveSaveEveryAttr) )
				_clock->add( __clockLabelSave, atof(stateParser->attribute(__saveSaveEveryAttr).c_str()), false );

			if( stateParser->hasAttribute(__saveTableEveryAttr) )
				_clock->add( __clockLabelTableOut, atof(stateParser->attribute(__saveTableEveryAttr).c_str()), false );
			
			if( stateParser->hasAttribute(__saveBlockEveryAttr) )
				_clock->add( __clockLabelBlockOut, atof(stateParser->attribute(__saveBlockEveryAttr).c_str()), false );
		}

		// If the restart file has the standard indexing, continue with higher indices.
		try
		{
			_saveFileIndexer->setIndex( stateFile );
			_saveFileIndexer->incrementIndex();
		}
		catch( std::runtime_error &ex )
		{
			if( !_quietMode )
				std::cout << "Could not extract index from state filename '" << stateFile
						  << "'; assigning index 0 to first state output after the load." << std::endl;
			_saveFileIndexer->setIndex(0);
		}
	}
	else
	{
		// If not a restart file, set ICs according to the parameters.
		_setInitialConfiguration();

		if( !_params->hasScalarParam(bsEndTime) )
			errors::throwException( "No end time (defined by parameter '" + bsEndTime + "')", __FILE__, __LINE__ );
		
		double T = _params->scalarParam(bsEndTime);
		if( T <= 0.0 )
			errors::throwException( "End time must be positive", __FILE__, __LINE__ );

		_clock = std::unique_ptr<timeStepper>( new timeStepper(dt,T) );		// Data output set below.
	}

	// Data gathering frequency determined from parameters (number of outputs requested).
	// If recovering from a saved file and not resetting time, these have already been set to maintain continuity with
	// the pre-save run.
	if( !stateParser || zeroTime )
	{
		int numSaves = _params->integerParamOrDefault(bsNumSaveStates,1);
		if( numSaves<0 ) errors::throwException( "Number of save states '" + bsNumSaveStates + "' must be non-negative", __FILE__, __LINE__ );
		if( numSaves>0 ) _clock->addNum( __clockLabelSave, numSaves, (stateParser==nullptr) );

		int numRows = _params->integerParamOrDefault(bsNumTableRows,100);
		if( numRows<0 ) errors::throwException( "Number of table rows '" + bsNumTableRows + "' must be non-negative", __FILE__, __LINE__ );
		if( numRows>0 ) _clock->addNum( __clockLabelTableOut, numRows, (stateParser==nullptr) );

		int numBlocks = _params->integerParamOrDefault(bsNumBlocks,1);
		if( numBlocks<0 ) errors::throwException( "Number of block time slices '" + bsNumBlocks + "' must be non-negative", __FILE__, __LINE__ );
		if( numBlocks>0 ) _clock->addNum( __clockLabelBlockOut, numBlocks, (stateParser==nullptr) );
	}

	// Calculation (not output) intervals. Ensure at least one per output. Calculate immediately if not loading.
	if( _clock->eventDefined(__clockLabelTableOut) )
	{
		_clock->add(
			__clockLabelTableCalc,
			std::min(_clock->interval(__clockLabelTableOut),double(__dataTableCalcInterval)), // Casting not needed after C++17?
			(stateParser==nullptr)
		);
		_data->setTableCalcInterval( _clock->integerInterval(__clockLabelTableCalc) );		// For e.g. stress tensor.

	logging::logFile << "Calculating and outputting table data every " << _clock->interval(__clockLabelTableCalc)
	                 << " and " << _clock->interval(__clockLabelTableOut) << ", resp." << std::endl;
	}

	if( _clock->eventDefined(__clockLabelBlockOut) )
	{
		_clock->add(
			__clockLabelBlockCalc,
			std::min(_clock->interval(__clockLabelBlockOut),double(__dataBlockCalcInterval)),
			(stateParser==nullptr)
		);

		logging::logFile << "Calculating and outputting block data every " << _clock->interval(__clockLabelBlockCalc)
        	             << " and " << _clock->interval(__clockLabelBlockOut) << ", resp." << std::endl;
	}

	// Percent complete messages.
	if( !_quietMode ) _clock->add( __clockLabelMessage, _clock->timeRemaining()/20.0, true );

	//
	// Optional termination conditions (if before end time).
	//
	_terminatePercDim = _params->integerParamOrDefault(bsTerm_percDim,0);

	if( _terminatePercDim<0 || _terminatePercDim>_dim ) 
		errors::throwException( "Percolation dimension (for termination) must be in range 0 to the spatial dimension", __FILE__, __LINE__ );

	if( _terminatePercDim &! _links->outputPercDim() )
		errors::throwException( "Requested termination based on percolation dimension but not to output this quantity", __FILE__, __LINE__ );

	if( _terminatePercDim )
		logging::logFile << "Will terminate before the end time if percolation dimension becomes greater than or equal to "
		                 << _terminatePercDim << "." << std::endl;

	//
	// Message to log, after the modules have added their messages.
	//
	logging::logFile << std::endl
	                 << "Parameters as parsed from '" << ( stateFile.empty() ? paramsFile : stateFile ) << "':" << std::endl
	                 << *_params << std::endl
	                 << std::endl
	                 << "Starting run." << std::endl;
}

BeadSpring::~BeadSpring()
{
	// Output coarse-grained timing information to the log file.
	logging::logFile << "Run complete." << std::endl << std::endl;
	_timer.cumulativeTimes( logging::logFile );
}


#pragma mark -
#pragma mark Initialisation
#pragma mark -

void BeadSpring::_initBox( flatXMLParser *stateParser )
{
	using namespace parameterLabels;

	// Need a box type to be specified.
	if( !_params->hasLabelParam(bsBoxType) )
		errors::throwException( "Need box type specified with label parameter '" + bsBoxType + "'", __FILE__, __LINE__ );

	// Initialise box as per given string. Defined in corresponding concrete classes.
	if( _params->labelParam(bsBoxType)==boxType_rectPer )
		_box = std::unique_ptr<boxRectPeriodic>( new boxRectPeriodic(_dim,_params.get(),_data) );

	if( _params->labelParam(bsBoxType)==boxType_LEStepShear )
		_box = std::unique_ptr<boxLEStepShear>( new boxLEStepShear(_dim,_params.get(),_data) );

	if( _params->labelParam(bsBoxType)==boxType_LEOscillatory )
		_box = std::unique_ptr<boxLEOscillatory>( new boxLEOscillatory(_dim,_params.get(),_data) );
		
	if( _box==nullptr )
		errors::throwException( "Did not recognise box type '" + _params->labelParam(bsBoxType) + "'", __FILE__, __LINE__ );
	
	// Inform the data object of the volume as is required in normalising some quantities.
	if( !_box->fixedVolume() )
		errors::throwException( "Data object assumes fixed box volume (for normalising e.g. stress tensor", __FILE__, __LINE__ );
	_data->setVolume( _box->volume() );
	
	// Recover from a saved state once the object has been initialised.
	if( stateParser!=nullptr ) _box->recoverState(stateParser);
}

void BeadSpring::_initSolver( flatXMLParser *stateParser )
{
	using namespace parameterLabels;
	
	// Check the box has been initialised.
	if( _box==nullptr )
		errors::throwException( "System error: Box must be initialised before the solver object.", __FILE__, __LINE__ );

	// Need a solver type to be specified.
	if( !_params->hasLabelParam(bsSolverType) )
		errors::throwException( "Need solver type specified with label parameter '" + bsSolverType + "'", __FILE__, __LINE__ );
	
	// Initialise solver as per the given string. Defined in corresponding concrete classes.
	if( _params->labelParam(bsSolverType)==solverType_BrownianDynamics )
		_solver = std::unique_ptr<solverBrownianDynamics>( new solverBrownianDynamics(_dim,_params.get(),_data) );
	
	if( _solver==nullptr )
		errors::throwException( "Did not recognise solver type '" + _params->labelParam(bsSolverType) + "'", __FILE__, __LINE__ );

	// Recover from a saved state once the object has been initialised.
	if( stateParser!=nullptr ) _solver->recoverState(stateParser);
}

void BeadSpring::_initBeads( flatXMLParser *stateParser )
{
	using namespace parameterLabels;
	
	// Check the solver and box have been initialised.
	if( _solver==nullptr || _box==nullptr )
		errors::throwException( "System error: Solver and box must be initialised before the beads object.", __FILE__, __LINE__ );
	
	// Do not need a beads type to be specified; defaults to the basic version.
	if( !_params->hasLabelParam(bsBeadsType) || _params->labelParam(bsBeadsType)==beads_typeBasic )
	{
		_beads = std::make_shared<beadsBase>(_dim,_params.get(),_box.get(),_data,_solver->needVelocityVector());
	}
	else
	{
		// Add beads classes derived from the base here.

		if( _beads==nullptr )
			errors::throwException( "Did not recognise beads type '" + _params->labelParam(bsBeadsType) + "'", __FILE__, __LINE__ );
	}

	// Recover from a saved state once the object has been initialised.
	if( stateParser!=nullptr ) _beads->recoverState(stateParser);
}

void BeadSpring::_initLinks( flatXMLParser *stateParser )
{
	using namespace parameterLabels;

	// Check both the box and the beads have already been initialised.
	if( _box==nullptr )
		errors::throwException( "System error: Box object must be initialised before the links object.", __FILE__, __LINE__ );
	if( _beads==nullptr )
		errors::throwException( "System error: Beads object must be initialised before the links object.", __FILE__, __LINE__ );

	// Do not need a crosslink type to be specified; defaults to the basic version.
	if( !_params->hasLabelParam(bsLinksType) || _params->labelParam(bsLinksType)==links_typeBasic )
	{
		_links = std::make_shared<linksBase>( _dim, _params.get(), _box.get(), _beads.get(), _data );
	}
	else
	{
		// All links classes other than the basic one are handled here.
		if( _params->labelParam(bsLinksType)==links_typeBeadSpecific )
			_links = std::make_shared<linksBeadSpecific>( _dim, _params.get(), _box.get(), _beads.get(), _data );
		
		if( _links==nullptr )
			errors::throwException( "Did not recognise links type '" + _params->labelParam(bsLinksType) + "'", __FILE__, __LINE__ );
	}

	// Recover from a saved state once the object has been initialised.
	if( stateParser!=nullptr ) _links->recoverState(stateParser);
}

void BeadSpring::_setInitialConfiguration()
{
	using namespace parameterLabels;

	std::string reqIC =_params->labelParamOrDefault(bsInitialConfig,bsIC_isotropic);
	
	// Default: Isotropic filaments, no crosslinks.
	if( reqIC==bsIC_isotropic )
	{
		_beads->setICIsotropic();
		_links->setICIsotropic();
		
		logging::logFile << "Set 'isotropic' initial condition." << std::endl;
		
		return;
	}

	// Triangular spring lattice; 2D only and must be monomeric.
	if( reqIC==bsIC_trilattice )
	{
		// Lattice parameters; spacing and no. of nodes in each direction.
		double a;
		int numX, numY;

		// Get parameters from the box. Throws exception if unsuitable geometry / dimensions.
		_box->geometryTriLattice( _beads->numBeads(), a, numX, numY );
		
		// The beads object can now populate its vectors.
		_beads->setICTriLattice( a, numX, numY );
		
		// The links can now be added.
		_links->setICTriLattice( numX, numY );
		
		// Inform log file of parameters.
		logging::logFile << "Set 'trilattice' initial condition with a " << numX << "x" << numY << " triangular lattice, "
		                 << "spacing a=" << a << "." << std::endl;

		return;
	}
	
	// If still here, user must have specified an IC that is not recognised.
	errors::throwException( "Did not understand requested initial condition '" + reqIC + "'", __FILE__, __LINE__ );
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Full state save.
void BeadSpring::_saveState() const
{
	// Get the filename and increase the index for the next call.
	std::string fname = _saveFileIndexer->filename();
	_saveFileIndexer->incrementIndex();
	
	// Will output as a simple type of XML.
	flatXMLParser xml( _outputPrefix+fname, ios::out );
	
	// XML Header. Obligatory.
	xml.XMLStartTag();
	
	// Opening tag. Also save the time and data/state output intervals as attributes.
	xml.addFloatAttributeWithPrecision( __saveTimeAttr, _clock->t(), DBL_DIG );

	if( _clock->eventDefined(__clockLabelSave) )
		xml.addFloatAttributeWithPrecision( __saveSaveEveryAttr , _clock->interval(__clockLabelSave), DBL_DIG );
	
	if( _clock->eventDefined(__clockLabelTableOut) )
		xml.addFloatAttributeWithPrecision( __saveTableEveryAttr, _clock->interval(__clockLabelTableOut), DBL_DIG );

	if( _clock->eventDefined(__clockLabelBlockOut) )
		xml.addFloatAttributeWithPrecision( __saveBlockEveryAttr, _clock->interval(__clockLabelBlockOut), DBL_DIG );

	xml.saveStartElement( __saveMainTag );

	// Echo parameters.
	*(xml.getOutputStream()) << *_params << std::endl;		// <Parameters>...</Parameters> block

	// Output blocks, one block per module, one at a time.
	_box   ->saveState(&xml);
	_beads ->saveState(&xml);
	_links ->saveState(&xml);
	_solver->saveState(&xml);
	
	// Closing tag for the document.
	xml.saveEndElement( __saveMainTag );
	
	// Successful? - if not, throw an exception.
	if( xml.getOutputStream()->fail() )
		errors::throwException( "Could not save XML file'" + fname + "'; output stream failed", __FILE__, __LINE__ );
		
	// Output to log file.
	logging::logFile << " - saved state to file " << fname << std::endl;
}


#pragma mark -
#pragma mark Data
#pragma mark -

void BeadSpring::_calculateAllTables() const
{
	_box   ->dataCalculateTable();
	_beads ->dataCalculateTable();
	_links ->dataCalculateTable();
	_solver->dataCalculateTable();
}

void BeadSpring::_calculateAllBlocks() const
{
	_box   ->dataCalculateBlock();
	_beads ->dataCalculateBlock();
	_links ->dataCalculateBlock();
	_solver->dataCalculateBlock();
}

void BeadSpring::_outputTableRow()
{
	_data->addScalar( __dataLabelTime, _clock->t() );
	_data->outputRowWithLabel( _clock->started() ? _solver->tableRowLabel() : __dataLabelIC );
	
	logging::logFile << " - output table row at time t=" << _clock->t()  << std::endl;
}

void BeadSpring::_outputBlockTimeSlice() const
{
	_data->outputBlockDataTimeSlice( _clock->t() );

	logging::logFile << " - output block time slice at time t=" << _clock->t() << std::endl;
}


#pragma mark -
#pragma mark Solver components
#pragma mark -

// Get bead pairings that are within the maximum interaction range.
void BeadSpring::_calcBeadSeparations()
{
	logging::detailedTracking( "Starting calculation of bead separations", __FILE__, __LINE__ );
	_timer.startTimerWithLabel( __timerLabelGetNearby );

	_box  ->update               ( _clock->t(), _clock->dt() );
	_box  ->updateInteractionList( _beads->numBeads(), _beads->positionVector() );
	_beads->getNearbyBeads       ();

	_timer.stopCurrentTimer();
	logging::detailedTracking( "Finished calculation of bead separations", __FILE__, __LINE__ );
}

// Force calculations. Bead calculation clears the force vector and so must go first.
void BeadSpring::_calcForces()
{
	logging::detailedTracking( "Starting calculation of forces", __FILE__, __LINE__ );
	_timer.startTimerWithLabel( __timerLabelCalcForces );

	_data->forceCalculationStart();				// For calculating e.g. stress tensor.
	_beads->forceCalculationStart();
	_links->forceCalculationStart();

	#pragma omp parallel						// Only fork-join once for all force calculations.
	{
		_beads->threadedCalculateForces();
		_links->threadedIncrementForces( _beads->positionVector(), _beads->forceVector() );
	}

	_links->forceCalculationStop();
	_beads->forceCalculationStop();
	_data->forceCalculationStop();

	_timer.stopCurrentTimer();
	logging::detailedTracking( "Finished calculation of forces", __FILE__, __LINE__ );
}

// Update system configuration.
void BeadSpring::_iterateBeads()
{
	logging::detailedTracking( "Starting iteration of beads", __FILE__, __LINE__ );
	_timer.startTimerWithLabel( __timerLabelUpdateSystem );

	_solver->update       ( _clock->t () );
	_solver->updateVectors( _clock->dt(), _beads->numDOF(), _beads->positionVector(), _beads->forceVector() );

	_timer.stopCurrentTimer();
	logging::detailedTracking( "Finished iteration of beads", __FILE__, __LINE__ );
}

// Update beads and links. Box is updated just prior to calculation of forces.
void BeadSpring::_updateSystem()
{
	logging::detailedTracking( "Starting update of system", __FILE__, __LINE__ );
	_timer.startTimerWithLabel( __timerLabelBeadsAndLinks );

	_beads->update( _clock->t(), _clock->dt() );
	_links->update( _clock->t(), _clock->dt(), _beads->numBeads() );

	_timer.stopCurrentTimer();			
	logging::detailedTracking( "Finished update of system", __FILE__, __LINE__ );
}

// Regular events (data gathering/output; state output).
void BeadSpring::_dataAndOutput()
{
	logging::detailedTracking( "Starting data calculation and output, and state output", __FILE__, __LINE__ );
	_timer.startTimerWithLabel( __timerLabelOutputAndSave );

	if( _clock->flag(__clockLabelTableCalc) ) _calculateAllTables();		// Always call tables before blocks.
	if( _clock->flag(__clockLabelBlockCalc) ) _calculateAllBlocks();
	if( _clock->flag(__clockLabelTableOut ) ) _outputTableRow();
	if( _clock->flag(__clockLabelBlockOut ) ) _outputBlockTimeSlice();
	if( _clock->flag(__clockLabelSave     ) ) _saveState();

	_timer.stopTimerWithLabel( __timerLabelOutputAndSave );
	logging::detailedTracking( "Finished data calculation and output, and state output", __FILE__, __LINE__ );
}


#pragma mark -
#pragma mark Run control.
#pragma mark -

void BeadSpring::startRun()
{
	// Loop over the run time.
	while( !_clock->end() )
	{
		// Check for premature termination. Will output data and saved states before 'break'ing.
		if( _prematureTermination() ) break;

		// Get forces even at t=0 for e.g. stress tensor calculations.
		_calcBeadSeparations();
		_calcForces();
		
		// Only update bead positions etc. once started, so initial data/state output corresponds to t=0.
		if( _clock->started() )
		{
			_iterateBeads();
			_updateSystem();

			_clock->step();
		}

		// Data gathering, data and state output.
		_dataAndOutput();

		// Status message.
		if( _clock->flag(__clockLabelMessage ) ) std::cout << 5*_clock->events(__clockLabelMessage) << " percent complete" << std::endl;

		_clock->start();
	}	
}

// Permature termination. Returns true (after sending a message to stdout and the log file) if run should quit now.
bool BeadSpring::_prematureTermination()
{
	// Set to 'true' and generate a suiatble message if premature temination required.
	bool prematureTermination = false;
	std::ostringstream out;

	//
	// Termination criteria other than exceeding the end time.
	//

	// Terminate once percolation reaches some minimum value.
	if( _terminatePercDim && _links->lastPercDim()>=_terminatePercDim )
	{
		prematureTermination = true;

		out << " - terminating at t=" << _clock->t() << ": percolation dimension at least "
		    << _terminatePercDim << "." << std::endl;
	}

	//
	// Handling premature termination.
	//

	// If not terminating early, do nothing.
	if( !prematureTermination ) return false;

	// If still here, enforce premature termination. Output messages to logfile and possible stdout.
	if( !_quietMode ) std::cout << out.str();
	logging::logFile << out.str();

	// Final outputs and state files, as this method is called before the usual output.
	// Data output can fail if there have been no calculations since the last output; catch
	// and do nothing (the XML files will still be properly terminated).
	_saveState();
	try { _outputTableRow      (); } catch( std::exception &err ) {}
	try { _outputBlockTimeSlice(); } catch( std::exception &err ) {}

	// 'break' from main simulation loop.
	return true;
}



