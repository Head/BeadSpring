#!/usr/local/bin/python3
# 'runs' class that gathers together all recognised output files from a series of
# independent runs with the same parameters but different pRNG seed, as is typically
# the case; args to override for e.g. a single run if need be.
#
# Assumes a range of standard filename-ing conventions, given by class methods.
#


#
# Imports.
#

# Standard imports.
from math import *
import os
import sys
import glob

# Local imports.
import analyseData

# From within this package. Not if being called directly.
try:
	from . import bsStates
except ImportError:
	pass



class bsRuns( object ):

	# Can optionally select only certain files to load (defaults to all).
	def __init__( self, dirName, verbose=False, table=True, block=True, states=False ):
		"""Parses a single run directory, or a series labelled consecutively, using standard filenaming conventions
		that are encapsulated in class methods. Will extract tables and block data by default; can switch this off.
		Can request to alse parse state archives; off by default as can be slow."""

		# Store parameters
		self._verbose = verbose
		self._rootDir = dirName

		# Initialise persistent objects.
		self.sc = None
		self.bd = None
		self.st = None

		self._numRuns = 0
		
		# Is this already an output directory (single run), or do we need to add 'run' suffices?
		if len(self._expectedFileNamesOn(dirName)) > 0:
			if self._verbose: print( "Assuming single run for {0}s' (found files '{1}')".format(dirName,self._expectedFileNamesOn(dirName)) )
			self._numRuns = 1
		else:
			
			# If not a single-run, is there at least one run sub-dir? If not, bail
			if len(glob.glob( os.path.join(dirName,bsRuns.runSubdirs()))) == 0:
				print( "bsRuns.__init__(): Cannot extract run[s] from '{}'; neither a single-run nor root of subdirectories of runs".format(dirName) )
				sys.exit(-1)

			# Can now, independently of what data is being extracted, determine the number of runs (assuming standard notation).
			while os.path.isdir( os.path.join(dirName,"run{}".format(self._numRuns))):
				self._numRuns += 1

			# If so, extend the dirname before extracting the table and block data.
			dirName = os.path.join( dirName, bsRuns.runSubdirs() )

		# Should have some number of runs by now.
		if self._numRuns == 0:
			print( "bsRuns.__init__(): Looked like a set of runs but could not extract any." )
			sys.exit( -1 )

		# dirName now contains the run subdir(s), possible only 1 and possible with UNIX wildcards
		self._runsDir = dirName
		if self._verbose: print( "Taking data from subdir(s): '{}'".format(self._runsDir) )
		
		# Get the expected files. Could override dataRunsWithTimes as per biofilm case.
		dataWithRuns = analyseData.genericDataGathering.dataRunsWithTimes
	
		if table : self.sc = dataWithRuns( os.path.join(self._runsDir,bsRuns.tableFile    () ), verbose=self._verbose )
		if block : self.bd = dataWithRuns( os.path.join(self._runsDir,bsRuns.blockDataFile() ), verbose=self._verbose )

		if states:
			self.st = []
			for run in range(self._numRuns):
				runDir = os.path.join( self._rootDir, "{0}{1}/{2}".format(bsRuns.runSubdir(),run,bsRuns.statesArchive()) )
				self.st.append( bsStates(runDir) )

		if self._verbose:
			print( "Extraction complete." )


	# Returns 'True' if the passed sub-dir looks like a run directory; currently
	# this means if it contains ALL of the expected files
	def _expectedFileNamesOn( self, dir ):
		return [ fname for fname in bsRuns.expectedFileNames() if os.path.isfile(os.path.join(dir,fname)) ]

	#
	# Accessors
	#
	def table( self ):
		return self.sc

	def blockData( self ):
		return self.bd
	
	def states( self ):
		"List of bsStates objects, one per run."
		return self.st
	
	def numRuns( self ):
		"Determined independently of parsed data or states."
		return self._numRuns

	def __str__( self ):

		str = "bsRuns object from base directory '{0}' with {1} runs containing:".format(self._runsDir,self._numRuns)

		if self.st: str += "\n\nFor each run, " + self.st[0].__str__() + "\n"

		if self.sc: str += self.sc.__str__() + "\n"
		if self.bd: str += self.bd.__str__() + "\n"

		return str
	
	def timeScaleUnit( self ):
		"Returns time scale unit as a string. Assumes same for table and block data. Taken from first run if not imposed."""
		return self.sc.timeScaleUnit()

	def  allParameters( self ):
		"""Returns all parameters from either table, block data, or states (first run, in that order)."""
		if self.sc: return self.sc   .allParameters()
		if self.bd: return self.bd   .allParameters()
		if self.st: return self.st[0].allParameters()
		print( "bsRuns::allParameters(): No data object to determine the parameters." )

	#
	# Class methods
	#
	
	# Expected filenames
	@classmethod
	def expectedFileNames( cls ):
		return [ cls.statesArchive(), cls.tableFile(), cls.blockDataFile(), cls.parametersFile() ]
	
	@classmethod
	def statesArchive( cls ):
		return "states.tar.gz"

	@classmethod
	def tableFile( cls ):
		return "scalars.xml"
	
	@classmethod
	def blockDataFile( cls ):
		return "block_data.xml"
		
	@classmethod
	def parametersFile( cls ):
		return "parameters.xml"

	# Run subdirectories, the plural in a format glob can understand.
	@classmethod
	def runSubdir( cls ):
		return "run"

	@classmethod
	def runSubdirs( cls ):
		return "{}*".format(cls.runSubdir())


#
# Executing from the command line
#
if __name__ == "__main__":

	import argparse
	import pyBeadSpring
	
	p = argparse.ArgumentParser( description="Plot raw data from BeadSpring output files" )

	p.add_argument( "paths", help="Path(s) up to but not including the 'run?' subdirectories", nargs='+' )

	p.add_argument( "-t",	"--time",	help="Average over time",	action="store_true" )
	p.add_argument( "-r",	"--runs",	help="Average over runs",	action="store_true" )

	p.add_argument( "-b",	"--block"  , help="Plot block data quantity",                    default=None )
	p.add_argument( "-s",	"--scalars", help="Spaced-delimited list of scalars to plot versus time", nargs="*", default=None )

	p.add_argument( "-a",   "--against", help="Plot scalar(s) against this quantity rather than time", default="t" )
	
	p.add_argument( "-x",	"--logx",	help="Log x-axis",	action="store_true", default=False	)
	p.add_argument( "-y",	"--logy",	help="Log y-axis",	action="store_true", default=False	)
	p.add_argument( "-l",	"--loglog",	help="Log both axes",	action="store_true", default=False	)
	
	p.add_argument( "-f",	"--fn",		help="Function string [must use 'x' as x-axis variable]",	default=None )
	
	args = p.parse_args()

	#
	# Validate arguments
	#

	# Cannot print more than one data type (if none, will list those available)
	if args.scalars and args.block:
		print( "Can only print one of 'scalar', 'block'" )
		sys.exit(-1)

	# Cannot (yet) modify the x-axis for blocks.
	if args.block and args.against != "t":
		print( "Can only plot scalars againts user-defined x-axes." )
		sys.exit(-1)

	#
	# Get object(s) and list everything. Time time scale from first run if there is more than one.
	#
	outputDirs = args.paths
	print( "Attempting to open runs with base path(s):\t{}".format(outputDirs) )

	fullObjects = []
	for dir in outputDirs:
		fullObjects.append( bsRuns(dir,verbose=True) )

	print( "\n{}".format(fullObjects[0]) )

	#
	# Plotting?
	#
	if args.scalars or args.block:

		# Reduce to just the require data type
		labels, objects = None, []

		if args.scalars:
			labels  = args.scalars
			objects = [ object.sc for object in fullObjects ]
	
		if args.block:					# Currently only allow one block data to be plotted at a time.
			labels = [ args.block ]
			objects = [ object.bd for object in fullObjects ]
		
		# Plot whatever was requested
		dataKwargs = { "timeAv":args.time, "runAv":args.runs }
		plotKwargs = { "logx"  :args.logx, "logy" :args.logy, "loglog":args.loglog, "function":args.fn }
	
		# Get label and dataWithRuns object
		dataSets = None
		for i, object in enumerate(objects):		# May need path name

			# Loop over all labels for this data set.
			for j, label in enumerate(labels):

				# Get data set(s) for this object; change labels if there are more than one.
				nextDataSets = object.getDataSets(label, **dataKwargs)
				if len(objects)>1 or len(labels)>1:

					for k, curve in enumerate(nextDataSets["curves"]):

						# Always try to use colour first, whatever we are trying to discriminate.
						if len(labels)>1 and len(objects)==1:
							curve["label"] = "{0} {1} {2}".format(outputDirs[i],curve["label"],label)
							curve["fmt"  ] = analyseData.pyplotAux.fmt(j,i,k)								# Colour / marker / dash

						if len(labels)==1 and len(objects)>1:
							curve["label"] = "{0} {1} {2}".format(outputDirs[i],curve["label"],label)
							curve["fmt"  ] = analyseData.pyplotAux.fmt(i,j,k)								# Colour / marker / dash
		
				# If plotting against something other than the default, modify the x-axes now.
				if args.against != "t":
					againstDataSet = object.getDataSets( args.against, **{"timeAv":False,"runAv":True} )
							
					nextDataSets["xLabel"] = args.against
					for curve in nextDataSets["curves"]:
						curve["x"] = againstDataSet["curves"][0]["y"]
						
				# Add to (or create) the list
				if dataSets==None:
					dataSets = nextDataSets
				else:
					dataSets["curves"].extend( nextDataSets["curves"] )
		
							
			
		
		# Use the class method to plot them
		analyseData.genericDataGathering.dataRunsWithTimes._plotDataSets( dataSets, **plotKwargs )
	
