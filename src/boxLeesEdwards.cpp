#include "boxLeesEdwards.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

boxLeesEdwards::boxLeesEdwards( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data )
	: boxRectPeriodic(dim,params,data), __shearStrain(0.0)
{
	// First label for the current shear.
	if( !_data->continuationRun() ) _data->addScalar( __currentShear, __shearStrain );
	
	// Request the corresponding shear stress be output.
	_data->outputShearStress( true );
}



#pragma mark -
#pragma mark Wrapping
#pragma mark -

// Wraps x centred on the origin. Follows Allen and Tildesly p.247.
void boxLeesEdwards::wrap( double *x ) const noexcept
{
	x[0] -= std::round(x[1]/_X[1]) * _X[1] * __shearStrain;

	boxRectPeriodic::wrap(x);
}

void boxLeesEdwards::offsetToPrimaryCell( const double *x, double *offset ) const noexcept
{
	boxRectPeriodic::offsetToPrimaryCell(x,offset);

	offset[0] -= floor(x[1]/_X[1]) * _X[1] * __shearStrain;
}


#pragma mark -
#pragma mark Update
#pragma mark -

void boxLeesEdwards::_setShearStrain( double gamma ) noexcept
{
	__shearStrain = gamma;
	_data->addScalar( __currentShear, __shearStrain );
}


#pragma mark -
#pragma mark Saving / loading
#pragma mark -

void boxLeesEdwards::_saveBoxState( flatXMLParser *xml ) const
{
	xml->addAttribute    ( __saveCurrentShearAttr, __shearStrain );
	xml->saveStartElement( __saveCurrentShearTag , true          );
	
	boxRectPeriodic::_saveBoxState(xml);
}

