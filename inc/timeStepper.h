//
// timeStepper class
//
// Handles an incrementing time step with an unlimited number of events that
// occur at regular intervals.
//
// Typical usage:
//
//	timeStepper clock(T);			// Initialise clock for total run time T
//	clock.Add( "image", max_T/10.0 );
//
//  Then:
//
//  clock.Flag("image") returns 'true' once every 10% of the run (once checked, the flag is reset)
//   - always returns false if the requested flag label does not exist.
//
//  clock.Events("image") returns the number of times this event has been activated
//   - will raise exception if the requested flag label does not exist
//
//  clock.End() returns true when the entire run is over
//
//
// Options:
// -------
// clock.Add( "name", Dt, true );		// Will set the initial flag to 'true', so it will be called immediately
// clock.Add_Num( "name", num_calls );		// Intervals measured as fraction of total time; call at end assured
//


#ifndef _USERDEF_TIMESTEPPER_H
#define _USERDEF_TIMESTEPPER_H


// Standard includes
#include <map>
#include <string>
#include <stdexcept>				// For exception handling
#include <sstream>
#include <math.h>
#include <climits>

class timeStepper {

protected:

	bool _started;			// Has the timer started (i.e. at least 1 time increment made)?

	long _count;			// Current no. time steps dt made

	double _dt;				// Smallest time step

	// Store interval, next time to set the flag to 'on', and the flag itself. Also allow a minimum value.
	std::map<std::string,long> _intervals;
	std::map<std::string,long> _next;
	std::map<std::string,bool> _flags;
	std::map<std::string,long> _num;					// Number of times each event has been activated
	std::map<std::string,long> _tot_num;				// If called by an integer, store here; used to ensure final output

	void _new_event(std::string,long,bool);		// Actually adds the new event

	// Name used for the main run timer (which is otherwise just a normal timer)
	static std::string _run_label() { return "__run__"; }
	

public:

	// Initialise with base (smallest) time step and (real) total run time
	timeStepper( double dt, double T ) : _started(false), _count(0), _dt(dt)  { addEvent( _run_label(), T, false ); }
	virtual ~timeStepper() {};

	// Add event; two alternative aliases; also an alternative integer version
	void addEvent(std::string,double,bool);
	void add( std::string label, double interval, bool immediately = false ) { addEvent( label, interval, immediately ); };
	void set( std::string label, double interval, bool immediately = false ) { addEvent( label, interval, immediately ); };

	void addNumEvent(std::string,long,bool);
	void addNum( std::string label, long num_ints, bool immediately = false ) { addNumEvent( label, num_ints, immediately ); };
	void setNum( std::string label, long num_ints, bool immediately = false ) { addNumEvent( label, num_ints, immediately ); };

	// Increment the clock by dt; check all counters and set all necessary flags to 'true'
	void operator++(int) { step(); }
	void step();

	// Returns with the base time step
	inline double dt() const { return _dt; }

	// Returns 'true' if the even exists.
	bool eventDefined( std::string label ) const;
	
	// Return with the current flag status; resets it to false.
	bool flag    ( std::string label );
	bool peekFlag( std::string label ) const;
	
	// Status of the main run timer
	inline bool started() const { return _started; }
	inline void start  ()       { _started = true; }
	       bool end    ();
	
	// Direct manipulation of data
	void clearAllFlags();		// Sets all flags to 'false'; no events are counted
	void setEvent(std::string,long);

	// Returns with the number of times this event has been activated, or its integer interval (i.e. divided by dt) or actual.
	long events(std::string) const;
	long integerInterval(std::string) const;
	inline double interval( std::string label ) const { return _dt*integerInterval(label); }

	// Set or retreive the current time
	inline void   setTime( double new_t ) { _count = long( ceil(new_t/_dt) ); }
	inline double t() const { return _count * _dt; };
	inline double operator() () const { return t(); };
	inline double timeRemaining() const { return interval(_run_label()) - t(); }

	// Sets the current time; also updates no. of events and flag as if the time had been incremented to this 
	void jumpForwardsTo(double);
	
	// Returns current time and status of all timeSteppers
	friend std::ostream& operator<< ( std::ostream &os,  timeStepper &t );
};





#endif



