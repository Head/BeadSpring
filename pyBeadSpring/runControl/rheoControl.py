#
# Perform runs from existing states that apply oscillatory shear with given frequency and strain
# amplitude. Crosslinking will be suppressed during the shear runs ("frozen").
#
# If intending to use this script, then the original runs should be on a directory called "preShear"
# (currently set by a class method); the run directory when calling this script should be the path
# up to but not including this run dir.
#
# In addition, the following parameters should be specified for the starting runs:
# - boxType="LEOscillatory"
# - gamma0="0"
# - omege="0"
# 
# If 'terminatePercDim' is defined, it will be set to zero once the shear runs start, and the flag
# to calculate the percolation dimension will be set to 'false'.
#

#
# Imports.
#

# Standard imports.
import sys
import os
import shutil
import pathlib
import math

# Parent class.
import runBase


#
# Script to control runs generating rheology data for the beadSpring code.
#
class rheoControl( runBase.runBase ):

	#
	# Command line parsing.
	#
	def setCmdLineOptions( self, parser ):
		"""Adds a command line argument for the frequency. Later accessible as 'self._args.omega'."""

		super().setCmdLineOptions(parser)

		parser.add_argument( "-w", "--omega", help="Frequency for a continuation run if non-zero [0.0]", type=float, default=0.0 )
		parser.add_argument( "-g", "--gamma", help="Maximum amplitude of oscillatory strain", type=float, default=0.05 )
	#
	# Setting up the directories.
	#
	def setUpDirectories( self, alreadyExists=False, path=None ):
		"""For omega=0, creates a pre-gel subdirectory within the given root directory. Otherwise, creates
		a post-gel directory with a name including the frequency."""

		# Root path extended by pre or pos-gel subdirectory.
		nestedPath = os.path.join( (path if path else self._root), (self.postShearDir() if self._args.omega>0 else self.preShearDir()) )

		# Call parent class method with nested path.
		super().setUpDirectories( alreadyExists=alreadyExists, path=nestedPath )
	
	def _createRunDir( self, runPath ):
		"Replaces parent method by copying over data from pre-gel to post-gel as required."

		# Shorthands.
		omega     = self._args.omega
		gamma0    = self._args.gamma
		execFile  = self.__class__.executable()
		statesArc = "{}s.tar.gz".format(self.__class__.stateFilePrefix())
		runScript = self.runScriptName()
		
		# If zero/invalid frequency, assume pre-gel run, the path for which has already been extended into a pre-gel branch.
		if omega<=0.0 or gamma0<=0.0:
			super()._createRunDir( runPath )
			return

		# If still here, must be a post-gel run. First, get the path with the pre-gel output.
		# Note that runPath is either the root plus post-gel, or the root plus post-gel plus 'run<>'.
		if os.path.basename(runPath) == self.postShearDir():
			preGelPath = os.path.join( self._root, self.preShearDir() ) 
		else:
			preGelPath = os.path.join( self._root, self.preShearDir(), os.path.basename(runPath) ) 

		# Make sure the pre-gel path exists.
		if not os.path.isdir( preGelPath ):
			print( "Cannot find pre-gel data starting from '{}'.".format(preGelPath) )
			sys.exit(-1)

		# Check the pre-gel directory has a states archive.
		if not os.path.isfile( os.path.join(preGelPath,statesArc) ):
			print( "Cannot extend run; '{}' has no states archive file '{}'.".format(preGelPath,statesArc) )
			sys.exit(-1)

		# This is just to make the remaining code more readable.
		postGelPath = runPath

		# Create the post-gel directory.
		if os.path.exists( postGelPath ):
			print( "Cannot create post-gelation directory '{}'; already exists.".format(postGelPath) )
			sys.exit(-1)

		if self._verbose:
			print( "Creating the directory '{}' starting from final state in '{}'.".format(postGelPath,preGelPath) )

		pathlib.Path( postGelPath ).mkdir( parents=True, exist_ok=True )	# Like mkdir -p in terminal.

		# Copy the executable over, and the states archive from the main run.
		if self._verbose:
			print( "Copying over '{}' and '{}', and creating run script '{}'.".format(execFile,statesArc,runScript) )

		shutil.copy( execFile, postGelPath )
		shutil.copy( os.path.join(preGelPath,statesArc), os.path.join(postGelPath,statesArc) )

		# Extra arguements to the executable: changes to parameters and zeroing the time.
		extTime = 2*math.pi * self.numCycles() / omega
		args = "-z -P outputPercDim=False -P terminatePercDim=0 -P gamma0={0} -P omega={1} -P freezeLinksAfter=0.0 -T {2}".format(gamma0,omega,extTime)

		# The run script should start with a command to unarchive the states files.
		startup  = "tar -xzf {}s.tar.gz\n".format(self.__class__.stateFilePrefix())
		startup += "rm -f {}s.tar.gz\n".format(self.__class__.stateFilePrefix())			# Not really necessary but makes directory more readable during a run.

		script = self._generateRunScript( os.path.basename(execFile), execArgs=args, startup=startup )
		with open( os.path.join(postGelPath,runScript), 'w' ) as f:
			f.write( script )

	#
	# Nested directoty name for pre- and post-gel runs.
	#
	def preShearDir( self ):
		return "preShear"
	
	def postShearDir( self ):
		try:
			return "shear_w{}_g{}".format( self.suffix(self._args.omega), self.suffix(self._args.gamma) )
		except KeyError as err:
			print( f"Cannot generate a filename for frequency {self._args.omega} and strain amplitude {self._args.gamma}." )
			print( f" - please update the 'suffix' static method in 'rheoControl' to include a new suffix mapping." )
			sys.exit(-1)

	#
	# Parameters specific to rheo-control.
	#
	@staticmethod
	def suffix( x ):
		"""Map from float to a human-readable suffix without a dot, suitable for files and directories.
		Will need to be extended as new frequencies and/or strain amplitudes are requested."""
		return {
			1		:	"1",
			0.5		:	"5em1",
			0.3		:	"3em1",
			0.2		:	"2em1",
			0.1		:	"1em1",
			0.05	:	"5em2",
			0.04	:	"4em2",
			0.03	:	"3em2",
			0.02	:	"2em2",
			0.01	:	"1em2",
			0.005	:	"5em3",
			0.003	:	"3em3",
			0.002	:	"2em3",
			0.001	:	"1em3",
			0.0005	:	"5em4",
			0.0003	:	"3em4",
			0.0002	:	"2em4",
			0.0001	:	"1em4",
		}[x] 

	@staticmethod
	def numCycles():
		"The number of cycles to be integrated over for post-gelation runs."
		return 7


	#
	# Parameters as class methods.
	#
	@classmethod
	def executable( cls ):
		return "beadSpring"

	@classmethod
	def description( cls ):
		return "Script for the control of multiple beadSpring runs from given start states for generating rheology data."

	@classmethod
	def stateFilePrefix( cls ):
		return "state"

	@classmethod
	def extendArg( cls ):
		return "--time"



#
# Main control sequence.
#
if __name__ == "__main__":
	rheoControl().launchRuns()
