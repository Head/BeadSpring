#include "solverBrownianDynamics.h"

#pragma mark -
#pragma mark Constructors / destructor.
#pragma mark -

solverBrownianDynamics::solverBrownianDynamics(
	short dim,
	const parameterParser *params,
	std::shared_ptr<dataBeadSpring> data )
	: solverBase(dim,data)
{
	using namespace parameterLabels;

	//
	// Get the parameters.
	//
	if( !params->hasScalarParam(solver_gamma) ) errors::throwException("Brownian Dynamics solver requires parameter '"+solver_gamma+"'",__FILE__,__LINE__);
	if( !params->hasScalarParam(solver_kT   ) ) errors::throwException("Brownian Dynamics solver requires parameter '"+solver_kT   +"'",__FILE__,__LINE__);

	_gamma = params->scalarParam(solver_gamma);
	_kT    = params->scalarParam(solver_kT   );
	
	if( _gamma <= 0.0 ) errors::throwException( "Parameter "+solver_gamma+" must be positive"    , __FILE__, __LINE__ );
	if( _kT    <  0.0 ) errors::throwException( "Parameter "+solver_kT   +" must be non-negative", __FILE__, __LINE__ );	

	//
	// Output to log.
	//
	logging::logFile << "Created instance of solverBrownianDynamics with \\gamma=" << _gamma <<" and kT=" << _kT << "." << std::endl;
}


#pragma mark -
#pragma mark Iteration
#pragma mark -

void solverBrownianDynamics::updateVectors( double dt, int Ndof, double *X, const double *F )
{
	double recipGamma = 1.0 / _gamma;

	// Rather than waste time calculating the noise terms when kT is zero, when the scale factor is zero anyway
	if( _kT )
	{
		// The constant factor for all terms, and other scratch variables.
		double scale = sqrt( 2*dt*_gamma*_kT );

		// Use OpenMP to perform update in parallel, with only a single fork-join construct.
		#pragma omp parallel
		{
			// Do as much per-thread set-up as possible before starting the loop. Note need to pass the reference to
			// to engine, otherwise a copy is created and we get the same random numbers each time the loop is called.
			auto t = omp_get_thread_num();
			auto nml = std::bind( pRNG::perThread_unitNormals[t], std::ref(pRNG::engines[t]) );

			// Now loop over all degrees of freedom.
			#pragma omp for
			for( auto i=0; i<Ndof; i++ )
				X[i] += ( scale*nml() + dt*F[i] ) * recipGamma;
		}
	}
	else
	{
		// This is the zero-temperature/gamma version, which is simpler.
		#pragma omp parallel for
		for( auto i=0; i<Ndof; i++ )
			X[i] += dt * F[i] * recipGamma;
	}
}


