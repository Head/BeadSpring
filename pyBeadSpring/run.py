#
# Standard run script. Essentially just overrides the base 'runControl' class
# and runs it. More exotic run control schemes may also be found, such as
# (at time of writing) 'rheoControl'.
#
# Normal usage would be something like:
#
# python3 pyBeadSpring/run.py <root directory> <run indices>
#
# This runs the jobs consecutively; there are also options to run concurrently, or to submit
# to the ARC queue. Call with '-h' to see all supported options.
#
# Note that if intending to use these runs as a starting point for generating rheology data,
# the 'rheoControl' script has some specifric requirements for the parameters file - see
# comments at the head of 'rheoControl' for details.
#


#
# Imports
#
from runControl import runBase	# Local 'package' which includes the base class, also called 'runControl'.


#
# Base class definition.
#
class run( runBase.runBase ):

	#
	# Parameters as class methods.
	#
	@classmethod
	def executable( cls ):
		return "beadSpring"

	@classmethod
	def description( cls ):
		return "Script for the control of multiple beadSpring runs with the same parameters."

	@classmethod
	def stateFilePrefix( cls ):
		return "state"

	@classmethod
	def extendArg( cls ):
		return "--time"

#
# If called from the command line.
#
if __name__ == "__main__":
    run().launchRuns()
