#include "beadsBase.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

beadsBase::beadsBase(
	short dim,
	const parameterParser *params,
	const boxBase *box,
	std::shared_ptr<dataBeadSpring> data,
	bool needVelocityVector )
	: _dim(dim), _params(params), _box(box), _dataBeadsBase(nullptr)
{
	using namespace parameterLabels;

	// Sanity check: If velocity vector required, will need to be added.
	if( needVelocityVector )
		errors::throwException( "Solver requires velocity vector but not yet supported", __FILE__, __LINE__ );
	
	//
	// Get filament specifications.
	//
	
	// Number of filaments and beads per filament.
	if( !_params->hasScalarParam(beads_numFils    ) ) errors::throwException( "Number of filaments not specified", __FILE__, __LINE__ );
	if( !_params->hasScalarParam(beads_beadsPerFil) ) errors::throwException( "Number of beads per filament not specified", __FILE__, __LINE__ );
	
	_N = _params->scalarParam(beads_numFils    );
	_M = _params->scalarParam(beads_beadsPerFil);
	
	if( _N<=0 ) errors::throwException("Number of filaments must be positive",__FILE__,__LINE__);
	if( _M<=0 ) errors::throwException("Number of beads per filament must be positive",__FILE__,__LINE__);

	// Inter-bead spacing must be defined even if M==1, as can be interpreted as bead diameter (and used as default for EV interactions).
	if( !_params->hasScalarParam(beads_spacing) ) errors::throwException( "Must specify bead diamter", __FILE__, __LINE__ );

	_b = _params->scalarParam(beads_spacing);

	if( _b  <= 0.0 ) errors::throwException( "Bead spacing must be positive", __FILE__, __LINE__ );

	// If M>=2, i.e. not isolated beads, must specify inter-bead spacing and spring constant.
	_ks = 0.0;
	if( _M >= 2 )
	{
		if( !_params->hasScalarParam(beads_spacing) )
			errors::throwException( "Bead spacing '" + beads_spacing + "' must be provided when M>1", __FILE__, __LINE__ );

		if( !_params->hasScalarParam(beads_stretchAE) )
			errors::throwException( "Cross-sectional area times Young's modulus '" + beads_stretchAE + "' must be provided when M>1", __FILE__, __LINE__ );
			
		_ks = params->scalarParam( beads_stretchAE ) / _b;
		
		if( _ks <= 0.0 ) errors::throwException( "Cross-sectional area times Young's modulus must be positive", __FILE__, __LINE__ );
	}
	
	// Can optionally specify a bending constant. If not given, taken to be zero.
	_kb = 0.0;
	if( _params->hasScalarParam(beads_bendKappa) )
	{
		_kb = 2 * _params->scalarParam(beads_bendKappa) / ( _b*_b*_b );
		
		if( _kb < 0.0 ) errors::throwException( beads_bendKappa+" must be positive", __FILE__, __LINE__ );
	}

	// Excluded volume interaction parameters. Use _EVEpsilon==0 to denote no EV interactions.
	_EVEpsilon = _params->scalarParamOrDefault( beads_epsilon, 0.0 );
	_EVSigma   = _params->scalarParamOrDefault( beads_sigma  , 0.0 );
	_EVCutOff  = _params->scalarParamOrDefault( beads_cutOff , 0.0 );

	if( _EVEpsilon<0.0 ) errors::throwException( "Excluded volume energy '"  + beads_epsilon + "' must be non-negative", __FILE__, __LINE__ );
	if( _EVSigma  <0.0 ) errors::throwException( "Excluded volume range '"   + beads_sigma   + "' must be non-negative", __FILE__, __LINE__ );
	if( _EVCutOff <0.0 ) errors::throwException( "Excluded volume cut-off '" + beads_cutOff  + "' must be non-negative", __FILE__, __LINE__ );

	// Fill in sensible default for the range and cut-off.
	if( _EVEpsilon && _EVSigma ==0.0 ) _EVSigma  = _b       / __twoToTheSixth;
	if( _EVEpsilon && _EVCutOff==0.0 ) _EVCutOff = _EVSigma * __twoToTheSixth;

	//
	// Expand containers to known sizes.
	//
	_X.resize( numDOF() );
	_F.resize( numDOF() );
	
	_X.shrink_to_fit();													// Since N and M fixed in this base class, can reduce memory
	_F.shrink_to_fit();													// by removing the (unnecessary) extra capacity.
	
	_nearbyBeads_numPairs.resize( omp_get_max_threads() );				// Per-thread containers filled by _getNearbyBeads() that store
	_nearbyBeads_indices .resize( omp_get_max_threads() );				// nearby bead indices and their separation squared.
	_nearbyBeads_rSqrd   .resize( omp_get_max_threads() );				// Note 'max' threads returns the number of OpenMP threads
	_nearbyBeads_dx      .resize( omp_get_max_threads() );				// for the next parallel region.

	//
	// Initialise the data-gathering (auxiliary) object.
	//
	_dataBeadsBase = std::unique_ptr<dataBeadsBase>( new dataBeadsBase(this,data,_dim,params) );
	
	//
	// Output to log.
	//
	logging::logFile << "Created instance of beadsBase with " << _N << " filament(s) each with " << _M <<" bead(s)." << std::endl;
	
	if( _EVEpsilon )
		logging::logFile << "Excluded volume interactions with energy \\epsilon=" << _EVEpsilon << ", range \\sigma=" << _EVSigma
		                 << " and cut-off=" << _EVCutOff << "." << std::endl;

	if( _box->fixedVolume() && _EVCutOff )
		logging::logFile << "Bead volume fraction (with radius based on excluded volume cut-off) = "
		                 << totalBeadVolume() / _box->volume() << "." << std::endl;

	if( _box->hasPeriodicBoundaries() )
	{
		__wrapToPrimaryCount = __wrapToPrimaryEvery;
		logging::logFile << "Will wrap filaments to primary cell every " << __wrapToPrimaryEvery << " iterations." << std::endl;
	}

	// Message about (somewhat ad hoc) force limitation. Could improve by moving to solver and determining threshold dynamically.
	logging::logFile << "Max force magnitude of " << __maxForceMag << " will be applied (number of times stored in table output)." << std::endl;
}
 


#pragma mark -
#pragma mark Initial Configurations
#pragma mark -

// Isotropic IC: All filaments straight with random position and orientation.
void beadsBase::setICIsotropic()
{
	// Scratch variables.
	double t[3];
	
	for( auto fil=0; fil<_N; fil++ )
	{
		// Get a random unit vector from the box object.
		_box->randomOrientation( t );
		
		// Start from a random location as given by the box object.
		_box->randomPosition( &_X[_index(fil,0)] );
		
		// Add bead by bead in the direction of t.
		for( auto bead=1; bead<_M; bead++ )
			for( auto k=0; k<_dim; k++ )
				_X[_index(fil,bead,k)] = _X[_index(fil,bead-1,k)] + _b * t[k];
	}
}

// Triangular lattice using parameters (probably) provided by the box. Assumes 2D.
void beadsBase::setICTriLattice( double a, int numX, int numY )
{
	// Sanity check.
	if( _dim!=2        ) errors::throwException( "Triangular lattice only possible in 2D"                , __FILE__, __LINE__ );
	if( !isMonomeric() ) errors::throwException( "Triangular lattice only possible for monomeric systems", __FILE__, __LINE__ );

	// Double loop over nodes.
	for( auto i=0; i<numX; i++ )
		for( auto j=0; j<numY; j++ )
		{
			auto i_j = i + j*numX;

			_X[_index(i_j,0,0)] = a * ( 0.25 + (j%2?i:i+0.5) );			// Quarter-cell shift (half would put nodes on the boundary).
			_X[_index(i_j,0,1)] = a * __root3over2 * ( j+0.5 );			// Half-cell shift vertically.
		}	
}


#pragma mark -
#pragma mark Utility methods.
#pragma mark -

void beadsBase::_tangent( int fil, int bead, double *tgt ) const
{
	// Get index of first and last bead on this filament.
	int
		firstBead = _firstBeadOnFilament(fil),
		lastBead  = ( fil==numFilaments() ? numBeads() : _firstBeadOnFilament(fil+1) ) - 1;
		
	// Can do nothing if only one bead; return tgt=0 to signify this.
	if( firstBead==lastBead )
	{
		for( auto k=0; k<_dim; k++ ) tgt[k] = 0.0;
		return;
	}

	// If at and end, use left or right finite difference, otherwise central.
	if( bead==firstBead )
		for( auto k=0; k<_dim; k++ ) tgt[k] = _X[_index(fil,bead+1,k)] - _X[_index(fil,bead  ,k)];
	
	if( bead==lastBead )
		for( auto k=0; k<_dim; k++ ) tgt[k] = _X[_index(fil,bead  ,k)] - _X[_index(fil,bead-1,k)];
	
	if( bead!=firstBead && bead!=lastBead )
		for( auto k=0; k<_dim; k++ ) tgt[k] = _X[_index(fil,bead+1,k)] - _X[_index(fil,bead-1,k)];

	// Normalise to a unit vector.
	_normalise(tgt);
}

void beadsBase::_normalise( double *x ) const
{
	double mag = sqrt( _dot(x,x) );
	for( auto k=0; k<_dim; k++ ) x[k] /= mag;
}


#pragma mark -
#pragma mark Separation and force calculations
#pragma mark -

// Distance squared between two beads, including any periodicity. Calculates then discards the separation vector.
double beadsBase::distSqrdBetweenBeads( int i, int j ) const
{
	double dx[3];

	for( auto k=0; k<_dim; k++ ) dx[k] = _X[_dim*j+k] - _X[_dim*i+k];
	_box->wrap( dx );

	return _dot(dx);
}

// Returns with the integer multiple of the box edge length between the two
// given bead indices (as 'bead2-bead1') for the given axis dimension.
int beadsBase::nmlsdPerShiftInDirn( int axis, unsigned long bead1, unsigned long bead2 ) const
{
	return _box->nmlsdPerShiftInDirn( axis, _X[_dim*bead2+axis] - _X[_dim*bead1+axis] );
}

// Get the bead indices and separations of all nearby beads, defined as those within the bare maximum interaction range
// as returned by the box ("bare" means without the scaling-up for the NN/Verlet lists).
// - Will not include beads that are adjacent on the same filament if notAdjacent==True.
void beadsBase::getNearbyBeads( bool notAdjacent )
{
	double maxRangeSqrd = _box->bareMaxInteractionRange() * _box->bareMaxInteractionRange();
	
	#pragma omp parallel
	{
		auto t = omp_get_thread_num();

		// Clear the index and separation-squared vectors; let the vector class worry about memory allocation.
		_nearbyBeads_numPairs[t] = 0u;

		_nearbyBeads_indices[t].clear();
		_nearbyBeads_rSqrd  [t].clear();
		_nearbyBeads_dx     [t].clear();

		// Use per-thread lists as calculated by the box object prior to calling.
		_perThread_getNearbyBeads( t, maxRangeSqrd, notAdjacent );
	}
}

// The per-thread method for calculation bead separations (bit long for a lambda expression).
void beadsBase::_perThread_getNearbyBeads( unsigned int threadNum, double maxRSqrd, bool notAdjacent )
{
	// Scratch variables.
	      unsigned long  numPairs = _box->numPairsInList   (threadNum);
	const unsigned long* pairList = _box->perThreadPairList(threadNum);
	double dx[3], r2;

	// Loop over all bead pairings.
	for( auto n=0u; n<numPairs; n++ )
	{
		auto i = pairList[2*n], j = pairList[2*n+1];

		// Always want i<j.
		if( i>j ) std::swap(i,j);

		// Skip adjacent beads on the same filament if requested. No need to check i==j+1 as j>i by this point.
		if( notAdjacent )
			if( _filamentForBead(i)==_filamentForBead(j) && j==i+1 ) continue;
		
		// Separation vector between bead centres, wrapped to any periodic BCs.
		for( auto k=0; k<_dim; k++ ) dx[k] = _X[_dim*j+k] - _X[_dim*i+k];
		_box->wrap( dx );
		
		// Distance squared. If exceeds maximum, skip to next pairing.
		r2 = _dot(dx);
		if( r2 > maxRSqrd ) continue;
		
		// Add to the per-thread lists.
		_nearbyBeads_numPairs[threadNum]++;

		_nearbyBeads_indices[threadNum].push_back( std::make_pair(i,j));
		_nearbyBeads_rSqrd  [threadNum].push_back( r2 );
		
		for( auto k=0; k<_dim; k++ )
			_nearbyBeads_dx[threadNum].push_back( dx[k] );	
	}
}


// Calculates all internal forces on the beads. Assumed to be called from within an
// OpenMP `#pragma omp parallel` region, but not a parallel loop - i.e. no `for`.
void beadsBase::threadedCalculateForces()
{
	// Clear the force vector.
	#pragma omp for
	for( auto i=0u; i<_F.size(); i++ ) _F[i] = 0.0;

	// Add the stretching and bending terms. Does not apply periodic wrapping for speed. Not required if monomeric.
	if( !isMonomeric() )
	{
		#pragma omp for
		for( auto fil=0; fil<numFilaments(); fil++ )
		{
			_addStretchForces(fil);
			_addBendForces(fil);
		}
	}

	// Get the excluded volume forces. Only if required, i.e. if epsilon>0.
	if( _EVEpsilon>0.0 )
		_addExcludedVolumeForces( omp_get_thread_num() );
}

// Stretch terms for the requested range of filaments.
void beadsBase::_addStretchForces( int fil )
{
	// Size of this filament.
	int M = _numBeadsOnFilament(fil);

	// Only if an actual filament, i.e. at least 2 beads on this filament.
	if( M<2 ) return;

	// Scratch variables.
	double dx[3], r, f_over_r;
	
	// Loop over all beads on this filament. Only need adjacent bead pairs, hence the loop range.
	for( auto bead=0; bead<M-1; bead++ )
	{
		// Separation vector between adjacent monomer centres. Ignore BC's here.
		for( auto k=0; k<_dim; k++ ) dx[k] = _X[_index(fil,bead+1,k)] - _X[_index(fil,bead,k)];

		r = sqrt( _dot(dx) );
		
		f_over_r = _ks * ( _b - r ) / r;
		
		for( auto k=0; k<_dim; k++ )
		{
			_F[_index(fil,bead+1,k)] += f_over_r * dx[k];
			_F[_index(fil,bead  ,k)] -= f_over_r * dx[k];
		}
		
		// Periodically add to the stress tensor.
		if( _dataBeadsBase->calculateStressNow() ) _dataBeadsBase->addToStressTensor_fr_r( - f_over_r, dx );
	}
}

// Bend terms for the requested range of filaments..
void beadsBase::_addBendForces( int fil )
{
	// No forces if there is no resistance to bending.
	if( !_kb ) return;

	// Beads on this filament.
	int M = _numBeadsOnFilament(fil);

	// Need triplets of beads on the same filament to have bending.
	if( M<3 ) return;

	// Scratch variables.
	double f[3];

	// Loop over all beads on this filament. Note this starts at 1 and ends before M-1, as only want triplets.
	for( auto bead=1; bead<M-1; bead++ )
	{
		// Get the vector force on the central bead.
		for( auto k=0; k<_dim; k++ )
			f[k] = _kb * ( _X[_index(fil,bead+1,k)] - 2*_X[_index(fil,bead,k)] + _X[_index(fil,bead-1,k)] );

		// Add force contributions.
		for( auto k=0; k<_dim; k++ )
		{
			_F[_index(fil,bead  ,k)] +=       f[k];
			_F[_index(fil,bead+1,k)] -= 0.5 * f[k];
			_F[_index(fil,bead-1,k)] -= 0.5 * f[k];
		}

		// Periodically add to the stress tensor.
		if( _dataBeadsBase->calculateStressNow() )
		{
			_dataBeadsBase->addToStressTensor_f_r( f, &_X[_index(fil,bead  )],  1.0 );
			_dataBeadsBase->addToStressTensor_f_r( f, &_X[_index(fil,bead+1)], -0.5 );
			_dataBeadsBase->addToStressTensor_f_r( f, &_X[_index(fil,bead-1)], -0.5 );
		}
	}
}

// Excluded volume forces.
void beadsBase::_addExcludedVolumeForces( unsigned int threadNum )
{
	// Scratch variables and shorthands.
	      unsigned long numPairs = _nearbyBeads_numPairs[threadNum];
	const indexPair*    pairList = _nearbyBeads_indices [threadNum].data();
	const double*       sepSqrds = _nearbyBeads_rSqrd   [threadNum].data();
	const double*       sepVecs  = _nearbyBeads_dx      [threadNum].data();

	double r2, sigmaOverR6, forceOverR, sigma2 = _EVSigma * _EVSigma, maxRSqrd = _EVCutOff * _EVCutOff;

	// Loop over all bead pairings.
	for( auto n=0u; n<numPairs; n++ )
	{
		// Recover separation-squared and apply checks.
		r2 = sepSqrds[n];
		if( r2 > maxRSqrd ) continue;			// Note EV range may differ from e.g. crosslinking range, so need to check here as well.
		if( !r2 ) errors::throwException( "Zero separation in excluded volume calculations", __FILE__, __LINE__ );
		
		// Get scalar force over the distrance r = sqrt(r2), raised to the power 6.
		sigmaOverR6 = sigma2 / r2;
		sigmaOverR6 = sigmaOverR6 * sigmaOverR6 * sigmaOverR6;
		
		forceOverR = 48.0 * _EVEpsilon * ( sigmaOverR6 - 0.5 ) * sigmaOverR6 / r2;
		
		// Apply force limitation and check how often applied (want to be essentially zero shortly after initialisation).
		if( std::abs(forceOverR) > __maxForceMag )
		{
			forceOverR = std::copysign(__maxForceMag,forceOverR);
			__diagNumForceLimits++;
		}

		// Add to the force vector, with no parallel synchronisation (expect small errors no larger than integration errors).
		for( auto k=0; k<_dim; k++ )
		{
			_F[ _dim*pairList[n].second + k ] += forceOverR * sepVecs[_dim*n+k];
			_F[ _dim*pairList[n].first  + k ] -= forceOverR * sepVecs[_dim*n+k];
		}
		
		// Add to stress tensor if required. Note sign flip for consistency with links (which subtracts f from second index).
		if( _dataBeadsBase->calculateStressNow() ) _dataBeadsBase->addToStressTensor_fr_r( - forceOverR, &sepVecs[_dim*n] );
	}
}


#pragma mark -
#pragma mark Accessors
#pragma mark -

// Returns the total volume occupied by beads (ignoring overlaps). Easy as they are mono-disperse.
// Take as the radius (half) the excluded volume cut-off.
double beadsBase::totalBeadVolume() const
{
	double radius = _EVCutOff / 2.0;

	return numBeads() * ( _dim==2 ? M_PI*radius*radius : (4*M_PI/3)*radius*radius*radius );
}


#pragma mark -
#pragma mark General updates
#pragma mark -

void beadsBase::update( double time, double dt )
{
	//
	// Wrap all beads to primary cell, to avoid excessive wrapping once they drift far apart.
	// Can be slow and only needs to be performed periodically, and only for periodic boxes.
	//
	if( _box->hasPeriodicBoundaries() )
	{
		if( --__wrapToPrimaryCount<0 )
		{
			// Reset count for next wrapping.
			__wrapToPrimaryCount = __wrapToPrimaryEvery;

			// Shorthand.
			double offset[3];

			// Move (by filament) into the primary cell.
			#pragma omp parallel for
			for( auto fil=0; fil<numFilaments(); fil++ )
			{
				// Get the offset; will be used by all beads on this filament.
				_box->offsetToPrimaryCell( &_X[_index(fil,0,0)], offset );

				// Move all beads by the same offset.
				for( auto bead=0; bead<_numBeadsOnFilament(fil); bead++ )
					for( auto k=0; k<_dim; k++ )
						_X[_index(fil,bead,k)] += offset[k];
			}
		}
	}
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Saving.
void beadsBase::saveState( flatXMLParser *xml ) const
{
	xml->saveStartElement( __saveBeadsTag );
	_saveBeadData(xml);
	xml->saveEndElement  ( __saveBeadsTag );
}

void beadsBase::_saveBeadData( flatXMLParser *xml ) const
{
	// First output the list of indices corresponding to the first bead on each filament.
	xml->addAttribute( __saveNumFilamentsAttr, numFilaments() );
	xml->saveStartElement( __saveFirstBeadsTag );
	
	for( auto fil=0; fil<numFilaments(); fil++ ) xml->appendDataString( _firstBeadOnFilament(fil) );
	xml->saveData();

	xml->saveEndElement( __saveFirstBeadsTag );
	
	// Output all positions. Do not bother with forces as they are transiently calculated anyway.
	xml->addAttribute( __saveNumBeadsAttr, numBeads() );
	xml->saveStartElement( __saveBeadPositionsTag );
	
	xml->appendDataString( _X );
	xml->saveData();

	xml->saveEndElement( __saveBeadPositionsTag );
}

// Loading.
void beadsBase::recoverState( flatXMLParser *xml )
{
	// Can read any attributes for main tag now.
	
	// May be overriden by child class.
	_recoverBeadData(xml);
}

void beadsBase::_recoverBeadData( flatXMLParser *xml )
{	
	// First beads (for polydisperse filaments) not yet supported, so just check for consistency.
	xml->readBlockWithTag( __saveFirstBeadsTag );

	if( atoi(xml->attribute(__saveNumFilamentsAttr).c_str()) != _N )
		errors::throwException( "Beads block in saved state has different number of filaments to parameters", __FILE__, __LINE__ );

	for( auto i=0; i<_N; i++ )
		if( _M*i != xml->dataVector()->at(i) )
			errors::throwException( "First bead indices in save state beads block does not match expected M", __FILE__, __LINE__ );
	
	// Get the bead positions.
	xml->readBlockWithTag( __saveBeadPositionsTag );

	if( atoi(xml->attribute(__saveNumBeadsAttr).c_str()) != _N*_M )
		errors::throwException( "Number of beads in saved state does match match the expected N*M", __FILE__, __LINE__ );
	
	// _X, _F should already be the correct size (given the above checks).
	for( auto i=0; i<numDOF(); i++ ) _X[i] = xml->dataArray()[i];
}

