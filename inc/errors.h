/**
 * @file errors.h
 * @author David Head
 * @brief Simple method(s) for throwing exceptions in the Bead-Spring code.
 */


#ifndef BEAD_SPRING_ERRORS_H
#define BEAD_SPRING_ERRORS_H


//
// Includes.
//
#include <sstream>
#include <stdexcept>


/// Simple method(s) for throwing exceptions in the Bead-Spring code.
namespace errors
{
	void throwException( std::string message, std::string file, int line );
}

#endif


