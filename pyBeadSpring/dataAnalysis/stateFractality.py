#
# Determines fractal dimension directly from state files, that is, not using the
# box counting table that is output by the beadSpring code. This is (a) to allow
# different types of fractal dimension to be measured, and (b) to allow for specific
# and non-standard selection of states.
#
# Note: Has never been tested on non-square systems or any 3D systems - should work
# in principle, but would to be checked.
#
# A typical session might look something like this.
#
# # Import the module.
# from pyBeadSpring.dataAnalysis.stateFractality import stateFractality
#
# # Extract states from a series of states.tar.gz files starting from the given path:
# sf = stateFractality.forArchivedFinalStates( <path> )
# print( sf )                       # Some basic statistics for the states just parsed.
#
# # Measure and fit box counting dimension.
# sf.boxCountDist()                 # Measure; can specify box sizes r considered.
# sf.fitBoxCountDist()              # Fit to Cook et al., Soft Matter, volume 19, pp. 2780-2791 (2023).
# sf.plotBoxCountDist()             # Display data and fit.
#
# The factory method forArchivedFinalStates() also has an option to extract the largest cluster,
# which is however slow for large systens. If this data is provided, all of the fit and plot routines
# will present results for the all-monomer data, and for monomers restricted to the largest cluster.
#
# For the spectral data, which returns the mean-squred displacements (MSD) of random walkers on the
# network and fits to a power law for the exponent (which is 2/d_w), the corresponding sequence is more like:
#
# sf.calculateSpectral( numWalkers=100, maxSteps=1000 )     # Simualtes multiple random walks.
# print( sf.calculateSpectralFit() )                        # Displays fit, including a readdable message.
# sf.plotSpectral()                                         # Plots the MSD data plus fiit.
#
# Note this is (a) scalar only, so would only give the scalar spectral dimension rather than the
# vector one, and in any case (b) still requires a (mass) fractal dimension to map to the spectral
# dimension using ds = 2 df / dw.
#

#
# Imports.
#

# Standard imports.
import os
import math
import sys
import heapq
import random

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

# Local to this module.
from .. import bsStates


#
# Main class definition.
#
class stateFractality( object ):

    #
    # Constructor.
    #
    def __init__( self, states : dict, boxDims : list ):
        """Pass with a list of states, each being a dictionary as extracted by bsStates.
        These will most likely have been extracted from saved state files using one of the provided factory
        (class) methods.
        
        Will need to specify the box dimensions as a list; the length of this also gives the dimension."""

        # Sanity check: Need at least 1 state.
        if not len(states):
            print( "Cannot initialise object of class stateFractality: Need at least one state." )
            sys.exit(-1)

        # Store the states and box dimension persistently.
        self._states = states
        self._boxDims = boxDims

        # Change positions in-place to numpy arrays, to speed up box counting.
        for state in self._states:
            posnArray = np.reshape( np.array(state["positions"],dtype="float64"), (state["numBeads"],self.dim()) )
            state["positions"] = posnArray

            # If the largest cluster indices were stored, also construct a numpy array of positions of monomers restricted to that cluster.
            if state["largestClusterIndices"] != None:
                clusterPosns = np.array( [ state["positions"][i] for i in state["largestClusterIndices"] ] )

            state["largestClusterPosns"] = ( None if state["largestClusterIndices"]==None else clusterPosns )
        
        # Initialise persistent quantities for the calculation of the box counting distribution.
        self._boxCount_r = None             # Box sizes for all box counts.

        self._boxCount_all_N = None         # Box count and fit for the full system ('all' monomers).
        self._boxCount_all_dN = None
        self._boxCount_all_fit = None

        self._boxCount_max_N = None         # Box count and fit for the largest cluster only.
        self._boxCount_max_dN = None
        self._boxCount_max_fit = None

        self._spectral_t = None             # Sim. for the spectral dimension.
        self._spectral_sqrdDisp = None
        self._spectral_error = None
        self._spectral_fit = None


    #
    # Standard methods.
    #
    def __repr__( self ):
        """Outputs some statistics about the states, intended for human reading on stdout."""
        msg  = f"stateFractality object with {len(self._states)} state(s):"
        msg += f"\n - dim={self.dim()} with box dimensions {self.boxDims()}."
        msg += f"\n - mean time point {self.meanTime()} and standard deviation {self.stdDevTime()}."
        msg += f"\n - mean number of beads {self.meanBeads()}."
 
        if self.meanInLargest() != None:
            msg += f"\n - mean number of beads in the largest cluster {self.meanInLargest()}."
 
        msg += f"\n - state dictionaries have {self._states[0].keys()}."

        return msg

    def __len__( self ):
        """Returns the number of states."""
        return len( self._states )

    #
    # Statistics about the stored states.
    #
    def dim( self ):
        """Returns the dimensionality of the box."""
        return len( self._boxDims )
    
    def boxDims( self ):
        """Returns the box dimensions as a list."""
        return self._boxDims

    def meanTime( self ):
        """Returns the mean time of all of the states."""
        return sum( [s["time"] for s in self._states] ) / len(self)

    def stdDevTime( self ):
        """Returns the unbiased standard deviation of the time points of all states."""

        # Number of states. Need at least one to return a meaningful standard deviation.
        n = len(self)
        if n <= 1:
            return None
        
        # Calculate the (biased) variance.
        mean = self.meanTime()
        sumSqrd = sum( [s["time"]**2 for s in self._states] )
        variance = ( sumSqrd/n - mean**2 )

        # Return with the (unbiased) standard deviation.
        return math.sqrt( n*variance/(n-1) )

    def meanBeads( self ):
        """Returns with the mean number of beads."""
        return sum( [s["numBeads"] for s in self._states] ) / len(self)
    
    def meanInLargest( self ):
        """Returns with the mean number of beads in the largest cluster, or 'None' if the largest cluster has not been provided."""
        if self._states[0]["largestClusterIndices"] != None:
            return sum( [len(s["largestClusterIndices"]) for s in self._states] ) / len(self)
        else:
            return None


    #
    # Box-counting dimension estimation.
    #

    # Functional form for fits to the box counting distribution.
    def _boxCount_logFitFn( self, r : float, xi : float, df : float, alpha : float ):
        """The functional form to fit to. xi is the crossover lengthscale (r' in the paper),
        df is the fractal dimension at short length scales, and 'alpha is the crossover exponent.
        The system (embedding) dimension is assumed at large length scales.
        See Cook et al., Soft Matter, volume 19, pp. 2780-2791 (2023).
        Note there was a typo in the publication - the denominator should be raised to the power alpha."""

        d, L = self.dim(), max(self._boxDims)

        numer = (L/xi)**d         + (L/xi)**df
        denom = (r/xi)**(d/alpha) + (r/xi)**(df/alpha)

        # Return the log of the prediction.
        return np.log( numer / denom**alpha )

    # Extract N(r). Also stores persistently. If the largest cluster has been provided, also calculate its distribution.
    def boxCountDist( self, rMin : float=1.0, rMult : float=2.0 ):
        """Calculates and returns the box counting dimension N(r), with r the box dize and
        N(r) the number of beads whose centres lie within a box of size r. If the positions of
        the largest cluster have been calculated, will separately calcdulate the distribution
        for the largest cluster.
        
        rMin  - lowest value of r.
        rMult - multiplier from the current value of r to the next highest."""

        # Sanity check.
        if rMin<=0.0:
            raise ValueError( f"Cannot perform box counting; minimum r value {rMin} must be positive." )

        if rMult<=1.0:
            raise ValueError( f"Cannot perform box counting: multiplier rMult={rMult} must be greater than 1" )

        # Loop over all monomers, or just the largest cluster.
        for largestCluster in (False,True):

            # Check (using the first state) if the largest cluster data has been provided in the first place.
            if largestCluster:
                try:
                    self._states[0]["largestClusterPosns"].shape            # Throws AttributeError if not a numpy array.
                except AttributeError:
                    continue

            # Initialise the arrays.
            rArray, sum_N, sum_Nsqrd = [], [], []

            # Loop through all r, starting from the minumum.
            r = rMin
            while r <= max(self._boxDims):
                
                # Initialise N taken across all states.
                total_N, total_Nsqrd = 0.0, 0.0

                # Loop through all states.
                for state in self._states:

                    # Bead positions have already been converted to a numpy array of size (nBeads,dim) in __init__().
                    positions = ( state["largestClusterPosns"] if largestCluster else state["positions"] )
                    scaledX   = ( positions / r ).astype( dtype="int" )

                    # np.unique() extracts unqique values, here (because of axis=0) unique cell indices (i,j) or (i,j,k).
                    boxCounts = np.unique( scaledX, axis=0 )

                    # N=boxCounts.shape[0] is the number of these (i.e. that contain at least one bead centre).
                    total_N     += boxCounts.shape[0]
                    total_Nsqrd += boxCounts.shape[0] ** 2

                # Store in the persistent arrays.
                rArray.append( r )
                sum_N.append( total_N )
                sum_Nsqrd.append( total_Nsqrd )

                # Increase the value of r.
                r *= rMult
            
            # Convert to numpy arrays and error bars.
            n = len(self)                   # Shorthand for the sample size = no. states.

            r  = np.array( rArray )
            N  = np.array( sum_N ) / n
            dN = ( np.zeros(r.shape) if n==1 else np.sqrt(np.array(sum_Nsqrd)/n-N**2)/(n-1) )

            # Store persistently.
            self._boxCount_r = r

            if largestCluster:
                self._boxCount_max_N = N
                self._boxCount_max_dN = dN
            else:
                self._boxCount_all_N = N
                self._boxCount_all_dN = dN

    # Plot the box counting data, with any fit that has been performed.
    def plotBoxCountDist( self, noShow : bool = False ):
        """Plots the box counting data N(r) and any recent fit. If the data (and fit) is available, will also
        plot the same data for the largest cluster only. Must have called the method to calculate
        N(r) prior to calling this method, otherwise will print an error message and return immediately.
        
        Uses matplotlib.pyplot and will display ("plt.show()") unless the flag to not show is set to True."""

        # Sanity check.
        try:
            if self._boxCount_r == None:
                print( "Cannot plot box count N(r): Not yet calculated." )
                return
        except ValueError:
            pass        # Probably means it was a numpy array.

        #
        # All-monomer data.
        #
        r, N, dN = self._boxCount_r, self._boxCount_all_N, self._boxCount_all_dN

        colour = "r"
        plt.errorbar( r, N, dN, capsize=5, fmt="o", color=colour, label="N(r)" )

        # If there has been a fit, plot that also.
        if self._boxCount_all_fit:
            plt.plot( r, np.exp(self._boxCount_logFitFn(r,*self._boxCount_all_fit[0])), ls="-", lw=1, color=colour, label="fit" )

        #
        # Largest cluster data.
        #
        try:

            N, dN = self._boxCount_max_N, self._boxCount_max_dN

            colour = "b"
            plt.errorbar( r, N, dN, capsize=5, fmt="o", color=colour, label="N(r) [largest cluster]" )

            if self._boxCount_max_fit:
                plt.plot( r, np.exp(self._boxCount_logFitFn(r,*self._boxCount_max_fit[0])), ls="-", lw=1, color=colour, label="fit [largest cluster]" )

        except ValueError:
            pass            # plt.errorbar() throws a ValueError if there was no data for the largest cluster data.

        #
        # Finalise and display.
        #
        plt.loglog()
        plt.legend()

        plt.xlabel( r"$r$" )
        plt.ylabel( r"$N(r)$" )

        if not noShow:
            plt.show()

    # Attempt to fit N(r) to a two-regime form.
    def fitBoxCountDist( self, p0 : list =[1.0,2.0,1.0], includeErrors : bool=True, largestCluster : bool=False ):
        """Attempts to fit the box counting data to the functional form of Cook et al., Soft Matter, volume 19,
        pp. 2780-2791 (2023). Must have called the method that calculates the box counting distribution prior
        to calling this method. Assumes that the embedding dimension, i.e. the dimension expected at large length
        scales, is the dimension of the states, and the maximum lengh L is the maximum system dimension.

        p0 = initial guess, in format [crossover length,fractal dimension at short lengths,sharpness exponent].
        If convergece fails, setting this to values that approximately work "by eye" may help convergence.

        includeErrors is a True/False flag to include the errors. If you get a warning about failure to calculate
        the covariance, consider setting this to 'False'.

        largestCluster is True/False to use the largest cluster data or not. If the data does not exist, an exceptin
        will be thrown.

        The fit function is:

                (L/xi)**d         + (L/xi)**df
        N(r) =  --------------------------------------
                (r/xi)**(d/alpha) + (r/xi)**(df/alpha)

        Returns with a dictionary with the returned fit and a readable interpretation.
        """

        # Sanity check.
        try:
            if self._boxCount_r == None:
                print( "Cannot fit to the box count N(r): Not yet calculated." )
                return
        except ValueError:
            pass        # Presume a thrown ValueError in the comparison means it was a numpy array.
 
        # Perform the fit. Actually fit the log of the data to the log of the prediction.
        r  =   self._boxCount_r
        N  = ( self._boxCount_max_N  if largestCluster else self._boxCount_all_N  )
        dN = ( self._boxCount_max_dN if largestCluster else self._boxCount_all_dN )

        fit = scipy.optimize.curve_fit( self._boxCount_logFitFn, r, np.log(N), p0=p0, sigma=(dN/N if includeErrors else None) )
            # Relative sigma "(dN/N)" is True by default.

        # Store the fit persistently.
        if largestCluster:
            self._boxCount_max_fit = fit
        else:
            self._boxCount_all_fit = fit

        # Return with fit parameters and a string to including function.
        fitmsg  =  "fit to two-regime form with "
        fitmsg += f"crossover length xi = {fit[0][0]} ({math.sqrt(fit[1][0][0])}), "
        fitmsg += f"short-length fractal dimension df = {fit[0][1]} ({math.sqrt(fit[1][1][1])}), "
        fitmsg += f"and crossover exponent alpha = {fit[0][2]} ({math.sqrt(fit[1][2][2])})."

        return { "fit":fit, "message":fitmsg }


    #
    # Spectral dimension estimation.
    #

    # For a single walker. Not intended for the user to call this directly.
    def _singleWalk( self, maxSteps : int, state : dict ):
        """Performs a single random walk starting from a randomly chosen node on the given state, and moving
        along one connection for each time step. Returns with squared displacement as a function of the number of time
        steps, as a 1D numpy array of length maxSteps - the trivial t=0 value is omitted. Assumes the connectivity
        lists have been calculated. Not intended to be called directly; normally the user would call another method
        such as ensembleWalk()."""

        #
        # Sanity checks for parameters.
        #
        if maxSteps <= 0:
            print( "FAILED in stateFractality::singleWalk(): Maximum number of steps must be positive." )
            return

        #
        # Shorthands.
        #
        nNodes = state["numBeads"]

        #
        # Initialise the random walk.
        #
        node = random.randrange( nNodes )                           # Initial node chosen randomly.
        x = np.array( [0.0]*self.dim(), dtype="float64" )           # d-dim vector for x coordinates.
        sqrdDisp = np.zeros( maxSteps, dtype="float64" )            # Sill store the squared displacements along the walk.

        #
        # Random walk main loop.
        #
        for t in range( maxSteps ):

            # Number of possible connections/crosslinks to move along.
            nConns = len( state[self.connListsLabel()][node] )
  
            # Choose next node. If there are no connections, the displacement is still included, but remains zero.
            # (Could optimise for this by precalculating the final result, but should be rare anyway).
            nextConnection = random.randrange(nConns) if nConns>0 else node

            # The next node to move to.
            nextNode = ( node if nConns==0 else state[self.connListsLabel()][node][nextConnection] )

            # The difference in position from the prevoous node, assuming fully periodic BCs.
            # Note if the BCs were not in fact periodic, this wrapping will be redundant rather than wrong.
            dx = state["positions"][nextNode] - state["positions"][node]

            for dim in range(self.dim()):

                while dx[dim] < -0.5 * self.boxDims()[dim]:
                    dx[dim] += self.boxDims()[dim]

                while dx[dim] >  0.5 * self.boxDims()[dim]:
                    dx[dim] -= self.boxDims()[dim]

            # Update node position and running displacement vector.
            node = nextNode
            x += dx

            # Update the (not mean) squared displacement. t=0 omitted.
            sqrdDisp[t] = np.sum( x*x )

        return sqrdDisp

    def calculateSpectral( self, numWalkers : int, maxSteps : int ):
        """Performs an ensemble of random walks, each starting from a randomly-selected node. Returns with the time points, the mean squared
        displacements, and the standard error of the same, all as 1D numpy arrays of length maxSteps."""

        #
        # Sanity check for number of walkers and number of steps.
        #
        if numWalkers <= 0:
            print( "FAILED in randomWalk::ensembleWalk(): Number of walkers must be positive." )
            return

        if maxSteps <= 0:
            print( "FAILED in randomWalk::ensembleWalk(): Maximum number of steps must be positive." )
            return

        #
        # Shorthands.
        #
        nStates = len(self._states)

        #
        # Required connectivity lists for each state which are not part of the standard objects.
        #
        for state in self._states:
            if self.connListsLabel() not in state.keys():

                # Currently only support monomeric systems. Should not be too hard to generalise to non-monomeric,
                # e.g. by also including adjacent beads on the same filament on the connectivity list.
                if state["numFilaments"] != state["numBeads"]:
                    print( "FAILED in stateFractality::ensembleWalk(): Currently only supports monomeric systems." )
                    return

                # Convert indices to a numpy array in-place.
                state["indices"] = np.array( state["indices"], dtype="int32" )

                # Construct the connectivity lists for each node. Tends to be pretty fast in practice, so no warning or option to not do this.
                state[ self.connListsLabel() ] = []
                for n in range( state["numBeads"] ):

                    # Get all nodes connected to n. In the loop, i is the index in the full list of crosslink indices, which are paired by crosslink.
                    conns = []
                    for i in np.asarray( state["indices"]==n ).nonzero()[0].tolist():
                        if i%2 == 0:
                            conns.append( state["indices"][i+1] )       # The other node index is either the next or the previous one on the full list,
                        else:                                           # depending on whether i is odd or even.
                            conns.append( state["indices"][i-1] )

                    # Add to the connectivity list-of-lists.
                    state[ self.connListsLabel() ].append( conns )

        #
        # Calculate the squared-displacements of multiple walkers per state.
        #
        perRunSqrdDisps = np.zeros( [nStates,maxSteps] )
        for stateIndex, state in enumerate(self._states):

            # Get the mean squared displacements for the given number of walkers for this state.
            sqrdDisps = np.zeros( [numWalkers,maxSteps] )
            for walker in range(numWalkers):
                sqrdDisps[walker,:] = self._singleWalk(maxSteps,state)

            # Add the mean to the per-run displacement-squared time series.
            perRunSqrdDisps[stateIndex,:] = np.mean(sqrdDisps,0)            # '0' axis to average between walkers.

        #
        # Store the time axis, the MSD, and the corresponding standard error between runs.
        #
        self._spectral_t = np.array( [1+t for t in range(maxSteps)] )
        self._spectral_sqrdDisp = np.mean(perRunSqrdDisps,0)
        self._spectral_error = ( np.zeros(maxSteps) if nStates==1 else np.std(perRunSqrdDisps,0)/math.sqrt(nStates-1) )

        #
        # Also return in format (x,y,dy).
        #
        return self._spectral_t, self._spectral_sqrdDisp, self._spectral_error

    # Functional form for fits to the spectral data.
    def _spectral_logFitFn( self, t, A : float, b : float ):
        """The functional form to fit the spectral data to. Simple power law Ax^{b}, logged (so need to fit to the log of the data)."""
        return np.log(A) + b * np.log(t)

    # Calculates the fit to the spectral data. Call after calculateSpectral().
    def calculateSpectralFit( self, p0 : list = [1,1], includeErrors : bool = True ):
        """Calculates the fit to the spectral fitting functions. Returns with a dictionary with the fit and a readable message.
        Also stores persistently; will be plotted alongside the data when plotSpectral() is called. p0 is the first guess
        to the fitting function Ax^{b} as [A,b]. The includeErrors flag trys to include the errors into the fit."""

        #
        # Sanity check that we have the data.
        #
        try:
            len(self._spectral_t)
        except:
            print( "FATAL in stateFractality::plotSpectral(): Spectral data must be calculated before it is plotted." )
            return
    
        # Perform the fit of log-data to log-function.
        t     = self._spectral_t
        disp2 = self._spectral_sqrdDisp
        error = self._spectral_error 

        fit = scipy.optimize.curve_fit( self._spectral_logFitFn, t, np.log(disp2), p0=p0, sigma=(disp2/error if includeErrors else None) )

        # Store the fit persistently.
        self._spectral_fit = fit

        # Return with fit parameters and a string to including function.
        fitmsg  =  "fit of displacement squared as a power law in time, |dx|^{2}=A t^{b},"
        fitmsg += f"prefactor A = {fit[0][0]} ({math.sqrt(fit[1][0][0])}) "
        fitmsg += f"and exponent b = {fit[0][1]} ({math.sqrt(fit[1][1][1])}) "
        fitmsg +=  "(note b=2/d_w and (scalar) d_s = 2 d_f / d_w)."

        return { "fit":fit, "message":fitmsg }
    

    # Plot the spectral data. Call after calculateSpectral().
    def plotSpectral( self, noShow : bool = False ):
        """Plots the spectral data and any fit. Assumes calculateSpectral() has already been called. If not, witll print an
        error message and return immediately. Option to not call 'plt.show()' at the end; useful if adding annotation."""

        #
        # Sanity check that we have the data.
        #
        try:
            len(self._spectral_t)
        except:
            print( "FATAL in stateFractality::plotSpectral(): Spectral data must be calculated before it is plotted." )
            return

        #
        # Plot the data.
        #
        t = self._spectral_t
        plt.errorbar( t, self._spectral_sqrdDisp, self._spectral_error, fmt="ro", capsize=5 )

        #
        # If the fit has been calculated, also plot that.
        #
        if self._spectral_fit:
            Ab = self._spectral_fit[0]
            plt.plot( t, np.exp( self._spectral_logFitFn(t,*Ab)), "k-", lw=2, label="fit" )

        #
        # Finalise and display (unless noShow==True).
        #
        plt.xlabel( "No. steps" )
        plt.ylabel( r"$|\Delta x|^{2}$" )

        plt.legend()
        plt.loglog()

        if not noShow:
            plt.show()

    #
    # Static methods.
    #

    # Label for the connectivity lists.
    @staticmethod
    def connListsLabel():
        "Label for the connectivity lists, which is a list-of-lists where the list for each node contains those nodes connected to it."
        return "connLists"

    #
    # Factory (class) methods.
    #
    @classmethod
    def forArchivedFinalStates(
            cls,
            path : str,
            statesArchive : str = "states.tar.gz",
            getLargestCluster : bool = False,
            firstRunOnly : bool = False
        ):
        """Looks for all state archives starting from the given path, and extracts the final state
        from each one. The full list is then used to initialise an object of 'stateFractality', which is then
        returned. There is also an option to extract the largest cluster and store its indices in
        the state dictionary; this can be slow, hence it is 'False' by default. It is also possible to
        only include the last state for the first run only; can be useful when debugging large systems."""

        # Collection of bsStates objects, each containing just a single state.
        states = []

        # Box dimensions. Will need to match all states and will be passed to the stateFractality constructor.
        boxDims = None

        # Walk through all run subdirectories.
        for root, dirs, files in os.walk( path ):
            if statesArchive in files:

                # Use the bsStates object with the final state option for each archive.
                run = bsStates( os.path.join(root,statesArchive), finalArchiveStateOnly=True )

                # Check all box dimensions are consistent.
                if boxDims == None:
                    boxDims = run.boxDims()
                else:
                    if len(boxDims) != len( run.boxDims() ):
                        print( "Cannot extract states: They do not all have the same number of box dimensions." )
                        sys.exit( -1 )

                    for i, L in enumerate(boxDims):
                        if L != run.boxDims()[i]:
                            print( "Cannot extract states: Box dimensions are not all the same." )
                            sys.exit( -1 )

                # Check to see if the indices of the largest cluster should also be stored.
                state = run.getState(0)
                state["largestClusterIndices"] = ( heapq.nlargest(1,run.getClusters(0),key=len)[0] if getLargestCluster else None )

                # Add to the list.
                states.append( state )

                # If onlt the first run was requested, we are done.
                if firstRunOnly:
                    break

        # Construct an instance of stateFractality() with these states.
        sf = stateFractality( states, boxDims )

        # Return with the stateFractality object.
        return sf


