/**
 * @file boxLEOscillatory.h
 * @author David Head
 * @brief Implements oscillatory shear in the x-y plan through Lees-Edwards boundary conditions.
 */


#ifndef BEAD_SPRING_BOX_LE_OSCILLATORY_H
#define BEAD_SPRING_BOX_LE_OSCILLATORY_H


//
// Includes.
//

// Standard includes.
#include <cmath>

// Project includes.
#include "dataBoxLEOscillatory.h"

// Parent class.
#include "boxLeesEdwards.h"



///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string boxType_LEOscillatory = "LEOscillatory";
	
	const std::string boxLEOscillatory_amplitude = "gamma0";			// Required.
	const std::string boxLEOscillatory_frequency = "omega";				// Required.
	const std::string boxLEOscillatory_startTime = "t0";				// Optional.
}


/**
 * @brief Implements oscillatory shear in the x-y plan through Lees-Edwards boundary conditions.
 *
 * Applies \f$\gamma(t) = \gamma_{0} \sin[ \omega*(t-t_{0}) ] \f$ for \f$ t>t_{0}\f$, 0 otherwise.
 * (sine is used so that strain increases smoothly at \f$t=t_{0}\f$).
 *
 * \f$t_{0}\f$ optional and defaults to zero.
 */
class boxLEOscillatory : public boxLeesEdwards
{

private:

protected:

	double _omega;					///< \f$ \omega \f$
	double _gamma0;					///< \f$ \gamma_{0} \f$
	double _startTime;				///< \f$ t_{0} \f$
	
	// State I/O.
	virtual void _saveBoxState   ( flatXMLParser *xml ) const override;
	virtual void _recoverBoxState( flatXMLParser *xml )       override;

	//
	// Data.
	//	
	std::unique_ptr<dataBoxLEOscillatory> _dataBoxLEOscillatory;
	friend dataBoxLEOscillatory;

public:

	/// Main constructor.
	boxLEOscillatory( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );
	virtual ~boxLEOscillatory() {}

	/// Applies current shear strain \f$ \gamma \f$ given time. Also updates the data class.
    virtual void update( double time, double dt ) override;
};



#endif
