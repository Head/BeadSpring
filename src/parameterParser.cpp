// Class definition
#include "parameterParser.h"


#pragma mark -
#pragma mark Parsing
#pragma mark -

// Initialise from filename
void parameterParser::parseFile( const std::string &filename )
{
	// Try to open up the text reader object; throws exception if ptr is returned NULL.
	flatXMLParser xml( filename, ios::in );

	// Jump to the block that has all of the parameters; assumed to be the only one.
	xml.readStartElementWithTag( __parametersBlock, true );

	// Walk through the block node-by-node. 'readBlockWithTag' returns 0 if no firther tags of that name were found.
	while( xml.readBlockWithTag(__parameterTag) )
		_IDs.push_back( _processNode(xml.attributesMap()) );
}


#pragma mark -
#pragma mark Utility
#pragma mark -

// Routines that indicate if a supplied string is likely to be integer or scalar (called AFTER checking for bools/flags)
bool parameterParser::_looksLikeInteger( string value ) const
{
	bool integer = true;
	for( string::const_iterator iter=value.begin(); iter!=value.end(); iter++ )
		integer = integer and __integer_char(*iter);
	return integer;
}
bool parameterParser::_looksLikeFloat( string value ) const
{
	bool decimal = true;
	for( string::const_iterator iter=value.begin(); iter!=value.end(); iter++ )
		decimal = decimal and __decimal_char(*iter);
	return decimal;
}


#pragma mark -
#pragma mark Parsing
#pragma mark -

// Parse a single node, provided as a std::map with both keys and values as std::strings.
// Returns the unique ID for that node.
int parameterParser::_processNode( std::map<std::string,std::string> attrs )
{
	//
	// Option 1: Parameter stored verbosely with "type", "name" and "value" attributes
	//
	if( attrs.size()==3 && attrs.count("type" )==1 && attrs.count("name" )==1 && attrs.count("value" )==1 )
	{
		// Try to parse the value according to its type; case sensitive (lower case assumed for attribute names).
		
		// Scalars
		if( attrs["type"]=="scalar" || attrs["type"]=="float" )
		{
			_scalars[_uniqueID] = _to_float(attrs["value"]);			// Returns 0.0 if could not be converted
			_names  [_uniqueID] = attrs["name"];
			return _uniqueID++;
		}
		
		// Integers
		if( attrs["type"]=="int" || attrs["type"]=="integer" )
		{
			_integers[_uniqueID] = _to_long(attrs["value"]);		// Returns 0 if could not be converted
			_names   [_uniqueID] = attrs["name"];
			return _uniqueID++;
		}

		// Switches
		if( attrs["type"]=="flag" || attrs["type"]=="bool" )
		{
			_flags[_uniqueID] = ( attrs["value"]=="yes" || attrs["value"]=="true" );
			_names[_uniqueID] = attrs["name"];
			return _uniqueID++;
		}

		// Labels
		if( attrs["type"]=="label" || attrs["type"]=="string" )
		{
			_labels[_uniqueID] = attrs["value"];
			_names [_uniqueID] = attrs["name"];
			return _uniqueID++;
		}
	}
	
	//
	// Option 2: The more concise form '<Parameter name="value" />'
	//
	if( attrs.size()==1 )
	{
		// The name and value of the parameter correspond to the only entry in the map.
		string name  = attrs.begin()->first;
		string value = attrs.begin()->second;

		// Need to determine type of the value. First try boolean.
		if( value=="true" || value=="True" || value=="TRUE" || value=="yes" || value=="Yes" || value=="YES"  ) 
		{
			_names[_uniqueID] = name;
			_flags[_uniqueID] = true;
			return _uniqueID++;
		}
		if( value=="false" || value=="False" || value=="FALSE" || value=="no" || value=="No" || value=="NO" )
		{
			_names[_uniqueID] = name;
			_flags[_uniqueID] = false;
			return _uniqueID++;
		}

		// If not, see if all numbers or minus signs; if nothing else, must be an integer (note that if the string had units
		// at the end, this would automatically violate this criterion and assume it was a scalar; this is fine, as dimensional
		// units are usually non-integer anyway)
		if( _looksLikeInteger(value) )
		{
			_names   [_uniqueID] = name;
			_integers[_uniqueID] = _to_long(value);
			return _uniqueID++;
		}

		// Try again for floats
		if( _looksLikeFloat(value) )
		{
			_names  [_uniqueID] = name;
			_scalars[_uniqueID] = _to_float(value);
			return _uniqueID++;
		}

		// If still here, assume it is a text variable
		_names [_uniqueID] = name;
		_labels[_uniqueID] = value;
		return _uniqueID++;
	}

	// If still here, was not parsed. Throw an exception.
	throw std::runtime_error( "Could not parse the parameters file - at least one parameter node had the incorrect number of attributes." );
}



#pragma mark -
#pragma mark Output
#pragma mark -

// Definition of overloaded '<<' operator acting on an ostream; outputs in XML format as a single block
ostream& operator<< ( ostream &os, const parameterParser &p )
{
	// Open tag for whole block (nb: not the whole file, so no ?xml-element)
	os << "<" << p.__parametersBlock << ">" << endl;
	
	// List all parameters/
	for( vector<long>::const_iterator c_iter=p._IDs.begin(); c_iter!=p._IDs.end(); c_iter++ )
		if( *c_iter != -1 )
		{
			os << "\t";
			p._outputSingleParameter( *c_iter, os );
			os << "\n";
		}
	
	// End tag for the whole output
	os << "</" << p.__parametersBlock << ">";

	return os;
}

// Outputs a single parameter as a single, self-closing XML node (with no tabs prefixed or eol) to the given output stream.
void parameterParser::_outputSingleParameter( long id, ostream &os ) const
{
	os << "<" << __parameterTag << " name='" << _names.find(id)->second << "'";

	if( _scalars.find(id) != _scalars.end() )
		os << " type='scalar' value='" << _scalars.find(id)->second << "'";

	if( _integers.find(id) != _integers.end() )
		os << " type='int' value='" << _integers.find(id)->second << "'";

	if( _flags.find(id) != _flags.end() )
		os << " type='flag' value='" << ( _flags.find(id)->second ? "true" : "false" ) << "'";

	if( _labels.find(id) != _labels.end() )
		os << " type='label' value='" << _labels.find(id)->second << "'";

	os << " />";				// Space; end of element node; also a new line.
}


#pragma mark -
#pragma mark Direct control of parameters
#pragma mark -

// Add ungrouped parameters; raises and exception if one with the same name and type already exists
void parameterParser::addScalarParam( const string &name, const double &val )
{
	if( hasScalarParam(name) || hasIntegerParam(name) ) throw ParameterAlreadyExists(name);
		
	_scalars[_uniqueID] = val;
	_names[_uniqueID]   = name;
	_IDs.push_back(_uniqueID++);
}

void parameterParser::addIntegerParam( const string &name, const long &val )
{
	if( hasIntegerParam(name) ) throw ParameterAlreadyExists(name);

	_integers[_uniqueID] = val;
	_names[_uniqueID] = name;
	_IDs.push_back(_uniqueID++);
}

void parameterParser::addLabelParam( const string &name, const string &val )
{
	if( hasLabelParam(name) ) throw ParameterAlreadyExists(name);

	_labels[_uniqueID] = val;
	_names[_uniqueID] = name;
	_IDs.push_back(_uniqueID++);
}

void parameterParser::addFlagParam( const string &name, const bool &val )
{
	if( hasFlagParam(name) ) throw ParameterAlreadyExists(name);

	_flags[_uniqueID] = val;
	_names[_uniqueID] = name;
	_IDs.push_back(_uniqueID++);
}

// Change existing, ungrouped parameters
void parameterParser::changeScalarParam( const string &name, const double &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || ( _scalars.find(ID)==_scalars.end() && _integers.find(ID)==_integers.end() ) ) throw CouldNotFindParameterError(name);
	if( _scalars .find(ID)!=_scalars .end() ) _scalars[ID]  = val;
	if( _integers.find(ID)!=_integers.end() ) _integers[ID] = (long)val;
}

void parameterParser::changeIntegerParam( const string &name, const long &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _integers.find(ID) == _integers.end() ) throw CouldNotFindParameterError(name);
	_integers[ID] = val;
}

void parameterParser::changeLabelParam( const string &name, const string &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _labels.find(ID) == _labels.end() ) throw CouldNotFindParameterError(name);
	_labels[ID] = val;
}

void parameterParser::changeFlagParam( const string &name, const bool &val )
{
	long ID = _IDForName(name);
	if( ID==-1 || _flags.find(ID) == _flags.end() ) throw CouldNotFindParameterError(name);
	_flags[ID] = val;
}

// Remove parameters; throw exception if they do not exist
void parameterParser::removeScalarParam( const string &name )
{
	long ID = _IDForName(name);
	if(ID==-1 || ( _scalars.find(ID)==_scalars.end() && _integers.find(ID)==_integers.end() ) ) throw CouldNotFindParameterError(name);
	_removeID(ID);
}
void parameterParser::removeIntegerParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _integers.find(ID) == _integers.end() ) throw CouldNotFindParameterError(name);
	_removeID(ID);
}
void parameterParser::removeLabelParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _labels.find(ID) == _labels.end() ) throw CouldNotFindParameterError(name);
	_removeID(ID);
}
void parameterParser::removeFlagParam( const string &name )
{
	long ID = _IDForName(name);
	if( ID==-1 || _flags.find(ID) == _flags.end() ) throw CouldNotFindParameterError(name);
	_removeID(ID);
}

// Remove an ID from the name map, ID vector and value map.
void parameterParser::_removeID( long ID )
{		
	// Remove the name (assume exists)
	_names.erase(ID);

	// Value maps
	if( _scalars .find(ID)!=_scalars .end() ) _scalars .erase(ID);
	if( _integers.find(ID)!=_integers.end() ) _integers.erase(ID);
	if( _labels  .find(ID)!=_labels  .end() ) _labels  .erase(ID);
	if( _flags   .find(ID)!=_flags   .end() ) _flags   .erase(ID);
	
	// Remove from the list of IDs.
	for( vector<long>::iterator iter=_IDs.begin(); iter!=_IDs.end(); iter++ )
		if( *iter == ID ) { _IDs.erase(iter); break; }
}

// Adding or changing the value of parameter without specifying the type.
void parameterParser::overwriteOrAddParam( std::string name, std::string value )
{
	//
	// Remove any parameter with the same label.
	//
	try 
	{
		removeScalarParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeIntegerParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeFlagParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	try 
	{
		removeLabelParam( name );
	}
	catch( CouldNotFindParameterError &err ) {}

	//
	// Create new parameter of the required type.
	//

	// Check for a boolean type first of all.
	if( value=="true" || value=="True" || value=="TRUE" || value=="yes" || value=="Yes" || value=="YES"  ) 
	{
		addFlagParam(name,true);
		return;
	}

	if( value=="false" || value=="False" || value=="FALSE" || value=="no" || value=="No" || value=="NO" )
	{
		addFlagParam(name,false);
		return;
	}
	
	// Integer parameters.
	if( _looksLikeInteger(value) && !hasScalarParam(name) )
	{
		addIntegerParam( name, _to_long(value) );
		return;
	}
	
	// Float parameters.
	if( _looksLikeFloat(value) )
	{
		addScalarParam( name, _to_float(value) );
		return;
	}

	// If still here, assume it was a label.
	addLabelParam( name, value );
}

#pragma mark -
#pragma mark Checking existence of parameters
#pragma mark -

// Ungrouped parameters
bool parameterParser::hasScalarParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && ( _scalars.find(ID)!=_scalars.end()||_integers.find(ID)!=_integers.end() ) );		// Also check integers
}
bool parameterParser::hasIntegerParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_integers.find(ID) != _integers.end()) );
}
bool parameterParser::hasLabelParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_labels.find(ID) != _labels.end()) );
}
bool parameterParser::hasFlagParam( const string &name ) const
{
	int ID = _IDForName(name);
	return ( (ID!=-1) && (_flags.find(ID) != _flags.end()) );
}

// Returns the unique ID for the given parameter name, or -1 if it was not found
long parameterParser::_IDForName( const string &name ) const
{
	for( map<long,string>::const_iterator c_iter=_names.begin(); c_iter!=_names.end(); c_iter++ )
		if( name.compare(c_iter->second)==0 )
			return c_iter->first;
	return -1;
}


#pragma mark -
#pragma mark Accessing parameters
#pragma mark -

// Return ungrouped parameters or raise an exception if not found (after trying for an integer)
double parameterParser::scalarParam( const string &name ) const
{
	long ID = _IDForName(name);
	if( _scalars .find(ID)!=_scalars .end() ) return _scalars .find(ID)->second;		// On scalars list; if not ...
	if( _integers.find(ID)!=_integers.end() ) return _integers.find(ID)->second;		// ... try the integers; if not ...
	throw CouldNotFindParameterError(name);												// not defined.
}
long parameterParser::integerParam( const string &name ) const
{
	if( !hasIntegerParam(name) ) throw CouldNotFindParameterError(name);
	return _integers.find(_IDForName(name))->second;
}
bool parameterParser::flagParam( const string &name ) const
{
	if( !hasFlagParam(name) ) throw CouldNotFindParameterError(name);
	return _flags.find(_IDForName(name))->second;
}
string parameterParser::labelParam( const string &name ) const
{
	if( !hasLabelParam(name) ) throw CouldNotFindParameterError(name);
	return _labels.find(_IDForName(name))->second;
}

// Return ungrouped parameters or the specified default if not found
double parameterParser::scalarParamOrDefault( const string &name, const double &def ) const
{
	if( hasIntegerParam(name) ) return integerParam(name);
	if( hasScalarParam (name) ) return _scalars.find(_IDForName(name))->second;
	return def;
}
long parameterParser::integerParamOrDefault( const string &name, const long &def ) const
{
	if( !hasIntegerParam(name) ) return def;
	return _integers.find(_IDForName(name))->second;
}
bool parameterParser::flagParamOrDefault( const string &name, const bool &def ) const
{
	if( !hasFlagParam(name) ) return def;
	return _flags.find(_IDForName(name))->second;
}
string parameterParser::labelParamOrDefault( const string &name, const string &def ) const
{
	if( !hasLabelParam(name) ) return def;
	return _labels.find(_IDForName(name))->second;
}
