#include "dataBeadsBase.h"


// Need definition of the main class now.
#include "beadsBase.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

dataBeadsBase::dataBeadsBase(
	const beadsBase *beads,
	std::shared_ptr<dataBeadSpring> data,
	short dim,
	const parameterParser *params )
	: _dim(dim), _beads(beads), _box(beads->_box), _dataBeadSpring(data)
{
	using namespace parameterLabels;

	// Parameters relating to data acquisition.
	_gr_yesNo = params->flagParamOrDefault( databeads_grYesNoAttr, false );

	// Ensure the initial time point has all labels that will end up in the table [i.e. not those in calculateTableRow()].
	if( !_dataBeadSpring->continuationRun() )
		_dataBeadSpring->addScalar( __dataLabel_diagNumForceLimits, 0 );

	// Output to log file.
	logging::logFile << ( _gr_yesNo ? "O" : "Not o") << "utputting the pair correlation function g(r) [set by flag "
	                 << "parameter '" << databeads_grYesNoAttr << "']." << std::endl;
}


#pragma mark -
#pragma mark Block data
#pragma mark -

void dataBeadsBase::calculateBlock() const
{
	_blockTangentCorrn();
	_blockBoxCounting ();

	if( _gr_yesNo ) _blockBeadPairDist();		// Only if requested (can be slow to calculate).
}

void dataBeadsBase::_blockTangentCorrn() const
{
	// Scratch variables.
	double t1[3], t2[3];

	// Loop over filaments, then beads within each filament.
	for( auto fil=0; fil<_beads->numFilaments(); fil++ )
	{
		int M = _beads->_numBeadsOnFilament(fil);

		for( auto i=0; i<M-1; i++ )
		{
			_beads->_tangent( fil, i, t1 );

			for( auto j=0; j<M-1; j++ )
			{
				_beads->_tangent( fil, j, t2 );

				_dataBeadSpring->addToFixedHist( __dataLabel_tgtCorrn, std::abs((long)j-i), _dot(t1,t2), 0, M-1, M-1 );
			}
		}
	}
}

void dataBeadsBase::_blockBeadPairDist() const
{
	// Shorthands.
	auto N = _beads->numBeads();
	auto X = _beads->positionVector();	
	
	// Sensible defaults for binning. Could also make parameters ... ?
	double binWidth = ( _beads->_b ? _beads->_b : _beads->_EVCutOff ) / 5.0;
	int    numBins  = int( 0.5*_box->shortestDimension() / binWidth );

	// Use a std::vector<> for this time instant's g(r).
	std::vector<double> g_r(numBins);
	for( auto &g : g_r ) g = 0.0;

	// Double loop over all pairings (cannot use NN/Verlet list here as need all pairings, including non-interacting).
	for( auto i=0; i<N; i++ )
		for( auto j=i+1; j<N; j++ )
		{
			// Separation vector between the two node centres, with wrapping.
			double dx[3];
			for( auto k=0; k<_dim; k++ ) dx[k] = X[j*_dim+k] - X[i*_dim+k];
			_box->wrap( dx );

			// Convert to a scalar separation and hence a bin index.
			double r = sqrt( _dot(dx) );
			int bin = int( r / binWidth );
			if( bin<0 || bin>=numBins ) continue;

			g_r[bin] += 2.0;					// Value is 2.0 as adds to both i rel. to j and vice versa.
		}

	// Add to the persistent histogram and apply normalisation at the same time.
	for( auto bin=0; bin<numBins; bin++ )
	{
		double norm = ( N/_box->volume() ) * N;
		
		double rInner = bin * binWidth, rOuter = (bin+1) * binWidth;
		switch( _dim )
		{
			case 2 : norm *=       M_PI * ( rOuter*rOuter        - rInner*rInner        )      ; break;
			case 3 : norm *= 4.0 * M_PI * ( rOuter*rOuter*rOuter - rInner*rInner*rInner ) / 3.0; break;
			default:
				errors::throwException( "Bad dimension when calculating bead g(r)", __FILE__, __LINE__ );
		}

		_dataBeadSpring->addToFixedHist( __dataLabel_beadPairCorrn, (bin+0.5)*binWidth, g_r[bin]/norm, 0.0, binWidth*numBins, numBins );
	}
}

// Box counting, i.e. to extract the fractal dimension.
void dataBeadsBase::_blockBoxCounting() const
{
	double x[3];

	// Smallest meaningful length - currently the bead spacing/diameter, but could be made user-defined.
	double l_min = _beads->_b;

	// Loop over all particles, counting the number of occupied boxes of size l.
	std::vector<int> numOccupied;

	double l = l_min;
	while( true )
	{
		// Initialise boxes for this length l.
		int num = 1 + (int)(_box->longestDimension()/l);
		std::vector<int> occupied( num*num*(_dim==3?num:1), 0 );	// Could make persistent, but doesn't impact profiling.

		// Loop over all particles and convert position to a box.
		for( auto n=0; n<_beads->numBeads(); n++ )
		{
			for( auto k=0; k<_dim; k++ ) x[k] = _beads->positionVector()[n*_dim+k];
			_box->wrap(x);

			// Convert to grid indices. Position already wrapped, but indices could still be negative.
			int
				nx =             (int)floor(x[0]/l),
				ny =             (int)floor(x[1]/l),
				nz = ( _dim==3 ? (int)floor(x[2]/l) : 0 );

			if( nx<0 ) nx += num;
			if( ny<0 ) ny += num;
			if( nz<0 ) nz += num;

			// Get the global index.
			int index = nx + num*ny + num*num*nz;

			// Set the box's occupied status to '1'.
			occupied[index] = 1;
		}

		// Count the number of occupied boxes.
		int total = 0;
		for( auto o : occupied ) if(o) total++;
		numOccupied.push_back( total );

		// Double the box size. Try to have one point with the entire system contained in one box.
		if( l > _box->longestDimension() ) break;
		l *= 2;
	}

	// Output the histogram. The abscissa n corresponds to a cubic box size l = 2^{n} l_min.
	for( auto n=0; n<(int)numOccupied.size(); n++ )
		_dataBeadSpring->addToFixedHist( __dataLabel_boxCounting, n, numOccupied[n], 0, numOccupied.size(), numOccupied.size() );

}
