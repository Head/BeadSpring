#include "linksBase.h"

#pragma mark -
#pragma mark Constructors / destructor.
#pragma mark -

linksBase::linksBase(
	short dim,
	const parameterParser *params,
	const boxBase *box,
	const beadsBase *beads,
	std::shared_ptr<dataBeadSpring> data )
	: _dim(dim), _box(box), _beads(beads), _dataLinksBase(nullptr)
{
	using namespace parameterLabels;

	//
	// Read in link-specific parameters.
	//
	_maxPerBead = params->integerParamOrDefault(links_maxPerBead,-1);
	
	_kA = params->scalarParamOrDefault(links_attachRate,0.0);
	_kD = params->scalarParamOrDefault(links_detachRate,0.0);
	
	_attachRange  = params->scalarParamOrDefault(links_attachRange ,0.0);
	_detachLength = params->scalarParamOrDefault(links_detachLength,0.0);
	
	_k  = params->scalarParamOrDefault(links_springConstant,0.0         );
	_l0 = params->scalarParamOrDefault(links_naturalLength ,_attachRange);

	_freezeLinksAfter = params->scalarParamOrDefault(links_freezeAfter,-1.0);

	//
	// Check parameters and parameter combinations for validity.
	//

	// If spring zero, no other link parameters make sense.
	if( !_k && (_attachRange||(_maxPerBead!=-1)||_kA||_kD||_detachLength||_attachRange||_l0) )
		errors::throwException( "Link parameters meaningless if the spring constant zero / not defined", __FILE__, __LINE__ );

	// Sanity check for parameter ranges.
	if( _maxPerBead==0 or _maxPerBead<-1 )
		errors::throwException( "Max. links per bead cannot be negative (except -1 meaning no limit)", __FILE__, __LINE__ );

	if( _kA < 0.0 )
		errors::throwException( "Attach rate must be non-negative", __FILE__, __LINE__ );

	if( _kD < 0.0 )
		errors::throwException( "Detach rate must be non-negative", __FILE__, __LINE__ );

	if( _detachLength < 0.0 )
		errors::throwException( "Detach length must be non-negative", __FILE__, __LINE__ );

	if( _kA>0.0 && _attachRange<=0.0 )
		errors::throwException( "Attach range must be positive (if attach rate positive)", __FILE__, __LINE__ );

	if( _k < 0.0 )
		errors::throwException( "Link spring constant must be non-negative", __FILE__, __LINE__ );

	if( _l0 < 0.0 )
		errors::throwException( "Link natural spring length must be positive", __FILE__, __LINE__ );

	//
	// Set flags for can attach or detach.
	//
	_canAttach = ( _kA>0.0 );
	_canDetach = ( _kD>0.0 || _detachLength>0.0 );

	//
	// Initialise links containers, one per thread.
	//
	_perThread_links.resize( omp_get_max_threads() );		// One std::set<indexPair> per thread.
	_perThread_links.shrink_to_fit();						// Probably unnecessary.

	_perThread_addLinks.resize( omp_get_max_threads() );
	_perThread_addLinks.shrink_to_fit();

	_perThread_delLinks.resize( omp_get_max_threads() );
	_perThread_delLinks.shrink_to_fit();

	_nLinks = 0;											// May be re-set by ICs or loading state.

	//
	// Initialise the data-gathering (auxiliary) object.
	//
	_dataLinksBase = std::unique_ptr<dataLinksBase>( new dataLinksBase(this,data,_dim,params) );

	//
	// Logging.
	//
	logging::logFile << "Initialised links object." << std::endl;

	if( _k ) logging::logFile << " - spring constant " << _k << "." << std::endl;
	
	if( _l0 ) logging::logFile << " - natural spring length " << _l0 << "." << std::endl;

	if( _kA ) logging::logFile << " - attach rate " << _kA << " if centers in a range " << _attachRange << "." << std::endl;
	
	if( _kD ) logging::logFile << " - detach rate " << _kD << "." << std::endl;
	
	if( _freezeLinksAfter > -1.0 ) logging::logFile << " - no attachement/detachment possible after freeze time t=" << _freezeLinksAfter << "." << std::endl;

	if( _detachLength ) logging::logFile << " - immediate detachment if crosslink length exceeds " << _detachLength << "." << std::endl;

	if( _maxPerBead>0 ) logging::logFile << " - max. " << _maxPerBead << " link end(s) for any one bead." << std::endl;

	// Optional data output. Also point the user towards the flag that can be used to select the option.
	if( _dataLinksBase->outputPercDim() )
		logging::logFile << " - calculating and outputting percolation dimension";
	else
		logging::logFile << " - not calculating or outputting percolation dimension";
	logging::logFile << " [set by flag parameter '" << dataLinks_outputPercDim << "']." << std::endl;
}


#pragma mark -
#pragma mark Link management
#pragma mark -

// Main update method.
void linksBase::update( double time, double dt, int numBeads )
{
	//
	// Link dynamics still allowed?
	//
	if( _freezeLinksAfter>=0.0 && time>_freezeLinksAfter ) _canAttach = _canDetach = false;

	//
	// Set/change the number of beads.
	//
	
	// Check number of beads has not changed. If so, would need to implement some sort of "newBeadIndexing()" method called by BeadSpring.
	if( !_linksPerBead.empty() && numBeads != (int)_linksPerBead.size() )
		errors::throwException( "Number of beads has changed; not yet supported by linksBase", __FILE__, __LINE__ );
	
	// If first time here, resize the vector that tracks the number of link heads for each bead.
	if( _linksPerBead.empty() )
	{
		_linksPerBead.resize( numBeads );
		_linksPerBead.shrink_to_fit();
		for( auto &l : _linksPerBead ) l = 0;
	}

	// If zero time step, no chance of attach/detach so return now.
	if( !dt ) return;

	// Keep track of the number of attachments and detachments.
	__nAttach = __nDetach = 0;
		
	//
	// Creation of new links.
	//
	if( _canAttach )
	{
		// For this thread's potential attachments, determine which should be added.
		#pragma omp parallel
		{
			_perThread_attachLinks( omp_get_thread_num(), dt );
		}

		// In the expectation that the number of new links to be added in any one time step
		// is small, perform the final uniqueness check and actual addition in serial.
		for( auto &addSet : _perThread_addLinks )
			for( auto &ij : addSet )
				_serial_attemptAddLink( ij );
	}

	//
	// Detachment of links.
	//
	if( numLinks() && _canDetach )
	{
		// Fill per-thread list of those to delete.
		#pragma omp parallel
		{
			_perThread_detachLinks( omp_get_thread_num(), dt );
		}

		// In the expectation that there will only ever be a small number of links to delete in
		// one time step, perform the final deletion in serial. Some scope for parallelisation -
		// the 'to delete' sets are on the same threads as the current links - but need to be 
		// careful about how to update the linksPerBead container.
		for( auto t=0u; t<_perThread_links.size(); t++ )
			for( auto &ij : _perThread_delLinks[t] )
			{
				_linksPerBead[ij.first ]--;			// Not thread-safe.
				_linksPerBead[ij.second]--;			// Not thread-safe.

				_perThread_links[t].erase( ij );	// Not iterating over this set, so no need for 'if..else(==)' construct.

				__nDetach++;						// std::atomic<>.
			}
	}

	//
	// Update the statistics.
	//
	_dataLinksBase->numAttach(__nAttach);
	_dataLinksBase->numDetach(__nDetach);

	_nLinks += __nAttach - __nDetach;			// Update the running total.

	// Uncomment for some (slow) checks after every round of additions.
//	debugCheckBadLinks();
}

// Per-thread creation of new links.
void linksBase::_perThread_attachLinks( unsigned int thread, double dt )
{
	// Per-thread shorthands from the beads object.
	      unsigned int numPairs = _beads->nearbyBeads_numPairs(thread);
	const indexPair*   indices  = _beads->nearbyBeads_indices (thread);
	const double*      rSqrd    = _beads->nearbyBeads_rSqrd   (thread);
	
	// Clear the list of new links to add.
	_perThread_addLinks[thread].clear();

	// Other shorthands and scratch variables.
	double maxRangeSqrd = _attachRange * _attachRange, probAttach = dt * _kA;
	
	// Loop over all nearby beads in range. Not in parallel, as we are already in a parallel block.
	for( auto n=0u; n<numPairs; n++ )
	{
		if( rSqrd[n] > maxRangeSqrd ) continue;

		// Create a new link?
		if( pRNG::stdUniform(thread) >= probAttach ) continue;
		
		// Add to the set of new links to be added. Will still need to be checked for identical links that already exist.
		_perThread_addLinks[thread].insert( indices[n] );
	}	
}

// Identify links to delete and add to per-thread vectors; will delete afterwards, in serial.
void linksBase::_perThread_detachLinks( unsigned int thread, double dt )
{
	// Shorthands.
	double detachLengthSqrd = _detachLength*_detachLength, probDetach = dt*_kD;
	
	// Clear the per-thread list of links to remove/delete.
	_perThread_delLinks[thread].clear();

	// Loop over all links handled by this thread.
	for( auto &ij : _perThread_links[thread] )
	{
		// Check for stochastic detachment.
		if( probDetach && pRNG::stdUniform(thread) < probDetach )
		{
			_perThread_delLinks[thread].insert( ij );
			continue;
		}
		
		// Check for overlong links. No obvious way of using the nearbyBeads lists to accelerate this.
		if( detachLengthSqrd && _beads->distSqrdBetweenBeads(ij.first,ij.second)>detachLengthSqrd )
		{
			_perThread_delLinks[thread].insert( ij );
			continue;
		}
	}
}

// Creates a new link. Assumed to be called from a serial context. Returns with the thread index
// controlling the container to which the link was added, or -1 if not added.
int linksBase::_serial_attemptAddLink( indexPair ij )
{
	// Does this link already exist? - in which case, don't add.
	for( auto &linkSet : _perThread_links )
		if( linkSet.find(ij) != linkSet.end() )		// std::set::find() has log complexity according to docs.
			return -1;

	// Will the addition of this link exceed the maximum allowed per bead?
	if( _linksPerBead[ij.first]>=(unsigned int)_maxPerBead || _linksPerBead[ij.second]>=(unsigned int)_maxPerBead )
		return -1;

	// Which thread's std::set should this be added to? Try to maintain load balancing by adding to the current smallest.
	auto minIter = std::min_element(
		_perThread_links.begin(),
		_perThread_links.end(),
		[] ( auto set1, auto set2 ) { return set1.size() < set2.size(); }
	);
	minIter->insert( ij );

	// Add the new link ends to the global per-bead counts.
	_linksPerBead[ij.first ]++;
	_linksPerBead[ij.second]++;
	
	// For data output.
	__nAttach++;		// Atomic variable so thread-safe.

	// Return the index of the container to which the link was added.
	int t=0;
	for( auto iter=_perThread_links.begin(); iter!=_perThread_links.end(); iter++, t++ )
		if( iter == minIter )
			return t;
	
	// Sanity check.
	errors::throwException("system error: could not find index of container to which a link as added",__FILE__,__LINE__);
	return -1;				// Just to avoid compiler warnings; should never reach here.
}


#pragma mark -
#pragma mark Quantities
#pragma mark -

// Energy for a single link.
double linksBase::_linkEnergy( indexPair ij ) const
{
	double ext = _linkExtension(ij);
	return 0.5 * _k * ext * ext;
}

// Extension for a single link.
double linksBase::_linkExtension( indexPair ij ) const
{
	return sqrt( _beads->distSqrdBetweenBeads(ij.first,ij.second) ) - _l0;
}


#pragma mark -
#pragma mark Force calculation
#pragma mark -

// Updates the force vector for all links. Assumed to be called from with a #pragma omp parallel block.
void linksBase::threadedIncrementForces( const double *X, double *F ) const
{
	for( auto &ij : _perThread_links[omp_get_thread_num()] )
		_singleLinkForce( ij, X, F );
}

// Force for a single link. Cannot use NN list as links are not guaranteed to be on it.
void linksBase::_singleLinkForce( indexPair ij, const double *X, double *F ) const
{
	// Scratch variables.
	double dx[3], r, f_over_r;
	
	// Separation vector between bead centres, wrapped to any periodic boundaries.
	for( auto k=0; k<_dim; k++ ) dx[k] = X[_dim*ij.second+k] - X[_dim*ij.first+k];
	_box->wrap( dx );

	// Get the scalar force. No way to avoid a square-root (?).
	r        = sqrt( _dot(dx) );
	f_over_r = _k * ( r - _l0 ) / r;

	// Apply equal-and-opposite forces. Contractile if r > l0 (so subtract from second and add to first).
	for( auto k=0; k<_dim; k++ )
	{
		F[ij.second*_dim+k] -= f_over_r * dx[k];
		F[ij.first *_dim+k] += f_over_r * dx[k];
	}
	
	// Periodically add to the stress tensor for output.
	if( _dataLinksBase->calculateStressNow() ) _dataLinksBase->addToStressTensor_fr_r(f_over_r,dx);
}


#pragma mark -
#pragma mark Initial configurations
#pragma mark -

void linksBase::setICTriLattice( int numX, int numY )
{
	// Sanity check.
	if( _dim!=2 ) errors::throwException( "Triangular lattice only possible for 2D boxes", __FILE__, __LINE__ );

	// Set up the per-bead lists.
	update(0.0,0.0,numX*numY);
	
	// Consider all possible links.
	for( auto i=0; i<numX; i++ )
		for( auto j=0; j<numY; j++ )
		{
			_latticeIC_trialConnection( i, j, i+1,               j  , numX, numY );			// Horizontal.
			_latticeIC_trialConnection( i, j, ( j%2 ? i-1 : i ), j+1, numX, numY );			// Up and to the left.
			_latticeIC_trialConnection( i, j, ( j%2 ? i : i+1 ), j+1, numX, numY );			// Up and to the right.
		}
	
	// Set running total of number of links.
	_nLinks = 0;
	for( auto &linkSet : _perThread_links ) _nLinks += linkSet.size();
}

void linksBase::_latticeIC_trialConnection( int i1, int j1, int i2, int j2, int numX, int numY )
{
	// If had a bond dilution parameter, would check here and return if absent.
	
	// Add the link. Bead indices i and j>i as a std::pair<>.
	if( i1 < 0     ) i1 += numX;			// Periodicity - could check box is periodic in these directions?
	if( i1 >= numX ) i1 -= numX;

	if( i2 < 0     ) i2 += numX;
	if( i2 >= numX ) i2 -= numX;

	if( j1 < 0     ) j1 += numY;
	if( j1 >= numY ) j1 -= numY;

	if( j2 < 0     ) j2 += numY;
	if( j2 >= numY ) j2 -= numY;

	auto global1 = i1 + j1*numX, global2 = i2 + j2*numX;

	auto i  = std::min(global1,global2), j = std::max(global1,global2);
	auto ij = std::make_pair(i,j);

	_serial_attemptAddLink( ij );				// Should only be called from a serial context.
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Checks the links containers for anything that looks wrong; throws an exception if so.
void linksBase::debugCheckBadLinks() const
{
	// Check the total number of links.	
	unsigned int linkTotal = 0u;
	for( auto &linkSet : _perThread_links ) linkTotal += linkSet.size();
	if( linkTotal != _nLinks ) errors::throwException( "Actual number of links does not match running total", __FILE__, __LINE__ );

	// Too many links for a single bead.
	for( auto &per : _linksPerBead )
		if( per > (unsigned int)_maxPerBead )
			errors::throwException( "More links on a single bead than allowed", __FILE__, __LINE__ );

	// Bad indices for the beads connected by links.
	for( auto &linkSet : _perThread_links )
		for( auto &link : linkSet )
		{
			if( link.first == link.second ) errors::throwException( "Bad link indices: Both bead indices the same", __FILE__, __LINE__ );
			if( link.first >  link.second ) errors::throwException( "Bad link indices: Not in the required order" , __FILE__, __LINE__ );
		}

	// Repeated links.
	for( auto i=0u; i<_perThread_links.size(); i++ )
		for( auto &link : _perThread_links[i] )
		{
			// Loop through all over links, including those in this thread's set.
			for( auto j=i; j<_perThread_links.size(); j++ )
			{
				auto nSame = std::count(_perThread_links[j].begin(),_perThread_links[j].end(),link);

				if( ( i==j && nSame>1 ) || ( i!=j && nSame>0 ) )
					errors::throwException( "Multiple links with the same bead indices", __FILE__, __LINE__ );
			}
		}

	// Links should match up to the number of links per bead.
	std::vector<unsigned int> debugLinksPerBead(_linksPerBead.size(),0);
	for( auto &linkSet : _perThread_links )
		for( auto &ij : linkSet )
		{
			debugLinksPerBead[ij.first ]++;
			debugLinksPerBead[ij.second]++;
		}

	for( auto n=0u; n<_linksPerBead.size(); n++ )
		if( _linksPerBead[n] != debugLinksPerBead[n] )
			errors::throwException( "Number of links per bead not consistent with current list of links", __FILE__, __LINE__ );
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Saving.
void linksBase::saveState( flatXMLParser *xml ) const
{
	xml->saveStartElement( __saveLinksTag );
	_saveLinkData(xml);
	xml->saveEndElement  ( __saveLinksTag );
	
}

void linksBase::_saveLinkData( flatXMLParser *xml ) const
{
	// Output link indices to file. No need to output links per bead; can re-construct on loading.
	xml->addAttribute( _saveNumLinksAttr, numLinks() );
	xml->saveStartElement( _saveIndicesTag );
	
	if( numLinks() )
	{
		for( auto &linkSet : _perThread_links )
			xml->appendDataString( linkSet );			// Not thread safe.

		xml->saveData();
	}

	xml->saveEndElement( _saveIndicesTag );
}

// Loading.
void linksBase::recoverState( flatXMLParser *xml )
{
	// Can read any attributes for main tag now.
	
	// May be overriden by child class.
	_recoverLinkData(xml);

	// If calculating the percolation dimension, which is stored persistently (for potential use as a termination
	// condition), calculate now so that it is correct rught from the start.
	if( _dataLinksBase->outputPercDim() ) _dataLinksBase->_calcPercolationDimension();
}

void linksBase::_recoverLinkData( flatXMLParser *xml )
{
	// Get the number of links from the indices tag.
	xml->readBlockWithTag( _saveIndicesTag );
	auto numLinks = atoi( xml->attribute(_saveNumLinksAttr).c_str() );
	
	// Reset and resize the containers, and the running count of the total number of links.
	// Assume per-thread containers sized to current number of threads which need not match the number of threads when saving.
	for( auto &linkSet : _perThread_links ) linkSet.clear();
	_nLinks = 0;

	_linksPerBead.clear();
	_linksPerBead.resize( _beads->numBeads() );
	_linksPerBead.shrink_to_fit();

	// Sanity check.
	if( xml->dataSize() != 2*numLinks )
		errors::throwException( "Given number of links does not match size of index array", __FILE__, __LINE__ );

	// Loop through all saved links and update the containers.
	unsigned int t = 0u;		// Thread index of per-thread set to add the next link to.

 	for( auto n=0; n<numLinks; n++ )
 	{
 		auto
 			i = (unsigned int)( xml->dataArray()[2*n  ] ),
 			j = (unsigned int)( xml->dataArray()[2*n+1] );

		// Add to the per-thread sets in a round-robin manner.
		_perThread_links[t++].emplace( std::make_pair(std::min(i,j),std::max(i,j)) );		// Should be i<j anyway.
		if( t==_perThread_links.size() ) t = 0u;
		
		_linksPerBead[ i ]++;
		_linksPerBead[ j ]++;

		_nLinks++;
 	}
 
 	// Good time to check integrity of the loaded link data. Slow.
 	debugCheckBadLinks();
}



