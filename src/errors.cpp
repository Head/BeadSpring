#include "errors.h"

void errors::throwException( std::string message, std::string file, int line )
{
	std::ostringstream out;
	out << message << " [in file " << file << ", line " << line << "]" << std::endl;
	throw std::runtime_error( out.str() );
}