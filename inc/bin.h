/*
 *  bin.h
 *
 * Entire file for bin class, _including_ implementation, since
 * gnu c++ doesn't seem to handle template classes very well.
 *
 * Enter new points by:
 *
 * b->add_point( arr, arr+num );	// arr - pointer to array of type double
 * b->add_point( iter.begin(), iter.end() );	// iterator, i.e. vectors or lists -
 * (see Lippmann, "Essential c++", 3.2 for examples of the latter)
 * b->add( iter1.begin(), iter1.end(), iter2.begin(), iter2.end() ); Joins two vectors together
 *
 * - this function returns true if the attempted point was compatible with
 * the class declaration, i.e. the same number of elements.
 *
 * Constructor "throw"'s (with const char* msg) if it fails to allocate the memory.
 *
 *  1/9/2004/DAH
 *
 */


#ifndef _UD_BIN_H
#define _UD_BIN_H



#include <iostream>
#include <math.h>
#include <vector>


using namespace std;


template< int num >
class bin {

public:

	bin();
	bin(const bin&);
	virtual ~bin();

	template < typename iteratorType >
		bool add_point(const iteratorType,const iteratorType);

	template < typename iterType1, typename iterType2 >
		bool add_point(const iterType1,const iterType1,const iterType2,const iterType2);
		
	inline double mean( const int i ) const
		{ return ( _counter ? _sum[i]/_counter : 0 ); }

	inline double error( const int i ) const
		{ return _std_err( mean(i), _sum_sqrd[i], _counter ); }
	
	inline double num_pts_added() const
		{ return _counter; }

	// Adding bins together
	void combine_bins ( bin<num>&, const bin<num>& );
	bin<num>& operator+=(const bin<num>&);

	//
	// OUTPUT
	//

	// Output to ostream
	template< int n >	
	friend ostream& operator<< ( ostream&, const bin<n>& );

	// Output as an STL vector<double>, in format x dx y dy z dz ...
	vector<double> Vector() const;
	template< typename iterType >
	vector<double> Vector(const iterType,const iterType) const;
	
	// Output as a recoverable string (in principle), in the format "N sum sum_sqrd sum sum_sqrd..."
	// Optional integer argument only outputs the first n specified
	void Full_Output(ostream&,int n=0) const;
	
	// Direct control of data; use with caution
	void setCounter( double counter ) { _counter = counter; }
	void setSumsForIndex( int i, double sum, double sumSqrd )
		{ if( i>=0 and i<num ) { _sum[i] = sum; _sum_sqrd[i] = sumSqrd; } }
	
private:

	double *_sum, *_sum_sqrd;
	double _counter;					// Fixed at double, to avoid overflows with int/long

	inline void _print( ostream& ) const;
	static inline double _std_err( double M, double S2, double N );
	inline void _update_totals( int i, double val ) { _sum[i] += val; _sum_sqrd[i] += val*val; }
};



// Constructor - allocates memory for the running totals, and initialises them to zero
template< int num >
bin<num>::
bin()
{
	_sum = new double[num];
	_sum_sqrd = new double[num];

	// Initialise all elements and the counter to zero
	if( _sum && _sum_sqrd ) {
		for( int i=0; i<num; i++ ) _sum[i] = _sum_sqrd[i] = 0;
		_counter = 0;
	} else throw "cannot allocate memory for variables within bin class";
}

// Copy constructor
template<int num>
bin<num>::bin( const bin &b )
{
	_sum = new double[num];
	_sum_sqrd = new double[num];

	// Initialise all elements and the counter to zero
	if( _sum && _sum_sqrd ) {
		for( int i=0; i<num; i++ ){
			_sum[i]      = b._sum[i];
			_sum_sqrd[i] = b._sum_sqrd[i];
		}
		_counter = 0;
	} else throw "cannot allocate memory for variables within bin class";
}

// Destructor - deallocates memory
template< int num >
bin<num>::
~bin()
{
	// Remove running total variable memory
	delete [] _sum;
	delete [] _sum_sqrd;
}



// Adds a new point to the running totals. Also checks for the wrong
// number of elements added (such as wrong dimension of vector).
// Returns "true" if the point was successfully added; otherwise false,
// in which case 
template< int num >
template < typename iteratorType >
bool bin<num>::
add_point( const iteratorType first, const iteratorType last )
{
	iteratorType iter = first;
	int trial_num = 0;

	// First check that the correct number of elements will be passed,
	// before altering the running totals (which thus remain fixed if the
	// attempted point addition fails).
	for( ; iter<last; iter++ ) trial_num++;
	if( trial_num != num ) return false;
	
	// Right number of elements - add to the running totals
	for( iter=first, trial_num=0 ; iter<last ; iter++, trial_num++ ) _update_totals( trial_num, *iter );
		
	// Increment counter, return with "okay"
	_counter++;
	return true;
}



// An alternative version of the "add_point" routine, that allows
// for two different types of iterator. The data is simply read as
// if they constitute a single list (nb: need same double)
template< int num >
template < typename iterType1, typename iterType2 >
bool bin<num>::
add_point( const iterType1 b1, const iterType1 e1, const iterType2 b2, const iterType2 e2 )
{
	iterType1 iter1 = b1;
	iterType2 iter2 = b2;
	int trial_num = 0;
	
	// Check that the correct number of elements will be passed
	for( ; iter1<e1; iter1++ ) trial_num++;
	for( ; iter2<e2; iter2++ ) trial_num++;
	if( trial_num != num ) throw "incorrect dimensionality of passed point";
	
	// Right number of elements - add to the running totals
	for( iter1=b1, trial_num=0 ; iter1<e1 ; iter1++, trial_num++ ) _update_totals( trial_num, *iter1 );
	for( iter2=b2; iter2<e2; iter2++, trial_num++ ) _update_totals( trial_num, *iter2 );
	
	// Increment counter, return with "okay"
	_counter++;
	return true;
}



// Output operator - calls the private "_print()" member of the bin class
template< int num >
inline ostream&
operator<< ( ostream &os, const bin<num> &bn )
{
	bn._print( os );
	return os;
}



// Combines two bins together, adding the second to the first.
template< int num >
void bin<num>::combine_bins ( bin<num> &to, const bin<num> &from )
{
	// No need to check for same "num" - wouldn't even compile otherwise
	for( int i=0; i<num; i++ )
	{
		to._sum[i] += from._sum[i];
		to._sum_sqrd[i] += from._sum_sqrd[i];
	}
	to._counter += from._counter;
}

// Adds second bin to this one
template< int num >
bin<num>& bin<num>::operator+=( const bin<num> &rhs )
{
	_counter += rhs._counter;

	for( unsigned short k=0; k<num; k++ )
	{
		_sum[k]      += rhs._sum[k];
		_sum_sqrd[k] += rhs._sum_sqrd[k];
	}

	return *this;
}



// The actual routine that ouputs the values - private member
template< int num >
inline void bin<num>::
_print( ostream &os ) const
{
	for( int i=0; i<num; i++ ) {

		// Find mean and std. dev. Use _std_err() here rather than error(), to avoid
		// calculating the mean twice.
		double mu = mean( i );
		double error = _std_err( mu, _sum_sqrd[i], _counter );
		
		os << mu << "\t" << error;
		if( i < num-1 ) os << "\t";			//  Tab-separated, but no tab at end
	}
}

// Output to ostream, with all internal data intact, so that it is (in principle) fully recoverable)
// Each triplet is of the form "N sum1 sum_sqrd1 sum2 sum_qrd_2 ..."
template< int num >
void bin<num>::Full_Output( ostream &os, int n ) const
{
	os << _counter << "\t";
	for( int i=0; i<( n ? n : num ); i++ ) os << _sum[i] << "\t" << _sum_sqrd[i] << ( i==num-1 ? "" : "\t" );
}

// Returns with a vector<double> of the current binning data, with optional scaling.
// Note that, with errors, the length of the returned vector<> is 2*dim
template< int num >
template< typename iterType >
vector<double> bin<num>::Vector( const iterType scale_b, const iterType scale_e ) const
{
	vector<double> x;
	
	iterType scale_iter = scale_b;

	for( int i=0; i<num; i++ )
	{
		double
			mu = mean(i),
			error = _std_err( mu, _sum_sqrd[i], _counter ),
			scale = *scale_iter++;
		
		x.push_back( mu/scale );
		x.push_back( error/scale );
	}
	return x;
}

// Argument-free version of the above (no scaling)
template< int num >
vector<double> bin<num>::Vector() const
{
	// Initialise dummy vector of 1.0's
	vector<double> dummy;
	dummy.resize(num,1.0);

	return Vector( dummy.begin(), dummy.end() );
}

// Standard error - static, so used by all invocations of the bin class
template< int num >
inline double bin<num>::
_std_err( double M, double S2, double N )
{
	if( N>1 ) {
		double temp = ( S2-M*M*N ) / ( N*N );
		if( temp >0 ) return sqrt( temp );
	}
	return 0;		// Only 0 or 1 points added (or some numerical error that gives temp<0)
}




#endif


