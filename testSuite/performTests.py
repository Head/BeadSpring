#
# Main controlling script for performing the scripts; the scripts themselves are embedded with the
# parameter/state files in the format of a Python package. Each test class is assumed to be derived
# from bfTestBase.
#
# If a new test is added, it needs to be included here.
#


#
# Imports
#

# Standard imports
import sys
import os

# The test classes
import bsTests


#
# Do not allow import of this module
#
if __name__ != "__main__":
	print( "Cannot import 'performTests'; must be executed from the command line." )
	sys.exit(-1)


#
# List all test classes alongside their names and whether or not they are 'slow' (which will require
# minutes to hours, possibly overnight runs).
#
allTests = {
	"tangentCorrelations" : [ bsTests.testTangentCorrelations, True  ],
	"excludedVolume"      : [ bsTests.testExcludedVolume     , False ],
	"linkEnergy"          : [ bsTests.testLinkEnergy         , True  ],
	"networkTopology"     : [ bsTests.testNetworkTopology    , False ],
	"shearTriLattice"     : [ bsTests.testShearTriLattice    , False ],
	"detachLinks"         : [ bsTests.testDetachLinks        , False ],
	"boxCount"            : [ bsTests.testBoxCount           , False ],
	"beadSpecificAttach"  : [ bsTests.testBeadSpecificAttach , False ]
}


#
# Command line parser
#
import argparse

p = argparse.ArgumentParser( description="Run automated tests for the beadSpring code" )


p.add_argument( "-e", "--executable", help="beadSpring executable",                       default="../beadSpring" )
p.add_argument( "-p", "--plot",       help="Show plots, pausing until cancelled by user", action ="store_true"    )
p.add_argument( "-v", "--verbose",    help="Verbose text output to stdout",               action ="store_true"    )
p.add_argument( "-l", "--list",       help="List all available tests and bail",           action ="store_true"    )
p.add_argument( "-t", "--test",       help="Perform the specific test and bail",          default=None            )
p.add_argument(		  "--all" ,       help="Perform all tests [use -l to see list]",      action ="store_true"    )
p.add_argument( "-f", "--fast",       help="Perform all fast tests only",                 action ="store_true"    )
p.add_argument( "-c", "--clean",      help="Clean only",             					  action ="store_true"    )

args = p.parse_args()

#
# List all options then bail
#
if args.list:
	print( "Available tests:" )
	for key in allTests.keys(): print( " - {0} [{1}]".format(key,("slow" if allTests[key][1] else "fast")) )
	print( "[tests marked 'slow' may require overnight runs, 'fast' should only take minutes]" )
	print( "[if a new test is not shown, update the list in this file and bsTests/__init__.py]" )
	sys.exit( -1 )


#
# Function to perform a single test with the given key name in allTests
#
def performSingleTest( testName, cleanOnly=False ):

	# Will work from the subdirectory with the corresponding test, so it can access requred files and clean up afterwards
	currDir      = os.getcwd()									# Current directory; will return here afterwards.
	fullExecPath = os.path.abspath( args.executable )			# Full pathname for the executable
	os.chdir( "bsTests/{}".format(testName) )					# Move to the local directory for these test

	# If cleaning, run the corresponding routine. If executing, place in a try...except clause to catch
	# failed test exceptions only.
	if cleanOnly:
		allTests[testName][0].removeAuxiliaryFiles(args.verbose)
	else:
		try:
			allTests[testName][0].performTest(fullExecPath,args.verbose,args.plot)
		except bsTests.bsTestBase.FailedTestError as err:
			print( "\nFAILED TEST: {}\n".format(err) )
																# Call class method performTest() with verbosity/plotting flags

	os.chdir( currDir )											# Move back to the original directory


#
# Perform a single test
#
if args.test and not args.all and not args.fast:
	if args.test not in allTests.keys():
		print( "Requested test '{}' not in list of recognised test names [call with -l to show list]".format(args.test) )
		sys.exit(-1)

	performSingleTest(args.test)

#
# Perform all fast tests.
#
if args.fast and not args.all:
	for testName in allTests.keys():
		if not allTests[testName][1]:				# 'True' for second item denotes a slow test.
			performSingleTest( testName )


#
# Perform all tests
#
if args.all:
	for testName in allTests.keys():
		performSingleTest( testName )



#
# Clean-up
#
if args.clean:
	for testName in allTests.keys():
		performSingleTest( testName, cleanOnly=True )
