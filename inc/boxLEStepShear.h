/**
 * @file boxLEStepShear.h
 * @author David Head
 * @brief Step shear applied via Lees-Edwards boundary conditions in the x-y plane.
 */


#ifndef BEAD_SPRING_LE_STEP_SHEAR_H
#define BEAD_SPRING_LE_STEP_SHEAR_H


//
// Includes.
//

// Standard includes.
#include <iostream>

// Project includes.
#include "boxLeesEdwards.h"


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string boxType_LEStepShear = "LEStepShear";
	
	const std::string boxLEStepShear_strain = "shearStrain";	
	const std::string boxLEStepShear_start  = "shearStart";	
}


/**
 * @brief Step shear applied via Lees-Edwards boundary conditions in the x-y plane.
 */
class boxLEStepShear : public boxLeesEdwards
{

private:

protected:

	double _shearStrain;			///< Shear strain \f$ \gamma \f$.
	double _startShear;				///< Time \f$ t_{0} \f$ to start shear.

public:

    /// Main constructors.
	boxLEStepShear( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );
    virtual ~boxLEStepShear() {}
    
    virtual void update( double time, double dt ) override;			///< Sets current shear strain; updates data.
};


#endif


 