#
# Tests for bead-specific attachment rules as implemented in the linksBeadSpecific:: class.
# May be extended in future when more bead-specific parameters are implemented.
#

# Standard imports
import numpy as np
import math
import matplotlib.pyplot as plt

# Base class
from bsTests import bsTestBase

class testBeadSpecificAttach( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname ):

		# Store private variables.
		self.__paramFile  = fname

		# Call parent method with required flags.
		super(testBeadSpecificAttach,self).__init__(executable,verbosity,plot)

	# Test method.
	def performTests( self ):

		#
		# Get the relevant parameters for this test and perform some basic sanity checks.
		#

		# Whatever else happens, must have at least 2 beads per filament; can't test bead-specic attachment with only 1.
		M = self._blockData.getParameter("M")
		if M==1:
			raise NotImplementedError( "Bead-specific attach test assumes a polymeric system (i.e. M>1)" )

		# Currently only support one link type per test.
		nLinkTypes = self._blockData.getParameter( "numLinkTypes" )
		if nLinkTypes!=1:
			raise NotImplementedError( "Bead-specific attach tests currently assume only one link type per test" )

		# Extract bead-specific rules for link type 0. Rather than attempt to parse in Python, just assume a known form.
		attachBetween_0 = self._blockData.getParameter( "attachBetween_0" )
		if attachBetween_0 != "0:0":
			raise NotImplementedError( "Bead-specific attach rules not understood - only a limited set current implemented" )


		#
		# Get and check the number of links per bead index (averaged over filaments) as measured along the filament,
		# with 0 being the head bead. Note that the '-1' in the line below refers to the final block data time.
		#
		b, nLinks  = [ self._blockData.getDataSets("linksPerBead")["curves"][-1][axis] for axis in ("x","y") ]


		#
		# Perform the test.
		#
		if attachBetween_0 == "0:0":		# Currently the only option ...

			if self._verbose:
				print( f"Checking bead-specific attachment for attachment rule '{attachBetween_0}' predicts all links on bead 0 at the last time interval." )

			# If requested, plot the last time interval's per-link data.
			if self._plot:
				plt.plot( b, nLinks, "o-", color="green" )

				plt.ylabel( "Mean number of links per bead per filament")

				plt.xlabel( "Bead index (0=head)")
				plt.xticks( b )

				plt.axhline( 0, color="k", linestyle="--" )
				plt.axhline( 1, color="k", linestyle="--" )

				plt.show()
			
			# Check 1: Should be no links on bead 1.
			if nLinks[1] > testBeadSpecificAttach.maximumNoAttachValue():
				if self._verbose:
					print( f"ERROR: mean number of links on bead 1 was {nLinks[1]}, but should have been essentially zero given the attachement rules." ) 
				raise bsTestBase.FailedTestError( "non-zero fraction of links on bead 1, which should have exactly none." )
			else:
				if self._verbose:
					print( f"- mean number of links on bead 1, {nLinks[1]}, was essentially zero, as it should be given the attachement rules." ) 

			# Check 2: Should be a high number of links on bead 0 (but not quite 1 given crowding, finite simulation time etc.)
			if nLinks[0] < testBeadSpecificAttach.minumumAttachValue():
				if self._verbose:
					print( f"ERROR: mean number of links on bead 0, {nLinks[0]}, should be much higher given the simulation time." )
				raise bsTestBase.FailedTestError( "too low fraction of links on bead 0, which should have many links." )
			else:
				if self._verbose:
					print( f"- mean number of links on bead 0, {nLinks[0]}, was suitably high given the simulation time." )

	# Arguments for the executable
	def _parameterFile( self ): return self.__paramFile
	def _numThreads   ( self ): return 1

	# Parameters used for checking.
	@classmethod
	def maximumNoAttachValue( cls ):
		"""The maximum allowed value for a bead to which there should have been no attachment."""
		return 1e-6

	@classmethod
	def minumumAttachValue( cls ):
		"""The minumum allowed value for a bead to which there should be attachement, given the short time of the simulation."""
		return 0.7

	# Cleanup; can use wildcards
	@classmethod
	def filesToDelete( self ): return ["block_data.xml","state_0???.*","scalars.xml","log.out"]


	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(1):
			fname = f"params_test{testNum}.xml"
			print( "Performing bead-specific attachement test starting from file '{}'".format(fname) )
			testBeadSpecificAttach(executable,verbosity,plot,fname)
			print( " - passed" )
