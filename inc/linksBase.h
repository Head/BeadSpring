/**
 * @file linksBase.h
 * @author David Head
 * @brief Concrete base class for the crosslinkers between connected-bead filaments (not within filaments).
 */


#ifndef BEAD_SPRING_LINKS_BASE_H
#define BEAD_SPRING_LINKS_BASE_H


//
// Includes.
//

// Standard includes.
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <utility>
#include <algorithm>
#include <omp.h>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "boxBase.h"
#include "beadsBase.h"
#include "dataBeadSpring.h"
#include "logging.h"
#include "errors.h"
#include "dataLinksBase.h"
#include "parameterParser.h"


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string links_typeBasic      = "basic";
	
	const std::string links_springConstant = "k";
	const std::string links_naturalLength  = "l0";

	const std::string links_attachRate     = "kA";
	const std::string links_detachRate     = "kD";

	const std::string links_attachRange    = "attachRange";
	const std::string links_detachLength   = "maxLinkLength";
	
	const std::string links_maxPerBead     = "maxLinksPerBead";
	
	const std::string links_freezeAfter    = "freezeLinksAfter";
}


/// Alias for index pairings. Matches that in beadsBase.h.
using indexPair = std::pair<unsigned long,unsigned long>;

/**
 * @brief Concrete base class for the crosslinkers between connected-bead filaments (not within filaments).
 * 
 * Has read-only access to `box` and `base` modules.
 */

class linksBase
{

private:

	constexpr static auto __saveLinksTag     = "Links";

	std::atomic<int> __nAttach;
	std::atomic<int> __nDetach;

protected:

	// XML tags for I/O. Can also be used by child classes.
	constexpr static auto _saveIndicesTag   = "Indices" ;
	constexpr static auto _saveNumLinksAttr = "numLinks";

	short _dim;												///< Dimensionality.

	bool _canAttach;
	bool _canDetach;

	const boxBase   *_box;
	const beadsBase *_beads;

	int _maxPerBead;										///< Max. no. of crosslink ends on any one bead.

	unsigned int _nLinks;									///< Current total number of links.

	double _k;												///< Spring constant.
	double _l0;												///< Spring natural length.

	double _kA;												///< Attachment rate (if in range).
	double _kD;												///< Detachment rate.
	
	double _attachRange;									///< Range within which attacement is possible.
	double _detachLength;									///< Maximum length of crosslink allowed.
	
	double _freezeLinksAfter;								///< No link attach/detach after this time.
	
	// Link storage and management. Use one std::set<> per thread.
	std::vector< std::set<indexPair> > _perThread_links;	///< Links (end indices) stored per-thread.
	std::vector< std::set<indexPair> > _perThread_addLinks;	///< New links to add stored per-thread.
	std::vector< std::set<indexPair> > _perThread_delLinks;	///< Links to delete stored per-thread.
	std::vector<unsigned int> _linksPerBead;				///< Count of link ends per bead; global, i.e. not per-thread.
	
	virtual void _perThread_attachLinks( unsigned int thread, double dt );
	virtual void _perThread_detachLinks( unsigned int threadNum, double dt );

	/// Attempt to add the per-thread links. Returns thread index of container to which it was added, or -1 if not.
	virtual int _serial_attemptAddLink( indexPair ij );
	
	/// Force calculations, per link.
	virtual void _singleLinkForce( indexPair ij, const double *X, double *F ) const;

	// Quantities.
	virtual double _linkEnergy   ( indexPair ij ) const;
	virtual double _linkExtension( indexPair ij ) const;

	//
	// Utility methods.
	//
	inline double _dot( double *x, double *y ) const noexcept { return ( x[0]*y[0] + x[1]*y[1] + (_dim==3?x[2]*y[2]:0.0) ); }
	inline double _dot( double *x            ) const noexcept { return ( x[0]*x[0] + x[1]*x[1] + (_dim==3?x[2]*x[2]:0.0) ); }

	//
	// Initial configurations.
	//
	virtual void _latticeIC_trialConnection( int i1, int j1, int i2, int j2, int numX, int numY );

	//
	// I/O.
	//
	virtual void _saveLinkData   ( flatXMLParser *xml ) const;
	virtual void _recoverLinkData( flatXMLParser *xml );

	//
	// Data.
	//	
	std::unique_ptr<dataLinksBase> _dataLinksBase;
	friend dataLinksBase;

public:

    // Constructors / destructor.
    linksBase() {}
    linksBase( short dim, const parameterParser *params, const boxBase *box, const beadsBase *beads, std::shared_ptr<dataBeadSpring> data );
    virtual ~linksBase() {}

	//
	// Accessors.
	//
	virtual double       longestInteraction() const noexcept { return _attachRange; }
	virtual unsigned int numLinks          () const noexcept { return _nLinks; }
	virtual bool         outputPercDim     () const noexcept { return _dataLinksBase->outputPercDim(); }
	virtual int          lastPercDim       () const noexcept { return _dataLinksBase->lastPercDim(); }


	//
	// Update and force calculations.
	//
	virtual void update( double t, double dt, int numBeads );					///< Attachment and detachment.
	virtual void forceCalculationStart() noexcept {}							///< Called from serial prior to force calculations.
	virtual void threadedIncrementForces( const double *X, double *F ) const;	///< Increments the force vector. Call from #pragma omp parallel.
	virtual void forceCalculationStop() noexcept {}								///< Called from serial after force calculations.


	//
	// Initial configurations.
	//
	virtual void setICIsotropic () { update(0.0,0.0,_beads->numBeads()); }		///< Initialise link containers only.
	virtual void setICTriLattice( int nNodes_x, int nNodes_y );					///< 2D only.
	
	//
	// Data.
	//
	virtual void dataCalculateTable() const { _dataLinksBase->calculateTableRow(); }
	virtual void dataCalculateBlock() const { _dataLinksBase->calculateBlock   (); }

	//
	// Debugging.
	//
	virtual void debugCheckBadLinks() const;
	
	//
	// I/O.
	//
	void saveState   ( flatXMLParser *xml ) const;
	void recoverState( flatXMLParser *xml );
};


#endif
