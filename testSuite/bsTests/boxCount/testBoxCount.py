#
# Tests for box counting by using pre-defined states of known fractal dimension.
#

# Standard imports
import numpy as np
import math
import scipy.optimize
import matplotlib.pyplot as plt

# Base class
from bsTests import bsTestBase

class testBoxCount( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname, extend ):

		# Store private variables.
		self.__stateFile  = fname
		self.__extendTime = extend

		# Call parent method with required flags.
		super(testBoxCount,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Sanity check.
		if self._blockData.getParameter("M") > 1:
			raise NotImplementedError( "Box count test assumes a monomeric system (i.e. M=1)" )

		if self._blockData.getParameter( "dimension" ) != 2:
			raise NotImplementedError( "Box count test currently asumes 2D" )

		#
		# Box counting; should have power law region with known slope (=fractal dimension).
		#
	
		# Get the final box counts. Note the abscissa x corresponds to a box edge of l_{0} 2^{x}.
		# Only one block data given the start configuration and extend time, so no [-1] for curves index.
		x, n  = [ self._blockData.getDataSets("log2BoxCount")["curves"][0][axis] for axis in ("x","y") ]

		# Message to stdout if requested.
		if self._verbose:
			print( "Checking the box counting predicts d_{f} equal to known value, and largest box had occupation 1." );

		# The predicted exponent.
		pred_df = math.log(3) / math.log(2)			# Sierpinski triangle's known d_{f}.

		# Get the (log of) fit to the (log of the) data. Ignore first point.
		# Since x-axis is already log'ed, fit to an exponential to get the underlying power law.
		def expFit( x, a, b ):
			return np.log([ a*math.exp(-b*_x) for _x in x ])

		# Range to fit against determined by eye for each state.
		fit = scipy.optimize.curve_fit( expFit, x[1:8], np.log(n[1:8]), p0=[n[0],pred_df] )
		a, b, db = fit[0][0], fit[0][1], math.sqrt(fit[1][1][1])

		# Display if requested.
		if self._plot:
			plt.plot( x, n, "ro--", label=r"${\rm Data}$" )
			plt.plot( x, np.exp(expFit(x,a,b)), "k-", label=r"${\rm fit}$" )

			plt.ylabel( r"$N(\ell)$" )
			plt.xlabel( r"${\rm Box\,edge\,}\ell$" )
			plt.xticks( x, [r"$2^{{{0}}}\ell_{{0}}$".format(int(_x)) for _x in x] )

			plt.semilogy()
			plt.legend()

			plt.show()
		
		# Check 1: Largest box size should have all partciles, hence an occupancy of 1.
		if abs(n[-1]-1.0) > testBoxCount.largestBoxTol():
			raise bsTestBase.FailedTestError( "largest box had an occupancy different to 1" )
		else:
			if self._verbose:
				print( "Largest box size had an occupancy of 1 (to within tolerance)" )

		# Check 2: The exponent. Rescale first, as actually fitted to N \propto e^{-bx} with x=log_{2}(r), rather than
		# N \propto r^{-d_f}; rearranging demonstrates that b needs to be divided by ln(2) before comparing to d_f.
		b  /= math.log(2)
		db /= math.log(2)

		if abs(pred_df-b) > testBoxCount.fractalDimSigma() * db:
			raise bsTestBase.FailedTestError( "fitted fractal dimension significantly different to expected value" )
		else:
			if self._verbose:
				print( "Fitted d_{{f}}={0:.4g} \pm {1:.4g} within tolerance of {2:.4g} (nb. ln(2) scaling applied)".format(b,db,pred_df) )

	# Arguments for the executable
	def _stateFile ( self ): return self.__stateFile
	def _extendTime( self ): return self.__extendTime
	def _numThreads( self ): return 1

	# Parameters used for checking.
	@classmethod
	def largestBoxTol( cls ):
		"""Tolerance for the largest box having an occupance of exactly 1. Can be tight."""
		return 1e-6

	@classmethod
	def fractalDimSigma( cls ):
		"""Sigma for the fractal dimension. Need to be generous as test systems are typically small."""
		return 2

	# Cleanup; can use wildcards
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_0???.*","scalars.xml","log.out"]




	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(1):
			fname = "state_test%i.xml" %testNum
			print( "Performing box count volume test starting from state '{}'".format(fname) )
			testBoxCount(executable,verbosity,plot,fname,1.0)		# Last two args are state filename and extend time. Don't try an extend of less than 1.0.
			print( " - passed" )
