#
# Viewer for states output by the Bead-Spring code. Much functionality inherited from the base class,
# this child really just maps from a bsStates object to graphical primitives. 
#
# Do not normally call directly; instead call the bsStates file, with the file (or archive) as
# argument, owhich defaults to displaying the system using this class.
#

#
# Imports.
#

# Standard imports.
import math

# Local imports.
from . import viewerBase

# From within this package
from .bsColourSchemes import bsColourSchemes
from .bsMovieController import bsMovieController

# Import OpenGL routines used in this file.
import glfw
from OpenGL.GL import *

class bsViewer( viewerBase.viewerBase ):

	def __init__(
			self,
			states,
			colourScheme=bsColourSchemes(),
			movieController=bsMovieController(),
			backgroundColour=(0.9,0.9,0.9),
			wireframeColour=(0.0,0.0,0.0) ):
		"""Call with an instance of bsStates and optionally a non-standard colour scheme and/or movie controller,
		providing project-specific control. The background colour, if specified, should be RGB in the form of a
		triplet of floats in the range [0,1]."""
		
		# Store parameters.
		self._states = states
		
		# Initialise key geometric variables and view options.
		self._dim        = states.dim()
		self._showBeads  = True
		self._showLinks  = True
		self._hiResBeads = False

		X =   self._states.X()
		Z =   self._states.Y()
		Y = ( self._states.Z() if self._dim==3 else 10.0 )

		# Initialise the colour scheme.
		self._colourScheme = colourScheme
		print( "Initial colour scheme {0} of {1}: {2}".format(self._colourScheme.currentSchemeIndex()+1,self._colourScheme.numSchemes(),self._colourScheme.description()) )

		# Initialise the movie controller.
		self._movieController = movieController
		print( "Initial movie controller type {0} of {1}: {2}".format(self._movieController.currentTypeIndex()+1,self._movieController.numTypes(),self._movieController.description()) )

		# Open up the main viewing window.
		super().__init__(
			[X,Y,Z],
			windowTitle = "Bead-Spring",
			backgroundRGB = backgroundColour,
			wireframeRGB = wireframeColour,
			quasi2D = (self._dim==2) )

		# Initialise the display lists
		glGenLists( len(self._states) )
		self._haveDrawnState = [False]*len(self._states)

		# Start the viewer			
		self._stateIndex = 0
		self.mainLoop()
	
	#
	# Utility routines.
	#

	# Applies periodic wrapping. Note the Y<->Z for box dimensions to aid viewing.
	def _wrapToPrimaryCell( self, x, y, z, state ):

		boxType = self._states.getParameter("boxType")
		
		# All Lees-Edwards-type boxes use the same wrapping. Current strain should be in the state file.
		if boxType in bsViewer.allLEBoxTypes():
			x -= math.floor(y/self._Z) * self._Z * state["LEShear"]

		# Normal periodic wrapping.
		if boxType in ("rectPeriodic",) + bsViewer.allLEBoxTypes():
			x -= self._X * math.floor(x/self._X)
			y -= self._Z * math.floor(y/self._Z)
			z -= self._Y * math.floor(z/self._Y)

		return [ x, y, z ]

	# Applies periodic wrapping for relative coordinates.
	def _wrapToNearestCopy( self, dx, dy, dz, state ):

		boxType = self._states.getParameter("boxType")
		
		# All Lees-Edwards-type boxes use the same wrapping. Current strain should be in the state file.
		if boxType in bsViewer.allLEBoxTypes():
			dx = dx - round(dy/self._Z) * self._Z * state["LEShear"]

		# Normal periodic wrapping.
		if boxType in ("rectPeriodic",) + bsViewer.allLEBoxTypes():
		
			try:
				dx = math.remainder(dx,self._X)						# math.remainder for Python 3.7.
				dy = math.remainder(dy,self._Z)
				dz = math.remainder(dz,self._Y)
			
			except AttributeError:

				while dx >   0.5*self._X: dx -= self._X				# Safe, may be slow.
				while dx < - 0.5*self._X: dx += self._X

				while dy >   0.5*self._Z: dy -= self._Z
				while dy < - 0.5*self._Z: dy += self._Z

				while dz >   0.5*self._Y: dz -= self._Y
				while dz < - 0.5*self._Y: dz += self._Y
			
		return [ dx, dy, dz ]
	
	# Bead coordinates for the given index, as [x,y,z].
	def _beadPosition( self, state, i ):
		if self._dim==3:
			return state["positions"][3*i:3*(i+1)]
		else:
			return state["positions"][2*i:2*(i+1)] + [0.0]		
	
	def displayState( self ):
		"""Called by parent to generate the view."""

		# Load state into a display list (will return immediately if this state has already been loaded)
		self._drawState( self._stateIndex )
		
		# Now draw the state from the display list (which may or may not have just been created)		
		glCallList( self._stateIndex+1 )


	def _drawState( self, index ):
		"""	Loads a state into its corresponding display list."""

		if self._haveDrawnState[index]: return
		
		#
		# Generate a new display state.		
		#
		glNewList( 1+index, GL_COMPILE )						# displayList indices start at 1
		self._haveDrawnState[index] = True						# Tell object this one has been drawn.

		state = self._states.getState(index)
	
		# Colour scheme may require some pre-processing for the entire state.
		self._colourScheme.newState( self._states, index )

		#
		# Beads.
		#
		
		# Resolution of the spheres.
		slices, stacks = 10, 10

		r = self._states.getParameter("b")/2.0

		for bead in range( state["numBeads"] ):

			# Get colour, which may depend on e.g. position along filament and/or the filament itself.
			rgb = self._colourScheme.beadColour(self._states,index,bead)
			if rgb==None: continue
			glColor( rgb )

			# Coordinates of the centre of this monomer. Need to apply wrapping here.
			x, y, z = self._beadPosition(state,bead)

			# Provide wrapping.
			x, y, z = self._wrapToPrimaryCell( x, y, z, state )
		
			if self._showBeads:
				if self._hiResBeads:
					self.drawSphere( [x,z,y], r, stacks=10, slices=20 )
				else:
					self.drawSphere( [x,z,y], r )

		#
		# Links.
		#
		if self._showLinks:
			for link in range( state["numLinks"] ):
				i, j = state["indices"][2*link:2*(link+1)]
			
				# Coordinates of both beads prior to periodic wrapping.
				x1, y1, z1 = self._beadPosition(state,i)
				x2, y2, z2 = self._beadPosition(state,j)
			
				# Apply periodic wrapping to bead i, and relative to that for bead j.
				dx, dy, dz = x2 - x1, y2 - y1, z2 - z1

				x1, y1, z1 = self._wrapToPrimaryCell( x1, y1, z1, state )
				dx, dy, dz = self._wrapToNearestCopy( dx, dy, dz, state )

				x2 = x1 + dx
				y2 = y1 + dy
				z2 = z1 + dz

				# Draw a line between beads i and j.
				glColor( self._colourScheme.linkColour() )
				glLineWidth(1)
				glDisable(GL_LIGHTING)

				glBegin(GL_LINES)
				glVertex3f(x1,z1,y1)							# As elsewhere, flip y<->z.
				glVertex3f(x2,z2,y2)
				glEnd()
					
		#
		# Finish up.
		#
		glEnable(GL_LIGHTING)
		glEndList()		# Finish this display list


	#
	# Keyboard commands and callback.
	#

	# Commands supported (output at start).
	def displayCommands( self ):
		super().displayCommands()
		print( "'i'        :\tInfo for current state" )
		print( "'n'        :\tMove to next state (if exists)" )
		print( "'p'        :\tMove to previous state (if exists)" )
		print( "'f'        :\tMove to final state (if not current state)" )
		print( "'r'        :\tStart generating movie images" )
		print( "'c'        :\tCycle colour scheme (if available)" )
		print( "'m'        :\tCycle movie controller (if available)" )
		print( "'b'        :\tToggle beads on/off" )
		print( "'l'        :\tToggle links on/off" )
		print( "'h'        :\tToggle hi-res beads on/off" )
		print( "'v'        :\tDisplay current centre and eye vectors" )
 
	# Keyboard callback.
	def _keyCallback( self, window, key, scancode, action, mode ):
		"""Adds to the camera controls, save figure and quit keys provided by the parent."""

		# Only deal with first press of a key here.
		if action == glfw.PRESS:

			# Echo state index and other information.
			if key == glfw.KEY_I:

				msg  = "State index {};".format( self._stateIndex )
				msg += " time t={};".format( self._states.getState(self._stateIndex)["time"] )
				msg += " nBeads={};".format( self._states.getState(self._stateIndex)["numBeads"] )
				msg += " nLinks={};".format( self._states.getState(self._stateIndex)["numLinks"] )
				msg += " nPoly={}." .format( self._states.getState(self._stateIndex)["numFilaments"] )
				
				print(msg)

				return
		
			# Increment state index.
			if key == glfw.KEY_N and self._stateIndex < len(self._states)-1:
				self._stateIndex += 1
				return
			
			# Decrement state index.
			if key == glfw.KEY_P and self._stateIndex > 0:
				self._stateIndex -= 1
				return

			# Jump to the final state.
			if key == glfw.KEY_F and self._stateIndex < len(self._states)-1:
				self._stateIndex = len(self._states) - 1
				return

			# Start generating images for a movie.
			if key == glfw.KEY_R:
				self._runMovie()
				return

			# Cycle colour scheme.
			if key == glfw.KEY_C:
				if self._colourScheme.cycleToNext(self._states,self._stateIndex):
					self._haveDrawnState = [False]*len(self._states)
					print( "New colour scheme {0} of {1}: {2}".format(self._colourScheme.currentSchemeIndex()+1,self._colourScheme.numSchemes(),self._colourScheme.description()) )
					return

			# Cycle movie controller.
			if key == glfw.KEY_M:
				if self._movieController.cycleToNext(self._states):
					print( "New movie controller type {0} of {1}: {2}".format(self._movieController.currentTypeIndex()+1,self._movieController.numTypes(),self._movieController.description()) )
					return

			# Toggle beads on/off.
			if key == glfw.KEY_B:
				self._showBeads = not self._showBeads
				self._haveDrawnState[self._stateIndex] = False
				print( "Show beads flag now {}.".format(self._showBeads) )
				return

			# Toggle links on/off.
			if key == glfw.KEY_L:
				self._showLinks = not self._showLinks
				self._haveDrawnState[self._stateIndex] = False
				print( "Show links flag now {}.".format(self._showLinks) )
				return

			# Toggle hi-res beads on/off.
			if key == glfw.KEY_H:
				self._hiResBeads = not self._hiResBeads
				self._haveDrawnState[self._stateIndex] = False
				print( "Hi-res beads flag now {}.".format(self._hiResBeads) )
				return

			# Display current centre and eye.
			if key == glfw.KEY_V:
				print( f"Current view centre (x,y,z)={self.getCentre()} and eye (R,\\theta,\\phi)={self.getEye()}." )

		# Call the parent class for the remaining methods.
		super()._keyCallback(window,key,scancode,action,mode)

	#
	# Movie image generation.
	#
	def _runMovie( self ):
		"""Generates images that can be stitched together into a movie. Behaviour depends on
		the current movie controller defined by self._movieController, which should be a instance
		of the class bsMovieController or derived from it."""

		# Some controller types may set the initial view.
		initCentre, initEye = self._movieController.initialCentreAndEye( self._states )
		if initCentre!=None and initEye!=None:
			self.setCentreAndEye( initCentre, initEye )

		# Loop through all states, in order.
		for i in range( len(self._states) ):
			self._stateIndex = i

			print( f" - generating image for state index {self._stateIndex}." )

			# All of self.displayAll() from the parent class 'viewerBase', except for swapping the
			# buffers and the final call to poll new events.
			self._preRender()
			self._drawSystemBox()
			self.displayState()

			# Save the image with a unique index of fixed width.
			self.saveImage( "image_{0:0>4}.jpg".format(self._stateIndex) )

			# Update the centre and/or eye, if the movie controller wants to.
			newCentre, newEye = self._movieController.newCentreAndEye( self.getCentre(), self.getEye(), self._states )
			if newCentre!=None and newEye!=None:
				self.setCentreAndEye( newCentre, newEye )


	#
	# Class methods.
	#
	
	@classmethod
	def allLEBoxTypes( cls ): return ("LEStepShear","LEOscillatory")



