#include "pRNG.h"


std::vector< std::mt19937_64 > pRNG::engines;
std::vector< std::normal_distribution<double> > pRNG::perThread_unitNormals;
std::vector< std::uniform_real_distribution<double> > pRNG::perThread_standardUniforms;


// Initialise engines and distributions.
void pRNG::initialise( long seed )
{
	// Set the first pRNG using the given seed.
	pRNG::engines.push_back( std::mt19937_64(seed) );

	// Use the first one to set the seeds of the remaining ones.
	std::uniform_int_distribution<long> seeding(0,seed);
	for( auto i=1; i<omp_get_max_threads(); i++ )
	{
		long nextSeed = seeding( pRNG::engines[0] );
		pRNG::engines.push_back( std::mt19937_64(nextSeed) );
	}

	// Also have per-thread distributions.
	for( auto i=0; i<omp_get_max_threads(); i++ )
	{
		pRNG::perThread_unitNormals.push_back( std::normal_distribution<double>(0,1) );
		pRNG::perThread_standardUniforms.push_back( std::uniform_real_distribution<double>(0,1) );
	}
}

// Returns unit normal for the given thread.
double pRNG::unitNormal( int t )
{
	return pRNG::perThread_unitNormals[t]( pRNG::engines[t] );
}

// Returns with the standard unoform for the given thread.
double pRNG::stdUniform( int t )
{
	return pRNG::perThread_standardUniforms[t]( pRNG::engines[t] );
}

// Returns a vector of integers in the range [0,n) that have been randomly shuffled.
std::vector<int> pRNG::shuffledIndices( int n, int t )
{
	// Initialise indices in-order.
	std::vector<int> vec;
	for( auto i=0; i<n; i++ ) vec.push_back(i);

	// Randomly shuffle.
	std::shuffle( vec.begin(), vec.end(), pRNG::engines[t] );

	return vec;
}
