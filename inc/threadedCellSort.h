//
// Version of cellList to work in parallel (SMP with C++11 threads), which uses a different
// internal storage to the memory-efficient Allen-Tildesly version of cellList that is however
// difficult to parallelise. At time of writing, not all of the options of cellList have
// been implemented in this version, but may be added at a later date.
//
// Templated by dimension d to help the compiler with optimisations (i.e. so loop sizes known
// at compile time), but only supports d=2 or 3 at present. There is a small .cpp file as
// a few of the routines have been specialised.
//
//
// Intended usage (see also the test code):
// ----------------------------------------
//
// threadedCellSort<2> cellList(r_max,L[0],L[1]);			// Initialise a 2D mesh, with maximum interaction range
//															// r_max (from which cell sizes are inferred).
//
// Each iteration:
//
// cellList.sortParticles_xyz( N, X );							// N particles, X assumed in (x0,y0[,z0],x1,y1[,z1],...) format.
// 
// cellList.generatePerThreadPairLists( naive );				// Generates one pair list per thread (as specified in constructor).
//																// if naive==true, will partition cells irrespective of their particle
//																// count; otherwise will attempt to load balance (takes extra time).
//
// unsigned long numPairs = cellList.numPairsInList(t);			// Number of pairings for thread t
// const unsigned long* pairList = cellList.perThreadPairList(t);
// ... so each potentially-interacting pair of particles have indices pairList[2*pair] and pairList[2*pair+1].
//
//
// There is also a serial-only, while-loop version, partly for legacy, but also because the loop itself is very concise
// and may be preferable when speed is not an issue. Note that it is assumed that the particles have been sorted, and also
// that the per-thread lists have been generated.
//
// unsigned long i, j;
// while( cellList.whileLoopNext(i,j) ) update(i,j,...)
//
// Note that the internal counters are reset when generating the (per-thread) pair lists, as per expected usage;
// if somehow used differently, would need to allow them to be reset separately.
//



#ifndef _UD_THREADED_CELL_SORT_H
#define _UD_THREADED_CELL_SORT_H


//
// Includes
//

// Standard includes
#include <iostream>
#include <thread>
#include <string>
#include <sstream>
#include <vector>
#include <omp.h>


using namespace std;


#pragma mark -
#pragma mark Class template definition
#pragma mark -

template< unsigned short dim >
class threadedCellSort
{

private:

	// "Large" values for no. particles in a cell (memory reserved to this size, to reduce re-allocations).
	// Set in initialiser.
	unsigned int __highParticlesPerCell;

	// The number of nearest neighbours in one direction (i.e. right/down only). Set in initialiser.
	unsigned short __numNN;

	// Multipliers used when calculating the cell index for each particle; also a routine to set them.
	unsigned long __multipliers[dim];
	inline void __setMultipliers() noexcept
		{ __multipliers[0] = 1; for( auto k=1; k<dim; k++ ) __multipliers[k] = _numCells[k-1] * __multipliers[k-1]; }

protected:

	// Box dimensions
	double _X[dim];

	// Intended cell size, size and number in each dimension, and total.
	double        _requestedCellSize;
	double        _unitCell[dim];
	unsigned int  _numCells[3];								// Fix at dim==3 to avoid compiler warnings.
	unsigned long _totalNumCells;

	// Lists of unidirectional (right/down/etc.) neighbouring cell indices; generated on construction and never changed.		
	vector<unsigned long> _cellNNs;

	// Each cell has a vector<> of particle indices (reserve set high for speed), and its own lock.
	vector<unsigned long> *_cellParticles;
	omp_lock_t *_cellLocks;
	
	// The total number of particles added at the last sort
	unsigned long _numParticles_lastSort;
	
	// Persistent store one pair list per thread.
	unsigned long **_perThreadPairLists;
	unsigned long * _perThreadPairLists_nPairs;
	
	// For the (serial) while-loop version, needs to store thread and pairing. Reset in sort().
	unsigned int  _whileLoop_pairList;
	unsigned long _whileLoop_pairNum;
	
	// However constructed, call this to initialise the lists.
	virtual void _initialise( double cellSize );
	
	// Generates the (unidirectional) list of neighbouring cells. Defined in .cpp.
	virtual void _generateCellNNs();

	// Sorting into cells
	unsigned long _cellIndexFor( const double *x ) const noexcept;
	
	// Generating the per-thread pair lists.
	void _genPairLists_cellPartition_perThread( unsigned int t, unsigned long start, unsigned long size );
	void _genPairLists_pairPartition_perThread( unsigned int t, unsigned long start, unsigned long size, bool reallocate );
	unsigned long _pairsForCell( unsigned long cell ) const noexcept;
	
public:

	//
	// Construct with cell size, box dimensions and no. threads [defaults to thread::hardware_concurrency()]
	//
	threadedCellSort() {}
	threadedCellSort( double cellSize, double  X, double Y );				// 2D constructor
	threadedCellSort( double cellSize, double  X, double Y, double Z );		// 3D constructor
	threadedCellSort( double cellSize, double *X );							// Arb. dimension.
	virtual ~threadedCellSort();
	
	//
	// Perform sorting and extract particles in adjacent cells
	//
	virtual      void           sortParticles_xyz( unsigned long N, const double *posns );		// Coordinate format (x0,y0[,z0],x1,y1[,z1],..)
	inline       unsigned long  numPairsInList   ( unsigned int t ) const { return _perThreadPairLists_nPairs[t]; }
	inline const unsigned long* perThreadPairList( unsigned int t ) const { return _perThreadPairLists       [t]; }
	
	virtual void generatePairLists_cellPartitioning();				// Equal as possible no. cells per thread; fast.
	virtual void generatePairLists_pairPartitioning();				// Equal as possible no. pairs per thread; slow.

	//
	// Reading the pairs in serial, using a while loop.
	//
	bool whileLoopNext( unsigned long &i, unsigned long &j ) noexcept;
	
	
	//
	// Accessors; note lack of range checking (for speed).
	//
	inline double         requestedCellSize() const noexcept { return _requestedCellSize; }
	inline unsigned long  totalNumCells    () const noexcept { return _totalNumCells; }
	       unsigned long  maxCellOccupancy () const noexcept;
	       unsigned long  totalPairListSize() const noexcept;
	inline unsigned short numCellNN        () const noexcept { return __numNN; }
	inline unsigned int   numPairLists     () const noexcept { return omp_get_max_threads(); }
	
    double ratioSmallestToLargestPairList() const noexcept;				// A measure of load balancing

	inline const vector< unsigned long > *particleVecForCell( unsigned long cell ) const noexcept
		{ return &_cellParticles[cell]; }

	inline unsigned long numInCell( unsigned long cell ) const noexcept
		{ return _cellParticles[cell].size(); }

	inline unsigned long cellNN( unsigned long cell, unsigned short relative ) const noexcept
		{ return _cellNNs[cell*__numNN+relative]; }

	inline unsigned long particleInCell( unsigned long cell, unsigned long index ) const noexcept
		{ return _cellParticles[cell][index]; }


	//
	// Output
	//
	template< unsigned short d >
	friend ostream& operator<< (ostream&,const threadedCellSort<d>&);
	
	//
	// Debugging; throws ::CheckFailed() exception if anything found to be wrong.
	//
	virtual void checkCellSortIntegrity() const;
	virtual void checkPairListIntegrity() const;

	//
	// Exceptions
	//
	class DimensionError;
	class BadCellDimensions;
	class CheckFailed;
};


#pragma mark -
#pragma mark Custom exceptions


template< unsigned short dim >
class threadedCellSort<dim>::DimensionError : public runtime_error
{
private:
	string makeWhat( string during, string why, string file, int line )
	{
		ostringstream out;
		out << "Dimension error during " << during << "; " << why 
			<< " [in file " << file << ", line " << line << "]" << endl;
		return out.str();
	}

public:
	DimensionError( string during, string why, string file, int line ) : runtime_error( makeWhat(during,why,file,line) ) {}
};

template< unsigned short dim >
class threadedCellSort<dim>::BadCellDimensions : public runtime_error
{
private:
	string makeWhat( string why, string file, int line )
	{
		ostringstream out;
		out << "Bad cell dimensions for (threaded) cell list; " << why 
			<< " [in file " << file << ", line " << line << "]" << endl;
		return out.str();
	}

public:
	BadCellDimensions( string why, string file, int line ) : runtime_error( makeWhat(why,file,line) ) {}
};

template< unsigned short dim >
class threadedCellSort<dim>::CheckFailed : public runtime_error
{
private:
	string makeWhat( string check, string why, string file, int line )
	{
		ostringstream out;
		out << "Check failed for " << check << "; " << why
			<< " [in file " << file << ", line " << line << "]" << endl;
		return out.str();
	}

public:
	CheckFailed( string check, string why, string file, int line ) : runtime_error( makeWhat(check,why,file,line) ) {}
};




#pragma mark -
#pragma mark Initialisation and clear-up
#pragma mark -

// 3D constructor with explicit box dimensions (i.e. one argument for each).
template< unsigned short dim >
threadedCellSort<dim>::threadedCellSort( double cellSize, double X, double Y, double Z )
{
	// Sanity check for dimension.
	if( dim==2 )
		throw DimensionError("initialisation of cell list","3D constructor for 2D object",__FILE__,__LINE__);

	// Copy over box dimensions and call the generic initialisation.
	_X[0] = X;
	_X[1] = Y;
	_X[2] = Z;
	_initialise( cellSize );	
}

// 2D constructor with explicit box dimensions (i.e. one argument for each).
template< unsigned short dim >
threadedCellSort<dim>::threadedCellSort( double cellSize, double X, double Y )
{
	// Sanity check for dimension.
	if( dim==3 )
		throw DimensionError("initialisation of cell list","2D constructor for 3D object",__FILE__,__LINE__);

	// Copy over box dimensions and call the generic initialisation.
	_X[0] = X;
	_X[1] = Y;
	_initialise( cellSize );	
}

// Arbitrary dimension constructor.
template< unsigned short dim >
threadedCellSort<dim>::threadedCellSort( double cellSize, double *X )
{
	// Copy over box dimensions and call the generic initialisation.
	for( unsigned short k=0; k<dim; k++ ) _X[k] = X[k];
	_initialise( cellSize );
}

// General initialisation.
template< unsigned short dim >
void threadedCellSort<dim>::_initialise( double cellSize )
{
	// Only 2D and 3D supported.
	if( dim!=2 and dim!=3 )
		throw DimensionError("initialisation of cell list","only 2D and 3D supported",__FILE__,__LINE__);
		
	// Also store the number of particles used for the last sort (0 means never sorted)
	_numParticles_lastSort = 0;

	// Reset the persist thread/pair counts for the (serial) while-loop version, in case needed.
	_whileLoop_pairList = 0u;
	_whileLoop_pairNum  = 0u;
	
	// Set the dimension-dependent constants, for all currently supported dimensions.
	__highParticlesPerCell = ( dim==2 ? 30 : 50 );				// Can be varied to reduce re-allocations.
	__numNN                = ( dim==2 ? 5  : 14 );				// Fixed.

	//
	// Initialise cells
	//
	
	// The intended cell size; sanity check and store.
	if( cellSize <= 0.0 )
		throw BadCellDimensions("requested cell size not positive",__FILE__,__LINE__);
	_requestedCellSize = cellSize;

	// The number of cells in each dimension, rounded down (so actual cell widths may be larger than requested).
	// If any less than 3, throw an exception (periodic BC implementation assumes at least 3 iin each direction)
	for( unsigned short k=0; k<dim; k++ )
	{
		_numCells[k] = static_cast<unsigned int>( _X[k] / cellSize );
		_unitCell[k] = _X[k] / _numCells[k];
		if( _numCells[k] < 3 )
			throw BadCellDimensions("requested cell size too large, resulting in fewer than 3 cells in at least one direction",__FILE__,__LINE__);
	}

	// The total number of cells.
	_totalNumCells = _numCells[0] * _numCells[1] * ( dim==3 ? _numCells[2] : 1 );	

	// Set the multipliers used for calculating the cell index
	__setMultipliers();
	
	// Allocate memory for the particle index vectors and the locks, one per cell (so high memory but should be fast).
	try
	{
		_cellParticles = new vector<unsigned long>[_totalNumCells];
		_cellLocks     = new omp_lock_t[_totalNumCells];
	}
	catch( const bad_alloc &e )
	{
		cerr << "Memory error allocating cells [in " << __FILE__ << ", line " << __LINE__ << "]" << endl;
		throw;
	}

	// Set each vector's reserve to something 'high' (to minimise chances of subsequent re-allocations, which can be slow).
	try
	{
		#pragma omp parallel for
		for( auto c=0u; c<_totalNumCells; c++ )
		{
			_cellParticles[c].reserve( __highParticlesPerCell );
		}
	}
	catch( const bad_alloc &e )
	{
		cerr << "Memory allocation setting the reserve for cells [in " << __FILE__ << ", line " << __LINE__ << "]" << endl;
		throw;
	}

	// Initialise all of the per-cell locks.
	#pragma omp parallel for
	for( auto c=0u; c<_totalNumCells; c++ )
		omp_init_lock( &_cellLocks[c] );

	//
	// Initialise the list of neighbouring cells.
	//
	
	// Allocate memory
	try
	{
		_cellNNs.resize( _totalNumCells*__numNN );
	}
	catch( bad_alloc &e )
	{
		cerr << "Cannot allocate memory for the NN cells [in " << __FILE__ << ", line " << __LINE__ << "]" << endl;
		throw;
	}

	// Use dim-specialised routine to actually fill the array
	_generateCellNNs();	

	//
	// Initialise the pair lists, only every store as one list per thread.
	//
	auto nThreads = omp_get_max_threads();
	try
	{
		_perThreadPairLists_nPairs = new unsigned long [nThreads];
		_perThreadPairLists        = new unsigned long*[nThreads];
	}
	catch( bad_alloc &e )
	{
		cerr << "Cannot allocate memory for the pair lists [in " << __FILE__ << ", line " << __LINE__ << "]" << endl;
		throw;
	}

	#pragma omp parallel
	{
		auto t = omp_get_thread_num();

		_perThreadPairLists_nPairs[t] = 0u;
		_perThreadPairLists       [t] = nullptr;
	}
}


// Destructor
template< unsigned short dim >
threadedCellSort<dim>::~threadedCellSort()
{
	#pragma omp parallel
	{
		delete [] _perThreadPairLists[omp_get_thread_num()];
	}

	#pragma omp parallel for
	for( auto c=0u; c<_totalNumCells; c++ )
		omp_destroy_lock( &_cellLocks[c] );

	delete [] _perThreadPairLists;
	delete [] _perThreadPairLists_nPairs;
	delete [] _cellParticles;
	delete [] _cellLocks;
}


#pragma mark -
#pragma mark Perform sorting
#pragma mark -

// Sort particles, where the coordinates are assumed to be in the 'xyz' format, i.e. (x0,y0[,z0],x1,y1[,z1],..)
template< unsigned short dim >
void threadedCellSort<dim>::sortParticles_xyz( unsigned long N, const double *X )
{
	// If no particles, return immediately.
	if( !N ) return;

	// Clear the current cells.
	#pragma omp parallel for
	for( auto c=0u; c<_totalNumCells; c++ )
		_cellParticles[c].resize( 0 );
	
	// Partition the list into as-equal-as-possible numbers of particles.
	if( omp_get_max_threads()>1 )
	{
		#pragma omp parallel for
		for( auto n=0u; n<N; n++ )
		{
			auto c = _cellIndexFor( &X[dim*n] );
			
			// Add to the cell, with thread protection.
			omp_set_lock( &_cellLocks[c] );
			_cellParticles[c].push_back( n );
			omp_unset_lock( &_cellLocks[c] );
		}
	}
	else
	{
		// No thread protection.
		for( auto n=0u; n<N; n++ )
		{
			auto c = _cellIndexFor( &X[dim*n] );
			_cellParticles[c].push_back( n );
		}
	}

	// Store the number of particles
	_numParticles_lastSort = N;

	// Reset the persist thread/pair counts for the (serial) while-loop version, in case needed.
	_whileLoop_pairList = 0u;
	_whileLoop_pairNum  = 0u;
}

// The cell index for the given particle position
template< unsigned short dim >
unsigned long threadedCellSort<dim>::_cellIndexFor( const double *_x ) const noexcept
{
	// The cell index and scratch variables
	unsigned long index = 0, bin;
	double x;

	// Loop over each dimension.
	for( auto k=0u; k<dim; k++ )
	{
		// Wrap this dimension's coordinate to periodic BCs; must use a copy
		x = _x[k];
		while( x <   0.0  ) x += _X[k]; 
		while( x >= _X[k] ) x -= _X[k];
		
		// Convert to integer
		bin = x / _unitCell[k];
		
		// Update the cell index
		index += __multipliers[k] * bin;							// Multipliers calculated in __setMultipliers()
	}

	return index;
}



#pragma mark -
#pragma mark Generate pair lists, partitioning by cells alone
#pragma mark -

// Generates the pair lists for each of the threads (as specified in the constructor).
// Assigns an almost-equal no. cells per thread, which are then converted to pair lists.
// This is fairly fast and easy to calculate, but can give poor load balancing when
// the no. of particles per cell varies widely (e.g. a system in which the particles
// only occupy half the box).
template< unsigned short dim>
void threadedCellSort<dim>::generatePairLists_cellPartitioning()
{
	#pragma omp parallel
	{
		unsigned int thread = omp_get_thread_num();
		unsigned int nThreads = omp_get_max_threads();

		// Partition cells into ranges, as far as possible equal in size.
		unsigned int N = totalNumCells(), M = N/nThreads;

		auto start = thread*M + ( thread<N-M*nThreads ? thread : N-M*nThreads );
		auto size  =        M + ( thread<N-M*nThreads ? 1      : 0            );

		// Calculate the total number of pairs for each thread in parallel; will store in _perThreadPairLists_nPairs[t].
		 _genPairLists_cellPartition_perThread( thread, start, size );
	}
}

// The per-thread routine for generating the pair lists. This also allocates memory for each pair list,
// and overwrites any existing ones.
template< unsigned short dim >
void threadedCellSort<dim>::_genPairLists_cellPartition_perThread(
	unsigned int  t,
	unsigned long start,
	unsigned long size )
{
	// Local shorthands
	unsigned long *pairList = _perThreadPairLists[t];

	// Get the number of pairs for this thread; store.
	unsigned long numPairs = 0;
	for( auto cell=start; cell<start+size; cell++ ) numPairs += _pairsForCell(cell);

	// Allocate memory for the list; only if required.
	if( numPairs > _perThreadPairLists_nPairs[t] )
	{
		delete [] _perThreadPairLists[t];
		try
		{
			_perThreadPairLists[t] = new unsigned long[2*numPairs];			// NB: Two indices per pair.
			pairList = _perThreadPairLists[t];								// Update local shorthand
		}
		catch( bad_alloc &e )
		{
			cerr << "Could not allocate memory for a per-thread pair list [in " << __FILE__ <<", line " << __LINE__ << "]" << endl;
			throw;
		}
	}
	_perThreadPairLists_nPairs[t] = numPairs;

	// Fill in the pair list
	unsigned long count = 0;
	for( auto cell=start; cell<start+size; cell++ )
	{
		// Don't double count the central cell (i.e. j>i only)
		for( auto i=0u; i<numInCell(cell); i++ )
			for( auto j=i+1; j<numInCell(cell); j++ )
			{
				pairList[count++] = particleInCell(cell,i);
				pairList[count++] = particleInCell(cell,j);
			}

		// For all other cells (i.e. relative>0), make all possible pairings. This part is the slowest
		// and also seems to scale badly (with g++); not yet clear why. Have tried: local (const'ed) ptrs;
		// loop re-ordering (outermost i-loop). Might be a memory cache issue?
		for( auto relative = 1; relative < numCellNN(); relative++ )
			for( auto i : *particleVecForCell(cell) )
				for( auto j : *particleVecForCell( cellNN(cell,relative) ) )
				{
					pairList[count++] = i;
					pairList[count++] = j;
				}

	} // End of "cell" loop

	// Quick check that the correct number was found
	if( count != 2*numPairs )
		throw CheckFailed("generate pair list","mis-calculated no. of pairs",__FILE__,__LINE__);
}


#pragma mark -
#pragma mark Generate pair lists, partitioning by numbers of pairs in cells
#pragma mark -

// Generates the pair lists for each of the threads (as specified in the constructor).
// Naively partitioning the cells between threads can lead to poor load balancing for
// heterogeneous systems, therefore also try to balance the number of pairs per thread.
template< unsigned short dim>
void threadedCellSort<dim>::generatePairLists_pairPartitioning()
{
	// Shorthands
	unsigned int nThreads = omp_get_max_threads();

	// Vector for the starts/sizes of cells per-thread, and if a re-allocation is required
	vector< bool > needsReallocation(nThreads);
	vector< unsigned long > startCell(nThreads,0), numCells(nThreads,0);
	
	//
	// Calculate the partitioning. Currently in serial; would be tricky to parallelise pass 2,
	// and in any case the main bottleneck with regards scaling seems to be in the per-thread routine.
	//

	// Pass 1: Calculate the total number of pairings.
	unsigned long totalNumPairs = 0u;

	#pragma omp parallel for reduction(+:totalNumPairs)
	for( auto cell=0u; cell<totalNumCells(); cell++ ) totalNumPairs += _pairsForCell(cell);

	// Pass 2: Which cell, and how many, each list should parse. Also calculate the total per thread.
	// Not sure how to parallelise this. Could replace whole thing with diffusive method?
	unsigned int t=0u;
	unsigned long runningTotalPairs=0u, lastPairStart=0u, lastCellStart=0u;

	for( auto cell=0u; cell<totalNumCells(); cell++ )
	{
		// Time to move onto the next thread's pair list? Note must do this before updating the running total.
		if( t<nThreads-1 && runningTotalPairs >= static_cast<double>(t+1)*totalNumPairs/nThreads )
		{
			startCell[t] = lastCellStart;
			numCells [t] = cell - lastCellStart;
			needsReallocation[t] = (runningTotalPairs-lastPairStart) > _perThreadPairLists_nPairs[t];
			_perThreadPairLists_nPairs[t] = runningTotalPairs - lastPairStart;

			lastCellStart = cell;
			lastPairStart = runningTotalPairs;
		
			if( ++t == nThreads-1 ) break;		// Can calculate final sizes from known totals
		}		

		// Same as the pass-1 calculation
		runningTotalPairs += _pairsForCell(cell);
	}
	
	// Fill in the data for the final pair list
	startCell[t] = lastCellStart;
	numCells [t] = totalNumCells() - lastCellStart;
	needsReallocation[t] = (totalNumPairs-lastPairStart) > _perThreadPairLists_nPairs[t];
	_perThreadPairLists_nPairs[t] = totalNumPairs - lastPairStart;

	// Sometimes (for small systems) the final cell has pair lists for multiple threads; check if so, and leave all
	// remaining threads pair lists empty (since only happens for small systems, efficiency not really an issue anyway).
	for( auto t1=t+1; t1<nThreads; t1++ )
	{
		startCell[t1] = totalNumCells() - 1;
		numCells [t1] = 0u;
		_perThreadPairLists_nPairs[t1] = 0u;
	}

	// Generate lists in parallel, partitioned by the cells
	#pragma omp parallel
	{
		auto t = omp_get_thread_num();
		_genPairLists_pairPartition_perThread( t, startCell[t], numCells[t], needsReallocation[t] );
	}
}

// The per-thread routine for generating the pair lists. This version assumes the partitioning has
// has already been performed, and the number of pairs for this thread's list is already known.
template< unsigned short dim >
void threadedCellSort<dim>::_genPairLists_pairPartition_perThread(
	unsigned int  t,
	unsigned long start,
	unsigned long size,
	bool reallcoate )
{
	// Local shorthands
	unsigned long *pairList = _perThreadPairLists[t];
	unsigned long  numPairs = _perThreadPairLists_nPairs[t];

	// Allocate memory for the list; only if required (as determined by caller)
	if( reallcoate )
	{
		delete [] _perThreadPairLists[t];
		try
		{
			_perThreadPairLists[t] = new unsigned long[2*numPairs];		// NB: Two indices per pair.
			pairList = _perThreadPairLists[t];								// Update local shorthand
		}
		catch( bad_alloc &e )
		{
			cerr << "Could not allocate memory for a per-thread pair list [in " << __FILE__ <<", line " << __LINE__ << "]" << endl;
			throw;
		}
	}
	_perThreadPairLists_nPairs[t] = numPairs;

	// Fill in the pair list
	unsigned long count = 0;
	for( auto cell=start; cell<start+size; cell++ )
	{
		// Avoid repeats in the central cell (i.e. j>i only)
		for( auto i=0u; i<numInCell(cell); i++ )
			for( auto j=i+1; j<numInCell(cell); j++ )
			{
				pairList[count++] = particleInCell(cell,i);
				pairList[count++] = particleInCell(cell,j);
			}

		// For all other cells (i.e. relative>0), make all possible pairings. This part is the slowest
		// and also seems to scale badly (with g++); not yet clear why. Have tried: local (const'ed) ptrs;
		// loop re-ordering (outermost i-loop). Might be a memory cache issue?
		for( auto relative = 1; relative < numCellNN(); relative++ )
			for( auto i : *particleVecForCell(cell) )
				for( auto j : *particleVecForCell(cellNN(cell,relative)) )
				{
					pairList[count++] = i;
					pairList[count++] = j;
				}

	} // End of "cell" loop

	// Quick check that the correct number was found
	if( count != 2*numPairs )
		throw CheckFailed("generate pair list","mis-calculated no. of pairs",__FILE__,__LINE__);
}


#pragma mark -
#pragma mark Accessors
#pragma mark -

// Returns with the maximum occupancy of all cells; useful to check it is not too close to
// the suggested maximum __highParticlesPerCell() (if it is, a performance hit could be incurred).
template< unsigned short dim >
unsigned long threadedCellSort<dim>::maxCellOccupancy() const noexcept
{
	unsigned long maxSoFar = 0;
	
	for( auto c=0; c<_totalNumCells; c++ )
		maxSoFar = std::max( maxSoFar, _cellParticles[c].size() );
	
	return maxSoFar;
}

// The total number of pairs
template<unsigned short dim>
unsigned long threadedCellSort<dim>::totalPairListSize() const noexcept
{
	// Sum the size of all of the available pair lists
	unsigned long sum = 0;
	for( auto pl=0u; pl<numPairLists(); pl++ ) sum += numPairsInList(pl);
	
	// Return number of pairs.
	return sum;
}

// Ratio of the smallest to the largest pair list sizes, to be used as a measure of
// theoretical load balancing. Returns zero if all lists zero length.
template< unsigned short dim >
double threadedCellSort<dim>::ratioSmallestToLargestPairList() const noexcept
{
	auto shortest=_perThreadPairLists_nPairs[0], longest=shortest;
	for( auto pl=1u; pl<numPairLists(); pl++ )
	{
		if( shortest>_perThreadPairLists_nPairs[pl] ) shortest=_perThreadPairLists_nPairs[pl];
		if( longest <_perThreadPairLists_nPairs[pl] ) longest =_perThreadPairLists_nPairs[pl];
	}
	
	if( longest )
		return static_cast<double>(shortest)/longest;
	else
		return 0.0;
}

// Reading in serial using a while-loop. Results in more concise code but cannot be parallelised.
template< unsigned short dim >
bool threadedCellSort<dim>::whileLoopNext( unsigned long &i, unsigned long &j ) noexcept
{
	// Keep on incrementing until a valid pair is found, or return false. Note that empty pair lists are possible.
	_whileLoop_pairNum++;
	while( _whileLoop_pairNum >= numPairsInList(_whileLoop_pairList) )
	{
		// Move to next list. If already on the last, return with 'false' denoting no more pairings to consider
		if( ++_whileLoop_pairList == numPairLists() ) return false;
		
		// Reset the pairing within this list to zero; if list size is zero, will loop at least once more.
		_whileLoop_pairNum = 0u;		
	}
	
	i = perThreadPairList(_whileLoop_pairList)[2*_whileLoop_pairNum  ];
	j = perThreadPairList(_whileLoop_pairList)[2*_whileLoop_pairNum+1];

	return true;
}

// The number of pairs corresponding to the given cell, i.e. all those within that cell,
// and also those in "below and to the right" cells.
template< unsigned short dim >
unsigned long threadedCellSort<dim>::_pairsForCell( unsigned long cell ) const noexcept
{
	unsigned long total = 0;
	
	// All pairs within the same cell, with no repeats
	total += numInCell(cell) * ( numInCell(cell)-1 ) / 2;
	
	// All pairs in adjacent cells, with no repeats
	for( auto relative=1; relative<numCellNN(); relative++ )
		total += numInCell(cell) * numInCell(cellNN(cell,relative));
	
	return total;
}


#pragma mark -
#pragma mark Output
#pragma mark -

template< unsigned short d >
ostream& operator<< ( ostream &os, const threadedCellSort<d> &c )
{
	// Generic message
	os << "Multi-threaded cell sorter for dimension " << d << "." << endl;

	// Box dimensions: Actual and no. cells in each direction.
	if( d==2 )
		os << " - box dimensions (" << c._X[0] << "," << c._X[1] << ") "
		   << "corresponding to (" << c._numCells[0] << "," << c._numCells[1] << ") cells;";

	if( d==3 )
		os << " - box dimensions (" << c._X[0] << "," << c._X[1] << "," << c._X[2] << ") "
		   << "corresponding to (" << c._numCells[0] << "," << c._numCells[1] << "," << c._numCells[2] << ") cells;";

	os << " total " << c._totalNumCells << "." << endl;

	// The unit cell
	os << " - unit cell (" << c._unitCell[0] << "," << c._unitCell[1];
	if( d==3 ) os << "," << c._unitCell[2];
	os << "), based on a requested cell size of " << c._requestedCellSize << "." << endl;
	
	return os;
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Check to see if the cells were (probably) sorted correctly.
template< unsigned short dim >
void threadedCellSort<dim>::checkCellSortIntegrity() const
{
	// Check each particle index appears once (and exactly once) in the cells.
	vector<bool> found( _numParticles_lastSort );
	for( auto n=0u; n<_numParticles_lastSort; n++ ) found[n] = false;

	for( auto c=0u; c<_totalNumCells; c++ )
	{
		for( auto &index : _cellParticles[c] )
		{
			// Fail if index exceeds range (nb. unsigned so cannot be negative)
			if( index>=_numParticles_lastSort ) throw CheckFailed("cell sort","found index exceeding N",__FILE__,__LINE__);
			
			// Fail if particle has already been found
			if( found[index] ) throw CheckFailed("cell sort","found repeated index",__FILE__,__LINE__);
			
			found[index] = true;
		}
	}

	// Check none were left out
	for( auto n=0u; n<_numParticles_lastSort; n++ )
		if( !found[n] )
			throw CheckFailed("cell sort","missed at least one index",__FILE__,__LINE__);
}


// Check some aspects of the pair lists (though not so much can be checked here as the cell sorting).
template< unsigned short dim>
void threadedCellSort<dim>::checkPairListIntegrity() const
{
	// Check none of the pairings are between a particle and itself
	for( auto list=0u; list<numPairLists(); list++ )
	{
		// Check for identical indices
		for( auto pairNum=0u; pairNum<numPairsInList(list); pairNum++ )
			if( _perThreadPairLists[list][2*pairNum] == _perThreadPairLists[list][2*pairNum+1] )
				throw CheckFailed("pair list","repeated index in same pairing",__FILE__,__LINE__);
	}

	// Check no pairs appear twice. This can be slow (nested double loop).
	for( auto list1=0u; list1<numPairLists(); list1++ )
		for( auto pairNum1=0u; pairNum1<numPairsInList(list1); pairNum1++ )
		{

			for( auto list2=list1; list2<numPairLists(); list2++ )
				for( auto pairNum2=0u; pairNum2<numPairsInList(list2); pairNum2++ )
				{
				
					// Only check forwards (with respect to list1/pairNum1)
					if( list1==list2 && pairNum2<=pairNum1 ) continue;
					
					// Check the pairings are not identical; two ways to check (since they were inserted unordered)
					if(    _perThreadPairLists[list1][2*pairNum1  ]==_perThreadPairLists[list2][2*pairNum2  ]
						&& _perThreadPairLists[list1][2*pairNum1+1]==_perThreadPairLists[list2][2*pairNum2+1] )
						throw CheckFailed("pair list","matched pairing",__FILE__,__LINE__);

					if(    _perThreadPairLists[list1][2*pairNum1  ]==_perThreadPairLists[list2][2*pairNum2+1]
						&& _perThreadPairLists[list1][2*pairNum1+1]==_perThreadPairLists[list2][2*pairNum2  ] )
						throw CheckFailed("pair list","matched pairing",__FILE__,__LINE__);
				}			
		}
}


#endif

