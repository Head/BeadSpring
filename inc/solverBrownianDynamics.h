/**
 * @file solverBrownianDynamics.h
 * @author David Head
 * @brief Solver for Brownian dynamics, treating each bead as separate.
 */


#ifndef BEAD_SPRING_SOLVER_BROWNIAN_DYNAMICS_H
#define BEAD_SPRING_SOLVER_BROWNIAN_DYNAMICS_H


//
// Includes.
//

// Standard includes.
#include <string>
#include <vector>
#include <thread>
#include <omp.h>
#include <functional>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "solverBase.h"				// Parent class.
#include "errors.h"
#include "logging.h"
#include "pRNG.h"
#include "parameterParser.h"


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string solverType_BrownianDynamics = "BrownianDynamics";
	
	const std::string solver_gamma = "gamma";
	const std::string solver_kT    = "kT"   ;
}


/**
 * @brief Solver for Brownian dynamics, treating each bead as separate.
 */
class solverBrownianDynamics : public solverBase
{

private:

protected:

	// Solver parameters.
	double _gamma;
	double _kT;

public:

    // Constructors / destructor.
    solverBrownianDynamics() {}
    solverBrownianDynamics( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );
    virtual ~solverBrownianDynamics() {}

	//
	// Requirements for the solver.
	//
	virtual bool needVelocityVector() const noexcept override { return false; }

	//
	// Iteration.
	//
	virtual void updateVectors( double dt, int Ndof, double *X, const double *F ) override;		///< Updates positions given forces.
	
	//
	// I/O.
	//
	virtual std::string tableRowLabel() const noexcept override { return "BD"; }
};


#endif

