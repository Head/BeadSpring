/**
 * @file dataBeadsBase.h
 * @author David Head
 * @brief Auxiliary class for beadsBase that performs the various data gathering routines.
 */


#ifndef BEAD_SPRING_DATA_BEADS_BASE_H
#define BEAD_SPRING_DATA_BEADS_BASE_H


//
// Includes.
//

// Standard includes.
#include <cmath>
#include <vector>

// Project includes.
#include "dataBeadSpring.h"
#include "boxBase.h"
#include "logging.h"


//
// Prototypes.
//
class beadsBase;


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
	const std::string databeads_grYesNoAttr = "pairCorrnFn";
}

/**
 * @brief Auxiliary class for beadsBase that performs the various data gathering routines.
 * 
 * Has read-only access to the `bead` and `box` modules, the former as a `friend`.
 */
class dataBeadsBase
{
private:

	constexpr static auto __dataLabel_tgtCorrn      = "tgtCorrn";
	constexpr static auto __dataLabel_beadPairCorrn = "bead_gr";
	constexpr static auto __dataLabel_boxCounting   = "log2BoxCount";

	constexpr static auto __dataLabel_diagNumForceLimits = "diagNumForceLimits";

protected:

	/// The dimension.
	short _dim;

	// The pointers to the beads object and the box.
	const beadsBase *_beads;
	const boxBase   *_box  ;
	
	/// The data object for the whole project.
	std::shared_ptr<dataBeadSpring> _dataBeadSpring;

	/// Whether or not to calculate expensive quantities.
	bool _gr_yesNo;

	//
	// Utility methods.
	//
	inline double _dot( const double *x, const double *y ) const noexcept { return ( x[0]*y[0] + x[1]*y[1] + (_dim==3?x[2]*y[2]:0.0) ); }
	inline double _dot( const double *x                  ) const noexcept { return ( x[0]*x[0] + x[1]*x[1] + (_dim==3?x[2]*x[2]:0.0) ); }

	//
	// Block quantities.
	//
	virtual void _blockTangentCorrn() const;
	virtual void _blockBeadPairDist() const;
	virtual void _blockBoxCounting () const;
		
public:

	dataBeadsBase( const beadsBase *beads, std::shared_ptr<dataBeadSpring> data, short dim, const parameterParser *params );
	virtual ~dataBeadsBase() {}
	
	// Regular data gathering.
	virtual void calculateBlock() const;			///< Calculate all (requested) block data quantities.
	
	// Methods for specific quantities.
	virtual void numForceLimits( int num ) const { _dataBeadSpring->addScalar(__dataLabel_diagNumForceLimits,num); }
	
	// Interface for the underlying data object. All thread safe.
	inline bool calculateStressNow    () const noexcept { return _dataBeadSpring->calculateStressNow(); }
	inline void addToStressTensor_fr_r( double f_over_r, const double *r, double weight=1.0 )
		{ _dataBeadSpring->addToStressTensor_fr_r(f_over_r,r,weight); }
	inline void addToStressTensor_f_r ( const double *f, const double *r, double weight=1.0 )
		{ _dataBeadSpring->addToStressTensor_f_r(f,r,weight); }
};


#endif
