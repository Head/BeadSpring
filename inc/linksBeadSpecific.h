/**
 * @file linksBeadSpecific.h
 * @author David Head
 * @brief Generalisation of linksBase:: to have link parameters that depend on bead locations along the filament(s).
 */


//
// Includes.
//

// Standard includes.
#include <iostream>
#include <string>
#include <algorithm>
#include <utility>
#include <cctype>               // For std::isdigit().

// Project includes.
#include "linksBase.h"          // Parent class.


///
/// Namespace for parameter labels.
///
namespace parameterLabels
{
    const std::string links_typeBeadSpecific = "beadSpecific";

    const std::string linksBeadSpecific_numLinkTypes = "numLinkTypes";
    const std::string linksBeadSpecific_attachBetween = "attachBetween";
}


/**
 * @brief Extended links class for which link properties can be bead-dependent.
 * 
 * Generalisation of the linksBase class to allow link parameters to depend on the beads that are, or will be,
 * crosslinked. May be extended in future to allow greater opportunities for generalisation.
 * 
 * To use, the parameters file should have the following:
 * ```
 *  <Parameter linksType="beadSpecific" />               // So BeadSpring constructs an instance of this class.
 *  <Parameter linksBeadSpecific_numLinkTypes="2" />     // Number of link types; can be 1 but must be positive.
 * ```
 * 
 * Specific values for each type can then be set as *e.g.*
 * ```
 *  <Parameter kA_0="..." />                                // Attach rate kA for the first link index.
 * ```
 * Non-bead specific values (*e.g.*, `KA=...`) will be used as the default if not defined.
 * 
 * So far, not all parameters have bead-specific values possible. If one is attempted, an exception
 * will be thrown and the code will need to be modified, probably along the lines of the changes already made
 * (the testSuite for this class could also be updated to check the new variations allowed).
 * 
 * Ranges of allowed bead indices can also be specified for each link type as *e.g.*
 * ```
 *  <Parameter attachBetween_0="1:0,2" />                  // Links bead index 1 to either 0 or 2.
 * ```
 * where the range format is a list of indices for each end of the crosslink, separated by a colon.
 * No range means any bead index for that end of the crosslink.
 *  
 * Note indexing starts at zero, and indices outside of the specified range will simply be ignored.
 * You can check the log file to see what was actually parsed.
 */
class linksBeadSpecific : public linksBase
{

private:

	constexpr static auto __saveTypesTag = "linkTypes";     ///< XML tag for link types.
    
protected:

    int _nLinkTypes;                                        ///< Number of link types; must be positive.

    std::vector<double> _linkAttachRates;                   ///< Bead-specific link attach rates.

    /// Stores bead indices for which attachment is possible, for each link type.
    std::vector< std::pair<std::vector<int>,std::vector<int>> > _linkAttachBeads;

    /// Augment parent-class data by storing an index for each link as a (per-thread) map.
    std::vector< std::map<indexPair,int> > _perThread_linkTypes;

    /// Augment parent class by storing an index for each link to be added. indexPair will be the same order as the link.
    std::vector< std::map<indexPair,int> > _perThread_addLinkTypes;

    /// The actual (attempted) addition of both link and link type to the persistent containers.
	virtual int _serial_attemptAddLink( indexPair ij ) override;
	
    /**
     * @brief Parses the string specifying bead ranges for attachment.
     * 
     * Parses the string (assumed to be specified in the parameters file) and returns a pair of
     * index lists for each end of the potential crosslink. Will check for validity during parsing
     * and throw an exception if anything invalid is found. The expected string format is
     * 
     * `[list1]:[list2]`
     * 
     * where each list is either empty (meaning any beads), or a list of bead indices separated
     * by commas or whitespace. Currently there ar no options to specify ranges within each list,
     * but this could be added ad a future date.
     * 
     * Note the parsing of indices is not very sophisticated or robust, so it is recommended to check
     * the outpout of the log file the first time a new set of indices are selected, as this will echo
     * how the parameter string was actually parsed.
     * 
     * @param arg1 std::string taken from the parameters file.
     * @return std::pair of std::vector<ints> denoting the bead index ranges.
     */
    std::pair<std::vector<int>,std::vector<int>> _parseBeadRangeString( std::string ) const;

    /// Attempts to add to the per-thread attempted new link lists, given bead-specific properties.
    virtual void _perThread_attachLinks( unsigned int thread, double dt );

   /**
     * @brief Removes links based on detachment rules, including exceeding some maximum length.
     * 
     * Adds links to be removed to per-thread lists declared in the parent class, so that the global
     * arrays such as the number of links per bead can be safely updated in a (quick) serial loop later on.
     * 
     * For this class, this method also removes the link types from the per-thread lists.
     * It does this in this method itself - it does not add to any list to be deleted later,
     * as there is currently no reason. This is different to attachment, where some checks still
     * need to be makde before a link is added - for detachment, any link flagged as detaching
     * in this method will definitely detach.
     * 
     * @param arg1 Thread number, normally omp_get_thread_num().
     * @param arg2 Time step; needed to convert detachment rates to detachment probabilities.
     * 
     */
	virtual void _perThread_detachLinks( unsigned int threadNum, double dt );

	//
	// I/O.
	//
	virtual void _saveLinkData   ( flatXMLParser *xml ) const override;
	virtual void _recoverLinkData( flatXMLParser *xml ) override;

public:

    // Constructors / destructor.
    linksBeadSpecific() {}
    linksBeadSpecific( short dim, const parameterParser *params, const boxBase *box, const beadsBase *beads,
                            std::shared_ptr<dataBeadSpring> data );
    virtual ~linksBeadSpecific() {}

	//
	// Debugging.
	//
	virtual void debugCheckBadLinks() const;        ///< Adds bead-specific checks to those conducted by the parent class.
};

 