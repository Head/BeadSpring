#
# Tests that the tangent correlations for a single filament decay exponentially as per the input \ell_{p}.
#

# Standard imports
import math
import scipy.optimize

# Base class
from bsTests import bsTestBase

class testTangentCorrelations( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname, tolerance ):

		# Store private variables
		self.__paramFile = fname
		self.__tolerance = tolerance

		# Call parent method with required flags.
		super(testTangentCorrelations,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Sanity check.
		if self._blockData.getParameter("dimension") != 3:
			raise NotImplementedError( "Tangent correlation test currently only supports d=3" )

		if self._blockData.getParameter("solverType") != "BrownianDynamics":
			raise NotImplementedError( "Tangent correlation test currently only supports Brownian dynamics" )

		if self._blockData.getParameter("N") != 1:
			raise NotImplementedError( "Tangent correlation test assumes exactly one filament" )

		if self._blockData.getParameter("M") <= 2:
			raise NotImplementedError( "Tangent correlation test requires more than two beads on the filament" )

		# Get the required parameters. Get see all by printing self._table.allParameters().
		M     = self._blockData.getParameter( "M"     )
		b     = self._blockData.getParameter( "b"     )
		kappa = self._blockData.getParameter( "kappa" )
		kT    = self._blockData.getParameter( "kT"    )

		ell_p = kappa / kT
		tol   = self.__tolerance

		# Get the final tangent correlation function. Length measured in terms of bead separation b.
		dimless_s, corrn  = [ self._blockData.getDataSets("tgtCorrn")["curves"][-1][axis] for axis in ("x","y") ]
		s = [ _s*b for _s in dimless_s ]

		# Fit to an exponential with a prefactor b; should really be 1.0 but appears to have a kink near the ends, posisbly because
		# these are calculated using left/right (rather than central) differences. Multiple by parameter rather than divide, so a should be 1/ell_p.
		def fn( x, a, b ):
			return [ b * math.exp(-_x*a) for _x in x ]

		# Skip first point as appears to be susceptible to some sort of discretisation error.
		try:
			fit = scipy.optimize.curve_fit( fn, s[1:], corrn[1:], p0=[1.0/ell_p,1.0] )
		except:
			raise bsTestBase.FailedTestError( "Failed to fit the tangent correlation function to an exponential (presuming not close to exponential)" )

		mean, error = fit[0][0], math.sqrt(fit[1][0][0])
		if self._verbose:
			print( "Fit to exponential was {0} ({1}), and 1/ell_p={2}.".format(mean,error,1.0/ell_p) )
			print( "(the end-correction gave a prefactor {0} ({1}) rather than 1.0).".format(fit[0][1],math.sqrt(fit[1][1][1])) )

		# Plotting (optional).
		if self._plot:
			import matplotlib.pyplot as plt

			b = fit[0][1]

			plt.plot( s, corrn, "-or", label=r"${\rm Data}$" )

			plt.plot( s, [ b*math.exp(-_s*mean) for _s in s ], lw=2, color="r", label="Short range fit +/- {}%".format(100*tol) )
			plt.fill_between( s, [ b*math.exp(-_s*((1+tol)*mean)) for _s in s], [b*math.exp(-_s*((1-tol)*mean)) for _s in s], color="r", alpha=0.25 )

			plt.plot( s, [ math.exp(-_s/ell_p) for _s in s ], lw=2, color="k", label=r"${\rm Prediction}$" )

			plt.xlabel( r"s" )
			plt.ylabel( r"$\langle t(0)\cdot t(s) \rangle$" )
			plt.semilogy()
			plt.legend()

			plt.show()

		# Rather than use the error (which can be very small), set a fracion of the mean value.
		sigma = abs( mean - 1.0/ell_p ) * ell_p
		if sigma > tol:
			raise bsTestBase.FailedTestError( "Fitted ell_p more than {}% from prediction".format(100*tol) )
		else:
			if self._verbose:
				print( "Within {}% of prediction; accepted.".format(100*tol) )


	# Arguments for the executable
	def _parameterFile( self ): return self.__paramFile

	# Cleanup; can use wildcards
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]




	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		tolerance = 0.15

		for testNum in range(2):
			fname = "params_test%i.xml" %testNum
			print( "Performing tangent correlation test starting from file '{}'".format(fname) )
			testTangentCorrelations(executable,verbosity,plot,fname,tolerance)
			print( " - passed" )
