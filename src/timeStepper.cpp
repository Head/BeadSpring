//
// timeStepper class
//
// Handles an incrementing time step with an unlimited number of events that
// occur at regular intervals, like a clock (which used to be this class's name)
//
// Typical usage:
//
//	timeStepper c(T);			// Initialise clock for total run time T
//	c.add( "image", max_T/10.0 );
//
//  Then:
//
//  c.flag("image") returns 'true' once every 10% of the run (once checked, the flag is reset)
//   - always returns false if the requested flag label does not exist.
//
//  c.events("image") returns the number of times this event has been activated
//   - will raise exception if the requested flag label does not exist
//
//  c.end() returns true when the entire run is over
//
//
// Options:
// -------
// c.add( "name", Dt, true );			// Will set the initial flag to 'true', so it will be called immediately
// c.addNum( "name", num_calls );		// Intervals measured as fraction of total time; call at end assured
//


#include "timeStepper.h"


// Add a timeStepper to the list; check validity of the interval.
void timeStepper::addEvent( std::string label, double interval, bool immediately = false )
{
	// Must have positive intervals
	if( interval <= 0 ) {
		std::ostringstream what;
		what << "timeStepper ERROR: Intervals must be positive; the attempted value '" << interval
			 << "' for the label '" << label << "' is not allowed (in timeSteppers.cpp)" << std::endl;
		throw std::invalid_argument( what.str() );
	}

	// Check number does not exceed allowed integer range on this system
	if( interval/_dt > LONG_MAX )
		throw std::overflow_error( "Requested interval exceeds maximum allowed (integer) value [in timeStepper::Add_Event()]" );
	
	// Convert real interval to integer value; rounds up (minimum of 1 since interval==0 trapped above)
	long num_counts = long( ceil( interval/_dt ) );
	
	// Add event
	_new_event( label, num_counts, immediately );
}


// Add a timeStepper to the list for a given number of intervals
void timeStepper::addNumEvent( std::string label, long num_ints, bool immediately = false )
{
	// Must have positive number of intervals
	if( num_ints <= 0 ) {
		std::ostringstream what;
		what << "timeStepper ERROR: Must have a positive number of intervals; the attempted number '" << num_ints
			 << "' for the label '" << label << "' is not allowed (in timeSteppers.cpp)" << std::endl;
		throw std::invalid_argument( what.str() );
	}

	long num_counts = _intervals[_run_label()] / num_ints;

	_new_event( label, num_counts, immediately );
}


// Actually adds the new event, be it specified by interval or fraction of total
void timeStepper::_new_event( std::string label, long num_counts, bool immediately )
{
	_intervals[label] = num_counts;				// Update interval - fixed interval (unless reset)
	_next[label]      = _count + num_counts;	// Next call
	_flags[label]     = immediately;			// Initial state can be true
	_num[label]       = 0;						// No. of times this event has been activated
	_tot_num[label]   = 0;						// Only used when called with an integer no. of intervals
}



// Return with the current flag status; reset to false. Raises an exception if the label was
// not found, with a meaningful message sent to std::err.
bool timeStepper::flag( std::string label ) {

	// See if the label exists; if not, return false
	if( !_flags.count(label) ) return false;

	// Return current flag status, and set to false (to signify it has now been read).
	if( _flags[label] ) { _flags[label] = false; return true; }
	return false;
}

// Same as flag() but does not alter/reset the value.
bool timeStepper::peekFlag( std::string label ) const
{
	if( !_flags.count(label) ) return false;

	return _flags.at(label);
}

// Clear all flags
void timeStepper::clearAllFlags()
{
	for( std::map<std::string,long>::iterator iter=_next.begin(); iter!= _next.end(); iter++ )
		_flags[iter->first] = false;
}

// Set the no. of events for the passed label
void timeStepper::setEvent( std::string label, long eventNum )
{
	// See if the label exists; if not, raise an exception
	if( !_num.count(label) ) {
		std::ostringstream what;
		what << "timeStepper ERROR: Attempt to access an unspecified event '" << label << "' [in timeStepper::Set_Event()]" << std::endl;
		throw std::invalid_argument( what.str() );
	}
	
	_num[label] = eventNum;
}


// Sets the current time; also updates no. of events and flag as if the time had been incremented to this 
void timeStepper::jumpForwardsTo( double new_t )
{
	// Check we really are going forwards
	if( new_t < t() )
		throw std::runtime_error( "timeStepper error: Attempted to jump 'forwards' to an earlier time [in timeStepper::Jump_Forwards_To()]" );
	
	// Set the new time; check not jumping past the final end-time
	setTime( new_t );
	if( _count > _next[_run_label()] )
		throw std::runtime_error( "timeStepper error: Attempted to jump past the end time [in timeStepper::Jump_Forwards_To()]" );

	// Set the next interval and update the event count as if the new time had been reached normally
	for( std::map<std::string,long>::iterator iter=_next.begin(); iter!= _next.end(); iter++ )
	{
		while( iter->second <= _count ) 
		{
			_num[iter->first]++;								// Increment the event count
			iter->second += _intervals[iter->first];			// Next flag interval
		}
	}
}
	

// Status of the main run timer. If any of the timers were called by a required integer number of events,
// will not return 'true' until all of these have been cleared for the last time. This helps avoid missed
// events due to numerical precision.
bool timeStepper::end() {
	if( _flags[_run_label()] )
	{
		// Check to see if any timers have yet to output their final interval, and their flag has not been reset to false
		bool all_done = true;
		for( std::map<std::string,long>::iterator iter=_next.begin(); iter!= _next.end(); iter++ )
			if( _tot_num[iter->first] and _num[iter->first] < _tot_num[iter->first] and _flags[iter->first] )
				all_done = false;

		return all_done;
	}
	return false;
}

// Returns 'true' if the even exists.
bool timeStepper::eventDefined( std::string label ) const
{
	return ( _num.find(label) != _num.end() );
}

// Return with the number of times this event has been activated
long timeStepper::events( std::string label ) const
{
	// See if the label exists; if not, raise an exception
	if( !_num.count(label) ) {
		std::ostringstream what;
		what << "timeStepper ERROR: Attempt to access an unspecified event '" << label << "' [in timeStepper::Events()]" << std::endl;
		throw std::invalid_argument( what.str() );
	};

	return _num.at(label);
}

// Returns with the integer interval for the given label
long timeStepper::integerInterval( std::string label ) const
{
	if( !_num.count(label) ) {
		std::ostringstream what;
		what << "timeStepper ERROR: Attempt to access an unspecified event '" << label << "' [in timeStepper::Integer_Interval()]" << std::endl;
		throw std::invalid_argument( what.str() );
	}

	return _intervals.at(label);
}

// Increment the clock; check all counters and set all necessary flags to 'true'
void timeStepper::step()
{
	_count++;
	_started = true;

	for( std::map<std::string,long>::iterator iter=_next.begin(); iter!= _next.end(); iter++ )
		if( !(_count%iter->second) )
		{
			_next[iter->first] += _intervals[iter->first];
			
			// Set flag to 'true' and increase the activation count.
			_flags[iter->first] = true;
			_num[iter->first]++;
		}
}


// Definition of overloaded '<<' operator acting on an ostream; lists current time
// and all defined counters, and their states
std::ostream& operator<< ( std::ostream &os, timeStepper &t )
{
	os << "Current time: " << t() << std::endl;

	if( t._intervals.empty() )
		os << " - no events defined" << std::endl;
	else {
		os << std::endl << "timeSteppers: Label / Interval / Next / Status / Events" << std::endl;
		for( std::map<std::string,long>::iterator iter = t._intervals.begin(); iter != t._intervals.end(); iter++ ) {
			os  << "'" << iter->first << "'"
				<< ", " << iter->second
				<< ", " << t._next[iter->first]
				<< ", " << ( t._flags[iter->first] ? "on" : "off" )
				<< ". " << t._num[iter->first]
				<< std::endl;
		}
	}

	return os;
}




