#include "boxRectPeriodic.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

boxRectPeriodic::boxRectPeriodic( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data )
	: boxBase(dim,params,data)
{
	using namespace parameterLabels;

	// Only supports 2D and 3D.
	if( _dim!=2 && _dim!=3 )
		errors::throwException( "Bad dimension in boxRectangularPerBC: Only 2D and 3D supported",__FILE__,__LINE__);

	_X.resize( _dim );

	// Need X, Y and (if 3D) Z dimensions.
	if( !_params->hasScalarParam(boxRectPerBC_X) ) errors::throwException( "Box X-dimension not specified",__FILE__,__LINE__);
	if( !_params->hasScalarParam(boxRectPerBC_Y) ) errors::throwException( "Box Y-dimension not specified",__FILE__,__LINE__);

	_X[0] = _params->scalarParam(boxRectPerBC_X);
	_X[1] = _params->scalarParam(boxRectPerBC_Y);

	if( _dim==3 )
	{
		if( !_params->hasScalarParam(boxRectPerBC_Z) ) errors::throwException( "Box Z-dimension not specified and dim==3",__FILE__,__LINE__);
		_X[2] = _params->scalarParam(boxRectPerBC_Z);
	}

	//
	// Inform logfile.
	//
	logging::logFile << "Construction of boxRectangularPerBC with dimensions " << "(" << _X[0] << "," << _X[1];
	if( _dim==3 ) logging::logFile << "," << _X[2];
	logging::logFile << ")." << std::endl;
}


#pragma mark -
#pragma mark Initialisation
#pragma mark -

void boxRectPeriodic::initialiseCells( double maxRange )
{
	if( maxRange<=0.0 )
		errors::throwException( "Cannot set cell/neighbour lists for non-positive interaction range", __FILE__, __LINE__ );

	//
	// Initialise the cell sort-only objects if requested.
	//
	if( !_useNeighbourLists )
	{
		// Re-initialising cell sorters is easier than NN lists (no 'N'), but may still require a think.
		if( _cellSort2D!=nullptr || _cellSort3D!=nullptr )
			errors::throwException( "Have not checked changes of cell sorters after initialisation", __FILE__, __LINE__ );

		if( _dim==2 )
		{
			_cellSort2D = std::unique_ptr< threadedCellSort<2> > (
				new threadedCellSort<2>(maxRange,_X[0],_X[1])
			);
			logging::logFile << *_cellSort2D;
		}
		else
		{
			_cellSort3D = std::unique_ptr< threadedCellSort<3> > (
				new threadedCellSort<3>(maxRange,_X[0],_X[1],_X[2])
			);
			logging::logFile << *_cellSort3D;
		}

		// Call the parent method and finish.
		boxBase::initialiseCells( maxRange );
		return;
	}

	//
	// If still here, must be using NN/Verlet lists.
	//
	
	// First get NN list parameters handled by the base class.
	int    NNFreq    = _NNSortFrequency;
	double R         = _NNRangeFactor * maxRange;

	if( _dim==2 )
	{
		// Could add option to change range (i.e. destroy old pointers and re-create; cf. Biofilm).
		if( _neighbourList2D!=nullptr )
			errors::throwException( "Cannot change neighbour lists after initialisation (yet)", __FILE__, __LINE__ );

		_neighbourList2D = std::unique_ptr< threadedNeighbourList<2> > (
			new threadedNeighbourList<2>(R,NNFreq,_X[0],_X[1])
		);

		logging::logFile << *_neighbourList2D;
	}

	if( _dim==3 )
	{
		// Could add option to change range (i.e. destroy old pointers and re-create; cf. Biofilm).
		if( _neighbourList3D!=nullptr )
			errors::throwException( "Cannot change neighbour lists after initialisation (yet)", __FILE__, __LINE__ );

		_neighbourList3D = std::unique_ptr< threadedNeighbourList<3> > (
			new threadedNeighbourList<3>(R,NNFreq,_X[0],_X[1],_X[2])
		);

		logging::logFile << *_neighbourList3D;
	}

	// Call the parent method.
	boxBase::initialiseCells( maxRange );
}


void boxRectPeriodic::geometryTriLattice( int N, double &a, int &numX, int &numY ) const
{
	// Sanity check.
	if( _dim!=2 ) errors::throwException( "Triangular lattice only possible for 2D boxes", __FILE__, __LINE__ );
	
	// Provisional lattice spacing. Get this expression from calculating error assuming triangular lattice and N=n_{x}n_{y} nodes.
	a = std::sqrt( volume() / (N*__root3over2) );

	if( std::remainder(_X[0],a) > __ICTriLatticeTol )
		errors::throwException( "Box x-dimension not commensurate with calculated lattice spacing of " + std::to_string(a), __FILE__, __LINE__ );

	if( std::remainder(_X[1],__root3over2*a) > __ICTriLatticeTol )
		errors::throwException( "Box y-dimension not commensurate with calculated lattice spacing of " + std::to_string(a), __FILE__, __LINE__ );

	// Get numbers of nodes in each direction. Assume both even (otherwise periodic BCs tricky/not possible).
	numX = static_cast<int>( std::round(_X[0]/              a ) );
	numY = static_cast<int>( std::round(_X[1]/(__root3over2*a)) );
	
	if( numX%1 || numY%1 )
		errors::throwException( "Need even number of lattice nodes in each direction", __FILE__, __LINE__ );
}



#pragma mark -
#pragma mark Geometry
#pragma mark -

// Random position somewhere in the box. Use parent method for orientation (default is fine).
void boxRectPeriodic::randomPosition( double *x ) const noexcept
{
	x[0] = pRNG::stdUniform(0) * _X[0];
	x[1] = pRNG::stdUniform(0) * _X[1];
	if( _dim==3 )
		x[2] = pRNG::stdUniform(0) * _X[2];
}



#pragma mark -
#pragma mark Wrapping methods
#pragma mark -

// Wraps x[d] in-place to origin-centred cell.
void boxRectPeriodic::wrap( double *x ) const noexcept
{
	for( auto k=0; k<_dim; k++ ) x[k] = remainder(x[k],_X[k]);
}

// Calculates (but does not apply) the offset required to get the passed point into the primary cell,
// which can then be added repeatedly to e.g. all beads on a filament (the original purpose).
void boxRectPeriodic::offsetToPrimaryCell( const double *x, double *offset ) const noexcept
{
	for( auto k=0; k<_dim; k++ ) offset[k] = - _X[k] * floor(x[k]/_X[k]);
}

// Returns the normalised (i.e. in units of edge length) periodic shift for the given
// relative coordinate along the same direction, i.e. a relPosn of X would return 1,
// whereas a relPosn of -X would return -1 for axis==0.
int boxRectPeriodic::nmlsdPerShiftInDirn( int axis, double relPosn ) const
{
	// Sanity check for the dimension.
	if( axis<0 || axis>=_dim )
		errors::throwException( "Bad axis number; outside valid range given dimension", __FILE__, __LINE__ );

	// Start from 0 and shift until in a range within half a box edge length of zero.
	int shift = 0;

	while( relPosn >  _X[axis]/2 ) { shift++; relPosn -= _X[axis]; }
	while( relPosn < -_X[axis]/2 ) { shift--; relPosn += _X[axis]; }

	return shift;
}
