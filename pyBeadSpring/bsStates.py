#
# Extracts and parses either a single state, or a sequence of states (i.e. a run), as output
# by the Bead-Spring code.
#
# Typical usage:
# ==============
#
# Create from supported file type, single state file or archive (will warn if not possible):
# obj = bsStates( "saved_state.xml" )
#
#
#

#
# Imports.
#

# Standard imports.
import tarfile
import os
import re
import sys

# Local imports.
import analyseData


class bsStates:

	def __init__( self, fname, finalArchiveStateOnly=False, verbose=True ):
		"""Tries to create an instance of bsStates from the given filename, which could be a single state
		or an archive of states. For exctracting from archive files, can specify to only extract the final
		state as determined by the 'getnames()' method of the tarfile object; defaults to False."""

		#
		# Store the filename and initialise variables.
		#
		self._verbose = verbose

		self._fname  = fname
		self._states = []
		self._params = None							# Extracted from the first parameters block encountered.

		self._dim = None		
		self._X   = None
		self._Y   = None
		self._Z   = None

		self._clusters = None						# Filled only when getClusters() called.

		#
		# Extract the state(s) and return.
		#
		
		# Currently only one archive format is understood. Anything else must be a single state in XML format.
		if not fname.endswith(".tar.gz") and not fname.endswith(".xml"):
			raise TypeError( "only .tar.gz and .xml states files are currently understood by bsStates method" )
		
		# If it looks like part of a succession of states in the usual convention, try to extract them all.
		if re.match( r"state_\d\d\d\d.xml", os.path.basename(fname) ):
		
			# Find the last first index (do not assume it is 0, although it usually will be).
			stateIndex = int( fname[ fname.rfind("_")+1 : fname.rfind(".") ] )
			while os.path.isfile( "state_{:04d}.xml".format(stateIndex-1) ):
				stateIndex -= 1

			if self._verbose:
				print( "Loading state files starting from index {}.".format(stateIndex) )
			
			# Now extract them all
			while os.path.isfile( os.path.join(os.path.dirname(fname),"state_{:04d}.xml".format(stateIndex)) ):
				self._addSingleState( os.path.join(os.path.dirname(fname),"state_{:04d}.xml".format(stateIndex)) )
				stateIndex += 1
			
			return
		
		# If looks like a single state file and not part of a succession of states, load just the one file.
		if fname.endswith(".xml"):
			self._addSingleState(fname)
			return
			
		# If an archive, extract state-by-state and add each.
		if fname.endswith(".tar.gz"):
			archive = tarfile.open( fname )
			for file in archive.getmembers():				# Path name (local) extracted as 'file.path'

				# If requested, skip all but the last file.
				if finalArchiveStateOnly and file.name != archive.getnames()[-1]:
					continue

				# Add the state to the object.
				self._addSingleState( archive.extractfile(file) )

			return


	def _addSingleState( self, file ):
		"""Attempts to parse a single state file as an XML tree. If successful, adds to the end of the list of stored states."""
		try:
			tree = analyseData.XMLTreeParser.parseFileAsTree( file )
		except IOError as err:
			print( "Was not able to parse file as an XML tree; error was: {}".format(err) )
			return

		# Sanity check: Must look like a BeadSpring file.
		if tree.baseTag() != bsStates.mainTag():
			print( "File '{}' does not appear to be a biofilm state (wrong base node tag)".format(file) )
			return						
		
		# Basic quantities from the state attributes.
		time = tree.nodeAtIndex([]).attributes[bsStates.timeAttr()]

		#
		# Extract from state file. Assume was output correctly to avoid repetition between the C++11 and Python code.
		#

		# Get the parameters as a subtree if not already specified. Assume only one parameters block, hence the '[0]'.
		if self._params == None:
			self._params = {}
			paramTree = tree.newSubTreeStartingAtNode( tree.nodesWithTag(bsStates.parametersTag())[0] )
	
			for node in paramTree.nodesWithTag(bsStates.parameterTag()):
				attrs = ( bsStates.paramNameAttr(),bsStates.paramTypeAttr(),bsStates.paramValueAttr() )
				name, type, value = [ node.attributes[attr] for attr in attrs ]
				
				if type=="int"   : self._params[name] = int  (value)
				if type=="scalar": self._params[name] = float(value)
				if type=="label" : self._params[name] =       value
				if type=="flag"	 : self._params[name] = bool (value)

		# Extract some special values.
		self._dim = self._params["dimension"]
		self._X   = self._params["X"]
		self._Y   = self._params["Y"]
		if self._dim==3:
			self._Z = self._params["Z"]
		
		#
		# Construct a state dictionary with fields for time, beads, cross-linkers, and anything else that can be saved.
		#
		state = { "time":time }
		
		# Need first-bead indices and positions from the beads block. Assume one such block, hence the '[0]'.
		beadsTree = tree.newSubTreeStartingAtNode( tree.nodesWithTag(bsStates.beadsTag())[0] )
		
		firstBeadsNode     = beadsTree.nodesWithTag(bsStates.firstBeadsTag())[0]
		state["numFilaments"] = firstBeadsNode.attributes[bsStates.numFilamentsAttr()]		
		state["firstBeads"  ] = [ int(s) for s in firstBeadsNode.dataString.split() ]

		positionsNode = beadsTree.nodesWithTag(bsStates.positionsTag())[0]
		state["numBeads" ] = positionsNode.attributes[bsStates.numBeadsAttr()]
		state["positions"] = [ float(s) for s in positionsNode.dataString.split() ]

		# Crosslinks. Need to know the link class as this affects how the indices were output. Defaults to 'basic'.
		try:
			linkClassStr = self._params["linksType"]		# Currently one of ['basic','beadSpecific']
		except KeyError:
			linkClassStr = "basic"

		linksTree = tree.newSubTreeStartingAtNode( tree.nodesWithTag(bsStates.linksTag())[0] );
		
		indicesNode = linksTree.nodesWithTag(bsStates.indicesTag())[0]
		state["numLinks"] = indicesNode.attributes[bsStates.numLinksAttr()]

		if linkClassStr == "beadSpecific":
			vals = [ int(valStr) for valStr in indicesNode.dataString.split() ]			# Triplets (node1,node2,type)

			state["indices"] = []

			for l in range( state["numLinks"] ):
				state["indices"].extend( [vals[3*l],vals[3*l+1]] )

		else:		# Currently can only be 'basic', corresponding to linksBase::
			state["indices"] = [ int(s) for s in indicesNode.dataString.split() ]		# Pairs (node1.node2)

		# Box.
		boxTree = tree.newSubTreeStartingAtNode( tree.nodesWithTag(bsStates.boxTag())[0] )
		try:
			LEShearNode = boxTree.nodesWithTag( bsStates.currentLEShearTag() )[0]
			state["LEShear"] = LEShearNode.attributes[bsStates.LEShearAttr()]
		except IndexError:
			pass
		
		#
		# Sanity check for the parsed data.
		#
		if state["numBeads"] * self._dim != len(state["positions"]):
			raise SystemError( "Length of positions vector does not match expected value" )
	
		if state["numLinks"] * 2 != len(state["indices"]):
			raise SystemError( "Length of link indices vector does not match the expected value" )
		
		#
		# Add to the current list of states.
		#
		self._states.append( state )

		return None

	#
	# Accessors.
	#
	
	# Number of states.
	def numStates( self ):
		return( len(self._states) if self._states else 0 )
	def __len__( self ):
		return self.numStates()
	
	# Key geometric quantities.
	def dim( self ):
		return self._dim
	
	def X( self ):
		return self._X

	def Y( self ):
		return self._Y

	def Z( self ):
		return self._Z
	
	def boxDims( self ):
		if self._dim==2:
			return [ self._X, self._Y ]
		if self._dim==3:
			return [ self._X, self._Y, self._X ]
		
	# Returns a single state.
	def getState( self, index ):
		if not self._states or index<0 or index>=len(self._states):
			print( "Cannot return state; bad index {} [in bsStates.state()]".format(index) )
		return self._states[index]

	# Parameters.
	def getParameter( self, name ):
		try:
			return self._params[name]
		except KeyError:
			print( "Parameter {} not part of specification.".format(name) )
	
	def allParameters( self ):
		return self._params

	# Filament and bead indexing.
	def filamentForBead( self, stateIndex, beadIndex ):
		state = self._states[stateIndex]
		
		fil = state["numFilaments"] - 1
		while( state["firstBeads"][fil] > beadIndex ) : fil -= 1

		return fil
	
	def beadAlongFilament( self, stateIndex, beadIndex ):
		fil = self.filamentForBead(stateIndex,beadIndex)
		return beadIndex - self._states[stateIndex]["firstBeads"][fil]

	def longestFilament( self, stateIndex ):
		firstBeads = self._states[stateIndex]["firstBeads"]
		lengths = [ firstBeads[i+1]-firstBeads[i] for i in range(len(firstBeads)-1) ]
		return ( max(lengths) if len(lengths) else self._states[stateIndex]["numBeads"] )
	
	# Special cases.
	def isMonomeric( self, stateIndex ):
		return ( self._states[stateIndex]["numBeads"] == self._states[stateIndex]["numFilaments"] )

	# Calculates the clusters of beads. Stored as self._clusters.
	def getClusters( self, index, noSingletons=False ):
		"""Returns sets of clusters defined by nodes (beads) and edges (links). All beads on the same
		filament are automatically considered part of the same cluster."""

		# Use filament number as index.
		state = self.getState(index)
		nodes = range( state["numFilaments"] )

		edges = []
		for l in range(state["numLinks"]):
			beadIndices = state["indices"][2*l:2*(l+1)]
			edges.append( [ self.filamentForBead(index,b) for b in beadIndices] )

		return analyseData.disjointSets().getDisjointSets(nodes,edges,noSingletons)


	#
	# Output.
	#
	
	# Simplified representation, usually to stdout and meant to be human readable but non-recoverable.
	def __str__( self ):
		msg = "bsStates object with {} state(s) from BeadSpring.".format(self.numStates())
		
		return msg
	
	#
	# Class methods.
	#
	
	# Expected XML tags for the saved state files. Case sensitive.
	@classmethod
	def mainTag( cls ): return "BeadSpring"

	@classmethod
	def parametersTag( cls ): return "Parameters"
	
	@classmethod
	def parameterTag( cls ): return "Parameter"
	
	@classmethod
	def beadsTag( cls ): return "Beads"	
	
	@classmethod
	def firstBeadsTag( cls ): return "FirstBeads"
	
	@classmethod
	def positionsTag( cls ): return "BeadPositions"
	
	@classmethod
	def linksTag( cls ): return "Links"
	
	@classmethod
	def indicesTag( cls ): return "Indices"
	
	@classmethod
	def boxTag( cls ): return "Box"
	
	@classmethod
	def currentLEShearTag( cls ): return "CurrentLEShear"

	# Expected XML attributes for the saved state files. Case sensitive.
	@classmethod
	def timeAttr( cls ): return "time"
	
	@classmethod
	def numFilamentsAttr( cls ): return "numFilaments"
	
	@classmethod
	def numBeadsAttr( cls ): return "numBeads"
	
	@classmethod
	def paramNameAttr( cls ): return "name"

	@classmethod
	def paramTypeAttr( cls ): return "type"

	@classmethod
	def paramValueAttr( cls ): return "value"
	
	@classmethod
	def numLinksAttr( cls ): return "numLinks"
	
	@classmethod
	def LEShearAttr( cls ): return "strain"
	
	
#
# If called from the command line. Would normally invoke the bsRuns class, but invoking bsStates can
# be useful for debugging.
#
if __name__ == "__main__":

	import argparse

	parser = argparse.ArgumentParser( description="Parses a single run of Bead-Spring states and uses to initialise the viewer." )
	parser.add_argument( "filename", help="archive or a single state file" )
	args = parser.parse_args()
	
	obj = bsStates( args.filename )
	
	print( obj )

	# If visualisation required.	
	import viewer.bsViewer
	viewer.bsViewer( obj )
	
	

