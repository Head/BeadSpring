/*
 *  histogram.h
 *
 * Template, so no .cpp file - all the implementation is included here
 * (GNU c++ compiler cannot currently handle template .cpp files
 * very elegantly - you need to provide each instance in .cpp. Just using
 * the header file means compilation time may be slow - shouldn't be a
 * problem for most scientific applications).
 *
 *  Created by David Head on Tue Sep 14 2004.
 *
 *
 
 USAGE
 ----------
 
 INITIALISE:	histogram<dim,num> hist( scale );
	- dim : dimensionality of space
	- num : dimensionality of vector
	- scale : size of bins, vector or passed explicitly (for low dims)
	- will warn (to stderr) if the (small) amount of memory allocation fails.
 
 ADD DATA:  newPoint( dim_vec, num_vec );
	- adds num_vec to closest bin corresponding to dim_vec
	- can also use newPoint( x,y ); when dim=num=1
	- returns FALSE if failed (i.e. out of memory; unsuitable vectors passed; etc.)
 
 ADD HISTOGRAMS:
	- can use operator+=; currently limited to histograms with the same origin and scale
	
 OUTPUT DATA:   ostream << hist << endl;
	- Can do this at any time

 EXPORT DATA:	exportXML(ostream&,label);
 	- Will output as an XML block with the supplied label attribute.

 IMPORT DATA:   recoverXML(istream&,label);
    - Will recover data from passed XML stream, looking for corresponding label; same format as "Export_XML"

 STATISTICS:
 	- maximum(double&,vector<double>&);   Value and argument of maximum (inner product if num>1)
 
 OPTIONAL:
	- setOrigin( dim_vector );		- By default, the origin (which is always in the
									MIDDLE of a bin) is set to the first point passed.
									Can overide by setting origin explicitly using this fn.
									Returns TRUE if successful. Can just pass a
									double if dim=1.
	
	- Clear(clear_origin=false);	- Deletes all bins; optionally resets origin
 */


#ifndef _UD_HISTOGRAM_H
#define _UD_HISTOGRAM_H


// Standard includes
#include <iostream>
#include <map>
#include <math.h>
#include <stdexcept>

// Local includes
#include "bin.h"
#include "indices.h"

// Non-standard includes
#include "flatXMLParser.h"



using namespace std;




template< int dim = 1, int num = 1 >
class histogram
{

public:

	// Constructors / destructor
	histogram( const double s1=1, const double s2=0, const double s3=0 );
	histogram(double*);
	histogram(const histogram&);		// Copy-constructor

	virtual ~histogram();
	
	// Add second histogram to this one
	histogram<dim,num>& operator+= (const histogram<dim,num>&);

	// SET THE ORIGIN
	// General format
	template< typename iteratorType >
	bool setOrigin(const iteratorType,const iteratorType);

	// Explicit versions
	virtual inline bool setOrigin( const double *x ) { return setOrigin( x, x+dim ); }			// Double vector
	virtual inline bool setOrigin( const double &x ) { return setOrigin( &x, &x+1 ); }			// Scalar
	virtual inline bool setOrigin( const double &x, const double &y )							// 2D vector
		{ double X[2] = { x, y }; return setOrigin( X ); }
	virtual inline bool setOrigin( const double &x, const double &y, const double &z )			// 3D vector
		{ double X[3] = { x, y, z }; return setOrigin( X ); }
	
	
	// ADD NEW POINTS
	// General form; compiler will try to select iterType1/2
	template< typename iterType1, typename iterType2 >
	bool newPoint(const iterType1,const iterType1,const iterType2,const iterType2);

	// Single scalar point (num=dim=1)
	virtual inline bool newPoint( const double &x, const double &y ) { return newPoint( &x, &x+1, &y, &y+1 ); }

	// Double vectors
	virtual inline bool newPoint( double *x, double *v ) { return newPoint( x, x+dim, v, v+num ); }

	// OUTPUT

	// ASCII string of (mean, error) pairings, separated by tabs
	template< int d, int n >
	friend ostream& operator<< (ostream&,const histogram<d,n>&);

	// XML block, fully recoverable: (n, sum, sum_sqrd, sum2, sum_sqrd2, ...) , also tab-separated, with XML header/footer
	// Two versions for each, one for a given i/ostream and one for an already-opened flatXMLParser object
	virtual void exportXML(ostream&,std::string);
	virtual void exportXML(flatXMLParser*,std::string);

	virtual void recoverXML(istream&,std::string);				// Recovers from XML-formatted input stream
	virtual void recoverXML(flatXMLParser*,std::string);

	// CLEAR ALL EXISTING DATA; CHECK if any data exists
	virtual void clear( bool clearOrigin=false );
	virtual bool empty() const { return _data.empty(); }		// Returns 'true' if no bins

	// STATISTICS
	virtual void maximum(double&,vector<double>&) const;		// Maximum and axis vector where it is to be found; ignores errors
	virtual void maximum(double&,double&) const;				// As above, but returns the radius of the axis vector
	
protected:

	typedef map< _index<dim>*, bin<dim+num>*, _index_ptr_cmp<dim> > this_map;

	this_map _data;
	double *_scale, *_origin;
	
	virtual void _initialise(double*);   // Called by all constructors

	virtual void _print(ostream&) const;

	template< typename iteratorType >
	bool _newOrigin(const iteratorType,const iteratorType);
	
	// Utility routines
	static inline double _magnitude(vector<double>);		// Magnitude of value or axes part of the pass
	static inline double _radius(vector<double>);			// bin->Vector(); ignores errors

	// Simple vector operations
	static double _dot( vector<double> x, vector<double> y, int d )
		{ double inner = 0.0; for( int i=0; i<d; i++ ) inner += x[i] * y[i]; return inner; }
	
	// Tolerance parameter
	static inline double _tol() { return 1e-7; }
};


// Constructors - can set global scale (for each dim), or each scale separately
template< int dim, int num>
histogram<dim,num>::
histogram( const double s1, const double s2, const double s3 )
{
	double *a = new double[3];		// Max. dimension is 3
	
	// This short-cut currently only supports up to 3 dimensions
	if( dim>3 )
	{
		cerr << "In histogram::histogram(...): Explicit scale initialisation only supprted"
			<< " for dimensions 1,2,3 : must pass pointer to array instead" << endl;
		return;
	}

	// Copy into an array, and pass to _initialise()
	if( a )
	{
		a[0] = s1; 
		if( s2 ) a[1] = s2; else a[1] = a[0];
		if( s3 ) a[2] = s3; else a[2] = a[0];
		_initialise( a );
		delete [] a;
	} else cerr << "MemAlloc err in histogram::histogram(...)" << endl;
}


// Constructors - can set global scale (for each dim), or each scale separately
template< int dim, int num>
histogram<dim,num>::
histogram( double *arr )
{
	// Just pass the array to _initialise()
	_initialise( arr );
}

// Copy constructor
template< int dim, int num >
histogram<dim,num>::histogram( const histogram &h )
{
	// Use map copy constructor for the map
	_data = h._data;
	
	// Initialise _scale and _origin; leave _origin as NULL if h._origin is NULL (possible if copying before first point)
	try
	{
		_scale = new double[dim];
		if( h._origin ) _origin = new double[dim]; else _origin = NULL;
	}
	catch( std::bad_alloc &e )
	{
		std::cerr << "Cannot allocate memory [in Hisotogram<> copy constructor]" << std::endl;
		throw e;
	}

	for( unsigned short i=0; i<dim; i++ )
	{
		_scale[i]  = h._scale[i];
		if( h._origin ) _origin[i] = h._origin[i];
	}
}


// Called by all constructors, which act as a filter to allow nicer input of scales
// by the user (_initialise() only works with passed arrays).
template< int dim, int num>
void histogram<dim,num>::
_initialise( double *arr )
{
	// Open up array for the scale vector
	_scale = new double[dim];
	_origin = NULL;

	if( _scale )
	{
		for( int i=0; i<dim; i++ )
			if( arr[i] ) _scale[i] = arr[i]; else _scale[i] = 1;
	}
	else 
	{
		cerr << "MemAlloc err in histogram::histogram(...)" << endl;
		return;
	}
}


// Destructor - frees up allocated memory
template< int dim, int num >
histogram<dim,num>::
~histogram()
{
	delete [] _origin;
	delete [] _scale;
	
	// Deallocate all of the bins and index objects previously allocated
	typename this_map::const_iterator data_iter;
	for( data_iter = _data.begin(); data_iter != _data.end(); data_iter++ )
		{ delete data_iter->first; delete data_iter->second; }
}


// Add second histogram to this one.
template< int dim, int num >
histogram<dim,num>& histogram<dim,num>::operator+= ( const histogram<dim,num> &rhs )
{
	// Require both origins and scales are the same - could (and probably should) generalise this at some point
	if( _origin==NULL or rhs._origin==NULL )
		throw std::runtime_error( "FATAL in histogram<>::operator+=() : In current version, both histograms need origins" );

	for( int k=0; k<dim; k++ )
		if( _origin[k] != rhs._origin[k] )
			throw std::runtime_error( "FATAL in histogram<>::operator+=() : In current version, both histograms need the same origin" );
	
	for( int k=0; k<dim; k++ )
		if( _scale[k] != rhs._scale[k] )
			throw std::runtime_error( "FATAL in histogram<>::operator+=() : In current version, both histograms need the same scale" );

	// Go through all of the bins for the RHS histogram; either copy over to this, or add to an existing one
	typename this_map::const_iterator rhs_iter = rhs._data.begin();
	for( rhs_iter = rhs._data.begin(); rhs_iter != rhs._data.end(); rhs_iter++ )
	{
		// If returned pointer to end of map, corresponding bin was not found; just insert into this->_data.
		// Otherwise add the data to the pre-existing bin
		typename this_map::iterator lhs_iter = _data.find( rhs_iter->first );
		if( lhs_iter == _data.end() )
		{
			// Create a copy of the data; delete in destructor and Clear()
			_index<dim> *_ords = new (nothrow) _index<dim>(*rhs_iter->first);
			if( !_ords ) throw "cannot allocate memory for _ords in histogram class";

			bin<dim+num> *new_bin = new (nothrow) bin<dim+num>(*rhs_iter->second);
			if( !new_bin ) throw "cannot allocate memory for bin class";
			
			_data.insert( _data.end(), pair<_index<dim>*, bin<dim+num>*>(_ords,new_bin) );
		}
		else
			*lhs_iter->second += *rhs_iter->second;		// Use the bin object's operator+= method
	}
	
	return *this;
}


// Clears arrays and optionally resets (actually deallocs) origin
template< int dim, int num >
void histogram<dim,num>::
clear( bool clear_origin )
{
	// Deallocate memory for origin; set point to NULL
	if( clear_origin )
	{
		delete [] _origin;
		_origin = NULL;
	}

	// Deallocate all of the bins and index objects previously allocated
	typename this_map::const_iterator data_iter;
	for( data_iter = _data.begin(); data_iter != _data.end(); data_iter++ )
		{ delete data_iter->first; delete data_iter->second; }

	_data.clear();
}



// Set the origin, presuming no points have been added yet - otherwise returns false
// Same format as the "add_point" routine. If the origin is not set, will default to (0,0,...)
template< int dim, int num >
template < typename iteratorType >
bool histogram<dim,num>::
setOrigin( const iteratorType first, const iteratorType last )
{
	// Can only set the origin before any points have been added
	// (can set as many times as wanted before this, though)
	if( !_data.empty() ) return false;
	
	// Insert into the origin (checks for consistency here - may still fail)
	return _newOrigin( first, last );
}

// Allocates memory for the origin (if required), sets the points (NO default here),
// assumes that the passed vector has the require length!
template< int dim, int num >
template < typename iteratorType >
bool histogram<dim,num>::
_newOrigin( const iteratorType first, const iteratorType last )
{
	iteratorType iter = first;
	int trial_dim = 0, i;

	// Check the passed array has the right number of elements
	for( ; iter<last; iter++ ) trial_dim++;
	if( trial_dim != dim ) return false;
	
	// Need to allocate memory for the origin vector
	if( !_origin )
	{
		_origin = new (nothrow) double[dim];
		if( !_origin ) return false;
	}

	// Fill the coords with the passed vector
	for( iter=first, i=0 ; iter<last ; iter++, i++ )
		_origin[i] = *iter;
	
	return true;
}


// Add a new point to the data array. Basic error checking; will return false
// if anything went wrong and output to stderr ONCE ONLY
template< int dim, int num >
template<typename iterType1, typename iterType2>
bool histogram<dim,num>::
newPoint( const iterType1 ord_b, const iterType1 ord_e,
		  const iterType2 val_b, const iterType2 val_e )
{
	bool success = true;

	try
	{
		// If the origin has not yet been set, use this (first) point
		if( !_origin && !_newOrigin( ord_b, ord_e ) ) throw "cannot set origin to first point";

		// Initialise the index class
		_index<dim> *_ords = new (nothrow) _index<dim>;
		if( !_ords ) throw "cannot allocate memory for _ords in histogram class";

		// Convert passed double ordinates to long format; add small 'tol' parameter to ensure points 'on'
		// a bin boundary always go the same way (in this case, towards the higher bin index for each dimension)
		iterType1 ord_iter = ord_b;
		for( int i=0; i<dim; ord_iter++, i++ )
			_ords->set( i, (long) floor( 0.5 + _tol() + ( *ord_iter - _origin[i] ) / _scale[i] ) );

		// See if this point already has a bin (need "typename" to resolve templates)
		typename this_map::iterator data_iter = _data.find( _ords );

		// Points to "end()" if not found
		if( data_iter == _data.end() )
		{
			bin<dim+num> *new_bin = new (nothrow) bin<dim+num>;
			if( !new_bin ) throw "cannot allocate memory for bin class";
			
			data_iter = _data.insert( _data.end(), pair<_index<dim>*, bin<dim+num>*>(_ords,new_bin) );
				// new_bin memory is deallocated in destructor
		} else delete _ords;	// If already exists, deallocate _ords now (otherwise will be done in destructor)

		// Found or not, need to add the point to the bin, coordinates and all
		if (! data_iter->second->add_point( ord_b, ord_e, val_b, val_e ) )
			throw "cannot add point to bin!";
	}
	catch( const char *msg )
	{
		static bool already_warned = false;
		if( !already_warned )
			cerr << "Error in histogram::add_point() : " << msg
				<< " (only 1 warning given per histogram)" << endl;
		success = false;
		already_warned = true;
	}

	return success;
}




//
// OUTPUT
//

// Output operator - calls the private "_print()" member of the histogram class
template< int dim, int num >
inline ostream&
operator<< ( ostream &os, const histogram<dim,num> &hist )
{
	hist._print( os );
	return os;
}


// Outputs to ostream : call via "<<"
template< int dim, int num >
void histogram<dim,num>::
_print( ostream &os ) const
{
	typename this_map::const_iterator data_iter;

	for( data_iter = _data.begin(); data_iter != _data.end(); )
	{
		os << *data_iter->second;
		if( ++data_iter != _data.end() ) os << endl;
	}
}

// XML block, fully recoverable: (n, sum, sum_sqrd, sum2, sum_sqrd2, ...) , also tab-separated, with XML header/footer
template< int dim, int num >
void histogram<dim,num>::
exportXML( flatXMLParser *parser, std::string label )
{
	// Currently only supports the 1d/1d case (will need to extend scale and origin for higher dim)
	if( dim != 1 ) throw std::runtime_error( "histogram cannot yet export dimensions greater than 1 [in histogram<>::Export_XML()]" );
	
	// Set up the attributes and output the start element
	parser->addAttribute( "object",    "histogram"       );
	parser->addAttribute( "label",     label             );
	parser->addAttribute( "format",    "n sx sx2 sy sy2" );
	parser->addAttribute( "point_dim", dim               );
	parser->addAttribute( "value_dim", num               );
	parser->addAttribute( "scale",     _scale[0]         );				// Would need generalising for dim > 1
	parser->addAttribute( "origin",    _origin[0]        );				// Would need generalising for dim > 1
	parser->saveStartElement( "Data" );

	// Use bin's output for each bin direct to the output stream
	typename this_map::const_iterator data_iter;
	for( data_iter = _data.begin(); data_iter != _data.end(); data_iter++ )
		{ data_iter->second->Full_Output(*parser->getOutputStream()); *parser->getOutputStream() << std::endl; }
	
	// End of data block
	parser->saveEndElement( "Data" );
}

// Output XML block from given ostream object
template< int dim, int num >
void histogram<dim,num>::exportXML( ostream &os, std::string label )
{
	flatXMLParser parser(os);
	exportXML( &parser, label );
}



// Recover from XML-formatted stream; parses stream looking for the given label, and converts back to raw data
// Assumes same formatting as Export_XML()
template< int dim, int num >
void histogram<dim,num>::
recoverXML( flatXMLParser *parser, std::string label )
{
	// Clear any existing data, including the origin
	this->clear(true);
	
	// Skip through all "Data" blocks until the required label is found
	while( parser->readBlockWithTag( "Data" ) )
	{
		// Skip blocks that do not have the required label
		if( !parser->hasAttribute("label")
			or parser->attribute("object").compare("histogram")!=0
			or parser->attribute("label").compare(label) != 0 ) continue;
					
		// Check dimensions match
		if( parser->attributeAsInt("point_dim") != dim or parser->attributeAsInt("value_dim") != num )
		{
			std::cerr << "Cannot recover histogram '" << label << "' from XML stream, as dimensions do not match "
				      << "[in histogram<>::Recover_XML()]" << std::endl;
			throw std::runtime_error( "Cannot recover histogram data" );
		}
					
		// Set scale and origin; currently only supports the 1d/1d version
		if( num != 1 or dim != 1 )
		{
			std::cerr << "Cannot recover histogram '" << label << "' from XML stream, as only histogram<1,1> objects"
				      << " are recoverable so far [in histogram<>::Recover_XML()]" << std::endl;
			throw std::runtime_error( "Cannot recover histogram data" );
		}
 		this->_scale[0] = parser->attributeAsDouble( "scale" );
 		this->setOrigin( parser->attributeAsDouble( "origin" ) );
 		
 		// Check the format is of the required type
		if( parser->dataSize() % 5 )
			throw std::runtime_error( "Cannot recover histogram from XML; not a whole number of lines [in histogram<>::Recover_XML()]" );
		
 		// Parse the data block 5 values (=1 row) at a time
 		for( int row=0; row<parser->dataSize(); row+=5 )
 		{
 			// Get the values
 			long N = (long) parser->dataArray()[row];
 			if( !N ) continue;
 			double
 				sumX  = parser->dataArray()[row+1],
 				sumX2 = parser->dataArray()[row+2],
 				sumY  = parser->dataArray()[row+3],
 				sumY2 = parser->dataArray()[row+4];

			// Dynamically allocate memory for the index and bin objects; will be deallocated when
			// the histogram<>object is deallocated
			_index<dim> *ords;
			bin<num+dim> *val;
			try
			{
				ords = new( _index<dim>  );
				val  = new( bin<num+dim> );
			}
			catch( ... )
				{ throw std::runtime_error( "Could not allocate memory for histogram index or bin [in histogram<>::Recover_XML()]" ); }

			// Set up the index object; expression here follows histogram<>::New_Point()
			ords->set( 0, (long) floor( 0.5 + _tol() + ( sumX/N - _origin[0] ) / _scale[0] ) );

			// Set up the bin object with known sums
			val->setCounter( N );
			val->setSumsForIndex( 0, sumX, sumX2 );
			val->setSumsForIndex( 1, sumY, sumY2 );

			// Check this index does not already exist (which would mean an error)
			typename this_map::iterator data_iter = _data.find( ords );
			if( data_iter != _data.end() )
			{
				std::cerr << "Cannot recover histogram '" << label << "' from XML stream; some mismatch in new bin indices"
						  << " [in histogram<>::Recover_XML()]" << std::endl;
				delete ords;
				delete val;
				throw std::runtime_error( "Cannot recover histogram data" );
			}
			
			// Add to the data map					
			data_iter = _data.insert( _data.end(), pair<_index<dim>*, bin<dim+num>*>(ords,val) );
		}
	}
}


// Output XML block from given ostream object
template< int dim, int num >
void histogram<dim,num>::recoverXML( istream &is, std::string label )
{
	flatXMLParser parser(is);
	recoverXML( &parser, label );
}




//
// STATISTICS
//
// Returns maximum value (Euler inner product if num>1) and vector argument where the max lies.
// Ignores errors.
template< int dim, int num >
void histogram<dim,num>::maximum( double &val, vector<double> &pos ) const
{
	// Start with the first value, and loop over the rest
	typename this_map::const_iterator data_iter = _data.begin();
	typename this_map::const_iterator max_iter  = _data.begin();
	double current_max = _magnitude( (data_iter++)->second->Vector() );		// data_iter now points to second bin

	for( ; data_iter != _data.end(); data_iter++ )
	{
		double mag = _magnitude( data_iter->second->Vector() );
		if( mag > current_max )
		{
			current_max = mag;
			max_iter = data_iter;
		}
	}
	
	val = current_max;
	
	// Copy axis position to the passed vector
	pos.clear();
	for( int i=0; i<dim; i++ ) pos.push_back( max_iter->second->Vector()[2*i] );
}

// As above, but the argument is the (scalar) radius of the 
template< int dim, int num >
void histogram<dim,num>::maximum( double &val, double &arg ) const
{
	vector<double> x;
	maximum( val, x );
	
	arg = sqrt( _dot( x, x, dim ) );
}



//
// UTILITY ROUTINES
//

// Magnitude of the value part only of the passed bin->Vector(); ignores errors
template< int dim, int num >
double histogram<dim,num>::_magnitude( vector<double> v )
{
	// If values are scalars, just return the mean
	if( num==1 ) return v[2*dim];

	double sum_sqrd = 0.0;
	for( int i=0; i<num; i++ ) sum_sqrd += v[2*(dim+i)] * v[2*(dim+i)];
	
	return sqrt( sum_sqrd );		// Return with the positive square root
}

// Magnitude of the index part of the passed bin->Vector(); ignores errors
template< int dim, int num >
double histogram<dim,num>::_radius( vector<double> v )
{
	// If only 1 axis, just return the value
	if( dim==1 ) return v[0];

	double sum_sqrd = 0.0;
	for( int i=0; i<dim; i++ ) sum_sqrd += v[2*i] * v[2*i];
	
	return sqrt( sum_sqrd );		// Return with the positive square root
}


#endif



