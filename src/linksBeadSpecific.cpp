#include "linksBeadSpecific.h"

#pragma mark -
#pragma mark Constructors / destructor.
#pragma mark -

linksBeadSpecific::linksBeadSpecific(
    short dim,
    const parameterParser *params,
    const boxBase *box,
    const beadsBase *beads,
    std::shared_ptr<dataBeadSpring> data
    )
{
    using namespace parameterLabels;

    //
    // Set pointers/parameters that would normally be set by the parent class (whose constructor is not
    // called here as some parts need to be changed).
    //
    _dim = dim;
    _box = box;
    _beads = beads;
    _dataLinksBase = nullptr;

    //
    //
    // Need the number of link types to be declared and to be at least 1.
    //
    if( !params->hasIntegerParam(linksBeadSpecific_numLinkTypes) )
        errors::throwException("Link type 'bead specific' requires the parameter '"+linksBeadSpecific_numLinkTypes+"'",__FILE__,__LINE__);

    _nLinkTypes = params->integerParam( linksBeadSpecific_numLinkTypes );

    if( _nLinkTypes < 1 )
       errors::throwException("Bad value '"+to_string(_nLinkTypes)+"' for bead-specific link parameter '"+linksBeadSpecific_numLinkTypes+"'; must be positive",__FILE__,__LINE__);

    // Can now set the size of the link-type containers.
    _linkAttachRates.resize( _nLinkTypes );
    _linkAttachRates.shrink_to_fit();             // Will not change so may as well remove the capacity now.

    _linkAttachBeads.resize( _nLinkTypes );
    _linkAttachBeads.shrink_to_fit();

    //
    // Parse the default values. This mirrors the parent class.
    //
	_maxPerBead = params->integerParamOrDefault(links_maxPerBead,-1);

	_kA = params->scalarParamOrDefault(links_attachRate,0.0);
	_kD = params->scalarParamOrDefault(links_detachRate,0.0);
	
	_attachRange  = params->scalarParamOrDefault(links_attachRange,0.0);
	_detachLength = params->scalarParamOrDefault(links_detachLength,0.0);
	
	_k  = params->scalarParamOrDefault(links_springConstant,0.0);
	_l0 = params->scalarParamOrDefault(links_naturalLength ,_attachRange);

	_freezeLinksAfter = params->scalarParamOrDefault(links_freezeAfter,-1.0);

    //
    // Parse as many link-specific parameters as have been defined and store in the relevant containers.
    //
    for( auto lt=0; lt<_nLinkTypes; lt++ )
    {
        _linkAttachRates[lt] = params->scalarParamOrDefault( links_attachRate+"_"+to_string(lt), _kA );

        // Attach only possible beteen specific bead indices.
        std::string attachBeads = params->labelParamOrDefault( linksBeadSpecific_attachBetween+"_"+to_string(lt), "" );

        // Read in the bead format and check for validity. See documentation for expected format.
        if( !attachBeads.empty() ) _linkAttachBeads[lt] = _parseBeadRangeString( attachBeads );

        // These options have not yet been implemented, but could be by following similar procedures to kA (presumably).
        for( auto &prefix : {links_detachRate,links_springConstant,links_naturalLength,links_attachRange,links_detachLength,links_maxPerBead} )
            if( params->hasScalarParam(prefix+"_"+to_string(lt)) )
                errors::throwException("Bead-specific option for parameter '"+prefix+"_"+to_string(lt)+"' not yet implemented",__FILE__,__LINE__);
    }

    //
    // Check the parameters and throw an exception for any inalid values and/or combinations.
    //
    for( auto &kA : _linkAttachRates )
        if( kA<0.0 )
    		errors::throwException("At least one attach rate (bead-specific or default) was negative",__FILE__,__LINE__);

    //
    // Set the _canAttach and _canDetach flags.
    //
    _canAttach = std::any_of( _linkAttachRates.begin(), _linkAttachRates.end(), [](float rate){return rate>0.0;} );
    _canDetach = ( _kD>0.0 || _detachLength>0.0 );

    // If any are positive, make sure the attach range is positive. Note _canAttach is 'true' if any one rate is positive./
    // If bead-specific ranges are ever implemented, would need to check rate versus range for each link-type index.
    if( _canAttach && _attachRange==0.0 )
        errors::throwException( "Attach range must be positive (if at least one attach rate is positive)", __FILE__, __LINE__ );

    //
    // Initialise links container defined by this class.
    //
    _perThread_links.resize( omp_get_max_threads() );           // Defined in parent class.
    _perThread_links.shrink_to_fit();

    _perThread_addLinks.resize( omp_get_max_threads() );         // Defined in parent class. 
    _perThread_addLinks.shrink_to_fit();

    _perThread_delLinks.resize( omp_get_max_threads() );         // Defined in parent class.
    _perThread_delLinks.shrink_to_fit();

    _perThread_linkTypes.resize( omp_get_max_threads() );       // Defined here.
    _perThread_linkTypes.shrink_to_fit();

    _perThread_addLinkTypes.resize( omp_get_max_threads() );    // Defined here.
    _perThread_addLinkTypes.shrink_to_fit();

    _nLinks = 0;											    // May be re-set by ICs or loading state.

	//
	// Initialise the data-gathering (auxiliary) object.
	//
	_dataLinksBase = std::unique_ptr<dataLinksBase>( new dataLinksBase(this,data,_dim,params) );

    //
    // Ouptut to log file. Note this comes after the corresponding output for the base class.
    //
    logging::logFile << "Bead-specific link properties specified for " << _nLinkTypes << " link type(s)."
                     << " In order of link type:" << std::endl;

    // Attach rates in readable format.
    logging::logFile << " - attach rate(s): ";
    for( auto cIter=_linkAttachRates.cbegin(); cIter!=_linkAttachRates.cend(); )
        logging::logFile << *cIter << ( ++cIter==_linkAttachRates.end() || _linkAttachRates.size()==1u ? "" : " : " );
    logging::logFile << "." << std::endl;

    // Bead ranges in readable format.
    logging::logFile << " - bead index ranges: ";
    for( auto lIter=_linkAttachBeads.cbegin(); lIter!=_linkAttachBeads.cend(); )
    {
        // If no ranges given, all beads can react for this link index.
        if( lIter->first.empty() )
            logging::logFile << "(all)";
        else
            for( auto iIter=lIter->first.cbegin(); iIter!=lIter->first.cend(); )
                logging::logFile << *iIter << ( ++iIter==lIter->first.end() || lIter->first.size()==1u ? "" : "," );

        logging::logFile << "<->";

       if( lIter->second.empty() )
            logging::logFile << "(all)";
        else
            for( auto iIter=lIter->second.cbegin(); iIter!=lIter->second.cend(); )
                logging::logFile << *iIter << ( ++iIter==lIter->second.end() || lIter->second.size()==1u ? "" : "," );
 
        if( ++lIter != _linkAttachBeads.cend() ) logging::logFile << " : ";
    }
    logging::logFile << "." << std::endl;

    // Remaining are currently not variable between beads.
    logging::logFile << "Link parameters for which bead-specific values are not yet supported:" << std::endl;
	if( _k ) logging::logFile << " - spring constant " << _k << "." << std::endl;
	if( _l0 ) logging::logFile << " - natural spring length " << _l0 << "." << std::endl;
	if( _kD ) logging::logFile << " - detach rate " << _kD << "." << std::endl;
	if( _freezeLinksAfter > -1.0 ) logging::logFile << " - no attachement/detachment possible after freeze time t=" << _freezeLinksAfter << "." << std::endl;
	if( _detachLength ) logging::logFile << " - immediate detachment if crosslink length exceeds " << _detachLength << "." << std::endl;
	if( _maxPerBead>0 ) logging::logFile << " - max. " << _maxPerBead << " link end(s) for any one bead." << std::endl;

	if( _dataLinksBase->outputPercDim() )
		logging::logFile << " - calculating and outputting percolation dimension";
	else
		logging::logFile << " - not calculating or outputting percolation dimension";
	logging::logFile << " [set by flag parameter '" << dataLinks_outputPercDim << "']." << std::endl;
}


#pragma mark -
#pragma mark Parsing
#pragma mark -

// Parses a string (expected to be from the parameters file) to specify bead indices for
// each link type for which crosslinking is possible. Throws an exception for invalid strings.
std::pair<std::vector<int>,std::vector<int>> linksBeadSpecific::_parseBeadRangeString( std::string str ) const
{
    using namespace parameterLabels;

    //
    // Sanity checks for basic string structure.
    //

    // Must have exactly one colon (to separate the two lists of end-indices).
    if( count(str.begin(),str.end(),':') != 1 )
        errors::throwException( "Cannot parse the " + linksBeadSpecific_attachBetween + " string '"
                                    + str + "': must have at exactly one colon", __FILE__, __LINE__ );

    // Check there are no hyphens/minus signs, as neither are supported (note hyphens might suggest a range).
    if( str.find('-') != std::string::npos )
        errors::throwException( "Cannot parse the " + linksBeadSpecific_attachBetween + " string '"
                                    + str + "': cannot contain negatives or hyphens", __FILE__, __LINE__ );

    //
    // Split the string into two substrings, each with its own bead index list.
    //
    const int colon = str.find(':');
    const std::array< std::string, 2 > listStrings{ str.substr(0,colon), str.substr(colon+1,std::string::npos) };

    std::array< std::vector<int>, 2 > lists;                // Will sort later so not const.

    //
    // Main parsing loop.
    //
    for( auto beforeAfter : {0,1} )
    {
        auto listStr = listStrings[beforeAfter];            // Shorthand.
        auto i = 0u, start = 0u;                            // Character index counters.

        // Get next index as one or more consecutive digits.
        while( i < listStr.size() )
        {
            while( !std::isdigit(listStr[i]) ) if( ++i>=listStr.size() ) break;
            start = i;
            while( std::isdigit(listStr[i]) ) if( ++i>=listStr.size() ) break;

            // Many ways to get here - make sure range is sensible.
            if( start<listStr.size() && i<=listStr.size() && i>=start )     // Maybe a bit *too* defensize ...
                lists[beforeAfter].push_back( std::stoi(listStr.substr(start,i-start)) );
        }
    }

    //
    // Sort the lists and check the indices. Throw an exception if anything looks wrong.
    // Note do not check for maximum bead number, as future extensions may include dynamic filamnet growth.
    //
    for( auto &i : {0,1} )
        std::sort( lists[i].begin(), lists[i].end() );
    
    // Check for repeated values in either list, separately.
    for( auto &list : lists )
        for( auto &i : list )
            if( std::count(list.begin(),list.end(),i) != 1 )
                errors::throwException( "Cannot parse the " + linksBeadSpecific_attachBetween + " string '"
                                    + str + "': repeated index '" + to_string(i) + "' in a single list", __FILE__, __LINE__ );

    //
    // Return with a pair of lists.
    //
    return std::pair< std::vector<int>, std::vector<int> > {lists[0],lists[1]};
}


#pragma mark -
#pragma mark Bead-specific attachement
#pragma mark -

// Check for new potential links, given bead-specific properties.
void linksBeadSpecific::_perThread_attachLinks( unsigned int thread, double dt )
{
	// Per-thread shorthands from the beads object. Same as the parent version.
	      unsigned int numPairs = _beads->nearbyBeads_numPairs(thread);
	const indexPair*   indices  = _beads->nearbyBeads_indices (thread);
	const double*      rSqrd    = _beads->nearbyBeads_rSqrd   (thread);

    // Clear both lists of new links to add.
	_perThread_addLinks[thread].clear();                    // Same as parent class.
    _perThread_addLinkTypes[thread].clear();                // Declared in this class.

    // Vector of all link attach rates for all link tyoes.
    std::vector<double> attachProbs( _nLinkTypes );
 
    // Largest range (squared) across all link types. Currently all the same anyway, but should be easy to generalise.
	double maxRangeSqrd = _attachRange * _attachRange;
	
	// Loop over all nearby beads.
	for( auto n=0u; n<numPairs; n++ )
	{
        // No point continuing if outside the largest possible attachment range for any link type.
		if( rSqrd[n] > maxRangeSqrd ) continue;

        // Clear vector of link attach rates.
        std::fill( attachProbs.begin(), attachProbs.end(), 0.0 );

        // Need the bead indices along their respective filaments, with the head index being 0.
        indexPair ij = indices[n];
        auto
            i = ij.first  - _beads->headBead(ij.first),
            j = ij.second - _beads->headBead(ij.second);

        // Loop through all link types and calculate probability of attachment in this time step for each type.
        for( auto l=0; l<_nLinkTypes; l++ )
        {
            // If/when bead-specific attach ranges implemented, would check rSqrd[n] against the specific range
            // for this link type, and 'continue' if outside.

            // Need i to be in one list and j in the other for this link type to be possible.
            // Empty lists corresponds to "(all)", i.e. all bead indices are valid.
            std::pair< std::vector<int>, std::vector<int> > *vb = &_linkAttachBeads[l];
            if(
                (
                    ( vb->first.empty()  || std::find(vb->first.begin(), vb->first.end(), i)!=vb->first.end() )
                    &&
                    ( vb->second.empty() || std::find(vb->second.begin(),vb->second.end(),j)!=vb->second.end() )
                )
                ||
                (
                    ( vb->first.empty()  || std::find(vb->first.begin(), vb->first.end(), j)!=vb->first.end() )
                    &&
                    ( vb->second.empty() || std::find(vb->second.begin(),vb->second.end(),i)!=vb->second.end() )
                )
             )
             {
                attachProbs[l] = dt * _linkAttachRates[l];
             }
        }
 
        // Accept first link type (sampled in random order) that successfully attaches.
        int typeToAttach = -1;
        for( auto &l : pRNG::shuffledIndices(_nLinkTypes,thread) )
        {
            if( attachProbs[l] && pRNG::stdUniform(thread) < attachProbs[l] )
            {
                typeToAttach = l;
                break;
            }
        }

        // If attach, add to both the list of links to add, and the map of link types to add.
        if( typeToAttach != -1 )
        {
    		_perThread_addLinks[thread].insert( ij );
            _perThread_addLinkTypes[thread][ij] = typeToAttach;
        }
	}	    
}

// The actual (attempted) addition of both link and link type to the persistent containers.
// This is assumed to be called from a serial context.
int linksBeadSpecific::_serial_attemptAddLink( indexPair ij )
{
    // Use parent class method to see to which thread's container the link was added, or -1 if not.
    int threadNum = linksBase::_serial_attemptAddLink(ij);

    // Return -1 for no addition.
    if( threadNum==-1 ) return -1;

    // If still here, the link pair ij has already been added. Need to find the corresponding link type.
    // This could be improved upon, but has been designed to impact the parent class as little as possible.
    // Also recall we will only rarely get this far anyway.
    int lt = -1;
    for( auto mapIter = _perThread_addLinkTypes.cbegin(); mapIter!=_perThread_addLinkTypes.cend(); mapIter++ )
        if( mapIter->find(ij) != mapIter->end() )
        {
            lt = mapIter->at(ij);
            break;
        }

    // Sanity check.
    if( lt==-1 )
        errors::throwException("System error: could not find link type for a link pairing that was added",__FILE__,__LINE__);

    // Now add the link type to the std::map on the thread responsible for the same link pairing.
    _perThread_linkTypes[threadNum][ij] = lt;

    // Return with which thread container the link (both index and type) was added to.
    return threadNum;
}


#pragma mark -
#pragma mark Bead-specific detachment
#pragma mark -

// Flags links for detachment based on bead-specific rules. Actually removes link types at the same time.
void linksBeadSpecific::_perThread_detachLinks( unsigned int t, double dt )
{
    // Construct std::set of all links handled by this thread to be deleted ('_perThread_delLinks').
    // Would need to be generalised here if/when bead-specific detachment rules are implemented.
    linksBase::_perThread_detachLinks( t, dt );

    // Remove all links flagged for deletion from the link-type std::map for this thread.
    for( auto &ij : _perThread_delLinks[t] )
        _perThread_linkTypes[t].erase( ij );
}


#pragma mark -
#pragma mark Debugging
#pragma mark -

// Calls the equivalent parent class method, then adds some additional checks for bead-specific links.
void linksBeadSpecific::debugCheckBadLinks() const
{
    // Parent class checks for e.g. repeated links, bad indices, too many for a bead - but nothing involving link
    linksBase::debugCheckBadLinks();

    // Check the new container for link types is of the same size as the links themselves. This should also be
    // the maximum number of threads, but no need to check that here.
    if( _perThread_links.size()!=_perThread_linkTypes.size() ) 
        errors::throwException("Debug error: Link indices and link type per-thread containers of different sizes",__FILE__,__LINE__);

    // Loop through the thread numbers to check the per-thread lists.
    for( auto threadNum=0u; threadNum<_perThread_linkTypes.size(); threadNum++ )
    {
        // Shorthands.
        auto links = &_perThread_links[threadNum];
        auto types = &_perThread_linkTypes[threadNum];

        // Easy check: Make sure they are the same size.
        if( links->size() != types->size() )
            errors::throwException("Debug error: Per-thread containers for links and link types of different size for at least one thread",__FILE__,__LINE__);

        // No point continuing if there are no links (and therefore, given the previous check, no types) for this thread number.
        if( !links->size() ) continue;

        // Make sure each link type is in the valid range.
        for( auto &t : *types )
            if( t.second<0 || t.second>=_nLinkTypes )
                errors::throwException("Debug error: Invalid link type outside of the allowed range for at least one link",__FILE__,__LINE__);

        // Make sure each link is a key in the link type std::map.
        for( auto &l : *links )
            if( types->find(l) == types->end() )
                errors::throwException("Debug error: Added link not found in link-type container",__FILE__,__LINE__);

        // Vice versa, may sure each key in the std::map is also in the std::set. Given uniqueness and have already
        // checked the sizes match, hard to see how this one can go wrong if the previous check is okay, but just in case ...
        for( auto &l : *types )
            if( links->find(l.first) == links->end() )
               errors::throwException("Debug error: Added link-type not found in link container",__FILE__,__LINE__);
    }
}


#pragma mark -
#pragma mark I/O
#pragma mark -

// Since link indices are stored as a std::set and the types as a std::map, there is no easy way to output the types
// separately to the indices. Instead, override the parent class method to output each link in a single XML block as:
// [index1] [index2] [type]
void linksBeadSpecific::_saveLinkData( flatXMLParser *xml ) const
{
	xml->addAttribute( _saveNumLinksAttr, numLinks() );
	xml->saveStartElement( _saveIndicesTag );
	
    if( numLinks() )
    {
        for( auto threadNum=0u; threadNum<_perThread_links.size(); threadNum++ )
            for( auto &ij : _perThread_links[threadNum] )
            {
                xml->appendDataString( ij.first );
                xml->appendDataString( ij.second );
                xml->appendDataString( _perThread_linkTypes[threadNum].at(ij) );
            }
        xml->saveData();
    }

    xml->saveEndElement( _saveIndicesTag );
}

// Largely repeats the parent method (which is not called), with additions for link types.
void linksBeadSpecific::_recoverLinkData( flatXMLParser *xml )
{
    // Get the number of links.
	xml->readBlockWithTag( _saveIndicesTag );
	auto numLinks = atoi( xml->attribute(_saveNumLinksAttr).c_str() );

    // Note the number of threads on saving may not match that on loadibg, so cannot assume the same number of containers.
	for( auto &linkSet : _perThread_links ) linkSet.clear();
    for( auto &typeMap : _perThread_linkTypes ) typeMap.clear();
  
	_nLinks = 0;

	_linksPerBead.clear();
	_linksPerBead.resize( _beads->numBeads() );
	_linksPerBead.shrink_to_fit();

	// Sanity check. Note we expect types to have been saved as well, so now 3 integers per link.
	if( xml->dataSize() != 3*numLinks )
		errors::throwException( "Given number of links does not match size of index array", __FILE__, __LINE__ );

	// Loop through all saved links and update the containers.
	unsigned int t = 0u;		// Thread index of per-thread sets/maps to add the next link to.

 	for( auto n=0; n<numLinks; n++ )
 	{
 		auto
 			i    = (unsigned int)( xml->dataArray()[3*n  ] ),
 			j    = (unsigned int)( xml->dataArray()[3*n+1] ),
            type = (unsigned int)( xml->dataArray()[3*n+2] );

		// Add to the per-thread sets/maps in a round-robin manner.
        auto ij = std::make_pair( std::min(i,j), std::max(i,j) );           // Although should be i<j anyway.

		_perThread_links[t].insert( ij );		
        _perThread_linkTypes[t][ij] = type;

        // Move to the next containers.
		if( ++t==_perThread_links.size() ) t = 0u;
		
        // Update the global arrays and counts.
		_linksPerBead[i]++;
		_linksPerBead[j]++;

		_nLinks++;
 	}
 
 	// Good time to check integrity of the loaded link data. Slow.
 	debugCheckBadLinks();
}

