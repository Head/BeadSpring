// Template specialisations

#include "threadedCellSort.h"



#pragma mark -
#pragma mark Generate cell nearest neighbours for dim=2, 3
#pragma mark -

// Generates the list of neighbouring cells; specialised to dimension.
template<>
void threadedCellSort<2>::_generateCellNNs()
{
	unsigned int count = 0;

	for( unsigned short y1=0; y1<_numCells[1]; y1++ )
		for( unsigned short x1=0; x1<_numCells[0]; x1++ )

			for( short dy=-1; dy<2; dy++ )
				for( short dx=-1; dx<2; dx++ )
				{
					// Skip 'earlier' cells in the list (to avoid double counting); central cell included
					if( dy<0 or (dy==0 and dx<0) ) continue;

					// Apply periodic boundary conditions
					short x2 = x1 + dx, y2 = y1 + dy;

					if( x2 <  0                   ) x2 += _numCells[0];
					if( x2 >= (short)_numCells[0] ) x2 -= _numCells[0];

					if( y2 <  0                   ) y2 += _numCells[1];
					if( y2 >= (short)_numCells[1] ) y2 -= _numCells[1];

					// Add sequentially to the list
					_cellNNs[count++] = x2 + _numCells[0]*y2;
				}
}

template<>
void threadedCellSort<3>::_generateCellNNs()
{
	unsigned long count = 0;

	for( unsigned short z1=0; z1<this->_numCells[2]; z1++ )
		for( unsigned short y1=0; y1<this->_numCells[1]; y1++ )
			for( unsigned short x1=0; x1<this->_numCells[0]; x1++ )

				for( short dz=-1; dz<2; dz++ )
					for( short dy=-1; dy<2; dy++ )
						for( short dx=-1; dx<2; dx++ )
						{
							// Skip 'earlier' cells in the list (to avoid double counting); central cell included
							if( dz<0 or ( dz==0 and (dy<0 or (dy==0 and dx<0)) ) ) continue;

							// Apply periodic boundary conditions
							short x2 = x1 + dx, y2 = y1 + dy, z2 = z1 + dz;

							if( x2 <  0                   ) x2 += _numCells[0];
							if( x2 >= (short)_numCells[0] ) x2 -= _numCells[0];

							if( y2 <  0                   ) y2 += _numCells[1];
							if( y2 >= (short)_numCells[1] ) y2 -= _numCells[1];

							if( z2 <  0                   ) z2 += _numCells[2];
							if( z2 >= (short)_numCells[2] ) z2 -= _numCells[2];

							// Add sequentially to the list
							_cellNNs[count++] = x2 + _numCells[0]*y2 + _numCells[0]*_numCells[1]*z2;
						}
}
