#
# Tests that the topology of networks, i.e. the connectivity and coordination number, is as expected for simple cases.
#

# Standard imports
import math
import scipy.optimize

# Base class
from bsTests import bsTestBase

class testNetworkTopology( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__parametersFile  = fname

		# Call parent method with required flags.
		super(testNetworkTopology,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Try to see what sort of topology is expected. 'None' if not defined.
		N          = self._table.getParameter( "N"               )
		M          = self._table.getParameter( "M"               )
		epsilon    = self._table.getParameter( "epsilon"         )
		maxPerBead = self._table.getParameter( "maxLinksPerBead" )

		# Monomeric, no EV.
		if epsilon==None and M==1:
			self._performMonomericGhostTest( N, maxPerBead )
			return

		# If still here, test type was not recognised.
		raise NotImplementedError( "Cannot perform networkTopology test: Expected topology not known." )


	# Arguments for the executable.
	def _parameterFile( self ): return self.__parametersFile
	def _numThreads   ( self ): return 1

	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]


	#
	# Test types.
	#
	def _performMonomericGhostTest( self, N, maxPerBead ):
		"""Ghost particles (i.e. epsilon==0) in small systems with low max. links per bead (so should saturate).
		Can get stuck in metastable state if maxPerBead>1, so only check such cases with maxPerBeads+1 particles total. """

		# Sanity check.
		if maxPerBead>1 and N != maxPerBead+1:
			raise NotImplementedError( "Test not reliable for maxPerBead>1 unless N=1+maxPerBead and long enough times simulated." )

		#
		# Check mean coordination number first.
		#
		t, z = [ self._table.getDataSets("meanLinkCoord")["curves"][0][label] for label in ("x","y") ]

		# Plot.
		if self._plot:
			import matplotlib.pyplot as plt

			plt.axhline( 0.0, color="k", lw=0.5 )
			plt.axvline( 0.0, color="k", lw=0.5 )

			plt.plot( t, z, "-ro", label=r"$\langle z\rangle\,{\rm (data)$" )
			plt.plot( t, [maxPerBead]*len(t), "k", lw=2, label=r"${\rm Prediction}$" )

			plt.xlabel( r"$t$" )
			plt.legend( loc="lower right" )
			plt.show()

		# Check against prediction.
		if self._verbose: print( "Checking data for mean coordination number z asymptotes to {}.".format(maxPerBead) )

		if z[-1] != maxPerBead:
			raise bsTestBase.FailedTestError( "Late-time mean coordination number differs from the predicted value." )

		if self._verbose: print( "Matches at final time point." )


		#
		# Check the number of links. Almost trivial given the previous result.
		#
		t, numLinks = [ self._table.getDataSets("numLinks")["curves"][0][label] for label in ("x","y") ]

		# Plot.
		prediction = maxPerBead*N/2
		if self._plot:
			import matplotlib.pyplot as plt

			plt.axhline( 0.0, color="k", lw=0.5 )
			plt.axvline( 0.0, color="k", lw=0.5 )

			plt.plot( t, numLinks, "-ro", label=r"${\rm No. links}$" )
			plt.plot( t, [prediction]*len(t), "k", lw=2, label=r"${\rm Prediction}$" )

			plt.xlabel( r"$t$" )
			plt.legend( loc="lower right" )
			plt.show()

		# Check against prediction.
		if self._verbose: print( "Checking data for total number of links asymptotes to {}.".format(prediction) )

		if numLinks[-1] != prediction:
			raise bsTestBase.FailedTestError( "Late-time number of links differs from the predicted value." )

		if self._verbose: print( "Matches at final time point." )

		#
		# Check the node degree distribution.
		#
		degree, hist = [ self._blockData.getDataSets("nodeDegree")["curves"][-1][axis] for axis in ("x","y") ]

		if self._plot:
			import matplotlib.pyplot as plt

			plt.axhline( 0.0, color="k", lw=0.5 )

			plt.plot( degree, hist, "-ro", label=r"${\rm Node\,degree}$" )

			plt.plot( degree, [0]*len(degree), "k", lw=2, label=r"${\rm Prediction\,for\,} n<n_{\rm max}$" )
			plt.plot( degree, [N]*len(degree), "k", lw=2, label=r"${\rm Prediction\,for\,} n=n_{\rm max}$" )

			plt.xlabel( r"$n$" )
			plt.legend( loc="lower right" )
			plt.show()

   		# Check against prediction.
		if self._verbose: print( "Checking node degree distribution corresponds to fully-saturated connections" )

		for n in range(len(degree)):
			if degree[n] < maxPerBead and hist[n] != 0.0:
				raise bsTestBase.FailedTestError( "Still some beads with unsaturated connectivity." )
			if degree[n] == maxPerBead and hist[n] != N:
				raise bsTestBase.FailedTestError( "Not all beads have maximum connectivity." )

		if self._verbose: print( "Distribution mathces expected form." )



	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(2):
			fname = "params_test{}.xml".format(testNum)
			print( "Performing network topology test starting from state '{}'".format(fname) )
			testNetworkTopology(executable,verbosity,plot,fname)
			print( " - passed" )
