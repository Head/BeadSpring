//
// Handles construction and output of regular tables of (typically) scalar variables,
// normal usage being identical sets of variables being output at consecutive times.
// 
// Still pretty basic; intended usage is:
//
// scalarTable tab( out_filename, "Identifying text", &parameters_object, &log_file );
//
// tab.aaaValue( label, double_value )
// tab.outputRow( "Label_for_this_row" );
//
// - will output parameters in commented list in header (on construction), and in footer
// (any parameters subsequently added to the params object)
// - will send simple message to log file for each row
//
// Could include: Check integrity of table (i.e. ensure same labels output for each row);
// coninuation of existing table.
//
// 14/10/2010/DAH: Now has optional flag in constructor; if set, the table file is continued
// (i.e. opened and appended) rather than created.
//
// 23/10/2011/DAH: Added optional flag in constructor; if set, will output in XML format rather
// than a straight table.
//


#include "scalarTable.h"


//
// CONSRUCTORS AND DESTRUCTOR
//
scalarTable::scalarTable( std::string fname, const parameterParser *p = NULL, bool cont )
	: _continuation(cont), _filename(fname), _outputtedHeadings(false), _params(p)
{
	// Creating a new file, or extendng an existing one
	_file = new ofstream( _filename.c_str(), ( _continuation ? ios::out|ios::in : ios::out ) );
	
	// If tried to extend a file but failed to open it, open a new one and reset the _continuation flag
	if( _continuation && !_file->is_open() )
	{
		_continuation = false;
		delete _file;
		_file = new ofstream( _filename.c_str(), ios::out );
	}

	// If file still not open, raise an exception
	if( !_file->is_open() )
	{
		ostringstream out;
		out << "Failed to open scalarTable file '" << _filename << "' [in " << __FILE__ << ", line " << __LINE__ << "'" << endl;
		throw std::ios_base::failure(out.str());
	}

	// If continuing, start from just before the "</scalarTable>" line, thus overwriting it
	if( _continuation )
	{
		_file->seekp( -9, ios::end );		// '9' is the length of "\n</scalarTable>"
		*_file << "         ";				// 9 spaces; ensure previous "</scalarTable>" is replaced by whitespace
		_file->seekp( -9, ios::end );		// Move back again; whitespace typically overwritten by subsequent rows
	}

	// Output the first line (as specified by the user), and the header (provided by the parameters object, if any)
	if( !_continuation )
	{
		*_file << "<?xml version='1.0'?>" << endl;
		*_file << "<Table>" << endl;
		
		if( _params ) *_file << *_params << endl;
	}
}

scalarTable::~scalarTable()
{
	*_file << "</Table>" << endl;
		// If the length of this string is changed, the _continuation option (that overwrites a fixed number of characters)
		// will also need to be rewritten.

	delete _file;
}


//
// OUTPUT
//

void scalarTable::outputRow( std::string label, bool maxPrecision )
{
	// Set maximum precision if required; since table files are normally small, there's usually no reason
	// not do to this, but there is a flag to use standard precision if preferred. DBL_DIG defined in <float.h>
	if( maxPrecision ) _file->precision(DBL_DIG);

	try
	{
		// If not yet done so, output the headings (if not extending an existing table)
		if( !_outputtedHeadings and !_continuation )
		{
			*_file << "<Headings> Label ";
			for( map<std::string,double>::const_iterator iter = _scalars.begin(); iter != _scalars.end(); )
			{
				*_file << _removeWhiteSpace( (*iter).first );
				*_file << ( ++iter == _scalars.end() ? " </Headings>\n" : " " );		// Iterate and output correct whitespace
			}	

			_outputtedHeadings = true;
		}

		// Loop; note no iter++ in 'for' command as want to check if on the last value (for outputting tabs)
		*_file << "<Row> " << label << " ";
		for( map<std::string,double>::const_iterator iter = _scalars.begin(); iter != _scalars.end(); )
		{
			*_file << (*iter).second;
			*_file << ( ++iter == _scalars.end() ? " </Row>\n" : " " );		// Iterate and output correct delimiter
		}

		// Flush file now, so quantities are immediately visible even if the run is still continuing
		_file->flush();		// Causes extra overhead; don't call unless needed

		// Clear the _scalars array
		_scalars.clear();
	}
	catch( std::exception &e )
	{
		std::cerr << "EXCEPTION in scalarTable::Output_Row(): " << e.what() << std::endl;
	}
}
