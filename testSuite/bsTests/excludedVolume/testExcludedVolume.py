#
# Tests for the excluded volume interactions. Just checks that g(r) is small within a fraction of a bead diameter.
#

# Standard imports
import numpy
import math
import scipy.optimize
import matplotlib.pyplot as plt

# Base class
from bsTests import bsTestBase

class testExcludedVolume( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname ):

		# Store private variables.
		self.__paramFile = fname

		# Call parent method with required flags.
		super(testExcludedVolume,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Sanity check.
		if self._blockData.getParameter("M") > 1:
			raise NotImplementedError( "Excluded volume test assumes a monomeric system (i.e. M=1)" )

		#
		# g(r).
		#

		# Get the required parameters. See all by printing self._table.allParameters().
		b = self._blockData.getParameter( "b" )
		
		# Get the final g(r).
		r, g_r  = [ self._blockData.getDataSets("bead_gr")["curves"][-1][axis] for axis in ("x","y") ]

		# Message to stdout if requested.
		if self._verbose:
			print( "Checking g(r) does not exceed the threshold {0} within a fraction {1} of the bead diameter.".format(testExcludedVolume.threshold() ,testExcludedVolume.rangeBeadFraction()) )
		
		# Plotting (optional).
		if self._plot:
			plt.axhline( 0.0, color="k" )	# x-axis.
			plt.axhline( 1.0, color="k" )	# Ideal gas / large-r limit.

			plt.plot( r, g_r, "-or", label=r"${\rm Data}$" )

			plt.axvline( b*testExcludedVolume.rangeBeadFraction(), color="b" )
			plt.axhline(   testExcludedVolume.threshold        (), color="b" )
			
			plt.xlabel( r"$r$" )
			plt.ylabel( r"$g(r)$" )
			plt.legend()

			plt.show()

		# Check that the g(r) for short lengths is below some threshold.
		for i in range(len(r)):
			if r[i] < b * testExcludedVolume.rangeBeadFraction():
				if g_r[i] > testExcludedVolume.threshold():
					raise bsTestBase.FailedTestError( "g(r) at short range exceeds threshold" )

	# Arguments for the executable
	def _parameterFile( self ): return self.__paramFile
	def _numThreads   ( self ): return 1

	# Parameters used for checking.
	@classmethod
	def rangeBeadFraction( cls ):
		"""Will not check any points r above the bead diameter b times this fraction."""
		return 0.8

	@classmethod
	def threshold( cls ):
		"""Except g(r) for small r to be below this value."""
		return 0.05

	# Cleanup; can use wildcards
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]




	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		for testNum in range(2):
			fname = "params_test%i.xml" %testNum
			print( "Performing excluded volume test starting from file '{}'".format(fname) )
			testExcludedVolume(executable,verbosity,plot,fname)
			print( " - passed" )
