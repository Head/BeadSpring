#
# Handles the colour schemes for the BeadSpring visualiser.
#


class bsColourSchemes:

	def __init__( self ):
		"""Initialises to the default colour scheme."""
		self._scheme = 0

	def currentSchemeIndex( self ):
		return self._scheme

	def numSchemes( self ):
		"""Number of schemes in this (possibly derived) class."""
		return 5
	
	def cycleToNext( self, allStates, stateIndex ):
		"""Cycles to the next scheme. Returns 'True' if changed, else 'False'"""

		# Cannot cycle if there is only one scheme.
		if self.numSchemes()==1:
			return False

		invalidScheme = True
		while invalidScheme:
			self._scheme = (self._scheme+1) % self.numSchemes()

			# Currently skip colouring along filament schemes when the system is all monomers.
			invalidScheme = allStates.isMonomeric(stateIndex) and self._scheme in (2,3)
			if invalidScheme:
				print( " - skipping scheme {}: Not meaningful for this state.".format(self._scheme+1) )

		return True

	def linkColour( self ):	
		"""Colour for the links in RGB format, to be distinct from beads."""
		return [ 1, 0, 0 ]
	
	def beadColour( self, allStates, stateIndex, beadIndex ):
		"""Returns [r,g,b] for bead index for the passed state and colour scheme index (so can cycle around variations).
		Some colour schemes (i.e. those involving distance along filament) can be slow; defaults to the fastest/simplest.
		If the bead should not be displayed at all, returns 'None'."""
		
		scheme = self._scheme % self.numSchemes()
		
		# Default bead colour: simple, should be fast.
		if scheme == 0:
			return [ 0, 0.5, 0.5 ]

		# Colour by filament only, no positional information. Should be reasonably fast.
		if scheme == 1:
			numFils    = allStates.getState(stateIndex)["numFilaments"]
			filNum     = allStates.filamentForBead(stateIndex,beadIndex)

			filRatio   = ( 0 if numFils   ==1 else filNum  /(numFils   -1) )

			green = filRatio
			blue  = 1 - green
		
			return [ 0, green, blue ]

		# Colour by filament, shade by position along filament. Can be slow.
		if scheme == 2:

			filRatio   = ( 0 if numFils   ==1 else filNum  /(numFils   -1) )
			alongRatio = ( 0 if longestFil==1 else alongFil/(longestFil-1) )

			shade = 1 - alongRatio		# So brightest is the head.
			green = filRatio
			blue  = 1 - green
		
			shade = 0.5*( 0.5 + shade )
			return [ 0, shade*green, shade*blue ]

		# Colour by position along filament, shade by filament index. Can be slow.
		if scheme == 3:
			numFils    = allStates.getState(stateIndex)["numFilaments"]
			longestFil = allStates.longestFilament(stateIndex)
			filNum     = allStates.filamentForBead(stateIndex,beadIndex)
			alongFil   = allStates.beadAlongFilament(stateIndex,beadIndex)
			

			filRatio   = ( 0 if numFils   ==1 else filNum  /(numFils   -1) )
			alongRatio = ( 0 if longestFil==1 else alongFil/(longestFil-1) )

			shade = filRatio		
			green = alongRatio
			blue  = 1 - green
		
			shade = 0.5*( 0.5 + shade )
			return [ 0, shade*green, shade*blue ]
		
		# Colour by cluster. Needs clusters to be identified
		if scheme == 4:
			
			# Find which cluster the bead is in. Note indices in the cluster decomposition refer to filaments.
			for i, cluster in enumerate(self._clusters):
				if allStates.filamentForBead(stateIndex,beadIndex) in cluster:
					ratio = ( i/(len(self._clusters)-1) if len(self._clusters)>1 else 0 )

					green = ratio
					blue  = 1 - ratio

					return [ 0, green, blue ]

			# If reached here, the bead was not in the cluster list - could be a singleton. Return 'None' to not display.
			return None

	def description( self ):
		return (
			"Same colour for every bead",
			"Coloured by filament index only",
			"Coloured by flament index, shaded by position along filament",
			"Coloured by position along filament, shaded by filament index",
			"Coloured by cluster from largest (blue) to smallest (green)"
		) [ self._scheme % self.numSchemes() ]

	# New state selected. Allows pre-processing.
	def newState( self, allStates, index ):
		if self._scheme == 4:

			# Sort by length so largest (=first) cluster is always the same colour.
			self._clusters = allStates.getClusters(index,noSingletons=True)
			self._clusters.sort( key=len, reverse=True )


	
	

	