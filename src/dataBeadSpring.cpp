#include "dataBeadSpring.h"

#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

dataBeadSpring::dataBeadSpring( short dim, std::string filePrefix, const parameterParser *params, bool append )
	: _dim(dim), _volume(0.0), _continuation(append)
{
	// Get filenames with the given prefix, otherwise standard.
	auto tablePath = filePrefix + ( filePrefix.size() ? "/" : "" ) + __tableFileName;
	auto blockPath = filePrefix + ( filePrefix.size() ? "/" : "" ) + __blockFileName;
	
	// Open up the scalar table. The constructor handles echoing of the parameters.
	_table = std::unique_ptr<scalarTable>( new scalarTable(tablePath,params,append) );

	// Check to see the block data file exists. If not, act as if not extending, i.e. include header etc.
	if( append )
	{
		ifstream extantFile( blockPath );
		append = extantFile.is_open();
	}

	// Open the block data file.
	_block = std::unique_ptr<flatXMLParser>( new flatXMLParser(blockPath,(append?ios::app:ios::out),append) );
	
	// Initialise block data file and echo parameters.
	if( !append )
	{
		_block->XMLStartTag();
		_block->saveStartElement( __blockFileTag );				// Can add attributes prior to this with adAttribute().
		*(_block->getOutputStream()) << *params << endl;		// <Parameters>...</Parameters> block.	
	}
	
	//
	// Quantities handled by this class that are shared by the modules.
	//
	
	// Initialise the stress tensors, to which all modules can contribute.
	_sum_fi_rj.resize(_dim);
	for( auto &v : _sum_fi_rj ) v.resize(_dim,0.0);

	_sum_fi_rj_instantaneous.resize(_dim);
	for( auto &v : _sum_fi_rj_instantaneous ) v.resize(_dim,0.0);

	// Reset count to zero and set a sensible frequency for calculation.
	_calcStressCount  = 0;
	_calcStressEvery  = 100;							// May be reduced if setTableCalcInterval() called.
	_numStressCalcs   = 0;
}

// Destructor. Largely handled by smart pointers.
dataBeadSpring::~dataBeadSpring()
{
	_block->saveEndElement( __blockFileTag );
}


#pragma mark -
#pragma mark Quantities distributed across modules
#pragma mark -

void dataBeadSpring::addToStressTensor_f_r( const double *f, const double *r, double weight )
{
	// Make thread safe with a mutex (RAII).
	std::lock_guard<std::mutex> lock(_stressMutex);

	for( auto i=0; i<_dim; i++ )
		for( auto j=0; j<_dim; j++ )
			_sum_fi_rj_instantaneous[i][j] += weight * f[i] * r[j];
}

void dataBeadSpring::addToStressTensor_fr_r( double f_over_r, const double *r, double weight )
{
	double f[3];
	for( auto k=0; k<_dim; k++ ) f[k] = f_over_r * r[k];

	addToStressTensor_f_r( f, r, weight );
}

void dataBeadSpring::_addSharedTableQuants()
{
	// Sanity checks.
	if( _volume<=0.0 )
		errors::throwException( "Cannot output shared table quantities; volume not yet defined", __FILE__, __LINE__ );

	// If no stress calculations (possible if resetting time after a restart), just output zero.
	if( !_numStressCalcs )
		errors::throwException( "Cannot output stress tensor; no data collected in this time interval", __FILE__, __LINE__ );

	// Calculate the pressure = Tr(\sigma)/d (modulo sign). Also normalise to number of updates.
	double P = ( _sum_fi_rj[0][0] + _sum_fi_rj[1][1] + (_dim==3?_sum_fi_rj[2][2]:0.0) ) / ( _dim * _volume * _numStressCalcs );
	_table->updateCurrentRow( __dataLabel_pressure, P );

	// Calculate the shear stress if requested by e.g. a Lees-Edwards-type box.
	if( __outputShearStress )
	{
		double sigma_xy = _sum_fi_rj[0][1] / ( _volume * _numStressCalcs );
		_table->updateCurrentRow( __dataLabel_shearStress, sigma_xy );	
	}

	// Reset count of stress calculations.
	_numStressCalcs = 0;

	// Clear the stress tensor.
	for( auto i=0; i<_dim; i++ )
		for( auto j=0; j<_dim; j++ )
			_sum_fi_rj[i][j] = 0.0;
}


#pragma mark -
#pragma mark Data output.
#pragma mark -

void dataBeadSpring::forceCalculationStart()
{
	// Skip first state after a restart.
	if( _calcStressCount==0 && _continuation )
	{
		_calcStressNow = false;
	}
	else
	{
		_calcStressNow = !(_calcStressCount%_calcStressEvery);
	}
	
	// Clear the instantaneous stress tensor if about to be over-written.
	if( _calcStressNow )
		for( auto i=0; i<_dim; i++ )
			for( auto j=0; j<_dim; j++ )
				_sum_fi_rj_instantaneous[i][j] = 0.0;
}

void dataBeadSpring::forceCalculationStop() 
{ 
	_calcStressCount++;
	
	// Add instantaneous stress to the one used for data output. Only if stress was calculated. Leave non-zero.
	if( _calcStressNow )
	{
		_numStressCalcs++;

		for( auto i=0; i<_dim; i++ )
			for( auto j=0; j<_dim; j++ )
				_sum_fi_rj[i][j] += _sum_fi_rj_instantaneous[i][j];
	}
}

void dataBeadSpring::outputRowWithLabel( std::string label )
{
	// Add quantities handled by this class prior to outputting the table.
	_addSharedTableQuants();

	outputNextTableRow(_table.get());
	_table->outputRow(label);
}

void dataBeadSpring::outputBlockDataTimeSlice( double t )
{
	_block->addAttribute    ( __blockTimeSliceTimeAttr, t );
	_block->saveStartElement( __blockTimeSliceTag );
	outputNextBlockData     ( _block.get()        );	
	_block->saveEndElement  ( __blockTimeSliceTag );
}	

