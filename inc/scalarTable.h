//
// Handles construction and output of regular tables of (typically) scalar variables,
// normal usage being identical sets of variables being output at consecutive times.
// 
// Still pretty basic; intended usage is:
//
// scalarTable tab( out_filename, "Identifying text", &parameters_object, append=true/false );
//
// tab.addToCurrentRow( label, double_value )
// tab.outputRow( "Label_for_this_row" );
//
// - will output parameters in commented list in header (on construction), and in footer
// (any parameters subsequently added to the params object)
// - will send simple message to log file for each row
//
// Could include: Check integrity of table (i.e. ensure same labels output for each row);
// coninuation of existing table.
//
// 14/10/2010/DAH: Now has optional flag in constructor; if set, the table file is continued
// (i.e. opened and appended) rather than created.
//
// 23/10/2011/DAH: Can now initialise using and XMLParameters object; this automatically
// sets the output to be an XML table. Could provide an option to force e.g. XML tables of
// non-XML parameters objects at a later date.
//
// 22/3/2019/DAH: Removed support for the (non-XML) parameterParser during construction.
//


#ifndef _UD_TABLE_H
#define _UD_TABLE_H

// Standard includes
#include <map>								// Quantities temporarily stored in a map
#include <fstream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <algorithm>
#include <float.h>							// Output precision

// Non-standard includes
#include "parameterParser.h"


class scalarTable
{

private:

protected:

	bool _continuation;						// Extending (rather than creating) a table file

	map<std::string,double> _scalars;		// Quantities for each row stored here

	ofstream *_file;						// File for output

	std::string _filename;					// File name for the output file

	bool _outputtedHeadings;				// Flag to say the table headings have been output

	const parameterParser *_params;

	static std::string _removeWhiteSpace(std::string str)
		{ string copy(str); replace_if(copy.begin(),copy.end(),::isspace,'_'); return copy; }
											// Returns with new string with all white space replaced by '_'

public:

	scalarTable(std::string fname,const parameterParser*,bool=false);
	virtual ~scalarTable();

	// Add a value to the current row, or a sequence of values stored in a map<string,double>
	void updateCurrentRow( std::string label, double    val  ) { _scalars[label] = val; }
	void updateCurrentRow( std::map<std::string,double> vals ) { for( auto &iter : vals ) updateCurrentRow( iter.first, iter.second ); }
	
	// Output the current row to file (pass label for this row); option to output at maximum precision (default)
	void outputRow(std::string,bool maxPrecision=true);
};

#endif


