/**
 * @file dataBeadSpring.h
 * @author David Head
 * @brief Data handling class for the full beadSpring object.
 */


#ifndef BEAD_SPRING_DATA_BEAD_SPRING_H
#define BEAD_SPRING_DATA_BEAD_SPRING_H


//
// Includes.
//

// Standard includes.
#include <string>
#include <memory>
#include <vector>
#include <mutex>

// Local includes.
#include "dataGatheringBase.h"
#include "scalarTable.h"

// Project includes.
#include "errors.h"
#include "parameterParser.h"


/**
 * @brief Data handling class for the full beadSpring object.
 * 
 * Labels must be the same for each time point, so if outputting to a label for t>0,
 * must also output something initially (i.e. at t=0).
 */
class dataBeadSpring : public dataGatheringBase
{

private:

	// Filenames and tags for the output files.
	constexpr static auto __tableFileName = "scalars.xml";
	constexpr static auto __blockFileName = "block_data.xml";

	constexpr static auto __blockFileTag           = "Block_Data";
	constexpr static auto __blockTimeSliceTag      = "Time_Slice";
	constexpr static auto __blockTimeSliceTimeAttr = "time";

	// Labels for quantities handled by this class (i.e. those distributed across modules).
	constexpr static auto __dataLabel_pressure    = "P";
	constexpr static auto __dataLabel_shearStress = "sigma_xy";

	// Data options.
	bool __outputShearStress = false;
	
protected:

	// System dimensionality and volume.
	short  _dim   ;
	double _volume;

	// Handlers for the table and block-data output.
	std::unique_ptr<scalarTable>   _table;
	std::unique_ptr<flatXMLParser> _block;

	/// Flag for if continuing a run from a saved file.
	bool _continuation;
	
	//
	// Quantities that are distributed across modules.
	//
	bool _calcStressNow;

	long _calcStressCount;
	long _calcStressEvery;
	long _numStressCalcs ;

	std::mutex _stressMutex;

	std::vector< std::vector<double> > _sum_fi_rj;
	std::vector< std::vector<double> > _sum_fi_rj_instantaneous;

	void _addSharedTableQuants();

public:

	dataBeadSpring( short dim, std::string filePrefix, const parameterParser *params, bool append=false );
	virtual ~dataBeadSpring();	

	//
	// Accessors.
	//
	inline bool continuationRun() const noexcept { return _continuation; }		///< If starting from a saved state.
	
	//
	// General modifiers.
	//
	inline void setVolume        ( double V   ) noexcept { _volume = V;                 }
	inline void outputShearStress( bool yesNo ) noexcept { __outputShearStress = yesNo; }

	//
	// Add to data store to be output next timw.
	//
	void addScalar     ( std::string label, double val ) { _instantaneousValue(label,val); }
	void addToFixedHist( std::string label, double abscissa, double value, double min=0.0, double max=1.0, int numBins=10 )
		{ _addToFixedSizeHist(label,abscissa,value,1.0,min,max,numBins); }

	//
	// Quantities that are distributed across modules.
	//
	inline void   setTableCalcInterval ( long num ) noexcept { _calcStressEvery = std::min(num,_calcStressEvery); }
	       void   forceCalculationStart();	
	       void   forceCalculationStop ();
	inline bool   calculateStressNow   () const noexcept { return _calcStressNow; }
	inline double instantaneousStress  ( int i, int j ) const { return _sum_fi_rj_instantaneous[i][j]/_volume; }

	void addToStressTensor_f_r ( const double *f, const double *r, double weight=1.0 );
	void addToStressTensor_fr_r( double f_over_r, const double *r, double weight=1.0 );

	//
	// Data output to files.
	//
	void outputRowWithLabel      ( std::string label );
	void outputBlockDataTimeSlice( double t          );
};

#endif

