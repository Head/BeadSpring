/**
 * @file boxLeesEdwards.h
 * @author David Head
 * @brief Rectangular box with Lees-Edwards boundary conditions applied in the x-y plane.
 */



#ifndef BEAD_SPRING_BOX_LEES_EDWARDS_H
#define BEAD_SPRING_BOX_LEES_EDWARDS_H


//
// Includes.
//

// Standard includes.
#include <cmath>
#include <string>

// Project includes.
#include "boxRectPeriodic.h"



/**
 * @brief Rectangular box with Lees-Edwards boundary conditions applied in the x-y plane.
 * 
 * Abstract - Requires the shear strain at any given moment of time to be defined by a child class.
 */
class boxLeesEdwards : public boxRectPeriodic
{

private:

	// Data file output.
	constexpr static auto __currentShear = "LEShear";
	
	// State save file.
	constexpr static auto __saveCurrentShearTag  = "CurrentLEShear";
	constexpr static auto __saveCurrentShearAttr = "strain";

	// Current shear strain; set by child class using protected methods.
	double __shearStrain;

protected:

	void _setShearStrain( double gamma ) noexcept;						///< Sets the current strain.

	virtual void _saveBoxState( flatXMLParser *xml ) const override;	///< Modifies the box save to include the strain.

public:
	
	/// Main constructor.
	boxLeesEdwards( short dim, const parameterParser *params, std::shared_ptr<dataBeadSpring> data );
	virtual ~boxLeesEdwards() {}

	//
	// Boundary conditions that need to be modified for Lees-Edwards.
	//
	virtual void wrap( double *x ) const noexcept override;											///< Includes strain offset.
	virtual void offsetToPrimaryCell( const double *x, double *offset ) const noexcept override;	///< Includes strain offset.
};


#endif

