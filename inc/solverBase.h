/**
 * @file solverBase.h
 * @author David Head
 * @brief Abstract base class for the solver, i.e. the algorithm to iterate bead positions.
 */


#ifndef BEAD_SPRING_SOLVER_BASE_H
#define BEAD_SPRING_SOLVER_BASE_H


//
// Includes.
//

// Standard includes.
#include <string>

// Local includes.
#include "flatXMLParser.h"

// Project includes.
#include "dataBeadSpring.h"

/**
 * @brief Abstract base class for the solver, i.e. the algorithm to iterate bead positions.
 * 
 * Has a friend class for data gathering. No access required to the other modules.
 */
class solverBase
{

private:

	constexpr static auto __saveSolverTag = "Solver";
	
protected:

	short _dim;																///< Dimensionality.
	
	std::shared_ptr<dataBeadSpring> _data;

	// I/O.
	virtual void _saveSolverState   ( flatXMLParser *xml ) const {}
	virtual void _recoverSolverState( flatXMLParser *xml )       {}
	
public:

    // Constructors / destructor.
    solverBase() {}
    solverBase( short dim, std::shared_ptr<dataBeadSpring> data );
    virtual ~solverBase() {}

	//
	// Requirements for the solver.
	//
	virtual bool needVelocityVector() const = 0;
	
	//
	// Iteration.
	//
	virtual void updateVectors( double dt, int N, double *X, const double *F ) = 0;
	
	//
	// Updates for e.g. scheduled changes in solver parameters.
	//
	virtual void update( double t ) {}

	//
	// Data.
	//
	virtual void dataCalculateTable() const {}
	virtual void dataCalculateBlock() const {}

	//
	// I/O.
	//
	void saveState   ( flatXMLParser *xml ) const;
	void recoverState( flatXMLParser *xml );

	virtual std::string tableRowLabel() const noexcept = 0;
};



#endif

