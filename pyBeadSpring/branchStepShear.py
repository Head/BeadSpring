#
# Class to calculate data from branch runs generated from separate saved states,
# where all runs are of box type "boxLEStepShear" but the strain is zero before each
# branch point, and non-zero afterwards.
#

#
# Imports
#

# Standard imports
import os
import sys

# Local imports
import pyBeadSpring
import analyseData


#
# Class definition
#
class branchStepShear:

	#
	# Constructor: Call with the directory name up to but not including the "run?" subdirs.
	# Will automatically extract the required data and store persistently in instance data.
	#
	def __init__( self, rootName, quant="sigma_xy", verbose=False ):

		# Store the root pathname and arguments.
		self._rootName = rootName
		self._verbose  = verbose
		self._quant    = quant

		# Initialise the mean/error arrays and strain amplitude.
		self._preMeans , self._preErrors  = None, None
		self._postMeans, self._postErrors = None, None
		self._diffMeans, self._diffErrors = None, None
		self._gamma = None			# If ever extending this to oscillatory shear, would need to also store type.

		# Quantities other than the shear stress not yet properly supported (e.g. axes labels, normalisation).
		if self._quant != "sigma_xy":
			print( "WARNING: Normalisation and labelling assumes quant is shear stress 'sigma_xy'" )

		# Get the main runs and the required parameters. These objects are transient to this method;
		# converted here to the required information which is stored persistently.
		self._mainTables = pyBeadSpring.bsRuns(self._rootName,verbose=self._verbose).sc
		if self._verbose:
			print( "Parsed main runs on '{}'.".format(self._rootName) )

		# Get and store some required parameters.
		self._numStates = int  ( self._mainTables.getParameter("numStates") )
		self._endTime   = float( self._mainTables.getParameter("endTime"  ) )
		
		# Get the tables for each branch run, not including from the t=0 state.
		dataWithRuns = analyseData.genericDataGathering.dataRunsWithTimes

		self._branchTables = []
		for run in range(self.numRuns()):

			self._branchTables.append( [] )
			for branch in range(self.numBranches()+1):

				tableName = "shearScalars_{:04d}.xml".format(branch)
				table = dataWithRuns( os.path.join(self._rootName,"run{}".format(run),tableName), type="table", verbose=self._verbose )

				self._branchTables[-1].append( table )

				# Get the strain amplitude. Depends on strain type.
				if table.getParameter("boxType") != "LEStepShear":
					print( "Box type '{}' for branch runs not understand; cannot extract shear type and magnitude.".format(table.getParameter("boxType")) )
					sys.exit(-1)

				if self._gamma != None and self._gamma != table.getParameter("shearStrain"):
					print( "Step shear '{}' not the same for all branch runs.".format(table.getParameter("shearStrain")) )
					sys.exit(-1)

				self._gamma = table.getParameter("shearStrain")

	# Accessing parameters for the main run.
	def allParameters( self ):
		return self._mainTables.allParameters()

	# Returns the mean and error arrays for pre-, post- and difference stress data.
	# One mean/error pair per branch point, including t=0 (assumed all zeroes)
	def preStresses( self, scale=1.0 ):
		if self._preMeans == None:
			print( "Stresses not yet calculated" )
		else:
			return [ [scale*m for m in self._preMeans], [scale*e for e in self._preErrors] ]

	def postStresses( self, scale=1.0 ):
		if self._postMeans == None:
			print( "Stresses not yet calculated" )
		else:
			return [ [scale*m for m in self._postMeans], [scale*e for e in self._postErrors] ]

	def diffStresses( self, scale=1.0 ):
		if self._diffMeans == None:
			print( "Stresses not yet calculated" )
		else:
			return [ [scale*m for m in self._diffMeans], [scale*e for e in self._diffErrors] ]

	# Numbers of runs and branches.
	def numRuns( self ):
		return len(self._mainTables)

	def numBranches( self ):
		return self._numStates

	# Returns the time of the i'th branch point, where i=0 corresponds to th first state with t=0.
	def branchTime( self, i ):
		return i * self._endTime / self._numStates

	# The branch times.
	def branchTimes( self ):
		return [ self.branchTime(i) for i in range(self.numBranches()+1) ]

	# The strain amplitude.
	def getGamma( self ):
		return self._gamma

	#
	# Analyse the data. Options to display results (as plot or table).
	#
	def analyseData( self, showResults=False, showPlot=False ):
	
		# Initialise the per-run arrays.
		self._preStresses, self._postStresses, self._differences = [], [], []
		for run in range(self.numRuns()):
			self._preStresses .append( [] )
			self._postStresses.append( [] )
			self._differences .append( [] )

		# Loop over all of the main runs
		for run in range(self.numRuns()):
			mainSet = self._mainTables.getDataSets(self._quant,runAv=False)["curves"][run]

			# Inner loop over all of the extraction points, including t=0.
			for branch in range(self.numBranches()+1):
				branchSet = self._branchTables[run][branch].getDataSets(self._quant,runAv=False)["curves"][0]

				# Get first and last time indices that correspond to the same range as the previous branch data set.
				# If on the first branch, use the same time interval instead.
				first = 0
				if branch > 0:
					for index in range(len(mainSet["x"])):
						if mainSet["x"][index] <= self.branchTime(branch-1): first = index

				last = len(mainSet["x"])-1
				if branch < self.numBranches():
					for index in range(len(mainSet["x"])):
						if mainSet["x"][len(mainSet["x"])-index-1] >= self.branchTime(branch if branch>0 else branch+1): last = len(mainSet["x"])-index-1

				# Sanity check. Uncomment the lines below to see what's going on.
				if first >= last:
					print( "Cannot determine suitable range of main set for averaging." )

#				if run==0:
#					print( "For branch {0}, taking time range [index] from t={1} [{2}] to t={3} [{4}].".format(branch,mainSet["x"][first],first,mainSet["x"][last],last) )
				preAverage  = sum( mainSet["y"][first:last] ) / ( last - first )

				# The 'post-average' (i.e. after the strain is applied) takes the whole data set except the vey
				# first point. True for all initial times, including t=0.
				postAverage = sum( branchSet["y"][1:] ) / ( len(branchSet["y"])-1 )
		
				# Store in the arrays
				self._preStresses [run].append( preAverage  )
				self._postStresses[run].append( postAverage )
				self._differences [run].append( postAverage - preAverage )

		# Convert to arrays of mean and error values for each branch point
		self._preMeans, self._preErrors, self._postMeans, self._postErrors, self._diffMeans, self._diffErrors = [], [], [], [], [], []
		for branch in range(self.numBranches()+1):

			# Means and errors
			preMean , preError  = analyseData.core.errors.stderr( [ self._preStresses [run][branch] for run in range(len(self._mainTables)) ] )
			postMean, postError = analyseData.core.errors.stderr( [ self._postStresses[run][branch] for run in range(len(self._mainTables)) ] )
			diffMean, diffError = analyseData.core.errors.stderr( [ self._differences [run][branch] for run in range(len(self._mainTables)) ] )

			# Store persistently
			self._preMeans  .append( preMean   )
			self._preErrors .append( preError  )
			self._postMeans .append( postMean  )
			self._postErrors.append( postError )
			self._diffMeans .append( diffMean  )
			self._diffErrors.append( diffError )
		
		# If requested, display the results numerically
		if showResults:
			print( "Stress statistics [pre-, post- and difference, each as 'mean(error)' or 'mean \pm error']:" )
			for i in range(self.numBranches()+1):

				# More readable strings
				preString  = analyseData.core.errors.errorString( self._preMeans [i], self._preErrors [i] )
				postString = analyseData.core.errors.errorString( self._postMeans[i], self._postErrors[i] )
				diffString = analyseData.core.errors.errorString( self._diffMeans[i], self._diffErrors[i] )
	
				extractTime = self.branchTime(i)
				print( "For extraction point {0} at t={1}:\t{2}\t{3}\t{4}".format(i,extractTime,preString,postString,diffString) )

		# If requested, plot graphically
		if showPlot:

			# Guide lines: Zero stress, and each extraction point
			plt.axhline( 0.0, color="k", linestyle="-" )
			for j in range(self._numStates):
				plt.axvline( self.branchTime(j), color="k", linestyle="-" )

			# Plot the stresses for the main and extraction data, colour-matching the same runs
			for run in range(self.numRuns()):

				# Plot the main run first. Skip the first data point as corresponds to the random IC.
				mainData = self._mainTables.getDataSets(self._quant,runAv=False)["curves"][run]
				fmt      = analyseData.core.pyplotAux.fmt( run, None, 0 )			# Colour; Marker; Linestyle
				plt.plot( mainData["x"][1:], mainData["y"][1:], fmt, label=mainData["label"] )

				# Overlay with each of the extraction runs, with the same line format. Don't show the t=0 data.
				for branch in range(self.numBranches()):
					branchData = self._branchTables[run][branch].getDataSets(self._quant,runAv=False)["curves"][0]
					plt.plot( branchData["x"], branchData["y"], analyseData.core.pyplotAux.fmt(run,None,branch%2) )
		
					# Also plot the averaged quantities after the application of this strain.
					plt.plot( branchData["x"], [self._postStresses[run][branch]]*len(branchData["x"]), fmt, lw=2 )

			plt.xlabel( r"$t$", fontsize=18 )
			plt.ylabel( (r"$\sigma_{xy}$" if self._quant=="sigma_xy" else self._quant), fontsize=18 )	
			plt.legend()

			plt.show()


#
# Command line
#
if __name__ == "__main__":

	import matplotlib.pyplot as plt
	
	# Get the directory name
	import argparse
	parser = argparse.ArgumentParser( description="Extract sigma_xy (or another quantity) from branched 'extraction' runs" )
	parser.add_argument( "root", help="Root directory (up to but not including 'run..')" ) 
	parser.add_argument( "-r", "--raw"  , help="Show raw data (graphically and numerically) [False]", action="store_true" )
	parser.add_argument( "-q", "--quant", help="Plot this quantity", default="sigma_xy" )
	args = parser.parse_args()

	# Initialise the object and analyse the data
	obj = branchStepShear( args.root, args.quant )
	obj.analyseData( showResults=args.raw, showPlot=args.raw )
	
	#
	# Plot data for the differences; include dimensional scaling factor
	#

	# Get the correct calibration factors for this protein type
	print( "Using BSA calibration scales." )
	stressScale, timeScale = 14296.0, 0.648*1e-3

	# Plot.
	mean, error = obj.diffStresses(stressScale)
	plt.errorbar( [t*timeScale for t in obj.branchTimes()], mean, error, fmt="ro-" )
	plt.xlabel( r"$t\,/\,{\rm ms}$", fontsize=18 )
	plt.ylabel( r"$\sigma_{xy}\,/\,{\rm Pa}$", fontsize=18 )
	plt.show()
	

