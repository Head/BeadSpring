#
# Controls a series of states to generate multiple image files that can then
# be stitched together into a movie.
#
# TODO:
# - inital centre and eye, at least partly based on box dimensions.
# - rotation; bfWindow uses -0.01 for eye.phi at each step.
#


class bsMovieController:

    def __init__( self ):
        """Initialises to the default movie controller."""
        self._type = 0

    def currentTypeIndex( self ):
        """Returns the integer index of the currently selected movie controller."""
        return self._type

    def numTypes( self ):
        """Number of movie controller in this, or a derived, class."""
        return 2

    def description( self ):
        return (
            "Static",
            "Rotating"
        ) [ self._type % self.numTypes() ]

    def cycleToNext( self, allStates ):
        """Cycles to the next movie controller. Returns 'True' if changed, else 'False', the latter
        if (for instance) the states passed do not allow certain indices of movie controller."""

        # Cannot cycle if there is only one type.
        if self.numTypes() == 1:
            return False

        # Cannot cycle from 'static' to 'rotatintg' type, as this is only possible for 3D systens.
        if self._type == 0:

            if allStates.dim() == 2:
                print( "Movie controller type 'Rotating' not available for 2D systems." )
                return False
            else:
                self._type = 1
                return True
        
        # Can always cycle from 'rotating' to 'static'.
        if self._type == 1:
            self._type = 0
            return True


    #
    # Setting and changing the views.
    #
    def initialCentreAndEye( self, allStates ):
        """"Returns with the centre and eye for the start of the movie, or '(None,None)' to use the current view.
        If not '(None,None)', should return a duple of two lists:
         - (x,y,z) for the centre.
         - (R,theta,phi) for the eye.
        These are passed to the parent view class, so should be in the same format."""

        # The 'static' movie controller just leaves it as it was when the user selected movie generation.
        if self._type==0:
            return (None,None)
        
        # The 'rotating' type always starts at a typical angle for a rectangular box.
        if self._type==1:

            # Get a suitable distance from the box.
            X, Y, Z = allStates.X(), allStates.Y(), allStates.Z()
            R = ( X**2 + Y**2 + Z**2 )**0.5

            # Centre and eye. Numerical constants here decided upon by trial-and-error with small and large systems.
            return ( (X/2,Y/2,0.8*Z/2), (R,1.1,0.7) )
    
    def newCentreAndEye( self, currCentre, currEye, allStates ):
        """Returns new centre and eye vectors based on the current values and the states. Called in betweem
        each frame when generating images for a movie. Returns (None,None) if no change is required."""

        if self._type == 0:
            return (None,None)
    
        if self._type == 1:
            # 0.05 change in \phi per frame, in the expectation of around 100 frames for a typical movie,
            # so this would be around one complete circle.
            newEye = ( currEye[0], currEye[1], currEye[2] - 0.05 )
            return ( currCentre, newEye )
