

#include "flatXMLParser.h"



#pragma mark -
#pragma mark Exceptions
#pragma mark -
	
//
// Custom exceptions (nested)
//
class flatXMLParser::MismatchedTag : public runtime_error {
private:
	string makeWhat( string tag ) {	
		ostringstream out;
		out << "ERROR reading XML file: tag '" << tag << "' found in start element not found in end element"
		    << " - is nesting present? [if so, may need to skip this start tag]" << std::endl;
		return out.str();
	}
public:
	MismatchedTag( string startTag ) : runtime_error( makeWhat(startTag) ) {};
};
class flatXMLParser::UnknownAttribute : public runtime_error {
private:
	string makeWhat( string attribute ) {
		ostringstream out;
		out << "flatXMLParser: Could not find attribute '" << attribute << "'" << endl;
		return out.str();
	}
public:
	UnknownAttribute( string attr ) : runtime_error( makeWhat(attr) ) {};
};


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

//
// Initialisation / destruction
//

// Construction options
flatXMLParser::flatXMLParser() { _init(); };
flatXMLParser::flatXMLParser( std::string filename, ios_base::openmode mode, bool overwriteLastTag )
{
	_init();
	
	// For input and output streams, just open at the beginning
	if( mode & ios::in  ) inputStream  = new ifstream( filename.c_str(), mode );
	if( mode & ios::out ) outputStream = new ofstream( filename.c_str(), mode );

	// If 'appending' to an existing flat-XML file, assume the last line is the global end-element and overwrite it
	if( mode & ios::app )
	{
		if( overwriteLastTag )
		{
			// Open up an input-output stream
			fstream *inOutStream = new fstream( filename.c_str(), ios::in | ios::out );

			// Try to find the last "<" in the file
			int i = 1;
			while( inOutStream->good() )							// Fails if e.g. seekg() tries to access before the start of file
			{
				inOutStream->seekg( -i, ios_base::end );			// negative relative to end (nb: cannot try i==0)
				if( inOutStream->peek() == 60 )						// ASCII 60 = "<"
				{
					inOutStream->seekp( -i, ios_base::end );		// Move the put pointer to the same place
					break;
				}
				else i++;
			}
			
			// Recast as an ostream, so remaining routines can be used as normal (but seekg()/peek() no longer available)
			outputStream = inOutStream;

		// If not overwriting, open as an ostream in append mode
		} else outputStream = new ofstream( filename.c_str(), ios::app );
	}
}
flatXMLParser::flatXMLParser( istream &in  )
	{ _init(); inputStream  = &in; _openedHere = false; }
flatXMLParser::flatXMLParser( ostream &out )
	{ _init(); outputStream = &out; _openedHere = false; }

// Generic initialisation routine; always called first
void flatXMLParser::_init()
{
	inputStream  = NULL;
	outputStream = NULL;
	_openedHere  = true;
}

// Destructor
flatXMLParser::~flatXMLParser()
{
	// If the stream objects were created here, must delete them (will also flush the output stream)
	if( _openedHere )
	{
		delete inputStream;
		delete outputStream;
	}
	else
	{
		// If not, just flush the output stream
		if( outputStream ) outputStream->flush();
	}
};



#pragma mark -
#pragma mark Saving
#pragma mark -
	
//
// Block construction and output
//

// Start tag for a whole XML file
void flatXMLParser::XMLStartTag()
{
	// Check the output stream is open
	if( outputStream == NULL )
		throw runtime_error( "Cannot output XML start tag; no output stream defined [in XMLParser::XMLStartTag()]" );
		
	*outputStream << "<?xml version='1.0'?>" << std::endl;
}

// Build-up a list of attributes
void flatXMLParser::clearAttributes() { attributes.clear(); }
void flatXMLParser::addAttribute( string label, string value ) { attributes[label] = value; }
//void flatXMLParser::addAttribute( string label, bool value )		// WARNING: strings get automatically cast as bools,
//{																	// so this method is called for strings.
//	attributes[label] = ( value ? "true" : "false" );
//}
void flatXMLParser::addAttribute( string label, int value )
{
	stringstream out;
	out << value;
	attributes[label] = out.str();
}
void flatXMLParser::addAttribute( string label, unsigned long value )
{
	stringstream out;
	out << value;
	attributes[label] = out.str();
}
void flatXMLParser::addAttribute( string label, long value )
{
	stringstream out;
	out << value;
	attributes[label] = out.str();
}
void flatXMLParser::addAttribute( string label, unsigned int value )
{
	stringstream out;
	out << value;
	attributes[label] = out.str();
}
void flatXMLParser::addAttribute( string label, double value )			// Uses current precision; note converted to string now
{
	stringstream out;
	out << value;
	attributes[label] = out.str();
}
void flatXMLParser::addFloatAttributeWithPrecision( string label, double value, int prec )
{
	stringstream out;
	out.precision(prec);
	out << value;
	attributes[label] = out.str();
}


// Construct the data string
void flatXMLParser::clearDataString() { data.clear(); }

void flatXMLParser::appendDataString( double *array, int length )
{
	for( int i=0; i<length; i++ )
		appendDataString( array[i] );
}

// Save block with given tag name
void flatXMLParser::saveBlock( string tag )
{
	saveStartElement( tag, data.empty() );		// Start element; if not data, close immediately
	if( !data.empty() )
	{
		saveData();
		saveEndElement( tag );
	}
}

// Save block in sections
void flatXMLParser::saveStartElement( string tag, bool andClose )
{
	// Check the output stream is open
	if( outputStream == NULL )
		throw runtime_error( "Cannot output block; no output stream defined [in XMLParser::saveBlock()]" );
	
	// Output the start element, with the attributes
	*outputStream << "<" << tag;
	
	for( map<string,string>::const_iterator iter = attributes.begin(); iter != attributes.end(); iter++ )
		*outputStream << " " << iter->first << "='" << iter->second << "'";
	
	// Close-up, possible with the end-block signifier "/"
	*outputStream << ( andClose ? " />" : ">" ) << std::endl;
	
	// Clear the attributes
	attributes.clear();
}

// Save data block
void flatXMLParser::saveData()
{
	// Check the output stream is open
	if( outputStream == NULL )
		throw runtime_error( "Cannot output block; no output stream defined [in XMLParser::saveBlock()]" );

	// Output the data; already in string format
	*outputStream << data << std::endl;
	
	// Clear the data string
	data.clear();
}

// Save a closing element
void flatXMLParser::saveEndElement( string tag )
{
	// Check the output stream is open
	if( outputStream == NULL )
		throw runtime_error( "Cannot output block; no output stream defined [in XMLParser::saveBlock()]" );

	// Output the end element
	*outputStream << "</" << tag << ">" << std::endl;		
}



#pragma mark -
#pragma mark Loading
#pragma mark -
	
//
// Block extraction; general version; not meant to be called directly
//
string flatXMLParser::read_block( string matchTag, bool reset, bool startOnly )
{
	// Is the input stream open?
	if( inputStream == NULL )
		throw runtime_error( "Cannot read input stream; not initialised [in XMLParser::read_block()]" );

	// Reset get-pointer to the start of the file
	if( reset ) inputStream->seekg(0,ios::beg);
	
	// Reset the attributes map and the data array
	attributes.clear();
	array.clear();
	string tag;

	// State of XML block during parsing
	bool
		inElement      = false,
		inStartElement = false,
		inDataBlock    = false,
		inEndElement   = false;

	// Walk through the file, extracting one whitespace-delimited segment at a time but prepending attributes labels
	// to values when there is a space after the label and before the equals sign.
	string segment, prevSegment="";
	while( *inputStream >> segment )
	{
		// If starting from the beginning of an XML file, can ignore the XML specification
		if( segment.find("?xml") != string::npos ) continue;

		// Look for the start of an XML tag
		if( segment.find("<")  != string::npos ) inElement    = true;
		if( segment.find("</") != string::npos ) inEndElement = true;

		// Look for start element with the required tag name (from "<" to first whitespace if no space between "<" and tag)
		if( inElement && tag.empty() && !inEndElement )
		{
			inStartElement = true;
			
			// Remove "<", ">" from either end
			tag = segment;
			if( tag.find("<")==0            ) tag = tag.substr(1,tag.size());
			if( tag.find(">")==tag.size()-1 ) tag = tag.substr(0,tag.size()-1);
			
			// If a matching-tag was provided, check this is the same; if not, skip to the next block
			if( !matchTag.empty() and matchTag.compare(tag)!=0)		// compare() returns 0 for match
			{
				inStartElement = inElement = false;		// Since not in start element, will not proceed to data block
				tag.clear();
				continue;
			}
		}

		// Parse attributes.
		if( inStartElement && segment.find("=")!=string::npos )
		{
			string
				label = segment.substr(0,segment.find("=")),
				value = segment.substr(segment.find("=")+1);
			
			// If the value ends in '/>', remove it. Note that it is still at the end of 'segment' and
			// so will still cause the 'break' a little firther down.
			if( value.find("/>")!=string::npos )
				value = value.substr( 0, value.find("/>") );

			// If the label is empty, assume there was whitespace between the label and the '=', in which case
			// the label should have been stored as the previous segment.
			if( label.empty() ) label = prevSegment;

			// Remove any trailing ">".
			if( value.rfind(">") == value.size()-1 )
				value = value.substr(0,value.size()-1);
			
			// If the value is enclosed by single quotes, remove them.
			if( value.find("'")==0 and value.rfind("'")==value.size()-1 )
				value = value.substr(1,value.size()-2);
			
			// Do the same for double quotes (although the output routines here don't do this).
			if( value.find('"')==0 and value.rfind('"')==value.size()-1 )
				value = value.substr(1,value.size()-2);

			// Add to the attribute map
			attributes[label] = value;
		}
		
		// Set the previous segment to the current straught after it may be potentially used in the next loop.
		prevSegment = segment;

		// Leaving a single-tag object; note no space allowed between the "/" and ">" characters
		if( inStartElement && segment.find("/>") != string::npos ) break;
		
		// Leaving a data block start tag; assume data block starts now; this must go after the check for "/>"
		if( inStartElement && segment.find(">") != string::npos )
		{
			inStartElement = inElement = false;
			inDataBlock = true;
			if( startOnly ) break; else continue;
		}

		// Leaving data block; entering the end tag
		if( inDataBlock && segment.find("<") != string::npos )
		{
			inDataBlock = inStartElement = false;
			inEndElement = inElement = true;
		}

		// If in end element, check the tags match; if so, bail now (unless trying to match a tag that's not yet been found)
		if( inEndElement && segment.find(tag)!=string::npos && !startOnly && !(!matchTag.empty() && tag.empty()) ) break;
		
		// If reached the end of the end-element and still not 'broken' by the line above, must be a mistake
		if( inEndElement && segment.find(">")!=string::npos && !startOnly && !(!matchTag.empty() && tag.empty()) )
			throw MismatchedTag( tag );
		
		// In data block; parse each whitespace-delimited segment as a double and append to the array
		if( inDataBlock )
			array.push_back( atof(segment.c_str()) );			// atof() returns 0.0 if failed to parse as a number

		// Leaving a tag
		if( segment.find(">") != string::npos ) inElement = inEndElement = inStartElement = false;
	}

	return tag;
}


// Returns string representation of the value; raises an out_of_range exception if does not exist
string flatXMLParser::attr_or_exception( string label )
{
	// Does the key exist?
	map<string,string>::const_iterator iter = attributes.find(label);
	if( iter == attributes.end() ) throw UnknownAttribute( label );
	
	return iter->second;
}

// Returns 'true' if the named attribute exists, else 'false'
bool flatXMLParser::hasAttribute( string label )
{
	map<string,string>::const_iterator iter = attributes.find(label);
	return ( iter != attributes.end() );
}



