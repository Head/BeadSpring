#include "boxBase.h"

#pragma mark -
#pragma mark Contructors / destructor
#pragma mark -

boxBase::boxBase( short d, const parameterParser *params, std::shared_ptr<dataBeadSpring> data )
		: _dim(d), _params(params), _data(data), _neighbourList2D(nullptr), _neighbourList3D(nullptr)
{
	using namespace parameterLabels;

	// To determine nearby beads, can either use NN (aka Verlet) lists, or just a straight cell-sorter.
	// - if no NN-list parameters are supplied, will assume a cell sorter.
	if( params->hasScalarParam(box_NNRangeFactor)
		|| params->hasIntegerParam(box_NNMaxNeighbours)
		|| params->hasIntegerParam(box_NNSortFrequency) )
	{
		_useNeighbourLists = true;

		// Get parameters for the NN/Verlet lists.
		_NNRangeFactor   = _params->scalarParamOrDefault (box_NNRangeFactor  ,2.0            );
		_NNMaxNeighbours = _params->integerParamOrDefault(box_NNMaxNeighbours,(_dim==2?30:60));
		_NNSortFrequency = _params->integerParamOrDefault(box_NNSortFrequency,10             );
		
		if( _NNRangeFactor   <= 1.0 ) errors::throwException("NN range scaling factor '"+box_NNRangeFactor+"' must be >1"       ,__FILE__,__LINE__);
		if( _NNMaxNeighbours <  2   ) errors::throwException("NN max. neighbours '"+box_NNMaxNeighbours+"' must be at least 2"  ,__FILE__,__LINE__);
		if( _NNSortFrequency <  1   ) errors::throwException("NN re-sort frequency '"+box_NNSortFrequency+"' must be at least 1",__FILE__,__LINE__);
	}
	else
	{
		_useNeighbourLists = false;			// Will cell sort at every time step.
	}
}


#pragma mark -
#pragma mark Geometry
#pragma mark -

void boxBase::randomOrientation( double *t ) const noexcept
{
	// Depends on dimension.
	if( _dim==2 )
	{
		double theta = 2*M_PI * pRNG::stdUniform(0);
	
		t[0] = cos(theta);
		t[1] = sin(theta);
	}

	if( _dim==3 )
	{
		double
			phi = 2*M_PI * pRNG::stdUniform(0),
			rho = 2*pRNG::stdUniform(0) - 1.0,
			sq = sqrt( 1 - rho*rho );

		t[0] = cos(phi) * sq;
		t[1] = sin(phi) * sq;
		t[2] = rho;
	}
}


#pragma mark -
#pragma mark Cell sorting or NN/Verlet lists
#pragma mark -

void boxBase::updateInteractionList( int N, const double *X )
{
	// Cell sort only. Use "_pairPartitioning" rather than "_cellPartitioning" to improve load balancing.
	if( !_useNeighbourLists )
	{
		if( _dim==2 )
		{
			_cellSort2D->sortParticles_xyz( N, X );
			_cellSort2D->generatePairLists_pairPartitioning();
		}
		else
		{
			_cellSort3D->sortParticles_xyz( N, X );
			_cellSort3D->generatePairLists_pairPartitioning();
		}

		return;
	}

	// If still here, assume using a NN/Verlet list.
	// The final flag switches between a faster partitioning suitable for homogeneous systems (true), and a slower one for heterogeneous
	// systems that has better load balancing (false). Testing runs on an N=10000 homogenous system showed little difference, so choose
	// the load balancing version by default.
	if( _dim==2 )
		_neighbourList2D->updateLists_xyz( N, X, false );
	else
		_neighbourList3D->updateLists_xyz( N, X, false );

		// Can check for missed contacts. VERY SLOW; not for production runs.
	//	if( _dim==2 )
	//		_neighbourList2D->checkForMissedContacts_xyz( N, X, _bareMaxRange );
	//	else
	//		_neighbourList3D->checkForMissedContacts_xyz( N, X, _bareMaxRange );
}

unsigned long boxBase::numPairsInList( unsigned int threadNum ) const
{
	if( _useNeighbourLists )
		return ( _dim==2 ? _neighbourList2D->numPairsInList(threadNum) : _neighbourList3D->numPairsInList(threadNum) );
	else
		return ( _dim==2 ? _cellSort2D->numPairsInList(threadNum) : _cellSort3D->numPairsInList(threadNum) );
}

const unsigned long* boxBase::perThreadPairList( unsigned int threadNum ) const
{
	if( _useNeighbourLists )
		return ( _dim==2 ? _neighbourList2D->perThreadPairList(threadNum) : _neighbourList3D->perThreadPairList(threadNum) );
	else
		return ( _dim==2 ? _cellSort2D->perThreadPairList(threadNum) : _cellSort3D->perThreadPairList(threadNum) );
}



#pragma mark -
#pragma mark I/O
#pragma mark -

void boxBase::saveState( flatXMLParser *xml ) const
{
	xml->saveStartElement( __saveBoxTag );
	_saveBoxState(xml);
	xml->saveEndElement( __saveBoxTag );
}

void boxBase::recoverState( flatXMLParser *xml )
{
	// Can read any attributes for main tag now.

	// May be overriden by child class.
	_recoverBoxState(xml);
}
