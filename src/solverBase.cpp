#include "solverBase.h"

#pragma mark -
#pragma mark Constructors / destructor.
#pragma mark -

solverBase::solverBase( short dim, std::shared_ptr<dataBeadSpring> data )
	: _dim(dim), _data(data)
{
}


#pragma mark -
#pragma mark I/O
#pragma mark -

void solverBase::saveState( flatXMLParser *xml ) const
{
	xml->saveStartElement( __saveSolverTag );
	_saveSolverState(xml);
	xml->saveEndElement  ( __saveSolverTag );	
}

void solverBase::recoverState( flatXMLParser *xml )
{
	// Can read any attributes for main tag now.
	
	// May be overriden by a child class.
	_recoverSolverState(xml);
};
