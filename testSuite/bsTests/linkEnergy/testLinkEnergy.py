#
# Tests that the link energy (tested on dumbells with no excluded volume interaction) obeys equipartition,
# i.e. (1/2)kT per degree of freedom. Only valid if beads never come close to overlapping, else this would
# creep up to (d/2)kT in d-dimensions, so must keep spring stiff enough to keep them apart.
#

# Standard imports
import math
import scipy.optimize

# Base class
from bsTests import bsTestBase

class testLinkEnergy( bsTestBase.bsTestBase ):

	def __init__( self, executable, verbosity, plot, fname, time ):

		# Store private variables. Initialise before calling parent constructor (which needs these).
		self.__stateFile  = fname
		self.__extendTime = time

		# Call parent method with required flags.
		super(testLinkEnergy,self).__init__(executable,verbosity,plot)


	# Test method.
	def performTests( self ):

		# Sanity check.
		if self._blockData.getParameter("M") != 1:
			raise NotImplementedError( "Link energy equipartition test assumes a monomeric system (i.e. M=1)" )

		if self._blockData.getParameter("N") != 2:
			raise NotImplementedError( "Link energy equipartition test assumes a dumbell (i.e. N=2, M=1)" )

 		# Get the required parameters. See all by printing self._table.allParameters().
		kT = self._table.getParameter( "kT" )

 		# Get the link energy against time.
		t, E = [ self._table.getDataSets("E_links")["curves"][0][label] for label in ("x","y") ]

		# Get the mean energy and error.
		def fn( x, a ):
			return [ a for _x in x ]

		try:
			if self._verbose: print( "Trying to fit single link energy to a constant value." )
			fit = scipy.optimize.curve_fit( fn, t, E, p0=[0.5] )
		except:
			raise bsTestBase.FailedTestError( "Failed to fit the link energy to a constant; presuming test failed." )

		# Get mean and error, and allowed deviation in terms of sigma.
		mean, error = fit[0][0], math.sqrt(fit[1][0][0])
		nSigma = testLinkEnergy.tolSigma()

		# Plotting (optional).
		if self._plot:
			import matplotlib.pyplot as plt

			plt.plot( t, E, "-or", label=r"$E_{\rm link}$" )

			plt.plot( t, [mean]*len(t), color="b", label=r"${\rm Mean}\pm" + str(nSigma) + r"\,\sigma$" )
			plt.fill_between( t, [mean+nSigma*error]*len(t), [mean-nSigma*error]*len(t), color="b", alpha=0.25 )

			plt.plot( t, [0.5*kT]*len(t), color="k", lw=2, label=r"$\frac{1}{2}k_{\rm B}T$" )

			plt.xlabel( r"$t$" )
			plt.ylabel( r"$E$" )
			plt.legend()

			plt.show()

		# Check.
		if abs(mean-0.5*kT) >  nSigma * error:
			raise bsTestBase.FailedTestError( "Measured mean differs from equipartition value by more than {} sigma.".format(nSigma) )

		if self._verbose:
			print( "Measured mean within {} sigma of 0.5 kT; passed.".format(nSigma) )


	# Arguments for the executable.
	def _stateFile ( self ): return self.__stateFile
	def _extendTime( self ): return self.__extendTime

	# Parameter for testing.
	@classmethod
	def tolSigma( cls ): return 3.0

	# Cleanup; can use wildcards.
	@classmethod
	def filesToDelete ( self ): return ["block_data.xml","state_????.*","scalars.xml","log.out"]




	#
	# Class methods
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):

		extendTime = 500.0
		for testNum in range(2):
			fname = "state_test{}.xml".format(testNum)
			print( "Performing link energy equipartition test starting from state '{}'".format(fname) )
			testLinkEnergy(executable,verbosity,plot,fname,extendTime)
			print( " - passed" )
