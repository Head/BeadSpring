//
// Code to simplify saving and loading of files in XML format without nesting
// (hence the prefix 'flat')
//
// This is NOT intended to be a general XML parser, but rather a collection of
// routines to simplify the types of XML blocks that often arise when loading
// and saving data from simulations. The format is limited to (per block):
// - Start tag with list of attributes
// - Data block (ASCII text of floats [nb: binary data not part of XML standard])
// - End tag
//
// If there is no data, only a single element (with attributes) of the form "<tag ... />" is output
//
// Note also that the reading routines are not very robust, and are only guaranteed to work with
// files output by this class. Any nesting needs to be handled 'by hand' by skipping the start elements
// or using readBlockWithTag().
//
// 
// Initialisation options:
// ======================
//
// flatXMLParser a( filename, ios::in/out/app, overwriteLastTag=true/false );
//												// Opens "filename" for input/output/append (as per second argument);
//												// will automatically 'delete' in destructor. If appending, and the
//												// overwriteLastTag flag is 'true' [defaults to false], will overwrite the
//												// final '<...>' block before writing anything new. This is useful for e.g.
//												// continuing output with a single, enclosing, global tag for the whole file.
// flatXMLParser a( istream );						// Uses given stream for input
// flatXMLParser a( ostream );						// Uses given stream for output; will flush but NOT delete in destructor
//
//
// Output routines:
// ===============
//
// a.XMLStartTag();  							// Outputs the start element for a whole XML file
// a.addAttribute(label,value);					// Adds an attribute; 'value' can be int, float or string
// a.appendDataString(value);					// Extends the data string; can be arrays, vectors (see class definition for
//												// current options; easy to extend in future if need be)
// a.saveBlock(tagName);						// Outputs element with given tag name; if no data, just a start element
//												// Clears the data string and the attributes map
//
// Or, can break up saving of the block:
//	virtual void saveStartElement(string,bool);				// Pass tag; outputs and clears attributes. If 'true', self-closing
//  virtual void saveStartElementWithPrecision(string,int);	// Will temporarily change precision and calls saveStartElement(string)
//	virtual void saveData();								// Outputs and clears the data array
//	virtual void saveEndElement(string);					// Pass tag
//
// Input routines:
// ==============
//
// a.readBlockWithTag(tag,reset);				// Read in next block with given tag name; flag to restart stream
// tag = a.readNextBlock();						// Returns empty string if no further block found
// a.readStartElementWithTag(tag,reset);		// Only goes as far as the start tag (and attributes); data is cleared.
//												// This way, if there is an enclosing tag for the whole file, can get
//												// it's attributes without worrying about nesting.
//
// double* a.dataArray();						// Pointer to first element of the data block
// vector<double>* a.dataVector();				// - alternative form for same
// a.attribute(string label);					// Returns the string representation of the attribute 'label', or throws
//												// and out-of-range exception if none. Also options provided with built-in
//												// conversion, "attributeAsInt()" etc.; can easily expand as needed.
//
// Other routines:
// ==============
//
// a.resetIStream();							// Resets any defined input stream to the beginning
//



#ifndef _UD_FLATXMLPARSER_H
#define _UD_FLATXMLPARSER_H


// Standard includes
#include <iostream>
#include <fstream>
#include <map>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <deque>
#include <set>
#include <cstdlib>


using namespace std;


class flatXMLParser
{

private:

	// Initialisation
	void _init();
	
	bool _openedHere;						// Used to decide whether or not to delete the streams in the destructor

protected:

	// Input and output streams; "NULL" if not opened
	istream *inputStream;
	ostream *outputStream;

	// Tag name
	string tag;
	
	// Attributes stored as an STL map
	map< string, string > attributes;
	
	// Data string (for outputing) and data array (for reading)
	string data;
	vector<double> array;
	
	// Returns the string representation of the required attribute, or raises an out-of-range exception if not found
	virtual string attr_or_exception(string);

	// Does the actual parsing
	virtual string read_block( string matchTag="", bool reset=false, bool startOnly=false );
	
public:	
	
	// Initialisation / destruction
	flatXMLParser();
	flatXMLParser(std::string,ios_base::openmode,bool overwriteLastTag=false);
	flatXMLParser(istream&);
	flatXMLParser(ostream&);
	virtual ~flatXMLParser();
	
	//
	// General routines
	//
	virtual void resetIStream() { if( inputStream  ) inputStream ->seekg(0,ios::beg); }
	virtual ostream* getOutputStream() { return outputStream; }
	virtual istream* getInputStream()  { return inputStream;  }
	

	//
	// Saving
	//
	
	// Start tags to an XML file
	virtual void XMLStartTag();
	
	// Build-up a list of attributes; string label and various options for value type
	virtual void clearAttributes();
	virtual void addAttribute(string,string);
//	virtual void addAttribute(string,bool);		// WARNING: strings get automatically cast as bool
	virtual void addAttribute(string,int);
	virtual void addAttribute(string,unsigned int);
	virtual void addAttribute(string,long);
	virtual void addAttribute(string,unsigned long);
	virtual void addAttribute(string,double);
	virtual void addFloatAttributeWithPrecision(string,double,int);
	
	//
	// Add to the data string; again, options provided for data type with templating where possible. All items are space-delimited
	//
	virtual void clearDataString();

	// Single values.
	template< class T >
	void appendDataString( T val )
	{
		if( !data.empty() ) data.append( " " );
		stringstream out;
		out << val;
		data.append( out.str() );	
	}
	
	// Vectors.
	template< class T >
	void appendDataString( const std::vector<T>& vec )
	{
		for( auto &v : vec ) appendDataString( v );
	}

	// Deques.
	template< class T >
	void appendDataString( const std::deque<T> &deq )
	{
		for( auto &v : deq ) appendDataString( v );
	}
	
	// Maps.
	template< class T1, class T2 >
	void appendDataString( const std::map<T1,T2> &dict )
	{
		for( auto &iter : dict )
		{
			appendDataString( iter.first  );
			appendDataString( iter.second );
		}	
	}

	// Vector of pairs.
	template< class T1, class T2 >
	void appendDataString( const std::vector< std::pair<T1,T2> > &vec )
	{
		for( auto &iter : vec )
		{
			appendDataString( iter.first  );
			appendDataString( iter.second );
		}	
	}
	
	// Sets of pairs.
	template< class T1, class T2 >
	void appendDataString( const std::set< std::pair<T1,T2> > &vec )
	{
		for( auto &iter : vec )
		{
			appendDataString( iter.first  );
			appendDataString( iter.second );
		}	
	}

	// Misc.
	virtual void appendDataString(double*,int);			// Adds given length of array, starting from element 0

	// Save block with given tag name
	virtual void saveBlock(string);
	
	// Save individual components of the block
	virtual void saveStartElement(string, bool andClose = false);	// Pass tag; outputs and clears the attributes
	virtual void saveData();										// Outputs and clears the data array
	virtual void saveEndElement(string);							// Pass tag
	
	//
	// Loading.
	//
	
	// Read next block with given tag; flag to reset istream to beginning. Returns 'false' if not found
	virtual bool readBlockWithTag( string tag, bool reset = false )
		{ return ( read_block(tag,reset).compare(tag) == 0 ); }

	// Read next block; returns tag string (empty if no more blocks)
	virtual string readNextBlock() { return read_block(); }

	// Only goes as far as the start tag (and attributes); data is cleared.
	virtual void readStartElementWithTag( string tag, bool reset = false ) { read_block(tag,reset,true); };

	// Next start element (any tag); goes no further. Returns with the tag
	virtual string readNextStartElement( bool reset = false ) { return read_block("",reset,true); };

	// Data handling options
	virtual inline double*         dataArray () { return &array[0];    }		// Pointer to start of the data array
	virtual inline vector<double>* dataVector() { return &array;       }		// Pointer to the vector object
	virtual inline long            dataSize  () { return array.size(); }		// Size of the data array

	virtual void pushBackData( deque<double> *deq )
		{ for( unsigned long i=0; i<array.size(); i++ ) deq->push_back( array[i] ); }
	virtual void pushBackData( deque<float>  *deq )
		{ for( unsigned long i=0; i<array.size(); i++ ) deq->push_back( array[i] ); }
	
	// Attribute handling options
	bool hasAttribute(string);							// True/false if named attributed exists
	int numAttributes()
		{ return attributes.size(); }					// No. of attributes
	map<string,string> attributesMap()						
		{ return attributes; }							// A copy of the current attributes map.
	string attribute( string label )
		{ return attr_or_exception(label); };			// Returns value string; raises error if not found
	int attributeAsInt( string label )
		{ return atoi( attribute(label).c_str() ); };	// Conversion to integer
	double attributeAsDouble( string label )
		{ return atof( attribute(label).c_str() ); };	// Converions to float	
		
	//
	// Custom exceptions
	//
	class MismatchedTag;
	class UnknownAttribute;
};

#endif


