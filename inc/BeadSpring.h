/**
 * @file BeadSpring.h
 * @author David Head
 * @brief Main class that contains solver, box, beads and crosslinks as other classes. 
 */


#ifndef BEAD_SPRING_H
#define BEAD_SPRING_H


//
// Includes.
//

// Standard includes.
#include <iostream>
#include <stdexcept>
#include <string>
#include <float.h>
#include <map>
#include <sstream>
#include <omp.h>

// Local includes.
#include "timeStepper.h"
#include "multiTimer.h"
#include "sequentialFilenames.h"
#include "flatXMLParser.h"

// Project includes: Modules.
#include "boxRectPeriodic.h"
#include "boxLEStepShear.h"
#include "boxLEOscillatory.h"
#include "linksBase.h"
#include "linksBeadSpecific.h"
#include "solverBase.h"
#include "solverBrownianDynamics.h"
#include "beadsBase.h"

// Project includes: General.
#include "logging.h"
#include "errors.h"
#include "dataBeadSpring.h"
#include "parameterParser.h"			// Based on XMLParameters but simplified to avoid requiring libxml2.


/**
 * Namespace for parameter labels.
 */
namespace parameterLabels
{
	const std::string bsDimension = "dimension";
	const std::string bsEndTime   = "endTime";
	const std::string bsTimeStep  = "dt";

	const std::string bsBoxType    = "boxType";
	const std::string bsLinksType  = "linksType";
	const std::string bsBeadsType  = "beadsType";
	const std::string bsSolverType = "solverType";
	
	const std::string bsNumSaveStates = "numStates";
	const std::string bsNumTableRows  = "numTableRows";
	const std::string bsNumBlocks     = "numBlocks";

	const std::string bsInitialConfig = "initialConfig";
	const std::string bsIC_isotropic  = "isotropic";
	const std::string bsIC_trilattice = "trilattice";

	const std::string bsTerm_percDim = "terminatePercDim";		///< Lower percolation dimension for termination (optional).
}


/**
 * @brief Main class that contains solver, box, beads and crosslinks as other classes. 
 * 
 * Includes four modules, each starting from a base class:
 * - **box** : Base class `boxBase`. Handles geometry, any periodic BCs *etc.*
 * - **solver** : Base class `solverBase`. How the bead positions are iterated in time.
 * - **beads** : Base class `beadsBase`. Handles filament stretching and bending, and other interactions.
 * - **links** : Base class `linksBase`. Handles creation, destruction *etc.* of crosslinks.
 */
class BeadSpring
{

private:

	constexpr static auto __savePrefix         = "state";
	constexpr static auto __saveMainTag        = "BeadSpring";
	constexpr static auto __saveTimeAttr       = "time";
	constexpr static auto __saveSaveEveryAttr  = "saveEvery";
	constexpr static auto __saveTableEveryAttr = "tableEvery";
	constexpr static auto __saveBlockEveryAttr = "blockEvery";
	
	
	constexpr static auto __clockLabelMessage   = "message";
	constexpr static auto __clockLabelSave      = "save";
	constexpr static auto __clockLabelTableCalc = "tableCalc";
	constexpr static auto __clockLabelTableOut  = "tableOut";
	constexpr static auto __clockLabelBlockCalc = "blockCalc";
	constexpr static auto __clockLabelBlockOut  = "blockOut";
	
	constexpr static auto __dataLabelTime = "t";
	constexpr static auto __dataLabelIC   = "IC";
	
	constexpr static auto __timerLabelGetNearby     = "NN lists, cell sorting and nearby nodes";
	constexpr static auto __timerLabelCalcForces    = "Calculate all forces";
	constexpr static auto __timerLabelUpdateSystem  = "Update system configuration using solver";
	constexpr static auto __timerLabelBeadsAndLinks = "Update beads and links objects";
	constexpr static auto __timerLabelOutputAndSave = "Data output and state saves";

	/// Interval for table calculations (not output). 1.0 is typically too small; can become 15% of run time.
	constexpr static auto __dataTableCalcInterval = 10.0;
	
	/// Interval for block calculations (not output).
	constexpr static auto __dataBlockCalcInterval = 10.0;

protected:

	short _dim;										///< Dimensionality.

	bool _quietMode;								///< No output to stdout.
	
	std::string _outputPrefix;						///< Prepended to all output files (but not the parameters file).

	std::unique_ptr<parameterParser> _params;		///< Parameter parsing object. Read-only to modules.
	
	// Pointers to modules.
	std::unique_ptr<boxBase   > _box;				///< System geometry; wrapping. Read-only to other modules.
	std::unique_ptr<solverBase> _solver;			///< Solver, i.e. Brownian dynamics.
	std::shared_ptr<beadsBase > _beads;				///< Beads and internal links.
	std::shared_ptr<linksBase > _links;				///< Cross-linkers between fibres.

	// Initialisation methods for the modules.
	void _initBox   ( flatXMLParser *stateParser );
	void _initSolver( flatXMLParser *stateParser );
	void _initBeads ( flatXMLParser *stateParser );
	void _initLinks ( flatXMLParser *stateParser );
	
	/// System initialisation.
	void _setInitialConfiguration();
	
	/// Run control uses a `timeStepper::`.
	std::unique_ptr<timeStepper> _clock;
	
	/// Coarse-grained timing of primary loops; for more precise timing data, use a profiler instead.
	multiTimer _timer;

	/// Permature termination. Returns true (after sending a message to stdout and the log file) if run should quit now.
	bool _prematureTermination();

	/// Optional terrmination criterion for when the percolation dimension is at least the given value; 0 means ignore.
	int _terminatePercDim;

	// For saving the current state using index filenames.
	std::unique_ptr<sequentialFilenames> _saveFileIndexer;
	void _saveState() const;
	
	/// Handles data storage and final output.
	std::shared_ptr<dataBeadSpring> _data;
	
	// Data calculation and output.
	void _calculateAllTables  () const;			///< Calls each module's `dataCalculateTable()` method.
	void _calculateAllBlocks  () const;			///< Calls each module's `dataCalculateBlock()` method.
	void _outputTableRow      ();				///< Outputs the next row to the table.
	void _outputBlockTimeSlice() const;			///< Outputs the next time slice of block data.
	
	// Solver components.
	void _calcBeadSeparations();				///< Get bead pairings that are within the maximum interaction range.
	void _calcForces();							///< Calculates the force vector; updates stress tensor.
	void _iterateBeads();						///< Updates the system configuration *via* the solver module.
	void _updateSystem();						///< General update of beads and links modules.
	void _dataAndOutput();						///< Decides when to calculate and output data, and output states.

public:

    // Constructors / destructor.
    BeadSpring() {}

	/**
	 * @brief Normal constructor. Also initialises the modules.
	 * 
	 * @param outputPrefix Prefix for all output files, possibly ending in '/' (assumes Unix).
	 * @param paramsFile   Parameters file. If empty, will attempt to extract parameters from the state file.
	 * @param logFile      Output log file. Will append if starting from a saved state.
	 * @param seed         The seed used for the pRNG. Will use clock if not defined.
	 * @param state_fname  File name of saved state to start from, else will determine ICs from parameters file.
	 * @param extendTime   How long to extend a saved state by.
	 * @param newParams    Map of parameter names to new values. Overrides those in parameters/state files.
	 * @param quietMode    No output to stdout if `true`.
	 * @param resetTime    Resets time to zero if recovering from a saved state.
	 */
	BeadSpring( std::string outputPrefix, std::string paramsFile, std::string logFile, long seed,
	            std::string state_fname, double extendTime, std::map<std::string,std::string> newParams,
				bool quietMode, bool resetTime );
    virtual ~BeadSpring();

    // Run control.
	/**
	 * @brief Main iteration loop.
	 * 
	 * The order of the iterations for each time step is:
	 * 1. Calculates bead separations.
	 * 2. Calculates the forces.
	 * 3. Iterates the beads and updates the system.
	 * 4. Gathers and outputs data.
	 */ 
	void startRun();
};


#endif




