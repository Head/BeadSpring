#include "dataBoxLEOscillatory.h"

// Need definition of the main class now.
#include "boxLEOscillatory.h"


#pragma mark -
#pragma mark Constructors / destructor
#pragma mark -

dataBoxLEOscillatory::dataBoxLEOscillatory( const boxLEOscillatory *boxLEOsc, std::shared_ptr<dataBeadSpring> data, short dim )
	: _dim(dim), _boxLEOsc(boxLEOsc), _dataBeadSpring(data)
{
	// To initialise data labels except on a continuation:
//	if( !_dataBeadSpring->continuationRun() ) _dataBeadSpring->addScalar( label, value );
}


#pragma mark -
#pragma mark Complex shear modulus
#pragma mark -

void dataBoxLEOscillatory::update( double t, double dt )
{
	// Shorthands for quantities in the box class.
	double
		omega     = _boxLEOsc->_omega,
		gamma0    = _boxLEOsc->_gamma0,
		startTime = _boxLEOsc->_startTime;
	
	// Identify which cycle we are in, and that of the previous time step.
	double tCycle = 2 * M_PI / omega;
	int
		thisCycle = int( std::floor((t   -startTime)/tCycle) ),
		prevCycle = int( std::floor((t-dt-startTime)/tCycle) );

	// Add to the integrated quantities.
	if( thisCycle>=0 && _dataBeadSpring->calculateStressNow() )
	{
		double
			stressNow = _dataBeadSpring->instantaneousStress(0,1),		
			timeNow   = t - dt;											// Since calculated on the previous time step.

		if( _lastStressTime >= 0.0 )
		{
			double
				avStress = ( stressNow + _lastStressValue ) / 2.0,
				midTime  = ( timeNow   + _lastStressTime  ) / 2.0;

			_intSigmaSin += avStress * (timeNow-_lastStressTime) * std::sin(omega*(midTime-startTime));
			_intSigmaCos += avStress * (timeNow-_lastStressTime) * std::cos(omega*(midTime-startTime));
		}

		_lastStressTime  = timeNow;
		_lastStressValue = stressNow;
	}	
	
	// Output G' and G'' at the end of each cycle. Note strain follows sin() rather than cos(), so G' is the sin-component.
	if( thisCycle!=prevCycle && thisCycle>0 )
	{
		_lastGp  =   2.0 * _intSigmaSin / ( gamma0 * tCycle );
		_lastGpp = - 2.0 * _intSigmaCos / ( gamma0 * tCycle );
		
		_intSigmaSin = _intSigmaCos = 0.0;
	}

	// Data to table.	
	_dataBeadSpring->addScalar( __dataLabel_Gp , _lastGp  );
	_dataBeadSpring->addScalar( __dataLabel_Gpp, _lastGpp );
}


#pragma mark -
#pragma mark State I/O
#pragma mark -

void dataBoxLEOscillatory::saveBoxState( flatXMLParser *xml ) const
{
	xml->addAttribute    ( __saveValueAttr     , _intSigmaSin );
	xml->saveStartElement( __saveIntSigmaSinTag, true         );

	xml->addAttribute    ( __saveValueAttr     , _intSigmaCos );
	xml->saveStartElement( __saveIntSigmaCosTag, true         );

	xml->addAttribute    ( __saveValueAttr         , _lastStressValue );
	xml->saveStartElement( __saveLastStressValueTag, true             );

	xml->addAttribute    ( __saveValueAttr        , _lastStressTime );
	xml->saveStartElement( __saveLastStressTimeTag, true         );

	xml->addAttribute    ( __saveValueAttr, _lastGp );
	xml->saveStartElement( __saveLastGpTag, true    );

	xml->addAttribute    ( __saveValueAttr , _lastGpp );
	xml->saveStartElement( __saveLastGppTag, true     );
}

void dataBoxLEOscillatory::recoverBoxState( flatXMLParser *xml )
{
	xml->readBlockWithTag( __saveIntSigmaSinTag );
	_intSigmaSin = std::atof( xml->attribute(__saveValueAttr).c_str() );

	xml->readBlockWithTag( __saveIntSigmaCosTag );
	_intSigmaCos = std::atof( xml->attribute(__saveValueAttr).c_str() );

	xml->readBlockWithTag( __saveLastStressValueTag );
	_lastStressValue = std::atof( xml->attribute(__saveValueAttr).c_str() );

	xml->readBlockWithTag( __saveLastStressTimeTag );
	_lastStressTime = std::atof( xml->attribute(__saveValueAttr).c_str() );

	xml->readBlockWithTag( __saveLastGpTag );
	_lastGp = std::atof( xml->attribute(__saveValueAttr).c_str() );

	xml->readBlockWithTag( __saveLastGppTag );
	_lastGpp = std::atof( xml->attribute(__saveValueAttr).c_str() );
}

