#
# Base class for beadSpring tests.
#


#
# Standard imports
#
import os
import sys
import glob


# Should be on $PYTHONPATH; if not, consider using sys.path.append() for a local fix.
import pyBeadSpring


# Failed test exceptions
class FailedTestError( AssertionError ):
	"One of the beadSpring tests failed"				# Just adds a doc string.


#
# Base class of the beadSpring test suite
#
class bsTestBase( object ):

	#
	# Constructor; pass executable pathname (required), and optional flags for verbosity and whether or not to plot.
	#
	def __init__( self, executable, verbose=False, plot=False ):

		# Store parameters
		self._verbose = verbose
		self._plot    = plot

		# Where to find the executable.
		self._executable = executable

		#
		# Set up the test.
		#
		self.setUpForTest()

		#
		# Perform the test; executes "biofilm" with "-q" [quiet mode] if verbosity is off; all other arguments need to be supplied
		#
		cmdString = "{}".format(self._executable)						# Executable name
		if not self._verbose: cmdString += " -q"						# Quiet mode

		# These options provided by child class; 'None' by default. Note there is no validity check here.
		if self._parameterFile() != None: cmdString += " -p {}".format( self._parameterFile() )
		if self._stateFile    () != None: cmdString += " -s {}".format( self._stateFile    () )
		if self._extendTime   () != None: cmdString += " -T {}".format( self._extendTime   () )
		if self._numThreads   () != None: cmdString += " -t {}".format( self._numThreads   () )

		self._shellCmd( cmdString )

		#
		# Get data objects
		#
		localRun = pyBeadSpring.bsRuns( ".",  verbose=self._verbose )	# Bundles together all data files

		try:
			self._table = localRun.table()
		except:
			self._table = None

		try:
			self._blockData = localRun.blockData()
		except:
			self._blockData = None

		#
		# Perform the tests
		#
		self.performTests()

		#
		# Clean up (class method, so it can be called purely to clean-up)
		#
		self.__class__.removeAuxiliaryFiles(self._verbose)


	#
	# Execute shell command directly; will echo if in verbose mode
	#
	def _shellCmd( self, cmd ):
		if self._verbose: print( "Executing shell command:\t'{}'".format(cmd) )
		return os.system( cmd )

	#
	# Overriden by derived classes (some optionally)
	#

	# Pre/post test options; some clean-up operations are provided given a list of files to delete
	def setUpForTest( self ):
		self.removeAuxiliaryFiles()

	# Cleaning-up is performed by class methods that can be called by the script without instancing any test objects
	@classmethod
	def removeAuxiliaryFiles( cls, verbose=False ):				# If overriden, should call parent method

		if cls.filesToDelete() != None:
			if verbose: print( "Removing files: {}".format(cls.filesToDelete()) )
			for file in cls.filesToDelete():
				for matchedFile in glob.glob(file):
					os.remove( matchedFile )

	@classmethod
	def filesToDelete(cls): return None		# Will delete listed files after the test; can use wildcards

	# Arguments for the executable code; None for no argument.
	def _parameterFile( self ): return None
	def _stateFile    ( self ): return None
	def _extendTime   ( self ): return None
	def _numThreads   ( self ): return None

	#
	# Class method to perform the test; must be overridden
	#
	@classmethod
	def performTest( cls, executable, verbosity, plot ):
		raise NotImplementedError
