#include "multiTimer.h"



// Stop a timer, if it exists; throws an exception if not
void multiTimer::stopTimerWithLabel( string key )
{
	if( _start.find(key) == _start.end() )
	{
		ostringstream what;
		what << "multiTimer ERROR: Attempt to access an unspecified timer '" << key << "' [in multiTimer::stopTimerWithLabel()]" << endl;
		throw invalid_argument( what.str() );
	}
	else
	{
		// Key exists; get duration from when it was started.
		auto duration = chrono::high_resolution_clock::now() - _start[key];
	
		// Either create or update the cumulative time
		if( _cumulative.find(key) == _cumulative.end() )
			_cumulative[key]  = duration;
		else
			_cumulative[key] += duration;
		
		// Also do the same thing for the total timer (which is added incrementaly, to avoid overflow)
		if( key != _entireLabel() )
			_cumulative[_entireLabel()] += chrono::high_resolution_clock::now() - _start[_entireLabel()];

		// Always re-start the 'entire' timer
		startTimerWithLabel( _entireLabel() );
	}

	// In case the concise stop...() is called before another Start(key)
	_current = "<Undefined>";
}




//
// OUTPUT ROUTINES
//
void multiTimer::cumulativeTimes( ostream &os )
{
	stopTimerWithLabel( _entireLabel() );

	os << "TIMER: Entire run took " << _readableTimeString(_cumulative[_entireLabel()]) << "."
	   << " Breakdown as follows:" << endl;

	chrono::duration<double> total = chrono::duration<double>::zero();
	for( auto &iter : _cumulative )
	{
		if( iter.first == _entireLabel() ) continue;		// The 'entire run'-key is handled separately

		os << " - " << iter.first << ":\t" << _readableTimeString(iter.second) << endl;

		total += iter.second;
	}
	
	// Also output the sum of the above routines (note that, if some timers are nested, this may exceed the
	// total time).
	os << ":Sum of listed timers: " << _readableTimeString(total) << " [nb: with nesting, may exceed total]" << endl;
}

// Wrapper for the cumulativeTimes() member function
ostream& operator<< ( ostream &os, multiTimer &t ) { t.cumulativeTimes( os ); return os; }

// Readable time with units; _currently does nothing for very small times; could use ms?
string multiTimer::_readableTimeString( chrono::duration<double> durn )
{
	// Only use the number of milliseconds from now on.
	long ms = chrono::duration_cast<chrono::milliseconds>( durn ).count();
	
	long days = 0, hours = 0, mins = 0, secs = 0;
	
	if( ms==0 ) return "0";			// If zero-time, just return zero with no units

	if( ms >= 24*60*60*1000 ) { days  = (long)ms/(24*60*60*1000); ms -= 24*60*60*1000*days ; }
	if( ms >=    60*60*1000 ) { hours = (long)ms/(   60*60*1000); ms -=    60*60*1000*hours; }
	if( ms >=       60*1000 ) { mins  = (long)ms/(      60*1000); ms -=       60*1000*mins ; }
	if( ms >=          1000 ) { secs  = (long)ms/(         1000); ms -=          1000*secs ; }

	// Convert to a string; only output non-zero units (t==0 is handled above)
	ostringstream out;
	if( days  ) out << days  << "d ";
	if( hours ) out << hours << "h ";
	if( mins  ) out << mins  << "m ";
	if( secs  ) out << secs  << "s ";
	if( ms    ) out << ms    << "ms";

	return out.str();
}
